<?php

namespace common\components\crawl;

use common\components\HopeFileHelper;
use common\extensions\extra\TextFormatHelper;

/**
 * @author QuyenNV
 * @todo Crawler jobs from careerbuilder.vn
 * Class CareerbuilderJobCrawler
 * @package common\components\crawl
 */
class CareerbuilderJobCrawler extends SiteCrawler
{

    public $crawlSource = "careerbuilder";
    public $crawlDomain = "https://careerbuilder.vn/";

    /**
     * @param $url
     * @return mixed
     * @todo Extract all data from job_url
     */
    public function crawlJob($url)
    {
        // $url = 'https://careerbuilder.vn/vi/tim-viec-lam/ky-su-dien-tu-dong-hoa.35B53C2B.html';
        $html = file_get_contents($url);
        $dom = new \DOMDocument();
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        $detailNode = $finder->query("//div[@class='row no-gutters']");
        foreach ($detailNode as $index => $value) {
            // get job title from apply-now banner
            $jobTitleNode = $finder->query("div[@class='col-12 mb-15']//section[@class='apply-now-banner  ']//div[@class='apply-now-content']//div[@class='job-desc']//p[@class='title']", $value);
            foreach ($jobTitleNode as $key => $item) {
                $strJobTitle = $this->getInnerHtmlValue($item);
                $strJobTitle = mb_strtolower(html_entity_decode(strip_tags($strJobTitle)));
                $crawlData['job_crawl_title'] = ucwords($strJobTitle);
            }
            // get company detail from apply-now banner
            $companyNode = $finder->query("div[@class='col-12 mb-15']//section[@class='apply-now-banner  ']//div[@class='apply-now-content']//div[@class='job-desc']//a[@class='employer job-company-name']", $value);
            foreach ($companyNode as $item) {
                // get company url
                $strCompanyUrl = $item->getAttribute('href');
                $crawlData['company_crawl_name'] = $item->nodeValue;
                $crawlData['company_link'] = $strCompanyUrl;
                $arrCompanyDetail = $this->getCompanyDetail($strCompanyUrl);
                $companyLogo = $arrCompanyDetail['logo'];
                if ($this->isAllowImageFile($companyLogo)) {
                    $crawlData['company_crawl_logo'] = $companyLogo;
                } else {
                    $urlGetLogo = $strCompanyUrl . '?ajax=1';
                    // get company logo
                    $crawlData['company_crawl_logo'] = $this->getCompanyLogo($urlGetLogo);
                }
//                $crawlData['company_crawl_name'] = $arrCompanyDetail['name'];
                $crawlData['company_crawl_number_people'] = $arrCompanyDetail['number_people'];
                $crawlData['company_crawl_address'] = $arrCompanyDetail['address'];
            }
            // job detail node
            $jobDetailNode = $finder->query("div[@class='col-lg-7 col-custom-xxl-9']//div[@class='tabs']", $value);
            foreach ($jobDetailNode as $key => $item) {
                // job detail content node
                $jobDetailContentNode = $finder->query("div[@id='tab-1']//section[@class='job-detail-content']", $item);
                // job details
                foreach ($jobDetailContentNode as $contentNode) {
                    // job basic detail node
                    $jobBasicDetailNode = $finder->query("div[@class='bg-blue']//div[@class='row']//div[@class='col-lg-4 col-sm-6 item-blue']", $contentNode);
                    foreach ($jobBasicDetailNode as $column => $basicDetailNode) {
                        if ($column == 0) {
                            // column 1: place
                            $jobPlaceNode = $finder->query("div[@class='detail-box']//div[@class='map']//p//a", $basicDetailNode);
                            $places = '';
                            foreach ($jobPlaceNode as $placeNode) {
                                $strPlace = $this->getInnerHtmlValue($placeNode);
                                $strPlace = mb_strtolower(html_entity_decode($strPlace));
                                $strPlace = TextFormatHelper::convertHtmlToText($strPlace);
                                $places .= ucwords(trim($strPlace)) . ', ';
                            }
                            $crawlData['job_crawl_place'] = trim(rtrim(trim($places), ","));
                        } elseif ($column == 1) {
                            // column 2: posted + category + type
                            $jobPostedAndCategoryAndTypeNode = $finder->query("div[@class='detail-box has-background']//ul//li//p", $basicDetailNode);
                            foreach ($jobPostedAndCategoryAndTypeNode as $row => $nodes) {
                                if ($row == 0) {
                                    // row 1: posted time
                                    $strPosted = $this->getInnerHtmlValue($nodes);
                                    $strPosted = mb_strtolower(html_entity_decode(strip_tags($strPosted)));
                                    $posted = str_replace("/", "-", trim($strPosted));
                                    $posted = date('Y-m-d', strtotime($posted));
                                    $crawlData['job_crawl_posted'] = $posted;
                                } elseif ($row == 1) {
                                    // row 2: category
                                    $strCategory = $this->getInnerHtmlValue($nodes);
                                    $strCategory = mb_strtolower(html_entity_decode(strip_tags($strCategory)));
                                    $crawlData['job_crawl_skills'] = trim(preg_replace('/\s+/', ' ', ucwords(trim($strCategory))));
                                    $crawlData['job_crawl_category'] = trim(preg_replace('/\s+/', ' ', ucwords(trim($strCategory))));
                                } elseif ($row == 2) {
                                    // row 3: type
                                    $strType = $this->getInnerHtmlValue($nodes);
                                    $strType = mb_strtolower(html_entity_decode(strip_tags($strType)));
                                    $crawlData['job_crawl_type'] = ucwords(trim($strType));
                                } else {
                                    // do nothing
                                }
                            }
                        } elseif ($column == 2) {
                            // column 3: salary + exp + position + deadline
                            $jobSalaryAndExpAndPosAndDeadlineNode = $finder->query("div[@class='detail-box has-background']//ul//li//p", $basicDetailNode);
                            foreach ($jobSalaryAndExpAndPosAndDeadlineNode as $row => $nodes) {
                                if ($jobSalaryAndExpAndPosAndDeadlineNode->length == 4) {
                                    if ($row == 0) {
                                        // row 1: salary
                                        $strSalary = $this->getInnerHtmlValue($nodes);
                                        $strSalary = mb_strtolower(html_entity_decode(strip_tags($strSalary)));
                                        $arrMinMaxSalary = [];
                                        preg_match_all('/[0-9]+/', $strSalary, $arrSalary);
                                        if (count($arrSalary[0]) == 1) {
                                            $arrMinMaxSalary['min'] = $arrSalary[0][0];
                                            $arrMinMaxSalary['max'] = $arrSalary[0][0];
                                        } elseif (count($arrSalary[0]) == 2) {
                                            $arrMinMaxSalary['min'] = $arrSalary[0][0];
                                            $arrMinMaxSalary['max'] = $arrSalary[0][1];
                                        } else {
                                            $arrMinMaxSalary['min'] = $arrSalary[0][0];
                                            $arrMinMaxSalary['max'] = $arrSalary[0][1];
                                        }
                                        $crawlData['min_salary'] = $arrMinMaxSalary['min'] <= 300 ? $arrMinMaxSalary['min'] * 10e5 : $arrMinMaxSalary['min'] * 23300;
                                        $crawlData['max_salary'] = $arrMinMaxSalary['max'] <= 500 ? $arrMinMaxSalary['max'] * 10e5 : $arrMinMaxSalary['max'] * 23300;
                                    } elseif ($row == 1) {
                                        // row 2: exp
                                        $strExp = $this->getInnerHtmlValue($nodes);
                                        $strSalary = mb_strtolower(html_entity_decode(strip_tags($strExp)));
                                        $crawlData['job_crawl_experience'] = ucwords(trim($strSalary));
                                    } elseif ($row == 2) {
                                        // row 3: position
                                        $strPosition = $this->getInnerHtmlValue($nodes);
                                        $strPosition = mb_strtolower(html_entity_decode(strip_tags($strPosition)));
                                        $crawlData['job_crawl_position'] = ucwords(trim($strPosition));
                                    } elseif ($row == 3) {
                                        // row 4: deadline
                                        $strDeadline = $this->getInnerHtmlValue($nodes);
                                        $strDeadline = mb_strtolower(html_entity_decode(strip_tags($strDeadline)));
                                        $strDeadline = str_replace("/", "-", $strDeadline);
                                        $crawlData['job_crawl_deadline'] = date('Y-m-d', strtotime($strDeadline));
                                    } else {
                                        // do nothing
                                    }
                                } elseif ($jobSalaryAndExpAndPosAndDeadlineNode->length == 3) {
                                    if ($row == 0) {
                                        // row 1: salary
                                        $strSalary = $this->getInnerHtmlValue($nodes);
                                        $strSalary = mb_strtolower(html_entity_decode(strip_tags($strSalary)));
                                        $arrMinMaxSalary = [];
                                        preg_match_all('/[0-9]+/', $strSalary, $arrSalary);
                                        if (count($arrSalary[0]) == 1) {
                                            $arrMinMaxSalary['min'] = $arrSalary[0][0];
                                            $arrMinMaxSalary['max'] = $arrSalary[0][0];
                                        } elseif (count($arrSalary[0]) == 2) {
                                            $arrMinMaxSalary['min'] = $arrSalary[0][0];
                                            $arrMinMaxSalary['max'] = $arrSalary[0][1];
                                        } else {
                                            $arrMinMaxSalary['min'] = $arrSalary[0][0];
                                            $arrMinMaxSalary['max'] = $arrSalary[0][1];
                                        }
                                        $crawlData['min_salary'] = $arrMinMaxSalary['min'] <= 300 ? $arrMinMaxSalary['min'] * 10e5 : $arrMinMaxSalary['min'] * 23300;
                                        $crawlData['max_salary'] = $arrMinMaxSalary['max'] <= 500 ? $arrMinMaxSalary['max'] * 10e5 : $arrMinMaxSalary['max'] * 23300;
                                    } elseif ($row == 1) {
                                        // row 2: position
                                        $strPosition = $this->getInnerHtmlValue($nodes);
                                        $strPosition = mb_strtolower(html_entity_decode(strip_tags($strPosition)));
                                        $crawlData['job_crawl_position'] = ucwords(trim($strPosition));
                                    } elseif ($row == 2) {
                                        // row 3: deadline
                                        $strDeadline = $this->getInnerHtmlValue($nodes);
                                        $strDeadline = mb_strtolower(html_entity_decode(strip_tags($strDeadline)));
                                        $strDeadline = str_replace("/", "-", $strDeadline);
                                        $crawlData['job_crawl_deadline'] = date('Y-m-d', strtotime($strDeadline));
                                    } else {
                                        // do nothing
                                    }
                                } else {
                                    // do nothing
                                }

                            }
                        } else {
                            // do nothing
                        }
                    }
                    // job desc, req, benefit node
                    $jobExtraDetailNode = $finder->query("div[@class='detail-row']", $contentNode);
                    foreach ($jobExtraDetailNode as $extraDetailNode) {
                        $nodeTitle = $finder->query("h3[@class='detail-title']", $extraDetailNode);
                        foreach ($nodeTitle as $k => $v) {
                            $blockTitle = mb_strtolower(trim($v->nodeValue));
                        }
                        $strDetail = $this->getInnerHtmlValue($extraDetailNode);
                        // $strDetail = mb_strtolower(html_entity_decode($strDetail));
                        // $strDetail = html_entity_decode($extraDetailNode->nodeValue);
                        if ($blockTitle === 'phúc lợi') {
                            $strBenefit = str_replace("Phúc lợi", "", $strDetail);
                            $crawlData['job_crawl_benefit'] = $this->removeTitle($strBenefit);
                        } elseif ($blockTitle === 'mô tả công việc') {
                            $strDesc = str_replace("Mô tả Công việc", "", $strDetail);
                            // $crawlData['job_crawl_description'] = TextFormatHelper::convertHtmlToText($this->removeTitle($strDesc));
                            $crawlData['job_crawl_description'] = $this->removeTitle($strDesc);
                        } elseif ($blockTitle === 'yêu cầu công việc') {
                            $strReq = str_replace("Yêu Cầu Công Việc", "", $strDetail);
                            // $crawlData['job_crawl_requirement'] = TextFormatHelper::convertHtmlToText($this->removeTitle($strReq));
                            $crawlData['job_crawl_requirement'] = $this->removeTitle($strReq);
                        } elseif ($blockTitle === 'thông tin khác') {
                            $strOtherReq = str_replace("Thông tin khác", "", $strDetail);
                            // $crawlData['job_crawl_other_requirement'] = TextFormatHelper::convertHtmlToText($this->removeTitle($strOtherReq));
                            $crawlData['job_crawl_other_requirement'] = $this->removeTitle($strOtherReq);
                        } else {
                            // do nothing
                        }
                    }

                }
            }
        }
        $crawlData['job_url'] = $url;
        return $crawlData;
    }

    /**
     * @param $detail
     * @return string
     */
    private function removeTitle($detail)
    {
        $str = preg_replace('/<h3(.*?)<\/h3>/', '', $detail);
        return trim($str);
    }

    /**
     * @param $fileUrl
     * @return bool
     */
    private function isAllowImageFile($fileUrl)
    {
        $extension = pathinfo($fileUrl, PATHINFO_EXTENSION);
        return mb_strtolower($extension) == 'png' || mb_strtolower($extension) == 'jpg' || mb_strtolower($extension) == 'jpeg';
    }

    /**
     * @param $companyUrl
     * @return mixed
     */
    private function getCompanyDetail($companyUrl)
    {
        $html = file_get_contents($companyUrl);
        $dom = new \DOMDocument();
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        // company node
        $companyInfoNode = $finder->query("//div[@class='company-info']//div[@class='info']");
        foreach ($companyInfoNode as $value) {
            // logo node
            $companyLogoNode = $finder->query("div[@class='img']//img", $value);
            foreach ($companyLogoNode as $item) {
                $arrCompanyDetail['logo'] = $item->getAttribute('src');
            }
            // detail node
            $companyInfoNode = $finder->query("div[@class='content']", $value);
            foreach ($companyInfoNode as $item) {
                // company title node
                $companyNameNode = $finder->query("p[@class='name']", $item);
                foreach ($companyNameNode as $companyName) {
                    $strCompanyName = $this->getInnerHtmlValue($companyName);
                    $strCompanyName = ucwords(trim(html_entity_decode(strip_tags($strCompanyName))));
                    $arrCompanyDetail['name'] = $strCompanyName;
                }
                // number people
                $companyNumberPeople = $finder->query("ul//li", $item);
                foreach ($companyNumberPeople as $companyNumber) {
                    $str = $this->getInnerHtmlValue($companyNumber);
                    $str = mb_strtolower(html_entity_decode(strip_tags($str)));
                    if (strpos(trim($str), 'quy mô công ty') !== false ||
                        strpos(trim($str), 'qui mô công ty') !== false ||
                        strpos(trim($str), 'qui mô') !== false ||
                        strpos(trim($str), 'quy mô') !== false) {
                        $arrCompanyDetail['number_people'] = preg_replace('/[^0-9-]/', '', $str);
                    }
                }
                // company address node
                $companyAddressNode = $finder->query("p[last()]", $item);
                foreach ($companyAddressNode as $companyAddress) {
                    $strAddress = $this->getInnerHtmlValue($companyAddress);
                    $strAddress = ucwords(trim(html_entity_decode(strip_tags($strAddress))));
                    $arrCompanyDetail['address'] = $strAddress;
                }
            }
        }
        return $arrCompanyDetail;
    }

    /**
     * @param $node
     * @return string
     */
    private function getInnerHtmlValue($node)
    {
        $tmp_dom = new \DOMDocument();
        $tmp_dom->appendChild($tmp_dom->importNode($node, true));
        return $tmp_dom->saveHTML();
    }

    /**
     * @param $url
     * @return string
     */
    public function getCompanyLogo($url)
    {
        $result = file_get_contents($url);
        // decode the result
        $arrResult = json_decode($result, true);
        // get only html_detail value
        $htmlDetail = $arrResult['html_detail'];
        // regrex to get src
        preg_match('/src=".*?\"/', $htmlDetail, $src);
        // remove unnecessary characters
        $companyLogo = str_replace('src="', '', $src[0]);
        $companyLogo = rtrim($companyLogo, '"');
        // check if logo is valid
        if (HopeFileHelper::isAllowedImageFile($companyLogo)) {
            return $companyLogo;
        } else {
            // if logo not valid, set default
            return null;
        }
    }

    /**
     * @param null $cursor
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Get all jobs from a certain page like /tuyen-dung?page=1
     */
    public function crawlBrowseJob($cursor = NULL)
    {           
        // Url browse từ CareerBuilder
        $browseUrl = "https://careerbuilder.vn/viec-lam/tat-ca-viec-lam-xxx-vi.html";
        if ($cursor) {
            $browseUrl = str_replace("xxx", "trang-" . $cursor, $browseUrl);
        } else {
            $browseUrl = str_replace("xxx", "trang-1", $browseUrl);
        }        
        // Lấy kết quả trả về
        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $browseUrl);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $headers = [
            'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.16 Safari/537.3',
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        // $output contains the output string
        $html = curl_exec($ch);        
        // close curl resource to free up system resources
        curl_close($ch);
        // Lấy mảng job trả về
        $arrJobData = $this->extractAllJobFromHtml($html);        
        // count total job crawl
        $this->totalJob += count($arrJobData);
        // check site out of job
        if (count($arrJobData) == 0 || $arrJobData == false) {
            $this->isRunningOutJobs = true;
            exit;
        }
        // Insert vào bảng x_job_crawl
        $count = 0;
        foreach ($arrJobData as $jobData) {
            $resultQuery = $this->insertToJobCrawlTable($jobData, 'browse_job', 0, $this->crawlSource);
            if ($resultQuery) {
                $count++;
                $this->newJob += 1;
            }
        }
        if ($count == count($arrJobData)) {
            return ['data' => $arrJobData];
        } else {
            return ['failed'];
        }
    }

    /**
     * @param $html
     * @return array
     * @todo extract job details, mainly to get the job_url
     */
    public function extractAllJobFromHtml($html)
    {
        // Dom object
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);

        // Các job trả về
        $arrJobItem = $finder->query("//a[@class='job_link']");
        // extract all new jobs from table, except from thead and tfoot
        $crawlData = []; // store job_url
        // each new jobs ~ a table row
        foreach ($arrJobItem as $key => $value) {
            $crawlData[$key]['job_url'] = $value->getAttribute('href');
        }
        return array_values($crawlData);
    }

    /**
     * @param null $cursor
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Browse for jobs by cursor then use the url to get the details
     */
    public function siteCrawler($cursor = NULL)
    {
        // browse jobs by cursor
        $crawlBrowseJob = $this->crawlBrowseJob($cursor);
        return $crawlBrowseJob;
    }

    /**
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Crawl all new jobs from site
     */
    public function crawlAllNewJobs()
    {
        $this->crawlAllNewJobsBySource($this->crawlSource);
    }
}
