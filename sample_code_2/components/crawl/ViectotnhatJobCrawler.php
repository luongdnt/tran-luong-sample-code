<?php

namespace common\components\crawl;

/**
 * @author QuyenNV
 * @todo Crawl jobs from viectotnhat.com
 * Class ViectotnhatCrawler
 * @package common\components\crawl
 */
class ViectotnhatJobCrawler extends SiteCrawler
{
    public $crawlSource = "viectotnhat";
    public $crawlDomain = "https://viectotnhat.com";

    /**
     * @param $url
     * @return mixed
     * @todo Extract all data from job_url
     */
    public function crawlJob($url)
    {
        $html = file_get_contents($url); // get content from url
        $dom = new \DOMDocument();
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        $metaNodes = $finder->query("//script[@type='application/ld+json']")->item(0)->nodeValue;
        // decode unicode to UTF8
        $metaNodes = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
        }, $metaNodes);
        // remove html tags
        $metaNodes = strip_tags(html_entity_decode(preg_replace("/[\n\r\t]/", "", $metaNodes))); // html5 string issues
        // remove one last }
        $metaNodes = substr($metaNodes, 0, strlen($metaNodes) - 1);
        $metaNodes = json_decode($metaNodes);
        $crawlData['job_url'] = $url;
        $crawlData['job_crawl_title'] = $metaNodes->title;
        $crawlData['job_crawl_category'] = $metaNodes->industry;
        $crawlData['job_crawl_description'] = $metaNodes->description;
        $crawlData['job_crawl_benefit'] = strip_tags($metaNodes->Jobbenefits);
        $crawlData['job_crawl_requirement'] = strip_tags($metaNodes->skills);
        $crawlData['job_crawl_degree'] = $metaNodes->qualifications;
        $crawlData['job_crawl_experience'] = strip_tags($metaNodes->experienceRequirements);
        $crawlData['job_crawl_skills'] = $metaNodes->industry;
        $crawlData['job_crawl_posted'] = $metaNodes->datePosted;
        $crawlData['job_crawl_deadline'] = $metaNodes->validThrough;
        $crawlData['job_crawl_type'] = $metaNodes->employmentType;
        $crawlData['company_crawl_name'] = $metaNodes->hiringOrganization->name;
        $crawlData['company_crawl_logo'] = $metaNodes->hiringOrganization->logo;
        $crawlData['job_crawl_place'] = $metaNodes->jobLocation->address->addressRegion;
        $currency = $metaNodes->baseSalary->currency;
        if (mb_strtolower($currency) == 'usd') {
            $crawlData['min_salary'] = $metaNodes->baseSalary->minValue * 22700;
            $crawlData['max_salary'] = $metaNodes->baseSalary->maxValue * 22700;
        } else {
            $crawlData['min_salary'] = $metaNodes->baseSalary->minValue;
            $crawlData['max_salary'] = $metaNodes->baseSalary->maxValue;
        }
        return $crawlData;
    }

    /**
     * @param null $cursor
     * @return array
     * @todo Get all jobs from a certain page like /tuyen-dung?page=1
     */
    public function crawlBrowseJob($cursor = NULL)
    {
        // Url browse từ CareerBuilder
        $browseUrl = "https://viectotnhat.com/viec-lam/tim-kiem?page=xxx";

        if ($cursor) {
            $browseUrl = str_replace("xxx", $cursor, $browseUrl);
        } else {
            $browseUrl = str_replace("xxx", "1", $browseUrl);
        }

        // Lấy kết quả trả về
        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $browseUrl);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // $output contains the output string
        $html = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch);
        // Lấy mảng job trả về
        $arrJobData = $this->extractAllJobFromHtml($html);
        // count total job crawl
        $this->totalJob += count($arrJobData);
        // check site out of job
        if (count($arrJobData) == 0 || $arrJobData == false) {
            $this->isRunningOutJobs = true;
            exit;
        }
        // Insert vào bảng x_job_crawl
        $count = 0;
        foreach ($arrJobData AS $jobData) {
            $resultQuery = $this->insertToJobCrawlTable($jobData, 'browse_job', 0, $this->crawlSource);
            if ($resultQuery) {
                $count++;
                $this->newJob += 1;
            }
        }
        if ($count == count($arrJobData)) {
            return ['data' => $arrJobData];
        } else {
            return ['failed'];
        }
    }

    /**
     * @param $html
     * @return array
     * @todo extract job details
     */
    public function extractAllJobFromHtml($html)
    {
        // Dom object
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);

        // Các job trả về
        $arrJobItem = $finder->query("//div[@class='row margin0 normal-job']//div[@class='row tr-box job-info']//div[@class='td-box col-xs-12 col-sm-6']");
        // extract all new jobs from table, except from thead and tfoot
        $crawlData = []; // store job_url

        // each new jobs ~ a table row
        foreach ($arrJobItem as $key => $value) {
            // xpath to get job_url
            $arrJobUrlNode = $finder->query("//h3[@class='job-name margin0']//a", $value);

            // except from thead and tfoot
            if ($key != 0 && $key != ($arrJobItem->length - 1)) {

                //
                foreach ($arrJobUrlNode as $index => $jobUrlNode) {
//                    print_r($jobUrlNode->getAttribute('href'));echo "<br><br>";
                    $jobUrl = $jobUrlNode->getAttribute('href');
                    $crawlData[$index]['job_url'] = $jobUrl;
                }
            }

        }
        return $crawlData;
    }

    /**
     * @param null $cursor
     * @return array
     * @todo Browse for jobs by cursor then use the url to get the details
     */
    public function siteCrawler($cursor = NULL)
    {
        // browse jobs by cursor
        $crawlBrowseJob = $this->crawlBrowseJob($cursor);
        return $crawlBrowseJob;
    }

    /**
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Crawl all new jobs from site
     */
    public function crawlAllNewJobs()
    {
        $this->crawlAllNewJobsBySource($this->crawlSource);
    }
}