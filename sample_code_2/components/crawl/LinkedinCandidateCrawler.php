<?php

namespace common\components\crawl;

use common\components\HopeFileHelper;
use common\models\XCrawlCandidate;

/**
 * @author QuyenNV
 * @todo Crawl candidate info from Linkedin
 * Class LinkedinCandidateCrawler
 * @package common\components\crawl
 */
class LinkedinCandidateCrawler
{

    /**
     * @param $profileUrl
     * @return \DOMNodeList
     * @todo Get contents by tag name
     */
    private static function getContentByCurl($profileUrl)
    {
        // get profile id
        $profileUrl = end(explode('/', rtrim($profileUrl, '/')));
        // encode only the id
        $profileUrl = 'https://www.linkedin.com/in/' . urlencode($profileUrl);
        // curl request
        $ch = curl_init($profileUrl);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // How to get this header:
        // Access user profile url by browser, f12 then choose the request url = user id
        // ex: access https://www.linkedin.com/in/luong-tran-nguyen-317a4031/ then find request to the url "luong-tran-nguyen-317a4031/"
        // right click, choose copy -> copy as cURL (bash) then paste it to notepad++, paste fields here
        $headers = [
            'pragma: no-cache',
            'cache-control: no-cache',
            'upgrade-insecure-requests: 1',
            'sec-fetch-dest: document',
            'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'sec-fetch-site: same-origin',
            'sec-fetch-mode: navigate',
            'accept-language: vi-VN,vi;q=0.9,fr-FR;q=0.8,fr;q=0.7,en-US;q=0.6,en;q=0.5',
            'authority: www.linkedin.com',
            'user-agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36',
            \Yii::$app->params['linkedin_cookie']
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING ,"");
        $info = curl_exec($ch);
        $info = mb_convert_encoding($info, 'HTML-ENTITIES', 'UTF-8');
        curl_close($ch);
        $dom = new \DOMDocument();
        @$dom->loadHTML($info);
        // return all html elements contains html tag <code></code>
        return $dom->getElementsByTagName('code');
    }

    /**
     * @param $profileUrl
     * @return mixed
     * @todo Get block containing all data
     */
    private static function getFullDataBlock($profileUrl)
    {
        $arrDataBlock = [];
        $arrBlockFromDom = self::getContentByCurl($profileUrl);
        foreach ($arrBlockFromDom as $key => $value) {
            if (
                strpos($value->nodeValue, 'com.linkedin.voyager.dash.deco.identity.profile.FullProfileSkills') ||
                strpos($value->nodeValue, 'com.linkedin.voyager.dash.deco.identity.profile.FullProfileEducations') ||
                strpos($value->nodeValue, 'com.linkedin.voyager.dash.deco.identity.profile.FullProfileLanguages')
            ) {
                $arrDataBlock[$key] = $value->nodeValue;
            }
        }
        return array_values($arrDataBlock)[0];
    }

    /**
     * @param $profileUrl
     * @return mixed
     * @todo Get Candidate mini profile block
     */
    private static function getMiniProfileBlock($profileUrl)
    {
        $arrDataBlock = [];
        $arrBlockFromDom = self::getContentByCurl($profileUrl);
        foreach ($arrBlockFromDom as $key => $value) {
            $arrMiniProfile = json_decode($value->nodeValue, true);
            if (
            strpos($arrMiniProfile['data']['entityUrn'], 'rn:li:fs_miniProfile:')
            ) {
                $arrDataBlock[$key] = $arrMiniProfile;
            }
        }
        return array_values($arrDataBlock)[0];
    }

    /**
     * @param $profileUrl
     * @param null $type
     * @return mixed
     * @todo Get in-need block
     */
    private static function getDataBlock($profileUrl, $type = NULL)
    {
        if (is_null($type)) {
            return self::getFullDataBlock($profileUrl);
        } elseif ($type == 'miniProfile') {
            return self::getMiniProfileBlock($profileUrl);
        } else {
            return false;
        }
    }

    /**
     * @param $arrIncludedData
     * @return array
     * @todo Get an array of industry
     */
    private static function getArrayIndustry($arrIncludedData)
    {
        $arrIndustry = [];
        foreach ($arrIncludedData as $key => $value) {
            if (strpos($value['entityUrn'], 'rn:li:fsd_industry:')) {
                $arrIndustry[$key] = $value['name'];
            }
        }
        return array_values($arrIndustry);
    }

    /**
     * @param $arrIncludedData
     * @return array
     * @todo get an array of education history
     */
    private static function getArraySchool($arrIncludedData)
    {
        $arrSchool = [];
        foreach ($arrIncludedData as $key => $value) {
            if (strpos($value['entityUrn'], 'n:li:fsd_profileEducation:')) {
                // school name
                $arrSchool[$key]['school_name'] = $value['schoolName'];
                // school urn
                $arrSchool[$key]['school_urn'] = $value['schoolUrn'];
                // graduation degree
                $arrSchool[$key]['degree_name'] = $value['degreeName'];
                // education duration
                $arrSchool[$key]['start_year'] = $value['dateRange']['start']['year'];
                $arrSchool[$key]['end_year'] = !empty($value['dateRange']['end']['year']) ? $value['dateRange']['end']['year'] : null;
                // education spec
                $arrSchool[$key]['specialization'] = $value['fieldOfStudy'];
                $arrSchool[$key]['description'] = $value['description'];
            }
        }
        // array to store official school array
        $arrOfficialSchool = [];
        // check null school name here
        foreach ($arrSchool as $key => $school) {
            if (empty($school['school_name'])) {
                // get school urn id
                $schoolUrnId = end(explode(':', $school['school_urn']));
                foreach ($arrIncludedData as $index => $value) {
                    if (strpos($value['entityUrn'], 'rn:li:fsd_school:' . $schoolUrnId)) {
                        $arrOfficialSchool[$key] = $school;
                        $arrOfficialSchool[$key]['school_name'] = $value['name'];
                    }
                }
            } else {
                $arrOfficialSchool[$key] = $school;
            }
        }
        return array_values($arrOfficialSchool);
    }

    /**
     * @param $arrIncludedData
     * @return array
     * @todo get an array of language
     */
    private static function getArrayLanguage($arrIncludedData)
    {
        $arrLanguage = [];
        foreach ($arrIncludedData as $key => $value) {
            if (strpos($value['entityUrn'], 'n:li:fsd_profileLanguage:')) {
                $arrLanguage[$key] = $value['name'];
            }
        }
        return array_values($arrLanguage);
    }

    /**
     * @param $arrIncludedData
     * @return array
     * @todo Get an array of job history
     */
    private static function getArrayJobHistory($arrIncludedData)
    {
        $arrJobHistory = [];
        foreach ($arrIncludedData as $key => $value) {
            if (strpos($value['entityUrn'], 'rn:li:fsd_profilePosition:')) {
                // job history company name
                $arrJobHistory[$key]['company_name'] = $value['companyName'];
                // job history title
                $arrJobHistory[$key]['job_title'] = $value['title'];
                // job history duration
                $startMonth = $value['dateRange']['start']['month'];
                $startYear = $value['dateRange']['start']['year'];
                $endMonth = $value['dateRange']['end']['month'];
                $endYear = $value['dateRange']['end']['year'];
                $arrJobHistory[$key]['start_year'] = !empty($startMonth) ? $startYear . '-' . $startMonth : $startYear;
                $arrJobHistory[$key]['end_year'] = !empty($endMonth) ? $endYear . '-' . $endMonth : $endYear;
                // job description
                $arrJobHistory[$key]['description'] = $value['description'];
            }
        }
        return array_values($arrJobHistory);
    }

    /**
     * @param $arrIncludedData
     * @return array
     * @todo Get an array of personal info (name, birth, job_title, about, address)
     */
    private static function getArrayPersonalInfo($arrIncludedData)
    {
        $arrPersonalInfo = [];
        foreach ($arrIncludedData as $key => $value) {
            if (strpos($value['entityUrn'], 'rn:li:fsd_profile:')) {
                $arrPersonalInfo['fullname'] = $value['firstName'] . ' ' . $value['lastName'];
                $arrPersonalInfo['birthday'] = !empty($value['birthDateOn']['month']) ? $value['birthDateOn']['month'] . '-' . $value['birthDateOn']['day'] : null;
                $arrPersonalInfo['job_title'] = $value['headline'];
                $arrPersonalInfo['summary'] = $value['summary'];
                $arrPersonalInfo['address'] = $value['address'];
            }
        }
        return $arrPersonalInfo;
    }

    /**
     * @param $arrIncludedData
     * @return array
     * @todo Get an array of user skills
     */
    private static function getArraySkill($arrIncludedData)
    {
        $arrSkill = [];
        foreach ($arrIncludedData as $key => $value) {
            if (strpos($value['entityUrn'], 'rn:li:fsd_skill:')) {
                $arrSkill[$key] = $value['name'];
            }
        }
        return array_values($arrSkill);
    }

    /**
     * @param $profileUrl
     * @return string
     * @todo Get profile avatar
     */
    private static function getProfileAvatar($profileUrl, $candidateID = NULL)
    {
        $miniProfileBlock = self::getDataBlock($profileUrl, 'miniProfile');
        $rootUrl = $miniProfileBlock['data']['picture']['rootUrl'];
        $artifacts = $miniProfileBlock['data']['picture']['artifacts'];
        $avatarUrlPath = '';
        foreach ($artifacts as $key => $value) {
            if (!empty($value['fileIdentifyingUrlPathSegment'])) {
                $avatarUrlPath = $value['fileIdentifyingUrlPathSegment'];
            }
        }
        $imageEncodedPath = $rootUrl . $avatarUrlPath;
        // save avatar immediately
        return HopeFileHelper::downloadImage($imageEncodedPath, $candidateID);
    }

    /**
     * @param $profileUrl
     * @param null $candidateID
     * @return array|bool
     * @todo Get an array of candidate info
     */
    public static function getCandidateRawData($profileUrl, $candidateID = NULL)
    {
        $jsonDataBlock = self::getDataBlock($profileUrl);
        // decode json to array
        $arrDataBlock = json_decode($jsonDataBlock, true);
        // array include data
        $arrIncludeData = $arrDataBlock['included'];
        // array store all candidate info
        $arrOfficialCandidateInfo = [];
        $arrOfficialCandidateInfo['personal_avatar'] = self::getProfileAvatar($profileUrl, $candidateID);
        $arrOfficialCandidateInfo['personal_data'] = self::getArrayPersonalInfo($arrIncludeData);
        $arrOfficialCandidateInfo['industry'] = self::getArrayIndustry($arrIncludeData);
        $arrOfficialCandidateInfo['school'] = self::getArraySchool($arrIncludeData);
        $arrOfficialCandidateInfo['language'] = self::getArrayLanguage($arrIncludeData);
        $arrOfficialCandidateInfo['job_history'] = self::getArrayJobHistory($arrIncludeData);
        $arrOfficialCandidateInfo['skill'] = self::getArraySkill($arrIncludeData);
        // check if any data returned, if not, return false
        if (empty($arrOfficialCandidateInfo['personal_avatar']) &&
            empty($arrOfficialCandidateInfo['personal_data']) &&
            empty($arrOfficialCandidateInfo['industry']) &&
            empty($arrOfficialCandidateInfo['school']) &&
            empty($arrOfficialCandidateInfo['language']) &&
            empty($arrOfficialCandidateInfo['job_history']) &&
            empty($arrOfficialCandidateInfo['skill'])
        ) {
            return false;
        } else {
            return $arrOfficialCandidateInfo;
        }
    }

    /**
     * @param $profileUrl
     * @return bool|int
     * @todo Crawl candidate data from linkedin then save to x_crawl_candidate table
     */
    public static function saveNewCandidate($profileUrl)
    {
        $arrCandidateInfo = self::getCandidateRawData($profileUrl);
        $xCrawlCandidate = new XCrawlCandidate();
        $xCrawlCandidate->name = $arrCandidateInfo['personal_data']['fullname'];
        $xCrawlCandidate->avatar = $arrCandidateInfo['personal_avatar'];
        $xCrawlCandidate->date_of_birth = $arrCandidateInfo['personal_data']['birthday'];
        $xCrawlCandidate->short_bio = $arrCandidateInfo['personal_data']['summary'];
        $xCrawlCandidate->language = implode(',', $arrCandidateInfo['language']);
        $xCrawlCandidate->degree = implode(',', array_column($arrCandidateInfo['school'], 'degree_name'));
        $xCrawlCandidate->created = date('Y-m-d H:i:s');
        $xCrawlCandidate->updated = date('Y-m-d H:i:s');
        $xCrawlCandidate->current_address = $arrCandidateInfo['personal_data']['address'];
        $xCrawlCandidate->linkedin = $profileUrl;
        $xCrawlCandidate->crawl_date = date('Y-m-d H:i:s');
        $xCrawlCandidate->crawl_source = 'linkedin';
        $xCrawlCandidate->crawl_status = 0;
        $xCrawlCandidate->crawl_url = $profileUrl;
        $xCrawlCandidate->personal_skills = implode(',', $arrCandidateInfo['skill']);
        $xCrawlCandidate->title = $arrCandidateInfo['personal_data']['job_title'];
        $xCrawlCandidate->industry = implode(',', $arrCandidateInfo['industry']);
        $xCrawlCandidate->education = json_encode($arrCandidateInfo['school']);
        $xCrawlCandidate->job_history = json_encode($arrCandidateInfo['job_history']);
        if (!self::isExistCandidate($profileUrl)) {
            return $xCrawlCandidate->save(false);
        } else {
            return XCrawlCandidate::updateAll(['updated' => date('Y-m-d H:i:s')], ['crawl_source' => $profileUrl]);
        }
    }

    /**
     * @param $profileUrl
     * @return array|\yii\db\ActiveRecord|null
     * @todo Check duplicated candidate crawled
     */
    private static function isExistCandidate($profileUrl)
    {
        return XCrawlCandidate::find()->where(['crawl_url' => $profileUrl])->one();
    }
}