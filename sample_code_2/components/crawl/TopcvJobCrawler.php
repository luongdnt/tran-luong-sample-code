<?php

namespace common\components\crawl;
/**
 * @author QuyenNV
 * @todo Crawl jobs from site topcv.vn
 * Class TopcvCrawler
 * @package common\components\crawl
 */
class TopcvJobCrawler extends SiteCrawler
{

    public $crawlSource = "topcv";
    public $crawlDomain = "https://www.topcv.vn/";

    /**
     * @todo Get all jobs from a certain page like /tuyen-dung?page=1
     */
    public function crawlBrowseJob($cursor = NULL)
    {
        // Url browse từ Topcv
        $browseUrl = "https://www.topcv.vn/tim-viec-lam-moi-nhat?salary=0&exp=0&company_field=0&page=";

        if ($cursor) {
            $browseUrl = $browseUrl . "" . $cursor;
        } else {
            $browseUrl = $browseUrl . "1";
        }
        
        // Lấy kết quả trả về
        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $browseUrl);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // $output contains the output string
        $html = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch);
        // Lấy mảng job trả về
        $arrJobData = $this->extractAllJobFromHtml($html);
        // count total job crawl
        $this->totalJob += count($arrJobData);       
        // check site out of job
        if (count($arrJobData) == 0 || $arrJobData == false) {
            $this->isRunningOutJobs = true;
            exit;
        }
        // Insert vào bảng x_job_crawl
        $count = 0;
        foreach ($arrJobData AS $jobData) {
            $resultQuery = $this->insertToJobCrawlTable($jobData, 'browse_job', 0, $this->crawlSource);
            if ($resultQuery) {
                $count++;
                $this->newJob += 1;
            }
        }
        if ($count == count($arrJobData)) {
            return ['data' => $arrJobData];
        } else {
            return ['failed'];
        }
    }

    /**
     * @todo extract job details
     */
    public function extractAllJobFromHtml($html)
    {
        // Dom object
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);

        // Các job trả về
        $arrJobItem = $finder->query("//div[@id='box-job-result']//div[@class='job-list search-result']//div[contains(@class, 'result-job-hover')]");
        // extract all new jobs from table, except from thead and tfoot
        $crawlData = []; // store job_url

        // each new jobs ~ a table row
        foreach ($arrJobItem as $key => $value) {
            // xpath to get job_url
            $arrJobUrlNode = $finder->query("//div[@class='row job']//div[@class='col-sm-8']//h4[@class='job-title']//a", $value);

            // except from thead and tfoot
            if ($key != 0 && $key != ($arrJobItem->length - 1)) {

                //
                foreach ($arrJobUrlNode as $index => $jobUrlNode) {
//                    print_r($jobUrlNode->getAttribute('href'));echo "<br><br>";
                    $jobUrl = $jobUrlNode->getAttribute('href');
                    $crawlData[$index]['job_url'] = $jobUrl;
                }
            }

        }
        return $crawlData;
    }

    /**
     * @todo Browse for jobs by cursor then use the url to get the details
     */
    public function siteCrawler($cursor = NULL)
    {
        // browse jobs by cursor
        $crawlBrowseJob = $this->crawlBrowseJob($cursor);
        return $crawlBrowseJob;
    }

    /**
     * @param $url
     * @return mixed
     * @todo Extract all data from job_url
     */
    public function crawlJob($url)
    {
        $html = file_get_contents($url);
        $dom = new \DOMDocument();
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        $metaNodes = $finder->query("//script[@type='application/ld+json']")->item(0)->nodeValue;
        $metaNodes = strip_tags(html_entity_decode(preg_replace("/[\n\r\t]/", "", $metaNodes))); // html5 string issues
        $metaNodes = json_decode($metaNodes);
//        print_r($metaNodes);die;
        $crawlData['job_url'] = $url;
        $crawlData['job_crawl_title'] = $metaNodes->title;
        // get job_description
        $jobDescNode = $metaNodes->description;
        $arrJobDesc = preg_split('/Yêu cầu ứng viên/', $jobDescNode);
        $arrJobReqAndBenefit = preg_split('/Quyền lợi được hưởng/', $arrJobDesc[1]);

        $crawlData['job_crawl_description'] = str_replace("Mô tả công việc", "", $arrJobDesc[0]);
        $crawlData['job_crawl_benefit'] = $arrJobReqAndBenefit[1];
        $crawlData['job_crawl_requirement'] = $arrJobReqAndBenefit[0];
        $crawlData['job_crawl_degree'] = $metaNodes->qualifications;
        $crawlData['job_crawl_experience'] = $metaNodes->experienceRequirements;
        $crawlData['job_crawl_skills'] = $metaNodes->skills;
        $crawlData['job_crawl_posted'] = $metaNodes->datePosted;
        $crawlData['job_crawl_deadline'] = $metaNodes->validThrough;
        $crawlData['job_crawl_type'] = $metaNodes->employmentType;
        $crawlData['company_crawl_name'] = $metaNodes->hiringOrganization->name;
        $crawlData['company_crawl_logo'] = $metaNodes->hiringOrganization->logo;
        $crawlData['job_crawl_place'] = $metaNodes->jobLocation[0]->address->addressRegion;
        $crawlData['min_salary'] = round($metaNodes->baseSalary->value->minValue);
        $maxSalary = $metaNodes->baseSalary->value->maxValue;
        // in case max salary like 11999999
        if ($maxSalary % 1000 != 0) {
            $maxSalary = ceil($maxSalary + 0.5);
            $crawlData['max_salary'] = $maxSalary;
        } else {
            $crawlData['max_salary'] = $maxSalary;
        }

        return $crawlData;
    }

    /**
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Crawl all new jobs from site
     */
    public function crawlAllNewJobs()
    {
        $this->crawlAllNewJobsBySource($this->crawlSource);
    }
}
