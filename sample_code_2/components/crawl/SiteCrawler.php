<?php

namespace common\components\crawl;

use common\extensions\extra\TextFormatHelper;
use common\extensions\mysql\libs\XJobCrawlLogQueries;
use common\extensions\mysql\libs\XJobCrawlQueries;
use common\models\XEmployerUnmatch;
use common\models\XJobCrawl;
use Yii;

/**
 * @author QuyenNV
 * @todo Extract data from sites then add to table x_job_crawl
 * Class SiteCrawler
 * @package common\components\crawl
 */
abstract class SiteCrawler
{
    protected $totalJob = 0;
    protected $newJob = 0;
    protected $start;
    protected $end;
    protected $isRunningOutJobs = false;
    protected $page = 1;

    /**
     * @param $url
     * @return mixed
     * @todo Extract all data from job_url
     */
    abstract public function crawlJob($url);

    /**
     * @param $jobCrawlModel
     * @param $keyword
     * @param $employerId
     * @return bool
     * @throws \yii\db\Exception
     * @todo Check whether data can be inserted to db
     */
    public function allowToInsert($jobCrawlModel, $keyword, $employerId)
    {
        $jobExist = XJobCrawl::find()
            ->where(
                ['crawl_job_url' => $jobCrawlModel['job_url'],
                ])
            ->one();
        if ($jobExist) {
            $jobExist->crawl_url = $jobCrawlModel['job_url'];
            $jobExist->updated = date('Y-m-d H:i:s');
            $jobExist->save();
            // Update cả vào bảng company
            Yii::$app->db->createCommand()->update(
                'x_employer_crawl',
                [
                    'updated' => date('Y-m-d H:i:s')
                ],
                [
                    'company_name' => $jobCrawlModel['company_crawl_name']
                ]
            )->execute();
            return false;
        }

        return true;
    }

    /**
     * @todo Get all jobs from a certain page like /tuyen-dung?page=1
     */
    abstract public function crawlBrowseJob($cursor = NULL);

    /**
     * @todo extract job details
     */
    abstract public function extractAllJobFromHtml($html);

    /**
     * @param $jobCrawlData
     * @param $keyword
     * @param null $employerId
     * @param $crawlSource
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Insert all extracted data to x_job_crawl
     */
    public function insertToJobCrawlTable($jobCrawlData, $keyword, $employerId = NULL, $crawlSource)
    {
        $arrCrawlJobUrl = $this->getAllJobCrawlUrl($crawlSource);
        $crawlJobUrl = trim($jobCrawlData['job_url']);
        if (in_array($crawlJobUrl, $arrCrawlJobUrl)) {
            return false;
        }

        // Phải crawl job về để lấy thêm data
        $jobCrawlData = $this->crawlJob($crawlJobUrl);
        $companyLink = trim($jobCrawlData['company_link']);
        // nếu không có title thì trả về lỗi
        if (!isset($jobCrawlData['job_crawl_title']) || $jobCrawlData['job_crawl_title'] == '') {
            return false;
        }

        // Nếu không crawl được
        if (!$jobCrawlData) {
            return false;
        }


        $jobCrawlModel = new XJobCrawl();
        if ($employerId) {
            $jobCrawlModel->employer_id = $employerId;
        } else {
            //matching employer
            $companyName = TextFormatHelper::replaceMultipleSpace($jobCrawlData['company_crawl_name']);
            // $companyName = 'Ominext';
            $companyNameSearch = TextFormatHelper::getCrawlCompanyNameToSearch($companyName);
            if (!empty($companyNameSearch)) {
                if (!empty($companyNameSearch[0]['id'])) {
                    $companyId = $companyNameSearch[0]['id'];
                } else {
                    $companyId = null;
                }

                //check on unmatch table
                $checkUnmatch = XEmployerUnmatch::find()
                                                ->where(['crawled_employer_name' => $companyName])
                                                ->andWhere(['unmatched_employer_id' => $companyId])
                                                ->one();
                //trường hợp không có trong bảng unmatch
                if (empty($checkUnmatch)) {
                    $jobCrawlModel->employer_id = $companyId;
                } else {
                    $jobCrawlModel->employer_id = -1;
                }
                $jobCrawlModel->migrate_employer = 1;
            }
        }
        $jobCrawlModel->keyword = $keyword;
        $jobCrawlModel->job_title = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_title']);
        $jobCrawlModel->job_category = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_skills']);
        $jobCrawlModel->job_skill = $jobCrawlData['job_crawl_category'] == '' ? TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_skills']) : TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_category']);
        $jobCrawlModel->job_place = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_place']);
        $jobCrawlModel->job_position = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_position']);
        $jobCrawlModel->job_year_experience = $jobCrawlData['job_crawl_experience'];
        $jobCrawlModel->company_name = TextFormatHelper::replaceMultipleSpace($jobCrawlData['company_crawl_name']);
        $jobCrawlModel->company_logo = trim($jobCrawlData['company_crawl_logo']);
        $jobCrawlModel->deadline = $jobCrawlData['job_crawl_deadline'];
        $jobCrawlModel->posted_time = $jobCrawlData['job_crawl_posted'];
        $jobCrawlModel->job_description = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_description']);
        $jobCrawlModel->job_benefit = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_benefit']);
        $jobCrawlModel->job_requirement = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_requirement']);
        $jobCrawlModel->created = date('Y-m-d H:i:s');
        $jobCrawlModel->updated = date('Y-m-d H:i:s');
        $jobCrawlModel->max_salary = $jobCrawlData['max_salary'] < 100000 ? (floor($jobCrawlData['max_salary'] * 22700 / 1000000)) * 1000000 : $jobCrawlData['max_salary'];
        $jobCrawlModel->min_salary = $jobCrawlData['min_salary'] < 100000 ? (floor($jobCrawlData['min_salary'] * 22700 / 1000000)) * 1000000 : $jobCrawlData['min_salary'];
        $jobCrawlModel->crawl_source = $crawlSource;
        $jobCrawlModel->crawl_job_url = $crawlJobUrl;
        $jobCrawlModel->crawl_url = $crawlJobUrl;
        $jobCrawlModel->job_level = trim($jobCrawlData['job_crawl_position']);
        $jobCrawlModel->job_type = trim($jobCrawlData['job_crawl_type']);
        $jobCrawlModel->job_degree = trim($jobCrawlData['job_crawl_degree']);
        $jobCrawlModel->job_other_requirement = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_other_requirement']);
        $jobCrawlModel->company_link = $companyLink;
        $jobCrawlModel->company_address = TextFormatHelper::replaceMultipleSpace($jobCrawlData['company_crawl_address']);

        // echo 'company: ' . $jobCrawlModel->company_name . '. employer id:' . $jobCrawlModel->employer_id . '. migrated:' . $jobCrawlModel->migrate_employer;
        return $jobCrawlModel->save(false);
    }

    /**
     * @todo Browse for jobs by cursor then use the url to get the details
     */
    abstract public function siteCrawler($cursor = NULL);

    /**
     *
     * @param $source
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    protected function crawlAllNewJobsBySource($source)
    {
        $xJobCrawlLog = new XJobCrawlLogQueries();
        
        // check if there is a running process, do not run crawl
        if ($xJobCrawlLog->isRunningProcess($source) == false) {            
            // save the first log
            $startLogID = $xJobCrawlLog->saveStartLog($source);
            while (!$this->isRunningOutJobs) {
                // crawl jobs from pages
                $crawlJob = $this->crawlBrowseJob($this->page);
                // count pages
                if ($crawlJob) {
                    $this->page += 1;
                }
                // update end log
                $xJobCrawlLog->saveEndLog($startLogID, $this->totalJob, $this->newJob, $this->page);
            }
            // if running out of jobs, update estimated time
            if ($this->isRunningOutJobs == true) {
                $xJobCrawlLog->updateEstimatedTime($startLogID);
            }
        }
    }

    /**
     * @param $crawlSource
     * @return array
     */
    private function getAllJobCrawlUrl($crawlSource)
    {
        // get time
        $timeStart = date('Y-m-d', strtotime("-45 days")) . ' 00:00:01';
        // get all crawl job url in last 45 days
        $arrCrawlJobUrl = XJobCrawl::find()->select('crawl_job_url')
            ->where(['crawl_source' => $crawlSource])
            ->andWhere(['>=', 'created', $timeStart])
            ->asArray()->all();
        // return only values
        return array_column($arrCrawlJobUrl, 'crawl_job_url');
    }
}
