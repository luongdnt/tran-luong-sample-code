<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\crawl;

use Yii;
use common\models\Employer;
use common\models\XJobCrawl;
use yii\helpers\ArrayHelper;
use common\components\HopeTimeHelper;
use common\models\Job;
use common\models\Place;
use common\models\JobPlaceAssoc;
use common\components\HopeUrlHelper;

/**
 * Description of HopeGeoHelper
 *
 * @author Luongtn
 */
class HopeCrawlMywork extends HopeCrawl {
    
    public $crawlSource = "mywork";
    public $crawlDomain = "https://mywork.com.vn";
    
    /**
     * Crawl job data thành 1 mảng data
     * @param type $url
     * @return type
     */
    public function crawlJob($url) {
        
        try{
            $html= file_get_contents($url);        
        }  catch (\Exception $e){
            return false;
        }
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);

        //company info
        $classname="company-info";
        $jobCompanyInfoNodes = $finder->query("//div[@class='{$classname}']/*[@class='job-company-info']/p");
        $jobTitleNode = $finder->query("//div[@class='title-job-info']");

        foreach ($jobTitleNode as $index => $node){
            $tmp_dom = new \DOMDocument(); 
            $tmp_dom->appendChild($tmp_dom->importNode($node,true));
            $innerHTML=trim($tmp_dom->saveHTML()); 
            $crawlData['job_crawl_title']= html_entity_decode(strip_tags($innerHTML));
        }
        
        foreach ($jobCompanyInfoNodes as $index => $node) 
        {                        
            $tmp_dom = new \DOMDocument(); 
            $tmp_dom->appendChild($tmp_dom->importNode($node,true));
            $innerHTML=trim($tmp_dom->saveHTML()); 
            if($index == 0){
                $crawlData['job_crawl_place']= str_replace("Nơi làm việc:" , "" ,html_entity_decode(strip_tags($innerHTML)));
                $crawlData['job_crawl_place']= str_replace("Nơi làm việc:" , "" ,$crawlData['job_crawl_place']);
            }elseif($index == 1){
                $crawlData['job_crawl_type']= str_replace("Loại hình công việc:" , "" ,html_entity_decode(strip_tags($innerHTML)));
            }elseif($index == 2){
                $strSalary= str_replace("Mức lương:" , "" ,html_entity_decode(strip_tags($innerHTML)));
                $strSalary = preg_replace('/[^0-9-]/', '', $strSalary);
                $arrSalary = explode("-", $strSalary);
                if(isset($arrSalary[1])){
                    $crawlData['min_salary'] = $arrSalary[0];
                    $crawlData['max_salary'] = $arrSalary[1];
                }elseif(isset($arrSalary[0])){
                    $crawlData['min_salary'] = $arrSalary[0];
                    $crawlData['max_salary'] = 0;
                }else{
                    $crawlData['min_salary'] = 0;
                    $crawlData['max_salary'] = 0;
                }
                
            }
        }
        
        // job info
        $classname="info_job";
        $jobInfoNodes = $finder->query("//div[@class='{$classname}']/li");
        foreach ($jobInfoNodes as $index => $node) 
        {
            $tmp_dom = new \DOMDocument(); 
            $tmp_dom->appendChild($tmp_dom->importNode($node,true));
            $innerHTML= $tmp_dom->saveHTML();             
            if($index == 0){
                $crawlData['job_crawl_experience']= str_replace("Kinh nghiệm" , "" ,html_entity_decode(strip_tags($innerHTML)));
            }elseif($index == 1){
                $crawlData['job_crawl_position']= str_replace("Cấp bậc" , "" ,html_entity_decode(strip_tags($innerHTML)));
            }elseif($index == 3){
                $crawlData['job_crawl_degree']= str_replace("Trình độ học vấn" , "" ,html_entity_decode(strip_tags($innerHTML)));
            }
        }        
        
        // job skill
        $classname="desjob-company";
        $jobSkillNodes = $finder->query("//div[@class='{$classname}']/span");
        foreach ($jobSkillNodes as $index => $node)
        {
            $tmp_dom = new \DOMDocument(); 
            $tmp_dom->appendChild($tmp_dom->importNode($node,true));
            $innerHTML= $tmp_dom->saveHTML();
            $crawlData['job_crawl_skills'] .= html_entity_decode(strip_tags($innerHTML)) ." | ";
        } 
        
        $crawlData['job_crawl_skills'] = rtrim($crawlData['job_crawl_skills'] , ' | ');

        // job description, benefit, requirement, 
        $classname="desjob-company";
        $jobDetailNodes = $finder->query("//div[@class='block_info contentdef']/div[@class='{$classname}']");
        foreach ($jobDetailNodes as $index => $node)
        {
            $tmp_dom = new \DOMDocument(); 
            $tmp_dom->appendChild($tmp_dom->importNode($node,true));
            $innerHTML= $tmp_dom->saveHTML();
            if($index == 0){
                $crawlData['job_crawl_description']= str_replace("Mô tả công việc" , "" ,html_entity_decode(strip_tags($innerHTML)));
            }elseif($index == 1){
                $crawlData['job_crawl_benefit']= str_replace("Quyền lợi được hưởng" , "" ,html_entity_decode(strip_tags($innerHTML)));
            }elseif($index == 2 || $index == 3){
                $crawlData['job_crawl_requirement'] .= str_replace("Yêu cầu công việc" , "" ,html_entity_decode(strip_tags($innerHTML)));
            }elseif($index == 4){
                $crawlData['job_crawl_other_requirement'] .= str_replace("Yêu cầu khác" , "" ,html_entity_decode(strip_tags($innerHTML)));
            }
        }         
        
        // job deadline
        $classname="job_deadline";
        $jobDeadlineNodes = $finder->query("//div[@class='{$classname}']");
        foreach ($jobDeadlineNodes as $index => $node)
        {
            $tmp_dom = new \DOMDocument(); 
            $tmp_dom->appendChild($tmp_dom->importNode($node,true));
            $innerHTML= $tmp_dom->saveHTML();
            $crawlData['job_crawl_deadline'] .= str_replace("(Hạn nộp:" , "" ,html_entity_decode(strip_tags($innerHTML)));
            $crawlData['job_crawl_deadline'] = str_replace(")" , "" ,$crawlData['job_crawl_deadline']);
            $crawlData['job_crawl_deadline'] = date('Y-m-d', strtotime($crawlData['job_crawl_deadline']));
        }                   
        
        // company name
        $companyNameNodes = $finder->query("//h2[@class='fullname-company']");
        foreach ($companyNameNodes as $index => $node)
        {
            $tmp_dom = new \DOMDocument(); 
            $tmp_dom->appendChild($tmp_dom->importNode($node,true));
            $innerHTML= $tmp_dom->saveHTML();
            $crawlData['company_crawl_name'] .= html_entity_decode(strip_tags($innerHTML));
        }  
        
        // company logo
        $companyLogoNodes = $finder->query("//div[@class='logo-company']/img");
        foreach ($companyLogoNodes as $index => $node)
        {
            $innerHTML= $node->getAttribute('src');
            $crawlData['company_crawl_logo'] .= $innerHTML;
        }  
        
        // company detail
        $classname="info-lienhe";
        $companyDetailNodes = $finder->query("//div[@class='{$classname}']/p");
        foreach ($companyDetailNodes as $index => $node)
        {
            $tmp_dom = new \DOMDocument(); 
            $tmp_dom->appendChild($tmp_dom->importNode($node,true));
            $innerHTML= $tmp_dom->saveHTML();
            if($index == 0){
                $crawlData['company_crawl_address']= html_entity_decode(strip_tags($innerHTML));
            }elseif(strpos(html_entity_decode(strip_tags($innerHTML)), 'Điện thoại:') !== false){
                $crawlData['company_crawl_phone']= str_replace("Điện thoại:" , "" ,html_entity_decode(strip_tags($innerHTML)));
            }elseif(strpos(html_entity_decode(strip_tags($innerHTML)), 'Website:') !== false){
                $crawlData['company_crawl_website'] .= str_replace("Website:" , "" ,html_entity_decode(strip_tags($innerHTML)));
            }elseif(strpos(html_entity_decode(strip_tags($innerHTML)), 'Quy mô công ty:') !== false){
                $crawlData['company_crawl_number_people'] .= str_replace("Quy mô công ty:" , "" ,html_entity_decode(strip_tags($innerHTML)));
            }
        }      
        
        // ngày duyệt
        
        
        array_walk_recursive($crawlData, function(&$value) {
            $value = trim($value);
        });
        
        return $crawlData;
    }
    
    public function getJobCrawlCategory($jobCrawlModel) {        
        return[];
    }
    
    /**
     * Lấy job degree
     * @param type $strDegree
     * @return type
     */
    public function getJobCrawlDegree($strDegree) {
        if($strDegree == "Trung cấp"){
            return ['degree_name' => "Trung cấp - Nghề", "degree_id" => 1];
        }elseif($strDegree == "Cao đẳng"){
            return ['degree_name' => "Cao Đẳng", "degree_id" => 2];
        }elseif($strDegree == "Đại học" || $strDegree == "Kỹ sư" || $strDegree == "Cử nhân"){
            return ['degree_name' => "Đại Học", "degree_id" => 3];
        }elseif($strDegree == "Trên đại học" || $strDegree == "Thạc sĩ" || $strDegree == "Thạc sĩ Y học"){
            return ['degree_name' => "Thạc sỹ", "degree_id" => 4];
        }else{
            return ['degree_name' => "Cao Đẳng", "degree_id" => 2];
        }
    }
    
    /**
     * Lấy job place
     * @param type $strPlace
     * @return type
     */
    public function getJobCrawlPlace($strPlace, $employerId) {
        
        // Lấy thông tin employer
//        $employerModel = \Yii::$app->db->createCommand(
//                    "SELECT ep.* "
//                . " FROM employer AS e, employer_product AS ep"
//                . " WHERE e.transfer_ref_id = ep.transfer_ref_id"
//                . " AND e.employer_id = $employerId"
//                )->queryOne();
        
        $arrPlace = explode(",", $strPlace);
        $arrPlaceResult = Place::find()
                ->select('place_name, place_id')
                ->where(['place_name' => $arrPlace])
                ->asArray()
                ->all();
        
        return $arrPlaceResult;
        
        $withEmployerAddress = false;
        // Check xem các place có nằm trong address của employer thì đưa vào
        foreach($arrPlace AS $index => $place){
            $lowerPlaceName = strtolower($place['place_name']);
            $lowerAddress = strtolower($employerModel['address']);
            if(strstr($lowerAddress, $lowerPlaceName) !== FALSE && !empty($employerModel['geo_latitude'])){
                $arrPlace[$index]['place_name'] = $employerModel['address'];
                $arrPlace[$index]['geo_latitude'] = $employerModel['geo_latitude'];
                $arrPlace[$index]['geo_longitude'] = $employerModel['geo_longitude'];
                $withEmployerAddress = true;
            }
        }            
        
        return $arrPlaceResult;
    }

    /**
     * Lấy job position
     * @param type $strPosition
     * @return type
     */
    public function getJobCrawlPosition($strPosition) {
        
        if(strstr($strPosition, "Mới tốt nghiệp") || strstr($strPosition, "Thực tập sinh") 
                || strstr($strPosition, "Hợp đồng")){
            return ['job_position_name' => "Mới tốt nghiệp/Thực tập sinh", "job_position_id" => 1];
        }elseif(strstr($strPosition, "Nhân viên") || strstr($strPosition, "Kỹ thuật viên/Kỹ sư") 
                || strstr($strPosition, "Mới đi làm")){
            return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
        }elseif(strstr($strPosition, "Trưởng") || strstr($strPosition, "trưởng")){
            return ['job_position_name' => "Trưởng nhóm/Trưởng phòng", "job_position_id" => 3];
        }elseif(strstr($strPosition, "Giám") || strstr($strPosition, "giám") || strstr($strPosition, "cấp cao")){
            return ['job_position_name' => "Giám đốc và cấp cao hơn", "job_position_id" => 4];
        }else{
            return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
        }
    }

    /**
     * Lấy job salary
     * @param type $strSalary
     */
    public function getJobCrawlSalary($strSalary) {
        $arrSalaryPiece = explode("-", $strSalary);
        $data['max_salary'] = 0;
        $data['min_salary'] = 0;        
        if(count($arrSalaryPiece) == 2){
            $data['min_salary'] = preg_replace('/[^\d]/', '', $arrSalaryPiece[0]) * 1000000;
            $data['max_salary'] = preg_replace('/[^\d]/','', $arrSalaryPiece[1]) * 1000000;
        }
        return $data;
    }

    /**
     * Lấy job type
     * @param type $strType
     * @return type
     */
    public function getJobCrawlType($strType) {
        if(strstr($strType, "Toàn thời gian") || strstr($strType, "hợp đồng") || strstr($strType, "Hợp đồng")){
            return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
        }elseif(strstr($strType, "Bán thời gian")){
            return ['job_type_name' => "Bán thời gian", "job_type_id" => 2];
        }elseif(strstr($strType, "Thực tập")){
            return ['job_type_name' => "Thực tập", "job_type_id" => 5];
        }else{
            return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
        }
    }
    
    /**
     * Lấy job experiment
     * @param type $strYearExp
     * @return type
     */
    public function getJobCrawlYearExp($strYearExp) {
        return ["minYearExp" => 0, "maxYearExp" => 0];
    }
    
    /**
     * Insert job place
     * 
     * @param type $strPlace
     * @param type $job_id
     * @param type $employer_id
     * @param type $crawlSource
     */
    public function insertJobPlace($strPlace, $job_id, $employer_id, $crawlSource) {
        
    }
    
    /**
     * Insert vào bảng job
     * @param type $jobCrawlModel
     * @return Job
     */
    public function insertToJobTable($jobCrawlModel) {
        // Lấy các thông tin job, job_place, salary, degree, position, exp, category
        $placeValue = $this->getJobCrawlPlace($jobCrawlModel->job_place, $jobCrawlModel->employer_id);
        $degreeValue = $this->getJobCrawlDegree($jobCrawlModel->job_degree);
        $jobTypeValue = $this->getJobCrawlType($jobCrawlModel->job_type);
        $positionValue = $this->getJobCrawlPosition($jobCrawlModel->job_position);
        $experimentValue = $this->getJobCrawlYearExp($jobCrawlModel->job_year_experience);
        $categoryValue = $this->getJobCrawlCategory($jobCrawlModel);    

        // Insert vào bảng job
        try{   
            $jobExist = $this->allowToInsertJobTable($jobCrawlModel);
            
            if(!$jobExist){
                $job = new Job();
                $job->addError('error', "Job tồn tại");                     
                return $job;
            }

            $job = new Job();
            $job->employer_id = $jobCrawlModel->employer_id;
            $job->job_title = $jobCrawlModel->job_title;            
            $job->crawl_detail_id = $jobCrawlModel->id;
            $job->crawl_source = $jobCrawlModel->crawl_source;
            $job->degree = $degreeValue['degree_name'];
            $job->degree_id = $degreeValue['degree_id'];
            $job->min_expect_salary = $jobCrawlModel->min_salary > 1000 ? floor($jobCrawlModel->min_salary / 1000000) : $jobCrawlModel->min_salary;
            $job->max_expect_salary = $jobCrawlModel->max_salary > 1000 ? floor($jobCrawlModel->max_salary / 1000000) : $jobCrawlModel->max_salary;
            $job->exp_min_require_year = $experimentValue['minYearExp'];
            $job->exp_max_require_year = $experimentValue['maxYearExp'];
            $job->job_type = $jobTypeValue['job_type_name'];
            $job->job_type_id = $jobTypeValue['job_type_id'];
            $job->job_position = $positionValue['job_position_name'];
            $job->job_position_id = $positionValue['job_position_id'];
            $job->job_category = implode(",", $categoryValue);            
            $job->job_description = $jobCrawlModel->job_description;
            $job->job_requirement = $jobCrawlModel->job_requirement;
            $job->job_benefit = $jobCrawlModel->job_benefit;
            $job->deadline = $jobCrawlModel->deadline ? $jobCrawlModel->deadline : HopeTimeHelper::getDayPlus(30);
            $job->created = HopeTimeHelper::getNow();
            $job->updated = HopeTimeHelper::getNow();
            $job->status = 0;
            $job->admin_id = Yii::$app->user->identity->user_id;
            $job->save();
            
            if($job->errors){                
                return $job;
            }
            $employerModel = Employer::find()
                    ->where(['employer_id' => $jobCrawlModel->employer_id])
                    ->one();
            // Insert vào bảng place assoc
            foreach($placeValue AS $place){
                $placeAssocModel = new JobPlaceAssoc();
                $placeAssocModel->job_id = $job->job_id;
                $placeAssocModel->place_id = $place['place_id'];
                $placeAssocModel->detail_address = $place['place_name'];
                $placeAssocModel->geo_latitude = $place['geo_latitude'];
                $placeAssocModel->geo_longitude = $place['geo_longitude'];
                $placeAssocModel->province_name = $place['place_name'];
                $placeAssocModel->district_name = '';
                $placeAssocModel->address_google = $place['place_name'];
                $placeAssocModel->save();
                if($placeAssocModel->errors){
                    return $job;
                }
            }
            // Insert vào bảng category_assoc
            foreach($categoryValue AS $categoryId){
                $categoryAssocModel = new JobCategoryAssoc();
                $categoryAssocModel->job_id = $job->job_id;
                $categoryAssocModel->job_category_id = $categoryId;
                $categoryAssocModel->save();
                if($categoryAssocModel->errors){
                    return $job;
                }
            }

            return $job;
        } catch (\Exception $ex) {
            print_r($ex->getMessage());exit;
            return $job;            
        }
    }
    
    /**
     * Lấy job benefit
     * @param type $arrBenefit
     * @return type
     */
    public static function getJobBenefit($arrBenefit){
        $strBenefit = '';
        foreach($arrBenefit AS $benefit){
            $strBenefit .= "\r\n<p>$benefit</p>";
        }
        return $strBenefit;
    }
            
    /**
     * Insert vào bảng job_crawl để duyệt
     * 
     * @param type $jobCrawlData
     * @param type $keyword
     * @param type $employerId
     */
    public function insertToJobCrawlTable($jobCrawlData, $keyword, $employerId=NULL){
        
        // Nếu không cho phép insert thì trả về luôn
        if(!$this->allowToInsert($jobCrawlData, $keyword, $employerId)){
            return false;
        }
        $crawlJobUrl = $jobCrawlData['job_url'];
        // Phải crawl job về để lấy thêm data        
        $jobCrawlData = $this->crawlJob($crawlJobUrl);
        
        // Nếu không crawl được 
        if(!$jobCrawlData){
            return false;
        }
        
        $jobCrawlModel = new XJobCrawl();
        if($employerId){
            $jobCrawlModel->employer_id = $employerId;
        }
        $jobCrawlModel->keyword = $keyword;
        $jobCrawlModel->job_title = $jobCrawlData['job_crawl_title'];
        $jobCrawlModel->job_category = $jobCrawlData['job_crawl_skills'];
        $jobCrawlModel->job_skill = $jobCrawlData['job_crawl_skills'];
        $jobCrawlModel->job_place = $jobCrawlData['job_crawl_place'];
        $jobCrawlModel->job_position = $jobCrawlData['job_crawl_position'];
        $jobCrawlModel->job_year_experience = $jobCrawlData['job_crawl_experience'];        
        $jobCrawlModel->company_name = $jobCrawlData['company_crawl_name'];
        $jobCrawlModel->company_logo = $jobCrawlData['company_crawl_logo'];
        $jobCrawlModel->deadline = $jobCrawlData['job_crawl_deadline'];
        $jobCrawlModel->posted_time = $jobCrawlData['job_crawl_posted'];
        $jobCrawlModel->job_description = $jobCrawlData['job_crawl_description'];
        $jobCrawlModel->job_benefit = $jobCrawlData['job_crawl_benefit'];
        $jobCrawlModel->job_requirement = $jobCrawlData['job_crawl_requirement'];
        $jobCrawlModel->created = date('Y-m-d H:i:s');
        $jobCrawlModel->updated = date('Y-m-d H:i:s');
        $jobCrawlModel->max_salary = $jobCrawlData['max_salary'] < 100000 ? (floor($jobCrawlData['max_salary'] * 22700/1000000)) * 1000000 : $jobCrawlData['max_salary'];
        $jobCrawlModel->min_salary = $jobCrawlData['min_salary'] < 100000 ? (floor($jobCrawlData['min_salary'] * 22700/1000000)) * 1000000 : $jobCrawlData['min_salary'];
        $jobCrawlModel->crawl_source = $this->crawlSource;
        $jobCrawlModel->crawl_job_url = $crawlJobUrl;
        $jobCrawlModel->crawl_url = $crawlJobUrl;
        $jobCrawlModel->job_level = $jobCrawlData['job_crawl_position'];
                
        return $jobCrawlModel->save();
        
    }
    
    /**
     * Tạo link search từ domain tương ứng
     * VD: https://mywork.com.vn/tim-viec-lam/tong-cong-ty-co-phan-buu-chinh-viettel.html
     * 
     * @param type $keyword
     */
    public function createSearchUrl($keyword){        
        // Encode keywork 
        $keywordSlug = HopeUrlHelper::slug($keyword);
        // Domain
        $domainSearch = $this->crawlDomain;
        // Link search theo format của từng trang
        // VD: https://mywork.com.vn/tim-viec-lam/tong-cong-ty-co-phan-buu-chinh-viettel.html
        $searchUrl = $domainSearch . "/tim-viec-lam/$keywordSlug.html";

        return $searchUrl;
    }
    
    /**
     * Search trên trang Vietnamwork : https://www.vietnamworks.com/C%C3%B4ng-ty-TNHH-ANS-Asia-kv
     * Từ kết quả trả về insert vào bảng x_job_crawl
     * @param type $keyword
     */
    public function searchJob($keyword, $employerId = NULL) {
        // Url search từ VNW
        $searchUrl = $this->createSearchUrl($keyword);                        
        
        // Lấy kết quả trả về
        // create curl resource 
        $ch = curl_init(); 
        // set url 
        curl_setopt($ch, CURLOPT_URL, $searchUrl); 
        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                
        // $output contains the output string 
        $html = curl_exec($ch); 
        // close curl resource to free up system resources 
        curl_close($ch);         
        
        // Lấy mảng job trả về
        $arrJobData = $this->getSearchJobResult($html);        
        // Insert vào bảng x_job_crawl
        foreach($arrJobData AS $jobData){
            $this->insertToJobCrawlTable($jobData, $keyword, $employerId);
        }
        
        return $arrJobData;
    }
    
    /**
     * Xử lý html lấy về thành mảng data
     * [job_title, job_url,employer_name, place, deadline, salary, posted_date]
     * 
     * @param type $html
     */
    public function getSearchJobResult($html){                                
        // Dom object
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        
        // Các job trả về
        $arrJobItem = $finder->query("//div[@class='job-relate tablesearch']/div[@class='panel panel-default']/div[@id='list-jobs']/div[@class='item ']");   
        
        $crawlData =[];
        // Lấy thông tin của từng job
        foreach ($arrJobItem as $index => $jobNode){
            
            $arrJobUrlNode = $finder->query("div[@class='title col-8']/a[@class='title']", $jobNode);
            
            foreach($arrJobUrlNode AS $index => $jobUrlNode){
                $jobUrl= $this->crawlDomain . $jobUrlNode->getAttribute('href');            
                $tmp_dom = new \DOMDocument(); 
                $tmp_dom->appendChild($tmp_dom->importNode($jobUrlNode,true));
                $jobTitle = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $jobData['job_title'] = $jobTitle;
                $jobData['job_url'] = $jobUrl;
            }                         
            
            // Company
            $companyNode = $finder->query("div[@class='bottom']/div[@class='col-5']/p[@class='desc']/a", $jobNode);            
            foreach($companyNode AS $index => $company){
                $companyName = $company->getAttribute('title');
                $jobData['employer_name'] = str_replace("Tìm việc làm của nhà tuyển dụng ", "", $companyName);
            }            
                        
            // Deadline
            $deadlineNode = $finder->query("div[@class='bottom']/div[@class='col-3']/p[@class='desc']", $jobNode);
            $tmp_dom = new \DOMDocument(); 
            foreach($deadlineNode AS $index => $deadlineNd){
                $tmp_dom->appendChild($tmp_dom->importNode($deadlineNd,true));
                $deadline = html_entity_decode(strip_tags($tmp_dom->saveHTML())); 
                $deadline = str_replace("/", "-", $deadline);
                $deadline = date('Y-m-d', strtotime($deadline));
                $jobData['deadline'] = $deadline;    
            }
            
            
            // Place
            $placeNode = $finder->query("div[@class='bottom']/div[@class='col-4']/p[@class='desc text-location']", $jobNode);            
            foreach($placeNode AS $index => $placeNd){
                $tmp_dom = new \DOMDocument();
                $tmp_dom->appendChild($tmp_dom->importNode($placeNd,true));
                $placeName = html_entity_decode(strip_tags($tmp_dom->saveHTML()));             
                $jobData['place'] = $placeName; 
            }
                           
            // Nếu deadline < tomorrow thì ko insert
            if(strtotime($jobData['deadline']) - time() > 86400){
                $crawlData[] = $jobData;
            }
                                    
        }
        return $crawlData;
    }
    
    //==================================BROWSE JOB============================//
    /**
     * Search trên trang Mywork : https://mywork.com.vn/tuyen-dung/trang/8
     * Từ kết quả trả về insert vào bảng x_job_crawl
     * @param type $keyword
     */
    public function crawlBrowseJob($cursor=NULL) {
        // Url browse từ VNW
        $browseUrl = "https://mywork.com.vn/tuyen-dung";
        
        if($cursor){
            $browseUrl = $browseUrl . "/trang/$cursor";
        } 
        
        // Lấy kết quả trả về
        // create curl resource 
        $ch = curl_init();
        // set url 
        curl_setopt($ch, CURLOPT_URL, $browseUrl); 
        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        // $output contains the output string 
        $html = curl_exec($ch); 
        // close curl resource to free up system resources 
        curl_close($ch);              
        // Lấy mảng job trả về
        $arrJobData = $this->extractAllJobFromHtml($html);
        
        // Insert vào bảng x_job_crawl
        foreach($arrJobData AS $jobData){
            $this->insertToJobCrawlTable($jobData, 'browse_job', 0);
        }
        
        return ['data' => $arrJobData];
    }    
    
    /**
     * Xử lý html lấy về thành mảng data
     * [job_title, job_url,employer_name, place, deadline, salary, posted_date]
     * 
     * @param type $html
     */
    public function extractAllJobFromHtml($html){        
        
        // Dom object
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        
        // Các job trả về
        $arrJobItem = $finder->query("//div[@class='row mw-box-item']/div[@class='col-md-12 col-lg-12 job-over-item']"
                . "//div[@class='row job-item']//div[@class='col-md-12 col-lg-12 job_info']");   
        
        $crawlData =[];
        // Lấy thông tin của từng job
        foreach ($arrJobItem as $index => $jobNode){
            
            $arrJobUrlNode = $finder->query("div[@class='company_name']//a[@class='el-tooltip item']", $jobNode);
            
            foreach($arrJobUrlNode AS $index => $jobUrlNode){
                $jobUrl= $this->crawlDomain . $jobUrlNode->getAttribute('href');            
                $tmp_dom = new \DOMDocument(); 
                $tmp_dom->appendChild($tmp_dom->importNode($jobUrlNode,true));
                $jobTitle = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $jobData['job_title'] = $jobTitle;
                $jobData['job_url'] = $jobUrl;
            }     
            
            // Company
            $companyNode = $finder->query("div[@class='company_name']//p[@class='j_company text_ellipsis']//a", $jobNode);        
            foreach($companyNode AS $index => $company){
                $tmp_dom = new \DOMDocument(); 
                $tmp_dom->appendChild($tmp_dom->importNode($company,true));
                $companyName = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $jobData['company_name'] = $companyName;
            }            
            
            // Deadline
            $deadlineNode = $finder->query("div[@class='extra-info']//div[@class='table-item']//div[@class='time']", $jobNode);
            
            $tmp_dom = new \DOMDocument(); 
            foreach($deadlineNode AS $index => $deadlineNd){
                $tmp_dom->appendChild($tmp_dom->importNode($deadlineNd,true));
                $deadline = html_entity_decode(strip_tags($tmp_dom->saveHTML())); 
                $deadline = str_replace("/", "-", $deadline);
                $deadline = date('Y-m-d', strtotime($deadline));
                $jobData['job_deadline'] = $deadline;    
            }
                        
            // Place
            $placeNode = $finder->query("div[@class='extra-info']//div[@class='table-item']//div[@class='location text_ellipsis']", $jobNode);       
            foreach($placeNode AS $index => $placeNd){
                $tmp_dom = new \DOMDocument();
                $tmp_dom->appendChild($tmp_dom->importNode($placeNd,true));
                $placeName = html_entity_decode(strip_tags($tmp_dom->saveHTML()));             
                $jobData['job_place'] = $placeName; 
            }
            
            // Lương
            $arrSalaryNode = $finder->query("div[@class='extra-info']//div[@class='table-item']//div[@class='dollar']", $jobNode);       
            foreach($arrSalaryNode AS $index => $salaryNode){
                $tmp_dom = new \DOMDocument();
                $tmp_dom->appendChild($tmp_dom->importNode($salaryNode,true));
                $salaryDesc = html_entity_decode(strip_tags($tmp_dom->saveHTML()));             
                $arrSalary = $this->getJobCrawlSalary($salaryDesc);
                $jobData['min_salary'] = $arrSalary['min_salary']; 
                $jobData['max_salary'] = $arrSalary['max_salary']; 
            }                        
            
            // Nếu deadline < tomorrow thì ko insert
            $crawlData[] = $jobData;
            if(strtotime($jobData['deadline']) - time() > 86400){
                
            }
                                    
        }
        return $crawlData;
    }    
    
    /**
     * Insert vào bảng job_crawl để duyệt
     * 
     * @param type $jobCrawlData
     * @param type $keyword
     * @param type $employerId
     */
    public function insertToJobCrawlTableDirect($jobCrawlData, $keyword, $employerId=NULL){
        
        // Nếu không cho phép insert thì trả về luôn
        if(!$this->allowToInsert($jobCrawlData, $keyword, $employerId)){
            return false;
        }
        $crawlJobUrl = $jobCrawlData['job_url'];                     
        
        // Nếu không crawl được 
        if(!$jobCrawlData){
            return false;
        }
        
        $jobCrawlModel = new XJobCrawl();
        if($employerId){
            $jobCrawlModel->employer_id = $employerId;
        }
        $jobCrawlModel->keyword = $keyword;
        $jobCrawlModel->job_title = $jobCrawlData['job_title'];
        $jobCrawlModel->job_category = $jobCrawlData['job_skills'];
        $jobCrawlModel->job_skill = $jobCrawlData['job_skills'];
        $jobCrawlModel->job_place = $jobCrawlData['job_place'];
        $jobCrawlModel->job_position = $jobCrawlData['job_position'];
        $jobCrawlModel->job_year_experience = $jobCrawlData['job_experience'];        
        $jobCrawlModel->company_name = $jobCrawlData['company_name'];
        $jobCrawlModel->company_logo = $jobCrawlData['company_logo'];
        $jobCrawlModel->deadline = $jobCrawlData['job_deadline'];
        $jobCrawlModel->posted_time = $jobCrawlData['job_posted'];
        $jobCrawlModel->job_description = $jobCrawlData['job_description'];
        $jobCrawlModel->job_benefit = $jobCrawlData['job_benefit'];
        $jobCrawlModel->job_requirement = $jobCrawlData['job_requirement'];
        $jobCrawlModel->created = date('Y-m-d H:i:s');
        $jobCrawlModel->updated = date('Y-m-d H:i:s');
        $jobCrawlModel->max_salary = $jobCrawlData['max_salary'] < 100000 ? (floor($jobCrawlData['max_salary'] * 22700/1000000)) * 1000000 : $jobCrawlData['max_salary'];
        $jobCrawlModel->min_salary = $jobCrawlData['min_salary'] < 100000 ? (floor($jobCrawlData['min_salary'] * 22700/1000000)) * 1000000 : $jobCrawlData['min_salary'];
        $jobCrawlModel->crawl_source = $this->crawlSource;
        $jobCrawlModel->crawl_job_url = $crawlJobUrl;
        $jobCrawlModel->crawl_url = $crawlJobUrl;
        $jobCrawlModel->job_level = $jobCrawlData['job_position'];
                
        return $jobCrawlModel->save();
        
    }
    
    //==============================END BROWSE JOB============================//

}
