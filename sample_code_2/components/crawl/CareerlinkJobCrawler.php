<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\crawl;

use common\extensions\extra\TextFormatHelper;
use Yii;
use common\models\Employer;
use common\models\XJobCrawl;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use common\components\HopeTimeHelper;
use common\models\Job;
use common\models\Place;
use common\models\JobPlaceAssoc;
use common\components\HopeUrlHelper;

/**
 * Description of HopeGeoHelper
 *
 * @author Luongtn
 * @modified QuyenNV
 */
class CareerlinkJobCrawler extends JobCrawler
{

    public $crawlSource = "careerlink";
    public $crawlDomain = "https://www.careerlink.vn";

    /**
     * Crawl job data thành 1 mảng data
     * @param $url
     *
     * @return mixed
     */
    public function crawlJob($url)
    {
        $html = file_get_contents($url);
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);

        $mainNode = $finder->query("//div[@class='container body-container']")->item(0);
        //company info
        $jobTitleNode = $finder->query("//div[@class='page-header job-header']/h1", $mainNode)->item(0);
        $crawlData['job_crawl_title'] = $jobTitleNode->textContent;

        // Ngày update
        $postedNode = $finder->query("//div[@class='row']/div[@class='col-sm-7']//div[@class='job-data']//dl//dd//span[@class='hidden']", $mainNode)->item(0);
        $crawlData['job_crawl_posted'] = date('Y-m-d', strtotime($postedNode->textContent));

        // Deadline
        $deadlineNode = $finder->query("//div[@class='row']/div[@class='col-sm-7']//div[@class='job-data']//dl//dd//span[@class='hidden']", $mainNode)->item(1);
        $crawlData['job_crawl_deadline'] = date('Y-m-d', strtotime($deadlineNode->textContent));


        // Category, deadline, salary, place, level, experiment
        $arrJobInfoNode = $finder->query("//div[@class='row']//div[@class='col-sm-7']//div[@class='job-data']", $mainNode)->item(0);
        // company info
        $arrCompanyNode = $finder->query("//ul[@class='list-unstyled critical-job-data']//li", $arrJobInfoNode);
        foreach ($arrCompanyNode as $index => $value) {
            if ($index == 0) {
                $crawlData['company_crawl_name'] = $value->textContent;
            } elseif ($index == 1) {
                $crawlData['company_crawl_address'] = $value->textContent;
            } elseif ($index == 2) {
                $jobSalaryRaw = preg_replace("/.*:/", "", $value->textContent);
                $jobSalary = str_replace("Lương:", "", $jobSalaryRaw);
                $jobSalary = preg_replace('/[^0-9-]/', '', $jobSalary);
                $arrSalary = explode("-", $jobSalary);
                if (isset($arrSalary[1])) {
                    $crawlData['min_salary'] = $arrSalary[0];
                    $crawlData['max_salary'] = $arrSalary[1];
                } elseif (isset($arrSalary[0])) {
                    $crawlData['min_salary'] = $arrSalary[0];
                    $crawlData['max_salary'] = 0;
                } else {
                    $crawlData['min_salary'] = 0;
                    $crawlData['max_salary'] = 0;
                }
            }
        }
        // job desc, job benefit
        // note: there are many ways to extract the benefit, it's flexible, job_description contains all by default
        $jobDescriptionNode = $finder->query("//div[@itemprop='description']", $arrJobInfoNode)->item(0);
        if (strpos($jobDescriptionNode->textContent, "* QUYỀN LỢI:")) {
            $jobDescription = explode("* QUYỀN LỢI:", $jobDescriptionNode->textContent);
            $crawlData['job_crawl_description'] = $jobDescription[0];
            $crawlData['job_crawl_benefit'] = $jobDescription[1];
        } elseif (strpos($jobDescriptionNode->textContent, "* ĐÃI NGỘ:")) {
            $jobDescription = explode("* ĐÃI NGỘ:", $jobDescriptionNode->textContent);
            $crawlData['job_crawl_description'] = $jobDescription[0];
            $crawlData['job_crawl_benefit'] = $jobDescription[1];
        } elseif (strpos($jobDescriptionNode->textContent, "* Chính sách phúc lợi:")) {
            $jobDescription = explode("* Chính sách phúc lợi:", $jobDescriptionNode->textContent);
            $crawlData['job_crawl_description'] = $jobDescription[0];
            $crawlData['job_crawl_benefit'] = $jobDescription[1];
        } elseif (strpos($jobDescriptionNode->textContent, "* QUYỀN LỢI ĐƯỢC HƯỞNG:")) {
            $jobDescription = explode("* QUYỀN LỢI ĐƯỢC HƯỞNG:", $jobDescriptionNode->textContent);
            $crawlData['job_crawl_description'] = $jobDescription[0];
            $crawlData['job_crawl_benefit'] = $jobDescription[1];
        } else {
            $crawlData['job_crawl_description'] = $jobDescriptionNode->textContent;
        }

        // job require
        $jobRequirementNode = $finder->query("//div[@itemprop='skills']", $arrJobInfoNode)->item(0);
        $crawlData['job_crawl_requirement'] = $jobRequirementNode->textContent;

        // category, position, place, degree, exp,
        $arrDescNode = $finder->query("//ul[@class='list-unstyled']", $arrJobInfoNode)->item(0);
        $menu_li = $arrDescNode->getElementsByTagName('li'); // get all li tag inside
        foreach ($menu_li as $key => $value) {
            if (strpos($value->textContent, "Ngành nghề việc làm:")) {
                // categories
                $arrCategoriesNode = $finder->query("//li[" . ($key + 1) . "]//ul//li", $arrDescNode);
                $categories = "";
                // get all text inside
                foreach ($arrCategoriesNode as $index => $node) {
                    $categories .= $node->textContent . ", ";
                }
                // remove last comma
                $categories = rtrim(trim($categories), ',');
                $crawlData['job_crawl_category'] = $categories;
            }
            if (strpos(mb_strtolower($value->textContent), "nơi làm việc")) {
                // job place
                $jobPlace = $value->textContent;
                $jobPlace = str_replace("Nơi làm việc:", "", $jobPlace); // remove unnecessary string
                $crawlData['job_crawl_place'] = $jobPlace;
            }
            if (strpos($value->textContent, "Trình độ học vấn:")) {
                // job_degree
                $jobDegree = $value->textContent;
                $jobDegree = str_replace("Trình độ học vấn:", "", $jobDegree); // remove unnecessary string
                $crawlData['job_crawl_degree'] = $jobDegree;
            }
            if (strpos($value->textContent, "Mức kinh nghiệm:")) {
                // exp
                $jobExperience = $value->textContent;
                $jobExperience = str_replace("Mức kinh nghiệm:", "", $jobExperience); // remove unnecessary string
                $crawlData['job_crawl_experience'] = $jobExperience;
            }
            if (strpos($value->textContent, "Loại công việc:")) {
                $jobType = $value->textContent;
                $jobType = str_replace("Loại công việc:", "", $jobType); // remove unnecessary string
                $crawlData['job_crawl_type'] = $jobType;
            }
        }
        // job position
        $jobPositonNode = $finder->query("//li[@itemprop='qualifications']", $arrDescNode)->item(0);
        $jobPostion = $jobPositonNode->textContent; // get job position
        $jobPostion = str_replace("Cấp bậc:", "", $jobPostion); // remove unnecessary string
        $crawlData['job_crawl_level'] = trim($jobPostion);

        array_walk_recursive($crawlData, function (&$value) {
            $value = trim($value);
        });

        return $crawlData;
    }

    public function getJobCrawlCategory($jobCrawlModel)
    {
        return [];
    }

    // <editor-fold defaultstate="collapsed" desc="Xử lý data trả về">

    /**
     * Lấy job degree
     * @param $strDegree
     *
     * @return array
     */
    public function getJobCrawlDegree($strDegree)
    {
        if ($strDegree == "Trung cấp") {
            return ['degree_name' => "Trung cấp - Nghề", "degree_id" => 1];
        } elseif ($strDegree == "Cao đẳng") {
            return ['degree_name' => "Cao Đẳng", "degree_id" => 2];
        } elseif ($strDegree == "Đại học" || $strDegree == "Kỹ sư" || $strDegree == "Cử nhân") {
            return ['degree_name' => "Đại Học", "degree_id" => 3];
        } elseif ($strDegree == "Trên đại học" || $strDegree == "Thạc sĩ" || $strDegree == "Thạc sĩ Y học") {
            return ['degree_name' => "Thạc sỹ", "degree_id" => 4];
        } else {
            return ['degree_name' => "Cao Đẳng", "degree_id" => 2];
        }
    }

    /**
     * Lấy job place
     * @param $strPlace
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getJobCrawlPlace($strPlace, $employerId)
    {

        // Lấy thông tin employer
//        $employerModel = \Yii::$app->db->createCommand(
//                    "SELECT ep.* "
//                . " FROM employer AS e, employer_product AS ep"
//                . " WHERE e.transfer_ref_id = ep.transfer_ref_id"
//                . " AND e.employer_id = $employerId"
//                )->queryOne();

        $arrPlace = explode(",", $strPlace);
        $arrPlaceResult = Place::find()
            ->select('place_name, place_id')
            ->where(['place_name' => $arrPlace])
            ->asArray()
            ->all();

        return $arrPlaceResult;

        $withEmployerAddress = false;
        // Check xem các place có nằm trong address của employer thì đưa vào
        foreach ($arrPlace as $index => $place) {
            $lowerPlaceName = strtolower($place['place_name']);
            $lowerAddress = strtolower($employerModel['address']);
            if (strstr($lowerAddress, $lowerPlaceName) !== FALSE && !empty($employerModel['geo_latitude'])) {
                $arrPlace[$index]['place_name'] = $employerModel['address'];
                $arrPlace[$index]['geo_latitude'] = $employerModel['geo_latitude'];
                $arrPlace[$index]['geo_longitude'] = $employerModel['geo_longitude'];
                $withEmployerAddress = true;
            }
        }

        return $arrPlaceResult;
    }

    /**
     * Lấy job position
     * @param $strPosition
     *
     * @return array
     */
    public function getJobCrawlPosition($strPosition)
    {

        if (strstr($strPosition, "Mới tốt nghiệp") || strstr($strPosition, "Thực tập sinh")
            || strstr($strPosition, "Hợp đồng") || strstr($strPosition, "Sinh viên")) {
            return ['job_position_name' => "Mới tốt nghiệp/Thực tập sinh", "job_position_id" => 1];
        } elseif (strstr($strPosition, "Nhân viên") || strstr($strPosition, "Kỹ thuật viên/Kỹ sư")
            || strstr($strPosition, "Mới đi làm")) {
            return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
        } elseif (strstr($strPosition, "Trưởng") || strstr($strPosition, "trưởng") || strstr($strPosition, "Quản") || strstr($strPosition, "quản")) {
            return ['job_position_name' => "Trưởng nhóm/Trưởng phòng", "job_position_id" => 3];
        } elseif (strstr($strPosition, "Giám") || strstr($strPosition, "giám") || strstr($strPosition, "cấp cao")) {
            return ['job_position_name' => "Giám đốc và cấp cao hơn", "job_position_id" => 4];
        } else {
            return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
        }
    }

    /**
     * Lấy job salary
     * @param $strSalary
     * @return mixed
     */
    public function getJobCrawlSalary($strSalary)
    {
        $strSalary = preg_replace('/[^\d-\/]/', '', $strSalary);
        $arrSalaryPiece = explode("-", $strSalary);
        $data['max_salary'] = 0;
        $data['min_salary'] = 0;
        if (count($arrSalaryPiece) == 2) {
            $data['min_salary'] = preg_replace('/[^\d]/', '', $arrSalaryPiece[0]);
            if ($data['min_salary'] > 1000000) {
                $data['min_salary'] = $data['min_salary'];
            } elseif ($data['min_salary'] > 200) {
                $data['min_salary'] = $data['min_salary'] * 23000;
            } else {
                $data['min_salary'] = $data['min_salary'] * 1000000;
            }
            $data['max_salary'] = preg_replace('/[^\d]/', '', $arrSalaryPiece[1]);
            if ($data['max_salary'] > 1000000) {
                $data['max_salary'] = $data['max_salary'];
            } elseif ($data['max_salary'] > 200) {
                $data['max_salary'] = $data['max_salary'] * 23000;
            } else {
                $data['max_salary'] = $data['max_salary'] * 1000000;
            }
        }
        return $data;
    }

    /**
     * Lấy job type
     * @param $strType
     *
     * @return array
     */
    public function getJobCrawlType($strType)
    {
        if (strstr($strType, "Toàn thời gian") || strstr($strType, "hợp đồng") || strstr($strType, "Hợp đồng")) {
            return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
        } elseif (strstr($strType, "Bán thời gian")) {
            return ['job_type_name' => "Bán thời gian", "job_type_id" => 2];
        } elseif (strstr($strType, "Thực tập")) {
            return ['job_type_name' => "Thực tập", "job_type_id" => 5];
        } else {
            return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
        }
    }

    /**
     * Lấy job experiment
     * @param $strYearExp
     *
     * @return array
     */
    public function getJobCrawlYearExp($strYearExp)
    {
        $strYearExp = preg_replace("/[^0-9-]/", "", $strYearExp);
        $arrYearExp = explode("-", $strYearExp);
        $result = ["minYearExp" => 0, "maxYearExp" => 0];
        if (!empty($arrYearExp[0])) {
            $result['minYearExp'] = $arrYearExp[0];
        }
        if (!empty($arrYearExp[1])) {
            $result['maxYearExp'] = $arrYearExp[1];
        }
        return $result;
    }

    /**
     * Lấy job benefit
     * @param $arrBenefit
     *
     * @return string
     */
    public static function getJobBenefit($arrBenefit)
    {
        $strBenefit = '';
        foreach ($arrBenefit as $benefit) {
            $strBenefit .= "\r\n<p>$benefit</p>";
        }
        return $strBenefit;
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Insert">
    /**
     * Insert job place
     *
     * @param $strPlace
     * @param $job_id
     * @param $employer_id
     * @param $crawlSource
     */
    public function insertJobPlace($strPlace, $job_id, $employer_id, $crawlSource)
    {

    }

    /**
     * Insert vào bảng job_crawl để duyệt
     *
     * @param $jobCrawlData
     * @param $keyword
     * @param $employerId
     * @return bool
     * @throws \yii\db\Exception
     */
    public function insertToJobCrawlTable($jobCrawlData, $keyword, $employerId = NULL)
    {
        // get all crawl job url in last 45 days
        $arrCrawlJobUrl = $this->getAllJobCrawlUrl($this->crawlSource);
        $crawlJobUrl = $jobCrawlData['job_url'];
        // if this job url in the list then return false
        if (in_array($crawlJobUrl, $arrCrawlJobUrl)) {
            return false;
        }

        $companyLogo = $jobCrawlData['company_logo'];
        // Phải crawl job về để lấy thêm data
        $jobCrawlData = $this->crawlJob($crawlJobUrl);
        // Nếu không crawl được
        if (!$jobCrawlData) {
            return false;
        }

        $jobCrawlModel = new XJobCrawl();
        if ($employerId) {
            $jobCrawlModel->employer_id = $employerId;
        }
        $jobCrawlModel->keyword = $keyword;
        $jobCrawlModel->job_title = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_title']);
        $jobCrawlModel->job_category = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_category']);
        $jobCrawlModel->job_skill = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_category']);
        $jobCrawlModel->job_place = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_place']);
        $jobCrawlModel->job_position = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_level']);
        $jobCrawlModel->job_year_experience = $jobCrawlData['job_crawl_experience'];
        $jobCrawlModel->company_name = TextFormatHelper::replaceMultipleSpace($jobCrawlData['company_crawl_name']);
        $jobCrawlModel->company_logo = $companyLogo;
        $jobCrawlModel->deadline = $jobCrawlData['job_crawl_deadline'];
        $jobCrawlModel->posted_time = $jobCrawlData['job_crawl_posted'];
        $jobCrawlModel->job_description = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_description']);
        $jobCrawlModel->job_benefit = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_benefit']);
        $jobCrawlModel->job_requirement = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_requirement']);
        $jobCrawlModel->job_other_requirement = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_other_requirement']);
        $jobCrawlModel->created = date('Y-m-d H:i:s');
        $jobCrawlModel->updated = date('Y-m-d H:i:s');
        $jobCrawlModel->max_salary = $jobCrawlData['max_salary'] < 100000 ? (floor($jobCrawlData['max_salary'] * 22700 / 1000000)) * 1000000 : $jobCrawlData['max_salary'];
        $jobCrawlModel->min_salary = $jobCrawlData['min_salary'] < 100000 ? (floor($jobCrawlData['min_salary'] * 22700 / 1000000)) * 1000000 : $jobCrawlData['min_salary'];
        $jobCrawlModel->crawl_source = $this->crawlSource;
        $jobCrawlModel->crawl_job_url = $crawlJobUrl;
        $jobCrawlModel->crawl_url = $crawlJobUrl;
        $jobCrawlModel->job_level = trim($jobCrawlData['job_crawl_level']);
        $jobCrawlModel->job_degree = trim($jobCrawlData['job_crawl_degree']);
        $jobCrawlModel->job_type = trim($jobCrawlData['job_crawl_type']);
        return $jobCrawlModel->save(false);
    }

    /**
     * Insert vào bảng job
     * @param $jobCrawlModel
     * @return Job
     */
    public function insertToJobTable($jobCrawlModel)
    {
        // Lấy các thông tin job, job_place, salary, degree, position, exp, category
        $placeValue = $this->getJobCrawlPlace($jobCrawlModel->job_place, $jobCrawlModel->employer_id);
        $degreeValue = $this->getJobCrawlDegree($jobCrawlModel->job_degree);
        $jobTypeValue = $this->getJobCrawlType($jobCrawlModel->job_type);
        $positionValue = $this->getJobCrawlPosition($jobCrawlModel->job_position);
        $experimentValue = $this->getJobCrawlYearExp($jobCrawlModel->job_year_experience);
        $categoryValue = $this->getJobCrawlCategory($jobCrawlModel);

        // Insert vào bảng job
        try {
            $jobExist = $this->allowToInsertJobTable($jobCrawlModel);

            if (!$jobExist) {
                $job = new Job();
                $job->addError('error', "Job tồn tại");
                return $job;
            }

            $job = new Job();
            $job->employer_id = $jobCrawlModel->employer_id;
            $job->job_title = $jobCrawlModel->job_title;
            $job->crawl_detail_id = $jobCrawlModel->id;
            $job->crawl_source = $jobCrawlModel->crawl_source;
            $job->degree = $degreeValue['degree_name'];
            $job->degree_id = $degreeValue['degree_id'];
            $job->min_expect_salary = $jobCrawlModel->min_salary > 1000 ? floor($jobCrawlModel->min_salary / 1000000) : $jobCrawlModel->min_salary;
            $job->max_expect_salary = $jobCrawlModel->max_salary > 1000 ? floor($jobCrawlModel->max_salary / 1000000) : $jobCrawlModel->max_salary;
            $job->exp_min_require_year = $experimentValue['minYearExp'];
            $job->exp_max_require_year = $experimentValue['maxYearExp'];
            $job->job_type = $jobTypeValue['job_type_name'];
            $job->job_type_id = $jobTypeValue['job_type_id'];
            $job->job_position = $positionValue['job_position_name'];
            $job->job_position_id = $positionValue['job_position_id'];
            $job->job_category = implode(",", $categoryValue);
            $job->job_description = ($jobCrawlModel->job_description);
            $job->job_requirement = ($jobCrawlModel->job_requirement);
            $job->job_benefit = ($jobCrawlModel->job_benefit);
            $job->job_other_requirement = strip_tags($jobCrawlModel->job_other_requirement);
            $job->deadline = $jobCrawlModel->deadline ? $jobCrawlModel->deadline : HopeTimeHelper::getDayPlus(30);
            $job->created = HopeTimeHelper::getNow();
            $job->updated = HopeTimeHelper::getNow();
            $job->status = 0;
            $job->admin_id = Yii::$app->user->identity->user_id;
            $job->save();

            if ($job->errors) {
                return $job;
            }
            $employerModel = Employer::find()
                ->where(['employer_id' => $jobCrawlModel->employer_id])
                ->one();
            // Insert vào bảng place assoc
            foreach ($placeValue as $place) {
                $placeAssocModel = new JobPlaceAssoc();
                $placeAssocModel->job_id = $job->job_id;
                $placeAssocModel->place_id = $place['place_id'];
                $placeAssocModel->detail_address = $place['place_name'];
                $placeAssocModel->geo_latitude = $place['geo_latitude'];
                $placeAssocModel->geo_longitude = $place['geo_longitude'];
                $placeAssocModel->province_name = $place['place_name'];
                $placeAssocModel->district_name = '';
                $placeAssocModel->address_google = $place['place_name'];
                $placeAssocModel->save();
                if ($placeAssocModel->errors) {
                    return $job;
                }
            }
            // Insert vào bảng category_assoc
            foreach ($categoryValue as $categoryId) {
                $categoryAssocModel = new JobCategoryAssoc();
                $categoryAssocModel->job_id = $job->job_id;
                $categoryAssocModel->job_category_id = $categoryId;
                $categoryAssocModel->save();
                if ($categoryAssocModel->errors) {
                    return $job;
                }
            }

            return $job;
        } catch (\Exception $ex) {
            print_r($ex->getMessage());
            exit;
            return $job;
        }
    }
    // </editor-fold>

    /**
     * Tạo link search từ domain tương ứng
     * VD: https://careerbuilder.vn/viec-lam/cong-ty-fpt-k-sortdv-r50-vi.html
     *
     * @param $keyword
     * @return string
     */
    public function createSearchUrl($keyword)
    {
        // Encode keywork
        $keywordSlug = HopeUrlHelper::slug($keyword);
        // Domain
        $domainSearch = $this->crawlDomain;
        // Link search theo format của từng trang
        // VD: https://careerbuilder.vn/viec-lam/cong-ty-fpt-k-sortdv-r50-vi.html
        $searchUrl = $domainSearch . "/viec-lam/{$keywordSlug}-k-sortdv-r50-vi.html";

        return $searchUrl;
    }

    /**
     * Lấy html từ url
     * @param $url
     *
     * @return bool|string
     */
    public function getHtmlFromUrl($url)
    {
        // Lấy kết quả trả về
        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // $output contains the output string
        $html = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch);

        return $html;
    }

    /**
     * Xử lý html lấy về thành mảng data
     * [job_title, job_url,employer_name, place, deadline, salary, posted_date]
     *
     * @param $html
     * @return array
     */
    public function getSearchJobResult($html)
    {
        // Dom object
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);

        // Các job trả về
        $arrJobItem = $finder->query("//div[@class='gird_standard ']"
            . "//dd[@class='brief ']");
        $crawlData = [];
        // Lấy thông tin của từng job
        foreach ($arrJobItem as $index => $jobNode) {

            $jobUrlNode = $finder->query("span[@class='jobtitle']/h3[@class='job']/a", $jobNode)->item(0);
            $jobUrl = $jobUrlNode->getAttribute('href');
            $jobData['job_title'] = $jobUrlNode->textContent;
            $jobData['job_url'] = $jobUrl;

            // Company
            $companyNode = $finder->query("span[@class='jobtitle']/p[@class='namecom']/a", $jobNode)->item(0);
            $companyName = $companyNode->textContent;
            $jobData['employer_name'] = $companyName;
            $jobData['employer_url'] = $companyNode->getAttribute('href');

            // logo
            $logoNode = $finder->query("span[@class='logoJobs']/table/tr/td/a/img", $jobNode)->item(0);
            $jobData['logo_url'] = $logoNode->getAttribute('data-original');

            // Place
            $placeNode = $finder->query("span[@class='jobtitle']/p[@class='location']", $jobNode)->item(0);
            $jobData['place'] = $placeNode->textContent;

            // Salary
            $salaryNode = $finder->query("span[@class='jobtitle']/p[@class='salary lred']", $jobNode)->item(0);
            $jobData['salary'] = str_replace("Lương: ", "", $salaryNode->textContent);

            // Ngày update
            $postedNode = $finder->query("div[@class='dateposted']", $jobNode)->item(0);
            $strPostedDate = str_replace("Ngày cập nhật: ", "", $postedNode->textContent);
            $jobData['job_crawl_posted'] = date('Y-m-d', strtotime($strPostedDate));

            $crawlData[] = $jobData;
        }
        return $crawlData;
    }

    /**
     * Search trên trang CarrerBuilder : https://careerbuilder.vn/viec-lam/cong-ty-fpt-k-vi.html
     * Từ kết quả trả về insert vào bảng x_job_crawl
     * @param $keyword
     * @return array
     * @throws \yii\db\Exception
     */
    public function searchJob($keyword, $employerId = NULL)
    {
        // Url search từ careerbuilder
        $searchUrl = $this->createSearchUrl($keyword);
        // Lấy html
        $html = $this->getHtmlFromUrl($searchUrl);
        // Lấy mảng job trả về từ html
        $arrJobData = $this->getSearchJobResult($html);
        // Insert vào bảng x_job_crawl
        foreach ($arrJobData as $jobData) {
            $this->insertToJobCrawlTable($jobData, $keyword, $employerId);
        }
        return $arrJobData;
    }

    //==================================BROWSE JOB============================//

    /**
     * Search trên trang careerlink : https://www.careerlink.vn/vieclam/list?view=detail&page=2
     * Từ kết quả trả về insert vào bảng x_job_crawl
     * @param $keyword
     * @return array
     * @throws \yii\db\Exception
     */
    public function crawlBrowseJob($cursor = NULL)
    {
        // Url browse từ VNW
        $browseUrl = "https://www.careerlink.vn/vieclam/list?view=detail&page=";

        if ($cursor) {
            $browseUrl .= $cursor;
        } else {
            $browseUrl .= 1;
        }

        // Lấy kết quả trả về
        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $browseUrl);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $headers = [
            'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36',
            'Connection: keep-alive',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Sec-Fetch-Site: same-origin',
            'Sec-Fetch-Mode: navigate',
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // $output contains the output string
        $html = curl_exec($ch);
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        // close curl resource to free up system resources
        curl_close($ch);
        // Lấy mảng job trả về
        $arrJobData = $this->extractAllJobFromHtml($html);
        // count total job crawl
        $this->totalJob += count($arrJobData);
        // check site out of job
        if (count($arrJobData) == 0 || $arrJobData == false) {
            $this->isRunningOutJobs = true;
            exit;
        }
        // Insert vào bảng x_job_crawl
        foreach ($arrJobData as $jobData) {
            $resultInsert = $this->insertToJobCrawlTable($jobData, 'browse_job', 0);
            if ($resultInsert) {
                $this->newJob += 1;
            }
        }
        return ['data' => $arrJobData];
    }

    /**
     * Xử lý html lấy về thành mảng data
     * [job_title, job_url,employer_name, place, deadline, salary, posted_date]
     *
     * @param $html
     * @return array
     */
    public function extractAllJobFromHtml($html)
    {

        // Dom object
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        $dom = new \DOMDocument();
        $dom->loadHTML('<?xml encoding="UTF-8">' . $html);
        $finder = new \DOMXPath($dom);
        // Các job trả về
        $arrJobItem = $finder->query("//div[contains(@class,'list-group list-search-result-group')]/div[contains(@class,'list-group-item')]");

        $crawlData = [];
        // Lấy thông tin của từng job
        foreach ($arrJobItem as $index => $jobNode) {

            // job_logo
            $arrJobLogoNode = $finder->query(".//div[@class='media']//img", $jobNode);

            foreach ($arrJobLogoNode as $index => $jobLogoNode) {
                $jobLogoUrl = $jobLogoNode->getAttribute('src');
                $jobData['company_logo'] = $this->crawlDomain . $jobLogoUrl;
            }

            // job_url
            $arrJobUrlNode = $finder->query(".//h2[@class='list-group-item-heading']//a", $jobNode);

            foreach ($arrJobUrlNode as $index => $jobUrlNode) {
                $jobUrl = $jobUrlNode->getAttribute('href');
                $jobData['job_title'] = trim($jobUrlNode->textContent);
                $jobData['job_url'] = $this->crawlDomain . $jobUrl;
            }
            // Company, place
            $otherInfoNode = $finder->query(".//div[@class='list-group-item-text clearfix']//p[@class='priority-data']//a", $jobNode);
            $companyNode = $otherInfoNode->item(0);
            $jobData['company_name'] = trim($companyNode->textContent);
            $jobData['job_place'] = "";
            foreach ($otherInfoNode as $index => $placeNode) {
                // Node 0 là company
                if ($index > 0) {
                    $jobData['job_place'] .= ("," . trim($placeNode->textContent));
                }
            }
            $jobData['job_place'] = ltrim($jobData['job_place'], ",");

            // Update date
            $arrPostedDateNode = $finder->query(".//div[@class='list-group-item-text clearfix']//p[@class='date pull-right']//small", $jobNode);

            foreach ($arrPostedDateNode as $index => $postedDateNode) {
                $postedDate = trim($postedDateNode->textContent);
                $postedDate = preg_replace('/[^\d-\/]/', '', $postedDate);
                $postedDate = str_replace("/", "-", $postedDate);
                $date = \DateTime::createFromFormat('d-m-Y', $postedDate);
                $postedDate = $date->format('Y-m-d');
                $jobData['job_posted'] = $postedDate;
            }


            // Lương
            $arrSalaryAndDescNode = $finder->query(".//div[@class='media']//div[@class='media-body']//small", $jobNode);
            $arrSalaryNode = $arrSalaryAndDescNode->item(0);
            // VD: Thương lượng | Quản lý / Trưởng phòng
            $arrSalaryDesc = explode("|", trim($arrSalaryNode->textContent));
            $strSalaryDesc = trim($arrSalaryDesc[0]);
            $arrSalary = $this->getJobCrawlSalary($strSalaryDesc);
            $jobData['min_salary'] = $arrSalary['min_salary'];
            $jobData['max_salary'] = $arrSalary['max_salary'];
            $jobDescNode = $arrSalaryAndDescNode->item(1);
            $jobData['job_description'] = trim($jobDescNode->textContent);

            // Nếu deadline < tomorrow thì ko insert
            $crawlData[] = $jobData;
            if (strtotime($jobData['deadline']) - time() > 86400) {

            }

            foreach ($jobData as $index => $value) {
                $jobData[$index] = mb_convert_encoding($jobData[$index], 'HTML-ENTITIES', 'UTF-8');
            }

        }

        return $crawlData;
    }

    /**
     * Insert vào bảng job_crawl để duyệt
     *
     * @param $jobCrawlData
     * @param $keyword
     * @param $employerId
     * @return bool
     * @throws \yii\db\Exception
     */
    public function insertToJobCrawlTableDirect($jobCrawlData, $keyword, $employerId = NULL)
    {

        // Nếu không cho phép insert thì trả về luôn
        if (!$this->allowToInsert($jobCrawlData, $keyword, $employerId)) {
            return false;
        }
        $crawlJobUrl = $jobCrawlData['job_url'];

        // Nếu không crawl được
        if (!$jobCrawlData) {
            return false;
        }

        $jobCrawlModel = new XJobCrawl();
        if ($employerId) {
            $jobCrawlModel->employer_id = $employerId;
        }
        $jobCrawlModel->keyword = $keyword;
        $jobCrawlModel->job_title = $jobCrawlData['job_title'];
        $jobCrawlModel->job_category = $jobCrawlData['job_skills'];
        $jobCrawlModel->job_skill = $jobCrawlData['job_skills'];
        $jobCrawlModel->job_place = $jobCrawlData['job_place'];
        $jobCrawlModel->job_position = $jobCrawlData['job_position'];
        $jobCrawlModel->job_year_experience = $jobCrawlData['job_experience'];
        $jobCrawlModel->company_name = $jobCrawlData['company_name'];
        $jobCrawlModel->company_logo = $jobCrawlData['company_logo'];
        $jobCrawlModel->deadline = $jobCrawlData['job_deadline'];
        $jobCrawlModel->posted_time = $jobCrawlData['job_posted'];
        $jobCrawlModel->job_description = $jobCrawlData['job_description'];
        $jobCrawlModel->job_benefit = $jobCrawlData['job_benefit'];
        $jobCrawlModel->job_requirement = $jobCrawlData['job_requirement'];
        $jobCrawlModel->created = date('Y-m-d H:i:s');
        $jobCrawlModel->updated = date('Y-m-d H:i:s');
        $jobCrawlModel->max_salary = $jobCrawlData['max_salary'] < 100000 ? (floor($jobCrawlData['max_salary'] * 22700 / 1000000)) * 1000000 : $jobCrawlData['max_salary'];
        $jobCrawlModel->min_salary = $jobCrawlData['min_salary'] < 100000 ? (floor($jobCrawlData['min_salary'] * 22700 / 1000000)) * 1000000 : $jobCrawlData['min_salary'];
        $jobCrawlModel->crawl_source = $this->crawlSource;
        $jobCrawlModel->crawl_job_url = $crawlJobUrl;
        $jobCrawlModel->crawl_url = $crawlJobUrl;
        $jobCrawlModel->job_level = $jobCrawlData['job_position'];

        return $jobCrawlModel->save(false);

    }

    //==============================END BROWSE JOB============================//

    /**
     * @param null $cursor
     * @return array
     * @throws \yii\db\Exception
     * @todo Browse for jobs by cursor then use the url to get the details
     * @author QuyenNV
     */
    public function careerLinkCrawler($cursor = NULL)
    {
        $arrJobDetail = [];
        // browse jobs by cursor
        $crawlBrowseJob = $this->crawlBrowseJob($cursor);
        // get details by urls
//        foreach ($crawlBrowseJob['data'] as $key => $data) {
//            $arrJobDetail[$key] = $this->crawlJob($data['job_url']);
//        }
//        return $arrJobDetail;
        return $crawlBrowseJob;
    }

    /**
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Crawl all new jobs from site
     */
    public function crawlAllNewJobs()
    {
        $this->crawlAllNewJobsBySource($this->crawlSource);
    }
}
