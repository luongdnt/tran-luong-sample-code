<?php

namespace common\components\crawl;

use common\extensions\extra\TextFormatHelper;
use common\extensions\mysql\libs\XJobCrawlLogQueries;
use common\models\XJobCrawl;

/**
 * @author QuyenNV
 * @todo Crawl all new jobs from vietnamwork site
 * @created Feb 24, 2020
 * Class VietnamworkJobCrawler
 * @package common\components\crawl
 */
class VietnamworkJobCrawler
{
    private $crawlDomain = 'https://www.vietnamworks.com';
    private $crawlSource = 'vietnamworks';
    private $totalJob = 0;
    private $newJob = 0;
    private $isRunningOutJobs = false;
    private $page = 0;
    private $cursor = null;

    /**
     * @inheritDoc
     */
    public function crawlBrowseJob($cursor = NULL)
    {
        // Url browse từ VNW
        $browseUrl = "https://jf8q26wwud-dsn.algolia.net/1/indexes/vnw_job_v2/browse?x-algolia-agent=Algolia%20for%20vanilla%20JavaScript%20(lite)%203.30.0%3Binstantsearch.js%201.6.0%3BJS%20Helper%202.26.1&x-algolia-application-id=JF8Q26WWUD&x-algolia-api-key=MTYxMjJlZmFmNTg5NTA1NjQ5YWUxMjYwZmI3ZjFlMzI3ZjRkMjU3N2U0ZDQ0MDVlZjVjYzM0YmQ3ZGRkNzY0NXRhZ0ZpbHRlcnM9JnVzZXJUb2tlbj1lZWI5NGNjZThkZDVjODVhODdlZTE0MjY4YjFlODAwNg%3D%3D";

        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $browseUrl);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if ($cursor) {
            $postData = '{"cursor":"' . $cursor . '"}';
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        }
        // $output contains the output string
        $html = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch);
        // Lấy mảng job trả về
        $arrJobData = $this->getBrowseJobResult($html);
        $arrBrowseParams = $this->getBrowseJobParams($html);
        // get array of official data by job data
        $arrOfficialJobData = $this->getOfficialJobData($arrJobData);
        // count total job crawl
        $this->totalJob += count($arrOfficialJobData);
        // check if there is no new job then exit
        if (count($arrOfficialJobData) == 0 || $arrJobData == false) {
            $this->isRunningOutJobs = true;
            exit;
        }
        // Insert vào bảng x_job_crawl
        foreach ($arrOfficialJobData as $jobData) {
            $resultInsert = $this->insertToJobCrawlTable($jobData, 'browse_job', 0);
            // count new job inserted to db
            if ($resultInsert) {
                $this->newJob += 1;
            }
        }

        return ['data' => $arrJobData, 'params' => $arrBrowseParams];
    }

    /**
     * @param $html
     * @return array
     * @todo Get jobs data from api
     */
    private function getBrowseJobResult($html)
    {
        $decodeResult = json_decode($html);
        $arrResult['hits'] = $decodeResult->hits;
        if ($arrResult) {
            return $arrResult;
        }
        return [];
    }

    /**
     * @param $html
     * @return array|mixed|object|null
     * @todo get other params from html
     */
    private function getBrowseJobParams($html)
    {
        $decodeResult = json_decode($html);
        unset($decodeResult->hits);
        return $decodeResult;
    }

    /**
     * @param $jobData
     * @param $keyword
     * @param null $employerId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Insert job data to db
     */
    private function insertToJobCrawlTable($jobData, $keyword, $employerId = NULL)
    {
        // check if job data exist
        // get all crawl job url in last 45 days
        $arrCrawlJobUrl = $this->getAllJobCrawlUrl($this->crawlSource);
        $crawlJobUrl = $jobData['crawl_job_url'];
        // if this job url in the list then return false
        if (in_array($crawlJobUrl, $arrCrawlJobUrl)) {
            return false;
        }

        $jobCrawlModel = new XJobCrawl();
        $jobCrawlModel->keyword = $keyword;
        $jobCrawlModel->job_title = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_title']);
        $jobCrawlModel->job_category = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_category']);
        $jobCrawlModel->job_skill = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_skill']);
        $jobCrawlModel->job_place = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_place']);
        $jobCrawlModel->job_position = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_title']);
        $jobCrawlModel->company_name = trim($jobData['crawl_company_name']);
        $jobCrawlModel->company_logo = $jobData['crawl_company_logo'];
        $jobCrawlModel->deadline = $jobData['crawl_job_deadline'];
        $jobCrawlModel->posted_time = $jobData['crawl_job_posted_time'];
        $jobCrawlModel->job_description = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_description']);
        $jobCrawlModel->created = date('Y-m-d H:i:s');
        $jobCrawlModel->updated = date('Y-m-d H:i:s');
        $jobCrawlModel->crawl_date = date('Y-m-d H:i:s');
        $jobCrawlModel->max_salary = $jobData['crawl_job_max_salary'];
        $jobCrawlModel->min_salary = $jobData['crawl_job_min_salary'];
        $jobCrawlModel->crawl_source = $this->crawlSource;
        $jobCrawlModel->crawl_job_url = $jobData['crawl_job_url'];
        $jobCrawlModel->crawl_url = $this->crawlDomain;
        $jobCrawlModel->job_level = trim($jobData['crawl_job_level']);
        $jobCrawlModel->job_benefit = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_benefit']);
        $jobCrawlModel->job_requirement = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_requirement']);
        return $jobCrawlModel->save();

    }

    /**
     * @param $jobCrawlData
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Check if job data can be inserted to db
     */
    private function isAllowInsert($jobCrawlData)
    {
        // check if job already exist
        $jobExist = XJobCrawl::find()
            ->where([
                'crawl_job_url' => $jobCrawlData['crawl_job_url'],
            ])->one();
        // if job exist, just update the time then return false
        if ($jobExist) {
            $jobExist->updated = date('Y-m-d H:i:s');
            $jobExist->save();
            return false;
        }
        return true;
    }

    /**
     * @param $arrJobData
     * @return array
     * @todo Get official job data array by job raw data array
     */
    private function getOfficialJobData($arrJobData)
    {
        $arrJobData = $arrJobData['hits'];
        // an array to store official data
        $arrOfficialJobData = [];
        foreach ($arrJobData as $key => $jobDatum) {
            $arrOfficialJobData[$key]['crawl_job_title'] = $jobDatum->jobTitle;
            $arrOfficialJobData[$key]['crawl_job_place'] = implode(',', $jobDatum->locationVIs);
            $arrOfficialJobData[$key]['crawl_job_category'] = implode(',', $jobDatum->categoryVIs);
            $arrOfficialJobData[$key]['crawl_job_description'] = $jobDatum->jobDescription;
            $arrOfficialJobData[$key]['crawl_job_requirement'] = $jobDatum->jobRequirement;
            $arrOfficialJobData[$key]['crawl_company_name'] = $jobDatum->company;
            $arrOfficialJobData[$key]['crawl_company_logo'] = $jobDatum->companyLogo;
            $arrOfficialJobData[$key]['crawl_job_skill'] = implode(',', $jobDatum->skills);
            $arrOfficialJobData[$key]['crawl_job_deadline'] = $jobDatum->expiredDate == null ? null : date("Y-m-d H:i:s", $jobDatum->expiredDate);
            $arrOfficialJobData[$key]['crawl_job_posted_time'] = $jobDatum->publishedDate == null ? null : date("Y-m-d H:i:s", $jobDatum->publishedDate);
            $arrOfficialJobData[$key]['crawl_job_max_salary'] = $this->calculateSalary($jobDatum->salaryMax);
            $arrOfficialJobData[$key]['crawl_job_min_salary'] = $this->calculateSalary($jobDatum->salaryMin);
            $arrOfficialJobData[$key]['crawl_job_benefit'] = implode('<br>', array_column($jobDatum->benefits, 'benefitValue'));
            $arrOfficialJobData[$key]['crawl_job_url'] = $this->crawlDomain . '/' . $jobDatum->alias . '-' . $jobDatum->objectID . '-jv';
            $arrOfficialJobData[$key]['crawl_job_level'] = $jobDatum->jobLevel;
        }
        return $arrOfficialJobData;
    }

    /**
     * @param $salary
     * @return float|int
     * @todo Calculate min and max salary
     *      If salary <= 300000, it's USD, convert to VND
     *      else return
     */
    private function calculateSalary($salary)
    {
        // USD
        if ($salary <= 300000) {
            return $salary * 23000;
        } else {
            return $salary;
        }
    }

    /**
     * @param $source
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Crawl all new jobs from Glints by source
     */
    private function crawlAllNewJobsBySource($source)
    {
        $xJobCrawlLog = new XJobCrawlLogQueries();
        if ($xJobCrawlLog->isRunningProcess($source) == false) {
            // save start crawl point
            $startLogID = $xJobCrawlLog->saveStartLog($source);
            // repeatedly
            while (!$this->isRunningOutJobs) {
                $crawlJob = $this->crawlBrowseJob($this->cursor);
                // if crawl successfully, count page and move to next cursor
                if ($crawlJob) {
                    $this->page += 1;
                    $this->cursor = $crawlJob['params']->cursor;
                }
                // if number of pages reaches end
                if ($this->page == $crawlJob['params']->nbPages) {
                    // save the end log
                    if ($startLogID) {
                        $xJobCrawlLog->saveEndLog($startLogID, $this->totalJob, $this->newJob, $this->page);
                        $xJobCrawlLog->updateEstimatedTime($startLogID);
                    }
                    exit;
                }
            }
        }
    }

    /**
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Crawl all new jobs from vietnamwork
     */
    public function crawlAllNewJobs()
    {
        $this->crawlAllNewJobsBySource($this->crawlSource);
    }

    /**
     * @param $crawlSource
     * @return array
     */
    private function getAllJobCrawlUrl($crawlSource)
    {
        // get time
        $timeStart = date('Y-m-d', strtotime("-45 days")) . ' 00:00:01';
        // get all crawl job url in last 45 days
        $arrCrawlJobUrl = XJobCrawl::find()->select('crawl_job_url')
            ->where(['crawl_source' => $crawlSource])
            ->andWhere(['>=', 'created', $timeStart])
            ->asArray()->all();
        // return only values
        return array_column($arrCrawlJobUrl, 'crawl_job_url');
    }
}