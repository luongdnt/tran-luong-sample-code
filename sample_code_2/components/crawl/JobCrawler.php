<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\crawl;

use common\extensions\mysql\libs\XJobCrawlLogQueries;
use common\extensions\mysql\libs\XJobCrawlQueries;
use Yii;
use common\models\Employer;
use common\models\XJobCrawl;

/**
 * Description of HopeGeoHelper
 *
 * @author Luongtn
 * @modified QuyenNV
 */
abstract class JobCrawler
{

    protected $totalJob = 0;
    protected $newJob = 0;
    protected $start;
    protected $end;
    protected $isRunningOutJobs = false;
    protected $page = 1;

    /**
     * Tìm job dựa vào keyword
     */
    abstract public function searchJob($keyword);

    abstract public function crawlJob($url);

    /**
     * Lấy danh sách các place
     * @param  $strPlace
     * @param  $crawlSource
     */
    public function getJobCrawlPlace($strPlace, $crawlSource)
    {
    }

    /**
     * Lấy danh sách các category
     * @param  $strCategory
     * @param  $crawlSource
     */
    public function getJobCrawlCategory($jobTitle, $companyName, $crawlSource)
    {
    }

    /**
     * Lấy degree
     * @param  $strDegree
     * @param  $crawlSource
     */
    public function getJobCrawlDegree($strDegree, $crawlSource)
    {
    }

    /**
     * Lấy mức lương
     * @param  $strSalary
     * @param  $crawlSource
     */
    public function getJobCrawlSalary($strSalary, $crawlSource)
    {
    }

    /**
     * Lấy loại công việc (toàn thời gian, bán thời gian...)
     * @param  $strType
     * @param  $crawlSource
     */
    public function getJobCrawlType($strType, $crawlSource)
    {
    }

    /**
     * Lấy năm kinh nghiệm
     * @param  $strYearExp
     * @param  $crawlSource
     */
    public function getJobCrawlYearExp($strYearExp, $crawlSource)
    {
    }

    /**
     * Lấy vị trí mong muốn
     * @param $strPosition
     * @param $crawlSource
     */
    public function getJobCrawlPosition($strPosition, $crawlSource)
    {
    }

    /**
     * Insert job ở bảng crawl vào bảng job
     * @param $jobCrawlModel
     */
    public function insertToJobTable($jobCrawlModel, $jobCrawlSource)
    {

    }

    /**
     * Insert job place
     */
    public function insertJobPlace($strPlace, $job_id, $employer_id, $crawlSource)
    {
    }

    /**
     * Có cho phép insert vào bảng hay không. Cần thỏa mãn để insert :
     * + Chưa có job_title của NTD trong bảng x_job_crawl
     * + Chưa có job_title của NTD trong bảng job
     * + Nếu cty crawl không khớp với cty jobsgo (đã từng bị loại) thì cũng ko insert
     * @param $jobCrawlModel
     * @param $keyword
     * @param $employerId
     * @return boolean
     * @throws \yii\db\Exception
     */
    public function allowToInsert($jobCrawlModel, $keyword, $employerId)
    {

        // Check nếu cty crawl không khớp với cty jobsgo (đã từng bị loại) thì cũng ko insert
        // Check $jobCrawlModel->crawl_job_url chưa tồn tại thì mới insert
        $jobExist = XJobCrawl::find()
            ->where([
                'crawl_job_url' => $jobCrawlModel['job_url'],
            ])
//            ->andWhere([">", 'employer_id', 0])
//            ->orWhere(['and',
//                ['job_title' => $jobCrawlModel->job_title],
//                ['company_name' => $jobCrawlModel->company_name],
//            ])
            ->one();
        if ($jobExist) {
            $jobExist->crawl_url = $jobCrawlModel['job_url'];
            $jobExist->updated = date('Y-m-d H:i:s');
            $jobExist->save();
            // Update cả vào bảng company
            Yii::$app->db->createCommand(
                "UPDATE x_employer_crawl
                    SET updated = :updated
                    WHERE company_name = :company_name"
            )
                ->bindValue(':company_name', $jobCrawlModel->company_name)
                ->bindValue(':updated', date('Y-m-d H:i:s'))
                ->execute();
            return false;
        }

        return true;
        // Check $jobCrawlModel->crawl_job_url chưa tồn tại thì mới insert
        $jobExist = XJobCrawl::find()
            ->where(['job_title' => $jobCrawlData['job_title'],
                'employer_id' => $employerId
            ])
            ->one();
        if ($jobExist) {
            // Update ngày update vào job và employer
            $jobExist->updated = date('Y-m-d H:i:s');
            $jobExist->save();
            return false;
        }

        // Check xem NTD đã có job_title này trong bảng job chưa
        // nếu có rồi thì không insert lại nữa
        $jobExistInJobTable = false;
        if ($employerId) {
            $jobExistInJobTable = Yii::$app->db->createCommand(
                "SELECT * FROM
                    (
                        SELECT job_id, j.employer_id
                        FROM job AS j, employer AS e
                        WHERE 
                        j.employer_id = e.employer_id
                        AND j.job_title = :job_title
                        AND (e.employer_id = :employer_id or e.name LIKE :employer_name)
                        ) AS j1
                        UNION
                        (
                        SELECT job_id, j.employer_id
                        FROM job_product AS j, employer_product AS e
                        WHERE 
                        j.employer_id = e.employer_id
                        AND j.job_title = :job_title
                        AND (e.employer_id = :employer_id or e.name LIKE :employer_name)
                    )"
            )
                ->bindValue(':job_title', $jobCrawlData['job_title'])
                ->bindValue(':employer_id', $employerId)
                ->bindValue(':employer_name', "%$keyword%")
                ->queryAll();
        }
        if ($jobExistInJobTable) {
            return false;
        }

        return true;
    }

    /**
     * Có cho phép insert vào bảng hay không. Cần thỏa mãn để insert :
     * + Chưa có job_title của NTD trong bảng x_job_crawl
     * + Chưa có job_title của NTD trong bảng job
     * + Nếu cty crawl không khớp với cty jobsgo (đã từng bị loại) thì cũng ko insert
     * @param $jobCrawlModel
     * @return boolean
     * @throws \yii\db\Exception
     */
    public function allowToInsertJobTable($jobCrawlModel)
    {

        // Check xem NTD đã có job_title này trong bảng job chưa
        // nếu có rồi thì không insert lại nữa
        $jobExistInJobTable = false;
        if ($employerId) {
            $jobExistInJobTable = Yii::$app->db->createCommand(
                "SELECT * FROM
                    (
                        SELECT job_id, j.employer_id
                        FROM job AS j, employer AS e
                        WHERE 
                        j.employer_id = e.employer_id
                        AND j.job_title = :job_title
                        AND (e.employer_id = :employer_id or e.name LIKE :employer_name)
                        ) AS j1
                        UNION
                        (
                        SELECT job_id, j.employer_id
                        FROM job_product AS j, employer_product AS e
                        WHERE 
                        j.employer_id = e.employer_id
                        AND j.job_title = :job_title
                        AND (e.employer_id = :employer_id or e.name LIKE :employer_name)
                    )"
            )
                ->bindValue(':job_title', $jobCrawlModel->job_title)
                ->bindValue(':employer_id', $jobCrawlModel->employer_id)
                ->bindValue(':employer_name', "%{$jobCrawlModel->keyword}%")
                ->queryAll();
        }
        if (!empty($jobExistInJobTable)) {
            return false;
        }

        return true;
    }

    /**
     *
     * @param $source
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    protected function crawlAllNewJobsBySource($source)
    {
        $xJobCrawlLog = new XJobCrawlLogQueries();
        // check if there is a running process, do not run crawl
        if ($xJobCrawlLog->isRunningProcess($source) == false) {
            // save the first log
            $startLogID = $xJobCrawlLog->saveStartLog($source);
            while (!$this->isRunningOutJobs) {
                // crawl jobs from pages
                $crawlJob = $this->crawlBrowseJob($this->page);
                // count pages
                if ($crawlJob) {
                    $this->page += 1;
                }
                // update end log
                $xJobCrawlLog->saveEndLog($startLogID, $this->totalJob, $this->newJob, $this->page);
            }
            // if running out of jobs, update estimated time
            if ($this->isRunningOutJobs == true) {
                $xJobCrawlLog->updateEstimatedTime($startLogID);
            }
        }
    }

    /**
     * @param $crawlSource
     * @return array
     */
    protected function getAllJobCrawlUrl($crawlSource)
    {
        // get time
        $timeStart = date('Y-m-d', strtotime("-45 days")) . ' 00:00:01';
        // get all crawl job url in last 45 days
        $arrCrawlJobUrl = XJobCrawl::find()->select('crawl_job_url')
            ->where(['crawl_source' => $crawlSource])
            ->andWhere(['>=', 'created', $timeStart])
            ->asArray()->all();
        // return only values
        return array_column($arrCrawlJobUrl, 'crawl_job_url');
    }
}
