<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\crawl;

use Yii;
use common\models\Employer;
use common\models\XJobCrawl;
use yii\helpers\ArrayHelper;
use common\components\HopeTimeHelper;
use common\models\Job;
use common\models\Place;
use common\models\JobPlaceAssoc;
use common\components\HopeUrlHelper;

/**
 * Description of HopeGeoHelper
 *
 * @author Luongtn
 */
class HopeCrawlTimviecnhanh extends HopeCrawl {
    
    public $crawlSource = "timviecnhanh";
    public $crawlDomain = "https://www.timviecnhanh.com";        
            
    // <editor-fold defaultstate="collapsed" desc="Xử lý data trả về">
    /**
     * Lấy job degree
     * @param type $strDegree
     * @return type
     */
    public function getJobCrawlDegree($strDegree) {
        if($strDegree == "Trung cấp"){
            return ['degree_name' => "Trung cấp - Nghề", "degree_id" => 1];
        }
        elseif($strDegree == "Cao đẳng"){
            return ['degree_name' => "Cao Đẳng", "degree_id" => 2];
        }
        elseif($strDegree == "Đại học" || $strDegree == "Kỹ sư" || $strDegree == "Cử nhân"){
            return ['degree_name' => "Đại Học", "degree_id" => 3];
        }
        elseif($strDegree == "Trên đại học" || $strDegree == "Thạc sĩ" || $strDegree == "Thạc sĩ Y học"){
            return ['degree_name' => "Thạc sỹ", "degree_id" => 4];
        }
        else{
            return ['degree_name' => "Cao Đẳng", "degree_id" => 2];
        }
    }
    
    /**
     * Lấy job place
     * @param type $strPlace
     * @return type
     */
    public function getJobCrawlPlace($strPlace, $employerId) {
        
        // Lấy thông tin employer
//        $employerModel = \Yii::$app->db->createCommand(
//                    "SELECT ep.* "
//                . " FROM employer AS e, employer_product AS ep"
//                . " WHERE e.transfer_ref_id = ep.transfer_ref_id"
//                . " AND e.employer_id = $employerId"
//                )->queryOne();
        $strPlace = str_replace(", ", ",", $strPlace);
        $arrPlace = explode(",", $strPlace);
        $arrPlaceResult = Place::find()
                ->select('place_name, place_id')
                ->where(['place_name' => $arrPlace])
                ->asArray()
                ->all();
        
        return $arrPlaceResult;
        
        $withEmployerAddress = false;
        // Check xem các place có nằm trong address của employer thì đưa vào
        foreach($arrPlace AS $index => $place){
            $lowerPlaceName = strtolower($place['place_name']);
            $lowerAddress = strtolower($employerModel['address']);
            if(strstr($lowerAddress, $lowerPlaceName) !== FALSE && !empty($employerModel['geo_latitude'])){
                $arrPlace[$index]['place_name'] = $employerModel['address'];
                $arrPlace[$index]['geo_latitude'] = $employerModel['geo_latitude'];
                $arrPlace[$index]['geo_longitude'] = $employerModel['geo_longitude'];
                $withEmployerAddress = true;
            }
        }            
        
        return $arrPlaceResult;
    }

    /**
     * Lấy job position
     * @param type $strPosition
     * @return type
     */
    public function getJobCrawlPosition($strPosition) {
        
        if(strstr($strPosition, "Mới tốt nghiệp") || strstr($strPosition, "Thực tập sinh") 
                || strstr($strPosition, "Hợp đồng") || strstr($strPosition, "Sinh viên"))
        {
            return ['job_position_name' => "Mới tốt nghiệp/Thực tập sinh", "job_position_id" => 1];
        }
        elseif(strstr($strPosition, "Nhân viên") || strstr($strPosition, "Kỹ thuật viên/Kỹ sư") 
                || strstr($strPosition, "Mới đi làm"))
        {
            return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
        }
        elseif(strstr($strPosition, "Trưởng") || strstr($strPosition, "trưởng") || strstr($strPosition, "Quản") || strstr($strPosition, "quản"))
        {
            return ['job_position_name' => "Trưởng nhóm/Trưởng phòng", "job_position_id" => 3];
        }
        elseif(strstr($strPosition, "Giám") || strstr($strPosition, "giám") || strstr($strPosition, "cấp cao"))
        {
            return ['job_position_name' => "Giám đốc và cấp cao hơn", "job_position_id" => 4];
        }
        else
        {
            return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
        }
    }

    /**
     * Lấy job salary
     * @param type $strSalary
     */
    public function getJobCrawlSalary($strSalary) {
        
    }

    /**
     * Lấy job type
     * @param type $strType
     * @return type
     */
    public function getJobCrawlType($strType) {
        if(strstr($strType, "Toàn thời gian") || strstr($strType, "hợp đồng") || strstr($strType, "Hợp đồng"))
        {
            return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
        }
        elseif(strstr($strType, "Bán thời gian") || strstr($strType, "làm thêm")){
            return ['job_type_name' => "Bán thời gian", "job_type_id" => 2];
        }
        elseif(strstr($strType, "Thực tập")){
            return ['job_type_name' => "Thực tập", "job_type_id" => 5];
        }
        else{
            return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
        }
    }
    
    /**
     * Lấy job experiment
     * @param type $strYearExp
     * @return type
     */
    public function getJobCrawlYearExp($strYearExp) {
        $strYearExp = preg_replace("/[^0-9-]/", "", $strYearExp);
        $arrYearExp = explode("-", $strYearExp);
        $result = ["minYearExp" => 0, "maxYearExp" => 0];
        if(!empty($arrYearExp[0])){
            $result['minYearExp'] = $arrYearExp[0];
        }
        if(!empty($arrYearExp[1])){
            $result['maxYearExp'] = $arrYearExp[1];
        }
        return $result;
    }
    
    /**
     * Lấy job benefit
     * @param type $arrBenefit
     * @return type
     */
    public static function getJobBenefit($arrBenefit){
        $strBenefit = '';
        foreach($arrBenefit AS $benefit){
            $strBenefit .= "\r\n<p>$benefit</p>";
        }
        return $strBenefit;
    }
    
    public function getJobCrawlCategory($jobCrawlModel) {        
        return[];
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Insert">
    /**
     * Insert job place
     * 
     * @param type $strPlace
     * @param type $job_id
     * @param type $employer_id
     * @param type $crawlSource
     */
    public function insertJobPlace($strPlace, $job_id, $employer_id, $crawlSource) {
        
    }
    
    /**
     * Insert vào bảng job_crawl để duyệt
     * 
     * @param type $jobCrawlData
     * @param type $keyword
     * @param type $employerId
     */
    public function insertToJobCrawlTable($jobCrawlData, $keyword, $employerId=NULL){        
        // Nếu không cho phép insert thì trả về luôn
        if(!$this->allowToInsert($jobCrawlData, $keyword, $employerId)){            
            return false;
        }
        $crawlJobUrl = $jobCrawlData['job_url'];        
        // Phải crawl job về để lấy thêm data        
        $jobCrawlData = $this->crawlJob($crawlJobUrl);                        
        
        // Nếu không crawl được 
        if(!$jobCrawlData){
            return false;
        }
        
        $jobCrawlModel = new XJobCrawl();
        if($employerId){
            $jobCrawlModel->employer_id = $employerId;
        }
        $jobCrawlModel->keyword = $keyword;
        $jobCrawlModel->job_title = $jobCrawlData['job_crawl_title'];
        $jobCrawlModel->job_category = $jobCrawlData['job_crawl_category'];
        $jobCrawlModel->job_skill = $jobCrawlData['job_crawl_category'];
        $jobCrawlModel->job_place = $jobCrawlData['job_crawl_place'];
        $jobCrawlModel->job_position = $jobCrawlData['job_crawl_position'];
        $jobCrawlModel->job_degree = $jobCrawlData['job_crawl_degree'];
        $jobCrawlModel->job_type = $jobCrawlData['job_crawl_type'];
        $jobCrawlModel->job_year_experience = $jobCrawlData['job_crawl_experience'];        
        $jobCrawlModel->company_name = $jobCrawlData['company_crawl_name'];
        $jobCrawlModel->company_logo = $jobCrawlData['company_crawl_logo'];
        $jobCrawlModel->deadline = $jobCrawlData['job_crawl_deadline'];
        $jobCrawlModel->posted_time = $jobCrawlData['job_crawl_posted'];
        $jobCrawlModel->job_description = $jobCrawlData['job_crawl_description'];
        $jobCrawlModel->job_benefit = $jobCrawlData['job_crawl_benefit'];
        $jobCrawlModel->job_requirement = $jobCrawlData['job_crawl_requirement'];
        $jobCrawlModel->job_other_requirement = $jobCrawlData['job_crawl_other_requirement'];
        $jobCrawlModel->created = date('Y-m-d H:i:s');
        $jobCrawlModel->updated = date('Y-m-d H:i:s');
        $jobCrawlModel->max_salary = $jobCrawlData['max_salary'] < 100000 ? (floor($jobCrawlData['max_salary'] * 22700/1000000)) * 1000000 : $jobCrawlData['max_salary'];
        $jobCrawlModel->min_salary = $jobCrawlData['min_salary'] < 100000 ? (floor($jobCrawlData['min_salary'] * 22700/1000000)) * 1000000 : $jobCrawlData['min_salary'];
        $jobCrawlModel->crawl_source = $this->crawlSource;
        $jobCrawlModel->crawl_job_url = $crawlJobUrl;
        $jobCrawlModel->crawl_url = $crawlJobUrl;
        $jobCrawlModel->job_level = $jobCrawlData['job_crawl_level'];
                
        return $jobCrawlModel->save();
        
    }
    
    /**
     * Insert vào bảng job
     * @param type $jobCrawlModel
     * @return Job
     */
    public function insertToJobTable($jobCrawlModel) {
        // Lấy các thông tin job, job_place, salary, degree, position, exp, category
        $placeValue = $this->getJobCrawlPlace($jobCrawlModel->job_place, $jobCrawlModel->employer_id);
        $degreeValue = $this->getJobCrawlDegree($jobCrawlModel->job_degree);
        $jobTypeValue = $this->getJobCrawlType($jobCrawlModel->job_type);
        $positionValue = $this->getJobCrawlPosition($jobCrawlModel->job_position);
        $experimentValue = $this->getJobCrawlYearExp($jobCrawlModel->job_year_experience);
        $categoryValue = $this->getJobCrawlCategory($jobCrawlModel);    

        // Insert vào bảng job
        try{   
            $jobExist = $this->allowToInsertJobTable($jobCrawlModel);
            
            if(!$jobExist){
                $job = new Job();
                $job->addError('error', "Job tồn tại");                     
                return $job;
            }

            $job = new Job();
            $job->employer_id = $jobCrawlModel->employer_id;
            $job->job_title = $jobCrawlModel->job_title;            
            $job->crawl_detail_id = $jobCrawlModel->id;
            $job->crawl_source = $jobCrawlModel->crawl_source;
            $job->degree = $degreeValue['degree_name'];
            $job->degree_id = $degreeValue['degree_id'];
            $job->min_expect_salary = $jobCrawlModel->min_salary > 1000 ? floor($jobCrawlModel->min_salary / 1000000) : $jobCrawlModel->min_salary;
            $job->max_expect_salary = $jobCrawlModel->max_salary > 1000 ? floor($jobCrawlModel->max_salary / 1000000) : $jobCrawlModel->max_salary;
            $job->exp_min_require_year = $experimentValue['minYearExp'];
            $job->exp_max_require_year = $experimentValue['maxYearExp'];
            $job->job_type = $jobTypeValue['job_type_name'];
            $job->job_type_id = $jobTypeValue['job_type_id'];
            $job->job_position = $positionValue['job_position_name'];
            $job->job_position_id = $positionValue['job_position_id'];
            $job->job_category = implode(",", $categoryValue);            
            $job->job_description = ($jobCrawlModel->job_description);
            $job->job_requirement = ($jobCrawlModel->job_requirement);
            $job->job_benefit = ($jobCrawlModel->job_benefit);
            $job->job_other_requirement = $jobCrawlModel->job_other_requirement;
            $job->deadline = $jobCrawlModel->deadline ? $jobCrawlModel->deadline : HopeTimeHelper::getDayPlus(30);
            $job->created = HopeTimeHelper::getNow();
            $job->updated = HopeTimeHelper::getNow();
            $job->status = 0;
            $job->admin_id = Yii::$app->user->identity->user_id;
            $job->save();
            
            if($job->errors){                
                return $job;
            }
            $employerModel = Employer::find()
                    ->where(['employer_id' => $jobCrawlModel->employer_id])
                    ->one();
            // Insert vào bảng place assoc
            foreach($placeValue AS $place){
                $placeAssocModel = new JobPlaceAssoc();
                $placeAssocModel->job_id = $job->job_id;
                $placeAssocModel->place_id = $place['place_id'];
                $placeAssocModel->detail_address = $place['place_name'];
                $placeAssocModel->geo_latitude = $place['geo_latitude'];
                $placeAssocModel->geo_longitude = $place['geo_longitude'];
                $placeAssocModel->province_name = $place['place_name'];
                $placeAssocModel->district_name = '';
                $placeAssocModel->address_google = $place['place_name'];
                $placeAssocModel->save();
                if($placeAssocModel->errors){
                    return $job;
                }
            }
            // Insert vào bảng category_assoc
            foreach($categoryValue AS $categoryId){
                $categoryAssocModel = new JobCategoryAssoc();
                $categoryAssocModel->job_id = $job->job_id;
                $categoryAssocModel->job_category_id = $categoryId;
                $categoryAssocModel->save();
                if($categoryAssocModel->errors){
                    return $job;
                }
            }

            return $job;
        } catch (\Exception $ex) {
            print_r($ex->getMessage());exit;
            return $job;            
        }
    }
    
    /**
     * Insert vào bảng job_crawl để duyệt
     * 
     * @param type $jobCrawlData
     * @param type $keyword
     * @param type $employerId
     */
    public function insertToCompanyCrawlTable($companyData, $keyword, $employerId=NULL){        
                
        // Nếu không crawl được 
        if(!$companyData){
            return false;
        }        
        $crawlTurnModel = new \common\models\XCrawlTurn();
        $crawlTurnModel->employer_id =  $employerId;
        $crawlTurnModel->keyword =  $keyword;
        $crawlTurnModel->created =  date('Y-m-d H:i:s');
        $crawlTurnModel->crawl_source =  $this->crawlSource;
        $crawlTurnModel->number_result =  intval($companyData['count_job']);
        $crawlTurnModel->crawl_page_total = intval(ceil($companyData['count_job']/15));        
        $crawlTurnModel->save();
        return $crawlTurnModel->save();
        
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Xử lý html lấy data">
    
    /**
     * Xử lý html lấy về thành mảng data
     * [job_title, job_url,employer_name, place, deadline, salary, posted_date]
     * 
     * @param type $html
     */
    public function getSearchJobResult($html){      
        
        // Dom object
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        $dom = new \DOMDocument();          
        @$dom->loadHTML('<?xml encoding="UTF-8">' .$html);
        $finder = new \DOMXPath($dom);
        
        // Các job trả về
        $arrJobItem = $finder->query("//div[@class='box-content']"
                . "//table[@class='table-content']//tbody/tr");     
        
        $crawlData =[];
        // Lấy thông tin của từng job
        foreach ($arrJobItem as $index => $jobNode){
            
            $jobUrlNode = $finder->query("td[@class='block-item w55']/a", $jobNode)->item(0);    
            
            $jobData['job_title'] = $jobUrlNode->getAttribute('title');            
            $jobData['job_url'] = $jobUrlNode->getAttribute('href');   
            
            // Company
            $companyNode = $finder->query("td[@class='block-item w55']/a", $jobNode)->item(1);              
            $jobData['employer_name'] = $companyNode->getAttribute('title');
            $jobData['employer_url'] = $companyNode->getAttribute('href');             
            
            // Place
            $placeNode = $finder->query("td[@class='text-center w15'][1]/span", $jobNode)->item(0);   
            $jobData['place'] = $placeNode->getAttribute('title');
            if(empty($jobData['place'])){
                $jobData['place'] = $placeNode->textContent;
            }
            // Salary
            $salaryNode = $finder->query("td[@class='text-center w15'][2]", $jobNode)->item(0);        
            $jobData['salary'] = str_replace("Lương: ", "", $salaryNode->textContent); 
            
            // Deadline
            $deadlineNode = $finder->query("td[@class='text-center w15'][3]", $jobNode)->item(0);      
            $strDeadlineDate = $deadlineNode->textContent;
            $jobData['job_crawl_deadline'] = date('Y-m-d', strtotime($strDeadlineDate));            
            
            $crawlData[] = $jobData;
        }        
        return $crawlData;
    }
    
    /**
     * Xử lý html lấy về thành mảng data company
     * [job_title, job_url,employer_name, place, deadline, salary, posted_date]
     * 
     * @param type $html
     */
    public function getCompanyFromHtml($html){                                
        // Dom object
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        
        // Count kết quả
        $countJobNode = $finder->query("//section[@class='body-home']"
                . "//h3[@class='title font-roboto text-primary']"
                . "//span[@class='count']")->item(0);                
        $countJob = $countJobNode->textContent;
        $crawlData['count_job'] = preg_replace("/[^0-9]/", "", $countJob);        
        // Lấy thông tin của từng job
        return $crawlData;
    }
    
    /**
     * Crawl job data thành 1 mảng data
     * @param type $url
     * @return type
     */
    public function crawlJob($url) {        
        try{            
            $html= file_get_contents($url);            
        }  catch (\Exception $e){
            return false;
        }
        $dom = new \DOMDocument();
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);        
        $mainNode = $finder->query("//div[@class='box-info-job']//div[@class='info-left']")->item(0);           
        if(!empty($mainNode)){
            return $this->crawlJobSpecial($html);
        }
        
        $mainNode = $finder->query("//article[@class='block-title']")->item(0);                
        //company info
        $jobTitleNode = $finder->query("//header[@class='block-title']/h1")->item(0);
        $crawlData['job_crawl_title']= $jobTitleNode->textContent;        
        // Ngày update
        $postedNode = $finder->query("//div[@class='bg-nau']/div[@class='col-xs-3 offset10']/time", $mainNode)->item(0);        
        $crawlData['job_crawl_posted'] = date('Y-m-d', strtotime($postedNode->textContent));               
        
        // Category, deadline, salary, place, level, experiment
        $arrJobInfoNode = $finder->query("//div[@class='row']//li", $mainNode);        
        foreach($arrJobInfoNode AS $index => $jobInfoNode){
            if(strpos($jobInfoNode->textContent, "Thành phố") !== false)
            { // place
                $jobPlace = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;
                $crawlData['job_crawl_place'] = str_replace("Việc làm ", "", $jobPlace);
            }
            elseif(strpos($jobInfoNode->textContent, "Hình thức làm việc") !== false)
            { // level
                $jobPosition = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;                             
                $crawlData['job_crawl_position'] = $jobPosition;                                  
            }
            elseif(strpos($jobInfoNode->textContent, "Kinh nghiệm") !== false)
            { // experiment           
                $jobType = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;
                $crawlData['job_crawl_experience'] = trim($jobInfoNode->textContent);              
            }
            elseif(strpos($jobInfoNode->textContent, "Mức lương") !== false)
            { // salary
                $jobSalaryRaw = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;     
                $jobSalary = str_replace("Lương:", "", $jobSalaryRaw);
                $jobSalary = preg_replace('/[^0-9-]/', '', $jobSalary);                
                $arrSalary = explode("-", $jobSalary);
                if(isset($arrSalary[1])){
                    $crawlData['min_salary'] = $arrSalary[0] < 100 ? $arrSalary[0] * 1000000 : $arrSalary[0];
                    $crawlData['max_salary'] = $arrSalary[1] < 100 ? $arrSalary[1] * 1000000 : $arrSalary[0];
                }elseif(isset($arrSalary[0])){
                    $crawlData['min_salary'] = $arrSalary[0] < 100 ? $arrSalary[0] * 1000000 : $arrSalary[0];
                    $crawlData['max_salary'] = 0;
                }else{
                    $crawlData['min_salary'] = 0;
                    $crawlData['max_salary'] = 0;
                }              
            }
            elseif(strpos($jobInfoNode->textContent, "Trình độ") !== false)
            { // level
                $jobDegree = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;                             
                $crawlData['job_crawl_degree'] = $jobDegree;                                  
            }            
            elseif(strpos($jobInfoNode->textContent, "Ngành nghề") !== false)
            { // category
                $jobCategory = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;                  
                $crawlData['job_crawl_category'] = $jobCategory;                 
            }
            elseif(strpos($jobInfoNode->textContent, "Tính chất công việc") !== false)
            { // level
                $jobLevel = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;                  
                $crawlData['job_crawl_type'] = $jobLevel;                 
            }
            elseif(strpos($jobInfoNode->textContent, "Giới tính") !== false)
            { // gender
                $jobGender = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;                  
                $crawlData['job_crawl_gender'] = $jobGender;                 
            }
        }
        
        $jobDetailNode = $finder->query("//table/tbody", $mainNode)->item(0);
        
        // job description
        $jobDescriptionNode = $finder->query("tr[1]/td[2]", $jobDetailNode)->item(0);
        
        $tmp_dom = new \DOMDocument(); 
        $tmp_dom->appendChild($tmp_dom->importNode($jobDescriptionNode,true));
        $innerHTML= $tmp_dom->saveHTML();        
        $innerHTML = preg_replace("/<.*td>/", "", $innerHTML);
        $innerHTML = preg_replace("/<a.*?>/", "", $innerHTML);
        $innerHTML = str_replace("</a>", "", $innerHTML);
        $crawlData['job_crawl_description'] = html_entity_decode($innerHTML);
        
        // job requirement
        $jobRequirementNode = $finder->query("tr[2]/td[2]", $jobDetailNode)->item(0);
        $tmp_dom = new \DOMDocument(); 
        $tmp_dom->appendChild($tmp_dom->importNode($jobRequirementNode,true));
        $innerHTML= $tmp_dom->saveHTML();    
        $innerHTML = preg_replace("/<.*td>/", "", $innerHTML);
        $innerHTML = preg_replace("/<a.*?>/", "", $innerHTML);
        $innerHTML = str_replace("</a>", "", $innerHTML);
        $crawlData['job_crawl_requirement'] = html_entity_decode($innerHTML);
        
        // Benefit
        $jobBenefitNode = $finder->query("tr[3]/td[2]", $jobDetailNode)->item(0);
        $tmp_dom = new \DOMDocument(); 
        $tmp_dom->appendChild($tmp_dom->importNode($jobBenefitNode,true));
        $innerHTML= $tmp_dom->saveHTML();  
        $innerHTML = preg_replace("/<.*td>/", "", $innerHTML);
        $innerHTML = preg_replace("/<a.*?>/", "", $innerHTML);
        $innerHTML = str_replace("</a>", "", $innerHTML);
        $crawlData['job_crawl_benefit'] = html_entity_decode($innerHTML);      
                                
        // deadline
        $jobDeadlineNode = $finder->query("tr[4]/td[2]", $jobDetailNode)->item(0);     
        if(strstr($finder->query("tr[4]/td[1]", $jobDetailNode)->item(0)->textContent,"Hạn nộp") == false){
            $jobDeadlineNode = $finder->query("tr[5]/td[2]", $jobDetailNode)->item(0);     
        }
        $crawlData['job_crawl_deadline'] = date('Y-m-d', strtotime($jobDeadlineNode->textContent));        
        // company name
        $companyNameNode = $finder->query("//div[@class='sidebar inner-10 col-xs-4']"
                . "/div[@class='block-sidebar']"
                . "/div[@class='content-sidebar box_vnb_right']"
                . "//div[@class='title-employer font-roboto line-height-15']")->item(0);
        $crawlData['company_crawl_name'] .= $companyNameNode->textContent;
        
        // company logo
        $companyLogoNode = $finder->query("//div[@class='sidebar inner-10 col-xs-4']"
                . "/div[@class='block-sidebar']"
                . "/div[@class='content-sidebar box_vnb_right']"
                . "//img")->item(0);
        if(!empty($companyLogoNode)){
            $crawlData['company_crawl_logo'] .= $companyLogoNode->getAttribute('src');         
        }
        
        // company address
        $companyAddressNode = $finder->query("//div[@class='sidebar inner-10 col-xs-4']"
                . "/div[@class='block-sidebar']"
                . "/div[@class='content-sidebar box_vnb_right']"
                . "//p")->item(0);
        $crawlData['company_crawl_address'] .= $companyAddressNode->textContent;                              
        
        array_walk_recursive($crawlData, function(&$value) {
            $value = trim($value);
        });
        return $crawlData;
    }
    
    /**
     * Crawl job data thành 1 mảng data
     * @param type $url
     * @return type
     */
    public function crawlJobSpecial($html) {
                
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        
        $mainNode = $finder->query("//div[@class='box-info-job']//div[@class='info-left']")->item(0);                
        //company info
        $jobTitleNode = $finder->query("h1", $mainNode)->item(0);
        $crawlData['job_crawl_title']= $jobTitleNode->textContent;        
        // Ngày update
        $postedNode = $finder->query("//div[@class='view-head']/span[@class='dsp-mo']", $mainNode)->item(0); 
        $strPostedDate = preg_replace("/[^0-9-]/", "", $postedNode->textContent);
        $crawlData['job_crawl_posted'] = date('Y-m-d', strtotime($strPostedDate));               
        
        // Deadline
        $deadlineNode = $finder->query("//div[@class='hnhs']", $mainNode)->item(0); 
        $strDeadline = preg_replace("/[^0-9-]/", "", $deadlineNode->textContent);
        $crawlData['job_crawl_deadline'] = date('Y-m-d', strtotime($strDeadline));     
        
        // Category, deadline, salary, place, level, experiment
        $arrJobInfoNode = $finder->query("//div[@class='item-info']", $mainNode);        
        foreach($arrJobInfoNode AS $index => $jobInfoNode){
            if(strpos($jobInfoNode->textContent, "Địa điểm làm việc") !== false)
            { // place
                $jobPlace = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;
                $crawlData['job_crawl_place'] = str_replace("Việc làm ", "", $jobPlace);
            }
            elseif(strpos($jobInfoNode->textContent, "Hình thức làm việc") !== false)
            { // level
                $jobPosition = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;                             
                $crawlData['job_crawl_position'] = $jobPosition;                                  
            }
            elseif(strpos($jobInfoNode->textContent, "Kinh nghiệm") !== false)
            { // experiment           
                $jobType = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;
                $crawlData['job_crawl_experience'] = trim($jobInfoNode->textContent);              
            }
            elseif(strpos($jobInfoNode->textContent, "Mức lương") !== false)
            { // salary
                $jobSalaryRaw = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;     
                $jobSalary = str_replace("Lương:", "", $jobSalaryRaw);
                $jobSalary = preg_replace('/[^0-9-]/', '', $jobSalary);                
                $arrSalary = explode("-", $jobSalary);
                if(isset($arrSalary[1])){
                    $crawlData['min_salary'] = $arrSalary[0] < 100 ? $arrSalary[0] * 1000000 : $arrSalary[0];
                    $crawlData['max_salary'] = $arrSalary[1] < 100 ? $arrSalary[1] * 1000000 : $arrSalary[0];
                }elseif(isset($arrSalary[0])){
                    $crawlData['min_salary'] = $arrSalary[0] < 100 ? $arrSalary[0] * 1000000 : $arrSalary[0];
                    $crawlData['max_salary'] = 0;
                }else{
                    $crawlData['min_salary'] = 0;
                    $crawlData['max_salary'] = 0;
                }              
            }
            elseif(strpos($jobInfoNode->textContent, "Yêu cầu bằng cấp") !== false)
            { // level
                $jobDegree = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;                             
                $crawlData['job_crawl_degree'] = $jobDegree;                                  
            }            
            elseif(strpos($jobInfoNode->textContent, "Ngành nghề") !== false)
            { // category
                $jobCategory = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;                  
                $crawlData['job_crawl_category'] = $jobCategory;                 
            }
            elseif(strpos($jobInfoNode->textContent, "Tính chất công việc") !== false)
            { // level
                $jobType = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;                  
                $crawlData['job_crawl_type'] = $jobType;                 
            }
            elseif(strpos($jobInfoNode->textContent, "Yêu cầu giới tính") !== false)
            { // gender
                $jobGender = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;                  
                $crawlData['job_crawl_gender'] = $jobGender;                 
            }
        }
        
        $jobDetailNode = $finder->query("//table/tbody", $mainNode)->item(0);
        
        // job description
        $jobDescriptionNode = $finder->query("//div[@class='des-info-job'][1]/p", $mainNode)->item(0);
        
        $tmp_dom = new \DOMDocument(); 
        $tmp_dom->appendChild($tmp_dom->importNode($jobDescriptionNode,true));
        $innerHTML= $tmp_dom->saveHTML();        
        $innerHTML = preg_replace("/<.*td>/", "", $innerHTML);
        $innerHTML = preg_replace("/<a.*?>/", "", $innerHTML);
        $innerHTML = str_replace("</a>", "", $innerHTML);
        $crawlData['job_crawl_description'] = html_entity_decode($innerHTML);
        
        // job requirement
        $jobRequirementNode = $finder->query("//div[@class='des-info-job'][1]/p", $mainNode)->item(1);
        $tmp_dom = new \DOMDocument(); 
        $tmp_dom->appendChild($tmp_dom->importNode($jobRequirementNode,true));
        $innerHTML= $tmp_dom->saveHTML();    
        $innerHTML = preg_replace("/<.*td>/", "", $innerHTML);
        $innerHTML = preg_replace("/<a.*?>/", "", $innerHTML);
        $innerHTML = str_replace("</a>", "", $innerHTML);
        $crawlData['job_crawl_requirement'] = html_entity_decode($innerHTML);
        
        // Benefit
        $jobBenefitNode = $finder->query("//div[@class='des-info-job'][1]/p", $mainNode)->item(2);
        $tmp_dom = new \DOMDocument(); 
        $tmp_dom->appendChild($tmp_dom->importNode($jobBenefitNode,true));
        $innerHTML= $tmp_dom->saveHTML();  
        $innerHTML = preg_replace("/<.*td>/", "", $innerHTML);
        $innerHTML = preg_replace("/<a.*?>/", "", $innerHTML);
        $innerHTML = str_replace("</a>", "", $innerHTML);
        $crawlData['job_crawl_benefit'] = html_entity_decode($innerHTML);      
                                     
        // company name
        $companyNameNode = $finder->query("//div[@class='both-contact'][2]/div[@class='job-company-name']")->item(0);
        $crawlData['company_crawl_name'] = $companyNameNode->textContent;
        
        // company logo
        $companyLogoNode = $finder->query("//span[@id='logo_sv_lg']/img")->item(0);        
        if(!empty($companyLogoNode)){
            $crawlData['company_crawl_logo'] = $companyLogoNode->getAttribute('src');         
        }
        
        // company address
        $companyAddressNode = $finder->query("//div[@class='both-contact'][2]/div")->item(2);
        $crawlData['company_crawl_address'] = $companyAddressNode->textContent;                              
        
        array_walk_recursive($crawlData, function(&$value) {
            $value = trim($value);
        });        
        return $crawlData;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Hàm crawl chính">
    /**
     * Search trên trang CarrerBuilder : https://careerbuilder.vn/viec-lam/cong-ty-fpt-k-vi.html
     * Từ kết quả trả về insert vào bảng x_job_crawl
     * @param type $keyword
     */
    public function searchJob($keyword, $page=0, $employerId = NULL) {
        // Url search từ careerbuilder
        $searchUrl = $this->createSearchUrl($keyword, $page);            
        // Lấy html
        $html = $this->getHtmlFromUrl($searchUrl);                
        // Lấy mảng job trả về từ html
        $arrJobData = $this->getSearchJobResult($html);           
        // Insert vào bảng x_job_crawl
        foreach($arrJobData AS $jobData){
            $this->insertToJobCrawlTable($jobData, $keyword, $employerId);
        }        
        return $arrJobData;
    }        
        
    /**
     * Chỉ crawl company và lưu vào để sau crawl danh sách job
     * @param type $keyword
     * @param type $employerId
     */
    public function crawlCompanyOnly($keyword, $employerId = NULL){
        // Url search company từ careerbuilder
        $searchUrl = $this->createSearchUrl($keyword);            
        // Lấy html
        $html = $this->getHtmlFromUrl($searchUrl);                
        // Lấy thông tin company từ html
        $arrCompanyData = $this->getCompanyFromHtml($html);   
                
        // Insert vào bảng x_crawl_turn
        $this->insertToCompanyCrawlTable($arrCompanyData, $keyword, $employerId);
        
        return $arrCompanyData;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Tạo link search, get html">
    
    /**
     * Tạo link search từ domain tương ứng
     * VD: https://careerbuilder.vn/viec-lam/cong-ty-fpt-k-sortdv-r50-vi.html
     * 
     * @param type $keyword
     */
    public function createSearchUrl($keyword, $page = NULL){        
        // Encode keywork 
        $keywordSlug = urlencode($keyword);
        // Domain
        $domainSearch = $this->crawlDomain;
        // Link search theo format của từng trang        
        // VD: https://www.timviecnhanh.com/vieclam/timkiem?tu_khoa=Công+ty+Cổ+Phần+Bán+Lẻ+Kỹ+Thuật+Số+FPT
        $searchUrl = $domainSearch . "/vieclam/timkiem?tu_khoa=$keywordSlug";
        
        if(!empty($page)){
            $searchUrl .= "&page=$page";
        }

        return $searchUrl;
    }
    
    /**
     * Lấy html từ url
     * @param type $url
     * @return type
     */
    public function getHtmlFromUrl($url){
        // Lấy kết quả trả về
        // create curl resource 
        $ch = curl_init(); 
        // set url 
        curl_setopt($ch, CURLOPT_URL, $url); 
        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                
        // $output contains the output string 
        $html = curl_exec($ch); 
        // close curl resource to free up system resources 
        curl_close($ch);
        
        return $html;
    } 
    
    // </editor-fold>          

}
