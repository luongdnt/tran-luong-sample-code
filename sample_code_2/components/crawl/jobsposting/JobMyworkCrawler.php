<?php

namespace common\components\crawl\jobsposting;

use common\models\XJobCrawl;
use Yii;

/**
 * @author QuyenNV
 * @todo Crawl all jobs from mywork site
 * Class JobMyworkCrawler
 * @package common\components\crawl\jobsposting
 */
class JobMyworkCrawler
{
    private $total_new_job = 50;
    private $total_new_job_page = 20;
    private $count_new_job = 20;
    private $crawl_source = 'Mywork';

    /**
     * @param null $cursor
     * @throws \yii\db\Exception
     * @author Crawl all new jobs from a certain page
     */
    public function crawlBrowseJob($cursor = NULL)
    {
        // Mywork general URL
        $browseUrl = "https://mywork.com.vn/tuyen-dung";

        if ($cursor) {
            $browseUrl = $browseUrl . "/trang/$cursor";
        }
        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $browseUrl);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // $output contains the output string
        $html = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch);
       
        // get job basic data array
        $arrJobData = $this->extractAllJobFromHtml($html);

        // insert jobs to x_job_crawl table
        foreach ($arrJobData as $key => $jobData) {
//            $resultQuery = $this->insertToJobCrawlTable($jobData, 'browse_job', 0);
//            if ($resultQuery) {
//                echo "<p style='color: blue'>" . $jobData['crawl_job_title'] . " inserted!</p>";
//            } else {
//                echo "<p style='color: red'>" . $jobData['crawl_job_title'] . " can not be inserted!</p>";
//            }
        }
    }

    /**
     * @param $html
     * @return array|bool
     * @todo Extract all job basic data from html pages
     */
    private function extractAllJobFromHtml($html)
    {
        // array to store job basic info
        $arrJobsBasicInfo = [];
        // Dom object
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        // get all script nodes
        $arrScripts = $finder->query("//script");
        $jobPostNodeKey = 0;
        // get key node which nodeValue contains window.__NUXT__=
        foreach ($arrScripts as $key => $value) {
            if (preg_match('/window.__NUXT__=/', $value->nodeValue)) {
                $jobPostNodeKey = $key;
            }
        }
        // get jobPosting string, remove the last ;
        $stringJobPosting = rtrim($arrScripts->item($jobPostNodeKey)->nodeValue, ';');
        // remove window.__NUXT__=
        $jsonJobPosting = preg_replace('/window.__NUXT__=/', '', $stringJobPosting);
        // decode json to array
        $arrJobsPosting = json_decode($jsonJobPosting, true);
        // get jobPosting status
        $jobsPostingStatus = array_column($arrJobsPosting['data'], 'pagingJobsNew')[0];
        // assign the attributes
        $this->total_new_job = $jobsPostingStatus['total'];
        $this->total_new_job_page = $jobsPostingStatus['total_pages'];
        $this->count_new_job = $jobsPostingStatus['count'];
        print_r($jobsPostingStatus);echo "<br>";
        // if this page have new jobs
        if ($this->count_new_job > 0) {
            // get new jobs details
            $arrNewJobs = array_column($arrJobsPosting['data'], 'listingJobsNew')[0];
            // assign job data values
            foreach ($arrNewJobs as $key => $newJob) {
                $arrJobsBasicInfo[$key]['crawl_job_id'] = $newJob['id'];
                $arrJobsBasicInfo[$key]['crawl_job_title'] = $newJob['title'];
                $arrJobsBasicInfo[$key]['crawl_job_place'] = $newJob['locations'];
                $arrJobsBasicInfo[$key]['crawl_company_name'] = $newJob['company_name'];
                $arrJobsBasicInfo[$key]['crawl_company_logo'] = $newJob['company_logo'];
                $arrJobsBasicInfo[$key]['crawl_url'] = 'https://mywork.com.vn/tuyen-dung/viec-lam/' . $newJob['id'] . '/' . $newJob['slug'] . '.html';
                $arrJobsBasicInfo[$key]['crawl_job_url'] = 'https://mywork.com.vn/tuyen-dung/viec-lam/' . $newJob['id'] . '/' . $newJob['slug'] . '.html';
            }
            return array_values($arrJobsBasicInfo);
        } else {
            return false;
        }
    }

    /**
     * @param $jobCrawlData
     * @param $keyword
     * @param $employerId
     * @return bool
     * @throws \yii\db\Exception
     * @todo Insert data to x_job_crawl table
     */
    private function insertToJobCrawlTable($jobCrawlData, $keyword, $employerId)
    {
        // if not allow insert to db, return false
        if (!$this->allowToInsert($jobCrawlData, $keyword, $employerId)) {
            print_r("Mywork jobs can not be inserted!");
            return false;
            //return false;
        }
        // get job url 
        $crawlJobUrl = $jobCrawlData['crawl_job_url'];

        // get more info from job url
        $jobCrawlExtraData = $this->crawlJob($crawlJobUrl);

        // if there is no data, return false
        if (!$jobCrawlExtraData) {
            return false;
        }

        $jobCrawlModel = new XJobCrawl();
        // in case there is employer_id
        if ($employerId) {
            $jobCrawlModel->employer_id = $employerId;
        }

        // insert to x_job_crawl
        $jobCrawlModel->job_title = $jobCrawlData['crawl_job_title'];
        $jobCrawlModel->job_place = $jobCrawlData['crawl_job_place'];
        $jobCrawlModel->job_category = $jobCrawlExtraData['crawl_job_category'];
        $jobCrawlModel->job_position = $jobCrawlExtraData['crawl_job_position'];
        $jobCrawlModel->job_description = $jobCrawlExtraData['crawl_job_description'];
        $jobCrawlModel->job_requirement = $jobCrawlExtraData['crawl_job_requirement'];
        $jobCrawlModel->job_other_requirement = $jobCrawlExtraData['crawl_job_other_requirement'];
        $jobCrawlModel->company_name = $jobCrawlData['crawl_company_name'];
        $jobCrawlModel->company_logo = $jobCrawlData['crawl_company_logo'];
        $jobCrawlModel->crawl_url = $jobCrawlData['crawl_url'];
        $jobCrawlModel->deadline = $jobCrawlExtraData['crawl_job_deadline'];
        $jobCrawlModel->posted_time = $jobCrawlExtraData['crawl_job_post_date'];
        $jobCrawlModel->crawl_date = $jobCrawlExtraData['crawl_job_crawl_date'];
        $jobCrawlModel->crawl_source = $jobCrawlExtraData['crawl_job_crawl_source'];
        $jobCrawlModel->keyword = $keyword;
        $jobCrawlModel->created = $jobCrawlExtraData['created'];
        $jobCrawlModel->updated = $jobCrawlExtraData['updated'];
        $jobCrawlModel->min_salary = $jobCrawlExtraData['crawl_job_salary_min'];
        $jobCrawlModel->max_salary = $jobCrawlExtraData['crawl_job_salary_max'];
        $jobCrawlModel->job_benefit = $jobCrawlExtraData['crawl_job_benefit'];
        $jobCrawlModel->crawl_job_url = $jobCrawlData['crawl_job_url'];
        $jobCrawlModel->job_level = $jobCrawlExtraData['crawl_job_level'];
        $jobCrawlModel->job_degree = $jobCrawlExtraData['crawl_job_degree'];
        $jobCrawlModel->job_type = $jobCrawlExtraData['crawl_job_type'];
        $jobCrawlModel->job_year_experience = $jobCrawlExtraData['crawl_job_experience'];
        return $jobCrawlModel->save();
    }

    /**
     * @param $jobCrawlData
     * @param $keyword
     * @param $employerId
     * @return bool
     * @throws \yii\db\Exception
     * @todo Check if the job can be inserted to x_job_crawl table
     */
    private function allowToInsert($jobCrawlData, $keyword, $employerId)
    {
        // check duplicate
        $jobExist = XJobCrawl::find()
            ->where([
                'crawl_url' => $jobCrawlData['crawl_url'],
            ])
            ->one();
        // if already exist, update date_updated in both x_job_crawl and x_employer_crawl
        if ($jobExist) {
            $jobExist->updated = date('Y-m-d H:i:s');
            $jobExist->save();
            // x_employer_crawl table also
            Yii::$app->db->createCommand()
                ->update(
                    'x_employer_crawl',
                    [
                        'updated' => date('Y-m-d H:i:s')
                    ],
                    [
                        '=', 'company_name', $jobCrawlData['crawl_company_name']
                    ]
                )->execute();
            return false;
        }
        return true;
    }

    /**
     * @param $crawlJobUrl
     * @return array
     * @todo Crawl extra job details from job url
     */
    private function crawlJob($crawlJobUrl)
    {
        // array to store job official details
        $arrJobExtraDetails = [];
        $html = file_get_contents($crawlJobUrl);
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        // get all script nodes
        $arrScripts = $finder->query("//script");
        $jobPostNodeKey = 0;
        // get key node which nodeValue contains window.__NUXT__=
        foreach ($arrScripts as $key => $value) {
            if (preg_match('/window.__NUXT__=/', $value->nodeValue)) {
                $jobPostNodeKey = $key;
            }
        }
        // get jobPosting string, remove the last ;
        $stringJobPosting = rtrim($arrScripts->item($jobPostNodeKey)->nodeValue, ';');
        // remove window.__NUXT__=
        $jsonJobPosting = preg_replace('/window.__NUXT__=/', '', $stringJobPosting);
        // decode json to array
        $arrJobsPosting = json_decode($jsonJobPosting, true);
        // get jobPosting details
        $arrJobsPostingDetail = array_column($arrJobsPosting['data'], 'jobDetail');
        // get job official details
        foreach ($arrJobsPostingDetail as $index => $jobPostingDetail) {
            // get array of job category
            $arrJobCategories = array_column($jobPostingDetail['categories'], 'name');
            $arrJobExtraDetails['crawl_job_category'] = implode(',', $arrJobCategories);
            $arrJobExtraDetails['crawl_job_position'] = $jobPostingDetail['rank'];
            $arrJobExtraDetails['crawl_job_description'] = $jobPostingDetail['description'];
            $arrJobExtraDetails['crawl_job_requirement'] = $jobPostingDetail['requirement'];
            $arrJobExtraDetails['crawl_job_other_requirement'] = $jobPostingDetail['cv_requirement'];
            $arrJobExtraDetails['crawl_job_benefit'] = $jobPostingDetail['benefit'];
            $expireDate = preg_replace('/\//', '-', $jobPostingDetail['expire_date']);
            $expireDate = date('Y-m-d', strtotime($expireDate));
            $arrJobExtraDetails['crawl_job_deadline'] = $expireDate;
            $postDate = preg_replace('/\//', '-', $jobPostingDetail['approved']['time']);
            $postDate = date('Y-m-d', strtotime($postDate));
            $arrJobExtraDetails['crawl_job_post_date'] = $postDate;
            $arrJobExtraDetails['crawl_job_crawl_date'] = date('Y-m-d H:i:s');
            $arrJobExtraDetails['crawl_job_crawl_source'] = $this->crawl_source;
            $arrJobExtraDetails['crawl_job_salary_min'] = $jobPostingDetail['salary_min'];
            $arrJobExtraDetails['crawl_job_salary_max'] = $jobPostingDetail['salary_max'];
            $arrJobExtraDetails['crawl_job_level'] = $jobPostingDetail['rank'];
            $arrJobExtraDetails['crawl_job_type'] = $jobPostingDetail['model'];
            $arrJobExtraDetails['crawl_job_degree'] = $jobPostingDetail['education'];
            $arrJobExtraDetails['crawl_job_experience'] = $jobPostingDetail['experience'];
            $arrJobExtraDetails['created'] = date('Y-m-d H:i:s');
            $arrJobExtraDetails['updated'] = date('Y-m-d H:i:s');
        }
        return $arrJobExtraDetails;
    }

    /**
     * @throws \yii\db\Exception
     * @todo Crawl all new job from a site
     */
    public function crawlAllNewJob()
    {
        for ($i = 1; $i < $this->total_new_job_page; $i++) {
            print_r($this->total_new_job_page);echo "<br>";
            print_r($i);echo "<br>";
            $this->crawlBrowseJob($i);
            var_dump($this->total_new_job_page);echo "<br>";
        }
    }
}