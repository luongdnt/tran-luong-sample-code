<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\crawl;

use Yii;
use common\models\Employer;
use common\models\XJobCrawl;
use yii\helpers\ArrayHelper;
use common\components\HopeTimeHelper;
use common\models\Job;
use common\models\Place;
use common\models\JobPlaceAssoc;
use common\components\HopeUrlHelper;

/**
 * Description of HopeGeoHelper
 *
 * @author Luongtn
 */
class HopeCrawlCareerbuilder extends HopeCrawl {
    
    public $crawlSource = "careerbuilder";
    public $crawlDomain = "https://careerbuilder.vn";
    
    /**
     * Crawl job data thành 1 mảng data
     * @param type $url
     * @return type
     */
    public function crawlJob($url) {
        
        try{            
            $html= file_get_contents($url);        
        }  catch (\Exception $e){
            return false;
        }
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        
        $mainNode = $finder->query("//div[@class='MyJobLeft']"
                . "/div[@class='LeftJobCB']")->item(0);                
        //company info
        $jobTitleNode = $finder->query("//div[@class='top-job-info']/h1", $mainNode)->item(0);
        $crawlData['job_crawl_title']= $jobTitleNode->textContent;        
        // Ngày update
        $postedNode = $finder->query("//div[@class='datepost']/span", $mainNode)->item(0);
        $crawlData['job_crawl_posted'] = date('Y-m-d', strtotime($postedNode->textContent));        
        
        // Category, deadline, salary, place, level, experiment
        $arrJobInfoNode = $finder->query("//div[@class='box2Detail']//li/p", $mainNode);
        $countJobInfoNode = ($arrJobInfoNode->length);
        
        foreach($arrJobInfoNode AS $index => $jobInfoNode){
            if(strpos($jobInfoNode->textContent, "Nơi làm việc") !== false){ // place
                $jobPlace = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;
                $crawlData['job_crawl_place'] = $jobPlace;
            }elseif(strpos($jobInfoNode->textContent, "Cấp bậc") !== false){ // level
                $jobLevel = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;                             
                $crawlData['job_crawl_level'] = $jobLevel;                                  
            }elseif(strpos($jobInfoNode->textContent, "Kinh nghiệm") !== false){ // experiment
                $jobExperiment = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;                
                $jobExperiment = preg_replace('/[^0-9-]/', '', $jobExperiment);                
                $crawlData['job_crawl_experience'] = $jobExperiment;              
            }elseif(strpos($jobInfoNode->textContent, "Lương") !== false){ // salary
                $jobSalaryRaw = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;     
                $jobSalary = str_replace("Lương:", "", $jobSalaryRaw);
                $jobSalary = preg_replace('/[^0-9-]/', '', $jobSalary);                
                $arrSalary = explode("-", $jobSalary);
                if(isset($arrSalary[1])){
                    $crawlData['min_salary'] = $arrSalary[0];
                    $crawlData['max_salary'] = $arrSalary[1];
                }elseif(isset($arrSalary[0])){
                    $crawlData['min_salary'] = $arrSalary[0];
                    $crawlData['max_salary'] = 0;
                }else{
                    $crawlData['min_salary'] = 0;
                    $crawlData['max_salary'] = 0;
                }              
            }elseif(strpos($jobInfoNode->textContent, "Ngành") !== false){ // category
                $jobCategory = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;                                 
                $crawlData['job_crawl_category'] = $jobCategory;             
            }elseif(strpos($jobInfoNode->textContent, "hạn") !== false){ // deadline
                $jobDeadline = preg_replace("/.*:/", "", $jobInfoNode->textContent) ;  
                $jobDeadline = preg_replace('/[^0-9-\/]/', '', $jobDeadline);  
                $crawlData['job_crawl_deadline'] = date('Y-m-d', strtotime(str_replace("/", "-", $jobDeadline)));                 
            }
        }
               
        // Benefit
        $arrJobBenefitNode = $finder->query("//div[@class='MarBot20'][1]/ul/li", $jobInfoNode);        
        $strJobBenefit = "";
        foreach($arrJobBenefitNode AS $jobBenefitNode){
            $strJobBenefit .="," . $jobBenefitNode->textContent;
        }
        $strJobBenefit = ltrim($strJobBenefit,",");
        $crawlData['job_crawl_benefit'] = $strJobBenefit;        
        
        // job description
        $jobDescriptionNode = $finder->query("//div[@class='MarBot20'][2]/div[@class='content_fck']", $jobInfoNode)->item(0);
        $tmp_dom = new \DOMDocument(); 
        $tmp_dom->appendChild($tmp_dom->importNode($jobDescriptionNode,true));
        $innerHTML= $tmp_dom->saveHTML();
        // thay các thẻ bằng dấu xuống dòng
        $strJobDescription = preg_replace('/<[^>]*>/', '/n', $innerHTML); 
        $crawlData['job_crawl_description'] = html_entity_decode($innerHTML);    
        
        // job requirement
        $jobRequirementNode = $finder->query("//div[@class='MarBot20'][3]/div[@class='content_fck']", $jobInfoNode)->item(0);
        $tmp_dom = new \DOMDocument(); 
        $tmp_dom->appendChild($tmp_dom->importNode($jobRequirementNode,true));
        $innerHTML= $tmp_dom->saveHTML();
        // thay các thẻ bằng dấu xuống dòng
        $strJobRequirement = preg_replace('/<[^>]*>/', '/n', $innerHTML); 
        $crawlData['job_crawl_requirement'] = html_entity_decode($innerHTML);    

        // other requirement        
        $jobOtherRequirementNode = $finder->query("//div[@class='MarBot20'][4]/div[@class='content_fck']", $jobInfoNode)->item(0);
        if(!empty($jobOtherRequirementNode)){
            $tmp_dom = new \DOMDocument(); 
            $tmp_dom->appendChild($tmp_dom->importNode($jobOtherRequirementNode,true));
            $innerHTML= $tmp_dom->saveHTML();
            // thay các thẻ bằng dấu xuống dòng
            $strJobOtherRequirement = preg_replace('/<[^>]*>/', '/n', $innerHTML); 
            $crawlData['job_crawl_other_requirement'] = html_entity_decode($innerHTML); 
        }
                         
        
        // company name
        $companyNameNode = $finder->query("//div[@class='box1Detail']/p[@class='TitleDetailNew']/span", $jobInfoNode)->item(0);
        $crawlData['company_crawl_name'] .= $companyNameNode->textContent;
        
        // company logo
        $companyLogoNode = $finder->query("//div[@class='box1Detail']/div[@class='align_center logocompany']"
                . "/table/*/td/a/img", $jobInfoNode)->item(0);
        if(!empty($companyLogoNode)){
            $crawlData['company_crawl_logo'] .= $companyLogoNode->getAttribute('src');         
        }
        
        // company address
        $companyAddressNode = $finder->query("//div[@class='box1Detail']/p[@class='TitleDetailNew']"
                . "/label", $jobInfoNode)->item(0);
        $crawlData['company_crawl_address'] .= $companyAddressNode->textContent;                              
        
        array_walk_recursive($crawlData, function(&$value) {
            $value = trim($value);
        });
        
        return $crawlData;
    }
    
    public function getJobCrawlCategory($jobCrawlModel) {        
        return[];
    }
    
    // <editor-fold defaultstate="collapsed" desc="Xử lý data trả về">
    /**
     * Lấy job degree
     * @param type $strDegree
     * @return type
     */
    public function getJobCrawlDegree($strDegree) {
        if($strDegree == "Trung cấp"){
            return ['degree_name' => "Trung cấp - Nghề", "degree_id" => 1];
        }elseif($strDegree == "Cao đẳng"){
            return ['degree_name' => "Cao Đẳng", "degree_id" => 2];
        }elseif($strDegree == "Đại học" || $strDegree == "Kỹ sư" || $strDegree == "Cử nhân"){
            return ['degree_name' => "Đại Học", "degree_id" => 3];
        }elseif($strDegree == "Trên đại học" || $strDegree == "Thạc sĩ" || $strDegree == "Thạc sĩ Y học"){
            return ['degree_name' => "Thạc sỹ", "degree_id" => 4];
        }else{
            return ['degree_name' => "Cao Đẳng", "degree_id" => 2];
        }
    }
    
    /**
     * Lấy job place
     * @param type $strPlace
     * @return type
     */
    public function getJobCrawlPlace($strPlace, $employerId) {
        
        // Lấy thông tin employer
//        $employerModel = \Yii::$app->db->createCommand(
//                    "SELECT ep.* "
//                . " FROM employer AS e, employer_product AS ep"
//                . " WHERE e.transfer_ref_id = ep.transfer_ref_id"
//                . " AND e.employer_id = $employerId"
//                )->queryOne();
        
        $arrPlace = explode(",", $strPlace);
        $arrPlaceResult = Place::find()
                ->select('place_name, place_id')
                ->where(['place_name' => $arrPlace])
                ->asArray()
                ->all();
        
        return $arrPlaceResult;
        
        $withEmployerAddress = false;
        // Check xem các place có nằm trong address của employer thì đưa vào
        foreach($arrPlace AS $index => $place){
            $lowerPlaceName = strtolower($place['place_name']);
            $lowerAddress = strtolower($employerModel['address']);
            if(strstr($lowerAddress, $lowerPlaceName) !== FALSE && !empty($employerModel['geo_latitude'])){
                $arrPlace[$index]['place_name'] = $employerModel['address'];
                $arrPlace[$index]['geo_latitude'] = $employerModel['geo_latitude'];
                $arrPlace[$index]['geo_longitude'] = $employerModel['geo_longitude'];
                $withEmployerAddress = true;
            }
        }            
        
        return $arrPlaceResult;
    }

    /**
     * Lấy job position
     * @param type $strPosition
     * @return type
     */
    public function getJobCrawlPosition($strPosition) {
        
        if(strstr($strPosition, "Mới tốt nghiệp") || strstr($strPosition, "Thực tập sinh") 
                || strstr($strPosition, "Hợp đồng") || strstr($strPosition, "Sinh viên"))
        {
            return ['job_position_name' => "Mới tốt nghiệp/Thực tập sinh", "job_position_id" => 1];
        }
        elseif(strstr($strPosition, "Nhân viên") || strstr($strPosition, "Kỹ thuật viên/Kỹ sư") 
                || strstr($strPosition, "Mới đi làm"))
        {
            return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
        }
        elseif(strstr($strPosition, "Trưởng") || strstr($strPosition, "trưởng") || strstr($strPosition, "Quản") || strstr($strPosition, "quản"))
        {
            return ['job_position_name' => "Trưởng nhóm/Trưởng phòng", "job_position_id" => 3];
        }
        elseif(strstr($strPosition, "Giám") || strstr($strPosition, "giám") || strstr($strPosition, "cấp cao"))
        {
            return ['job_position_name' => "Giám đốc và cấp cao hơn", "job_position_id" => 4];
        }
        else
        {
            return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
        }
    }

    /**
     * Lấy job salary
     * @param type $strSalary
     */
    public function getJobCrawlSalary($strSalary) {
        $strSalary =preg_replace('/[^\d-\/]/', '', $strSalary);
        $arrSalaryPiece = explode("-", $strSalary);
        $data['max_salary'] = 0;
        $data['min_salary'] = 0;        
        if(count($arrSalaryPiece) == 2){
            $data['min_salary'] = preg_replace('/[^\d]/', '', $arrSalaryPiece[0]);
            if($data['min_salary'] > 200){
                $data['min_salary']  = $data['min_salary'] * 23000;
            }else{
                $data['min_salary']  = $data['min_salary'] * 1000000;
            }
            $data['max_salary'] = preg_replace('/[^\d]/','', $arrSalaryPiece[1]);
            if($data['max_salary'] > 200){
                $data['max_salary']  = $data['max_salary'] * 23000;
            }else{
                $data['max_salary']  = $data['max_salary'] * 1000000;
            }
        }
        return $data;
    }

    /**
     * Lấy job type
     * @param type $strType
     * @return type
     */
    public function getJobCrawlType($strType) {
        if(strstr($strType, "Toàn thời gian") || strstr($strType, "hợp đồng") || strstr($strType, "Hợp đồng")){
            return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
        }elseif(strstr($strType, "Bán thời gian")){
            return ['job_type_name' => "Bán thời gian", "job_type_id" => 2];
        }elseif(strstr($strType, "Thực tập")){
            return ['job_type_name' => "Thực tập", "job_type_id" => 5];
        }else{
            return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
        }
    }
    
    /**
     * Lấy job experiment
     * @param type $strYearExp
     * @return type
     */
    public function getJobCrawlYearExp($strYearExp) {
        $strYearExp = preg_replace("/[^0-9-]/", "", $strYearExp);
        $arrYearExp = explode("-", $strYearExp);
        $result = ["minYearExp" => 0, "maxYearExp" => 0];
        if(!empty($arrYearExp[0])){
            $result['minYearExp'] = $arrYearExp[0];
        }
        if(!empty($arrYearExp[1])){
            $result['maxYearExp'] = $arrYearExp[1];
        }
        return $result;
    }
    
    /**
     * Lấy job benefit
     * @param type $arrBenefit
     * @return type
     */
    public static function getJobBenefit($arrBenefit){
        $strBenefit = '';
        foreach($arrBenefit AS $benefit){
            $strBenefit .= "\r\n<p>$benefit</p>";
        }
        return $strBenefit;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Insert">
    /**
     * Insert job place
     * 
     * @param type $strPlace
     * @param type $job_id
     * @param type $employer_id
     * @param type $crawlSource
     */
    public function insertJobPlace($strPlace, $job_id, $employer_id, $crawlSource) {
        
    }
    
    /**
     * Insert vào bảng job_crawl để duyệt
     * 
     * @param type $jobCrawlData
     * @param type $keyword
     * @param type $employerId
     */
    public function insertToJobCrawlTable($jobCrawlData, $keyword, $employerId=NULL){        
        // Nếu không cho phép insert thì trả về luôn
        if(!$this->allowToInsert($jobCrawlData, $keyword, $employerId)){            
            return false;
        }
        $crawlJobUrl = $jobCrawlData['job_url'];        
        // Phải crawl job về để lấy thêm data        
        $jobCrawlData = $this->crawlJob($crawlJobUrl);                        
        
        // Nếu không crawl được 
        if(!$jobCrawlData){
            return false;
        }
        
        $jobCrawlModel = new XJobCrawl();
        if($employerId){
            $jobCrawlModel->employer_id = $employerId;
        }
        $jobCrawlModel->keyword = $keyword;
        $jobCrawlModel->job_title = $jobCrawlData['job_crawl_title'];
        $jobCrawlModel->job_category = $jobCrawlData['job_crawl_category'];
        $jobCrawlModel->job_skill = $jobCrawlData['job_crawl_category'];
        $jobCrawlModel->job_place = $jobCrawlData['job_crawl_place'];
        $jobCrawlModel->job_position = $jobCrawlData['job_crawl_level'];
        $jobCrawlModel->job_year_experience = $jobCrawlData['job_crawl_experience'];        
        $jobCrawlModel->company_name = $jobCrawlData['company_crawl_name'];
        $jobCrawlModel->company_logo = $jobCrawlData['company_crawl_logo'];
        $jobCrawlModel->deadline = $jobCrawlData['job_crawl_deadline'];
        $jobCrawlModel->posted_time = $jobCrawlData['job_crawl_posted'];
        $jobCrawlModel->job_description = $jobCrawlData['job_crawl_description'];
        $jobCrawlModel->job_benefit = $jobCrawlData['job_crawl_benefit'];
        $jobCrawlModel->job_requirement = $jobCrawlData['job_crawl_requirement'];
        $jobCrawlModel->job_other_requirement = $jobCrawlData['job_crawl_other_requirement'];
        $jobCrawlModel->created = date('Y-m-d H:i:s');
        $jobCrawlModel->updated = date('Y-m-d H:i:s');
        $jobCrawlModel->max_salary = $jobCrawlData['max_salary'] < 100000 ? (floor($jobCrawlData['max_salary'] * 22700/1000000)) * 1000000 : $jobCrawlData['max_salary'];
        $jobCrawlModel->min_salary = $jobCrawlData['min_salary'] < 100000 ? (floor($jobCrawlData['min_salary'] * 22700/1000000)) * 1000000 : $jobCrawlData['min_salary'];
        $jobCrawlModel->crawl_source = $this->crawlSource;
        $jobCrawlModel->crawl_job_url = $crawlJobUrl;
        $jobCrawlModel->crawl_url = $crawlJobUrl;
        $jobCrawlModel->job_level = $jobCrawlData['job_crawl_level'];
                
        return $jobCrawlModel->save();
        
    }
    
    /**
     * Insert vào bảng job
     * @param type $jobCrawlModel
     * @return Job
     */
    public function insertToJobTable($jobCrawlModel) {
        // Lấy các thông tin job, job_place, salary, degree, position, exp, category
        $placeValue = $this->getJobCrawlPlace($jobCrawlModel->job_place, $jobCrawlModel->employer_id);
        $degreeValue = $this->getJobCrawlDegree($jobCrawlModel->job_degree);
        $jobTypeValue = $this->getJobCrawlType($jobCrawlModel->job_type);
        $positionValue = $this->getJobCrawlPosition($jobCrawlModel->job_position);
        $experimentValue = $this->getJobCrawlYearExp($jobCrawlModel->job_year_experience);
        $categoryValue = $this->getJobCrawlCategory($jobCrawlModel);    

        // Insert vào bảng job
        try{   
            $jobExist = $this->allowToInsertJobTable($jobCrawlModel);
            
            if(!$jobExist){
                $job = new Job();
                $job->addError('error', "Job tồn tại");                     
                return $job;
            }

            $job = new Job();
            $job->employer_id = $jobCrawlModel->employer_id;
            $job->job_title = $jobCrawlModel->job_title;            
            $job->crawl_detail_id = $jobCrawlModel->id;
            $job->crawl_source = $jobCrawlModel->crawl_source;
            $job->degree = $degreeValue['degree_name'];
            $job->degree_id = $degreeValue['degree_id'];
            $job->min_expect_salary = $jobCrawlModel->min_salary > 1000 ? floor($jobCrawlModel->min_salary / 1000000) : $jobCrawlModel->min_salary;
            $job->max_expect_salary = $jobCrawlModel->max_salary > 1000 ? floor($jobCrawlModel->max_salary / 1000000) : $jobCrawlModel->max_salary;
            $job->exp_min_require_year = $experimentValue['minYearExp'];
            $job->exp_max_require_year = $experimentValue['maxYearExp'];
            $job->job_type = $jobTypeValue['job_type_name'];
            $job->job_type_id = $jobTypeValue['job_type_id'];
            $job->job_position = $positionValue['job_position_name'];
            $job->job_position_id = $positionValue['job_position_id'];
            $job->job_category = implode(",", $categoryValue);            
            $job->job_description = ($jobCrawlModel->job_description);
            $job->job_requirement = ($jobCrawlModel->job_requirement);
            $job->job_benefit = ($jobCrawlModel->job_benefit);
            $job->job_other_requirement = strip_tags($jobCrawlModel->job_other_requirement);
            $job->deadline = $jobCrawlModel->deadline ? $jobCrawlModel->deadline : HopeTimeHelper::getDayPlus(30);
            $job->created = HopeTimeHelper::getNow();
            $job->updated = HopeTimeHelper::getNow();
            $job->status = 0;
            $job->admin_id = Yii::$app->user->identity->user_id;
            $job->save();
            
            if($job->errors){                
                return $job;
            }
            $employerModel = Employer::find()
                    ->where(['employer_id' => $jobCrawlModel->employer_id])
                    ->one();
            // Insert vào bảng place assoc
            foreach($placeValue AS $place){
                $placeAssocModel = new JobPlaceAssoc();
                $placeAssocModel->job_id = $job->job_id;
                $placeAssocModel->place_id = $place['place_id'];
                $placeAssocModel->detail_address = $place['place_name'];
                $placeAssocModel->geo_latitude = $place['geo_latitude'];
                $placeAssocModel->geo_longitude = $place['geo_longitude'];
                $placeAssocModel->province_name = $place['place_name'];
                $placeAssocModel->district_name = '';
                $placeAssocModel->address_google = $place['place_name'];
                $placeAssocModel->save();
                if($placeAssocModel->errors){
                    return $job;
                }
            }
            // Insert vào bảng category_assoc
            foreach($categoryValue AS $categoryId){
                $categoryAssocModel = new JobCategoryAssoc();
                $categoryAssocModel->job_id = $job->job_id;
                $categoryAssocModel->job_category_id = $categoryId;
                $categoryAssocModel->save();
                if($categoryAssocModel->errors){
                    return $job;
                }
            }

            return $job;
        } catch (\Exception $ex) {
            print_r($ex->getMessage());exit;
            return $job;            
        }
    }
    // </editor-fold>
                                
    /**
     * Tạo link search từ domain tương ứng
     * VD: https://careerbuilder.vn/viec-lam/cong-ty-fpt-k-sortdv-r50-vi.html
     * 
     * @param type $keyword
     */
    public function createSearchUrl($keyword){        
        // Encode keywork 
        $keywordSlug = HopeUrlHelper::slug($keyword);
        // Domain
        $domainSearch = $this->crawlDomain;
        // Link search theo format của từng trang
        // VD: https://careerbuilder.vn/viec-lam/cong-ty-fpt-k-sortdv-r50-vi.html
        $searchUrl = $domainSearch . "/viec-lam/{$keywordSlug}-k-sortdv-r50-vi.html";

        return $searchUrl;
    }
    
    /**
     * Lấy html từ url
     * @param type $url
     * @return type
     */
    public function getHtmlFromUrl($url){
        // Lấy kết quả trả về
        // create curl resource 
        $ch = curl_init(); 
        // set url 
        curl_setopt($ch, CURLOPT_URL, $url); 
        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                
        // $output contains the output string 
        $html = curl_exec($ch); 
        // close curl resource to free up system resources 
        curl_close($ch);
        
        return $html;
    }
    
    /**
     * Xử lý html lấy về thành mảng data
     * [job_title, job_url,employer_name, place, deadline, salary, posted_date]
     * 
     * @param type $html
     */
    public function getSearchJobResult($html){                                
        // Dom object
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        
        // Các job trả về
        $arrJobItem = $finder->query("//div[@class='gird_standard ']"
                . "//dd[@class='brief ']");                
        $crawlData =[];
        // Lấy thông tin của từng job
        foreach ($arrJobItem as $index => $jobNode){
            
            $jobUrlNode = $finder->query("span[@class='jobtitle']/h3[@class='job']/a", $jobNode)->item(0);            
            $jobUrl= $jobUrlNode->getAttribute('href');            
            $jobData['job_title'] = $jobUrlNode->textContent;
            $jobData['job_url'] = $jobUrl;                                                
            
            // Company
            $companyNode = $finder->query("span[@class='jobtitle']/p[@class='namecom']/a", $jobNode)->item(0);
            $companyName = $companyNode->textContent;
            $jobData['employer_name'] = $companyName;           
            $jobData['employer_url'] = $companyNode->getAttribute('href');
            
            // logo
            $logoNode = $finder->query("span[@class='logoJobs']/table/tr/td/a/img", $jobNode)->item(0);                        
            $jobData['logo_url'] = $logoNode->getAttribute('data-original');    
            
            // Place
            $placeNode = $finder->query("span[@class='jobtitle']/p[@class='location']", $jobNode)->item(0);      
            $jobData['place'] = $placeNode->textContent; 
                           
            // Salary
            $salaryNode = $finder->query("span[@class='jobtitle']/p[@class='salary lred']", $jobNode)->item(0);      
            $jobData['salary'] = str_replace("Lương: ", "", $salaryNode->textContent); 
            
            // Ngày update
            $postedNode = $finder->query("div[@class='dateposted']", $jobNode)->item(0);      
            $strPostedDate = str_replace("Ngày cập nhật: ", "", $postedNode->textContent); 
            $jobData['job_crawl_posted'] = date('Y-m-d', strtotime($strPostedDate));            
            
            $crawlData[] = $jobData;
        }
        return $crawlData;
    }
    
    /**
     * Search trên trang CarrerBuilder : https://careerbuilder.vn/viec-lam/cong-ty-fpt-k-vi.html
     * Từ kết quả trả về insert vào bảng x_job_crawl
     * @param type $keyword
     */
    public function searchJob($keyword, $employerId = NULL) {
        // Url search từ careerbuilder
        $searchUrl = $this->createSearchUrl($keyword);            
        // Lấy html
        $html = $this->getHtmlFromUrl($searchUrl);                
        // Lấy mảng job trả về từ html
        $arrJobData = $this->getSearchJobResult($html);          
        // Insert vào bảng x_job_crawl
        foreach($arrJobData AS $jobData){
            $this->insertToJobCrawlTable($jobData, $keyword, $employerId);
        }        
        return $arrJobData;
    }
    
    //==================================BROWSE JOB============================//
    /**
     * Search trên trang CB : https://careerbuilder.vn/viec-lam/tat-ca-viec-lam-trang-2-vi.html
     * Từ kết quả trả về insert vào bảng x_job_crawl
     * @param type $keyword
     */
    public function crawlBrowseJob($cursor=NULL) {
        // Url browse từ VNW
        $browseUrl = "https://careerbuilder.vn/viec-lam/tat-ca-viec-lam-trang-xxx-vi.html";
        
        if($cursor){
            $browseUrl = str_replace("xxx", $cursor, $browseUrl);
        }else{
            $browseUrl = str_replace("xxx", 1, $browseUrl);
        }
        
        // Lấy kết quả trả về
        // create curl resource 
        $ch = curl_init();
        // set url 
        curl_setopt($ch, CURLOPT_URL, $browseUrl); 
        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        // $output contains the output string 
        $html = curl_exec($ch); 
        // close curl resource to free up system resources 
        curl_close($ch);              
        // Lấy mảng job trả về
        $arrJobData = $this->extractAllJobFromHtml($html);
        
        // Insert vào bảng x_job_crawl
        foreach($arrJobData AS $jobData){
            $this->insertToJobCrawlTableDirect($jobData, 'browse_job', 0);
        }
        
        return ['data' => $arrJobData];
    }    
    
    /**
     * Xử lý html lấy về thành mảng data
     * [job_title, job_url,employer_name, place, deadline, salary, posted_date]
     * 
     * @param type $html
     */
    public function extractAllJobFromHtml($html){        
        
        // Dom object
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        
        // Các job trả về
        $arrJobItem = $finder->query("//div[@class='gird_standard ']/dl/dd");        
        
        $crawlData =[];
        // Lấy thông tin của từng job
        foreach ($arrJobItem as $index => $jobNode){
            
            // job_logo
            $arrJobLogoNode = $finder->query("span[@class='logoJobs']//img[@class='lazy-new']", $jobNode);
            
            foreach($arrJobLogoNode AS $index => $jobLogoNode){
                $jobLogoUrl= $jobLogoNode->getAttribute('data-original');            
                $jobData['company_logo'] = $jobLogoUrl;
            }
            
            // job_url
            $arrJobUrlNode = $finder->query("span[@class='jobtitle']/h3[@class='job']/a", $jobNode);
            
            foreach($arrJobUrlNode AS $index => $jobUrlNode){
                $jobUrl= $jobUrlNode->getAttribute('href');            
                $tmp_dom = new \DOMDocument(); 
                $tmp_dom->appendChild($tmp_dom->importNode($jobUrlNode,true));
                $jobTitle = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $jobData['job_title'] = $jobTitle;
                $jobData['job_url'] = $jobUrl;
            }     
            
            // Company
            $companyNode = $finder->query("span[@class='jobtitle']/p[@class='namecom']/a", $jobNode);       
            foreach($companyNode AS $index => $company){
                $tmp_dom = new \DOMDocument(); 
                $tmp_dom->appendChild($tmp_dom->importNode($company,true));
                $companyName = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $jobData['company_name'] = $companyName;
            }            
            
            // Update date
            $arrPostedDateNode = $finder->query("div[@class='dateposted']", $jobNode);       
            
            $tmp_dom = new \DOMDocument(); 
            foreach($arrPostedDateNode AS $index => $postedDateNode){
                $tmp_dom->appendChild($tmp_dom->importNode($postedDateNode,true));
                $postedDate = html_entity_decode(strip_tags($tmp_dom->saveHTML()));                 
                $postedDate = preg_replace('/[^\d-\/]/', '', $postedDate);                
                $postedDate = str_replace("/", "-", $postedDate);
                $date = \DateTime::createFromFormat('d-m-Y', $postedDate);
                $postedDate = $date->format('Y-m-d');
                $jobData['job_posted'] = $postedDate;
            }
            
            // Place
            $placeNode = $finder->query("span[@class='jobtitle']/p[@class='location']", $jobNode);
            foreach($placeNode AS $index => $placeNd){
                $tmp_dom = new \DOMDocument();
                $tmp_dom->appendChild($tmp_dom->importNode($placeNd,true));
                $placeName = html_entity_decode(strip_tags($tmp_dom->saveHTML()));             
                $jobData['job_place'] = $placeName; 
            }
            
            // Lương
            $arrSalaryNode = $finder->query("span[@class='jobtitle']/p", $jobNode);
            foreach($arrSalaryNode AS $index => $salaryNode){
                $tmp_dom = new \DOMDocument();
                $tmp_dom->appendChild($tmp_dom->importNode($salaryNode,true));
                $salaryDesc = html_entity_decode(strip_tags($tmp_dom->saveHTML()));             
                $arrSalary = $this->getJobCrawlSalary($salaryDesc);
                $jobData['min_salary'] = $arrSalary['min_salary']; 
                $jobData['max_salary'] = $arrSalary['max_salary']; 
            }                        
                        
            // Nếu deadline < tomorrow thì ko insert
            $crawlData[] = $jobData;
            if(strtotime($jobData['deadline']) - time() > 86400){
                
            }
                                    
        }
        
        return $crawlData;
    }    
    
    /**
     * Insert vào bảng job_crawl để duyệt
     * 
     * @param type $jobCrawlData
     * @param type $keyword
     * @param type $employerId
     */
    public function insertToJobCrawlTableDirect($jobCrawlData, $keyword, $employerId=NULL){
        
        // Nếu không cho phép insert thì trả về luôn
        if(!$this->allowToInsert($jobCrawlData, $keyword, $employerId)){
            return false;
        }
        $crawlJobUrl = $jobCrawlData['job_url'];                     
        
        // Nếu không crawl được 
        if(!$jobCrawlData){
            return false;
        }
        
        $jobCrawlModel = new XJobCrawl();
        if($employerId){
            $jobCrawlModel->employer_id = $employerId;
        }
        $jobCrawlModel->keyword = $keyword;
        $jobCrawlModel->job_title = $jobCrawlData['job_title'];
        $jobCrawlModel->job_category = $jobCrawlData['job_skills'];
        $jobCrawlModel->job_skill = $jobCrawlData['job_skills'];
        $jobCrawlModel->job_place = $jobCrawlData['job_place'];
        $jobCrawlModel->job_position = $jobCrawlData['job_position'];
        $jobCrawlModel->job_year_experience = $jobCrawlData['job_experience'];        
        $jobCrawlModel->company_name = $jobCrawlData['company_name'];
        $jobCrawlModel->company_logo = $jobCrawlData['company_logo'];
        $jobCrawlModel->deadline = $jobCrawlData['job_deadline'];
        $jobCrawlModel->posted_time = $jobCrawlData['job_posted'];
        $jobCrawlModel->job_description = $jobCrawlData['job_description'];
        $jobCrawlModel->job_benefit = $jobCrawlData['job_benefit'];
        $jobCrawlModel->job_requirement = $jobCrawlData['job_requirement'];
        $jobCrawlModel->created = date('Y-m-d H:i:s');
        $jobCrawlModel->updated = date('Y-m-d H:i:s');
        $jobCrawlModel->max_salary = $jobCrawlData['max_salary'] < 100000 ? (floor($jobCrawlData['max_salary'] * 22700/1000000)) * 1000000 : $jobCrawlData['max_salary'];
        $jobCrawlModel->min_salary = $jobCrawlData['min_salary'] < 100000 ? (floor($jobCrawlData['min_salary'] * 22700/1000000)) * 1000000 : $jobCrawlData['min_salary'];
        $jobCrawlModel->crawl_source = $this->crawlSource;
        $jobCrawlModel->crawl_job_url = $crawlJobUrl;
        $jobCrawlModel->crawl_url = $crawlJobUrl;
        $jobCrawlModel->job_level = $jobCrawlData['job_position'];
                
        return $jobCrawlModel->save();
        
    }
    
    //==============================END BROWSE JOB============================//

}
