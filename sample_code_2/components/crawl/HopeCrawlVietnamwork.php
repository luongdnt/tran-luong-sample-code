<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\crawl;

use common\models\JobCategoryAssoc;
use Yii;
use common\models\Employer;
use common\models\XJobCrawl;
use yii\helpers\ArrayHelper;
use common\components\HopeTimeHelper;
use common\models\Job;
use common\models\Place;
use common\models\JobPlaceAssoc;

/**
 * Description of HopeGeoHelper
 *
 * @author Luongtn
 */
class HopeCrawlVietnamwork extends HopeCrawl
{

    public $crawlSource = "vietnamwork";
    public $crawlDomain = "https://vietnamworks.com";

    public function crawlJob($url)
    {

    }

    public function getJobCrawlCategory($jobCrawlModel)
    {
        return [];
    }

    public function getJobCrawlDegree($strDegree)
    {
        if ($strDegree == "Trung cấp") {
            return ['degree_name' => "Trung cấp - Nghề", "degree_id" => 1];
        } elseif ($strDegree == "Cao đẳng") {
            return ['degree_name' => "Cao Đẳng", "degree_id" => 2];
        } elseif ($strDegree == "Đại học" || $strDegree == "Kỹ sư" || $strDegree == "Cử nhân") {
            return ['degree_name' => "Đại Học", "degree_id" => 3];
        } elseif ($strDegree == "Trên đại học" || $strDegree == "Thạc sĩ" || $strDegree == "Thạc sĩ Y học") {
            return ['degree_name' => "Thạc sỹ", "degree_id" => 4];
        } else {
            return ['degree_name' => "Cao Đẳng", "degree_id" => 2];
        }
    }

    public function getJobCrawlPlace($strPlace, $employerId)
    {

        // Lấy thông tin employer
//        $employerModel = \Yii::$app->db->createCommand(
//                    "SELECT ep.* "
//                . " FROM employer AS e, employer_product AS ep"
//                . " WHERE e.transfer_ref_id = ep.transfer_ref_id"
//                . " AND e.employer_id = $employerId"
//                )->queryOne();

        $arrPlace = explode(",", $strPlace);
        $arrPlaceResult = Place::find()
            ->select('place_name, place_id')
            ->where(['place_name' => $arrPlace])
            ->asArray()
            ->all();

        return $arrPlaceResult;

        $withEmployerAddress = false;
        // Check xem các place có nằm trong address của employer thì đưa vào
        foreach ($arrPlace as $index => $place) {
            $lowerPlaceName = strtolower($place['place_name']);
            $lowerAddress = strtolower($employerModel['address']);
            if (strstr($lowerAddress, $lowerPlaceName) !== FALSE && !empty($employerModel['geo_latitude'])) {
                $arrPlace[$index]['place_name'] = $employerModel['address'];
                $arrPlace[$index]['geo_latitude'] = $employerModel['geo_latitude'];
                $arrPlace[$index]['geo_longitude'] = $employerModel['geo_longitude'];
                $withEmployerAddress = true;
            }
        }

        return $arrPlaceResult;
    }

    public function getJobCrawlPosition($strPosition)
    {

        if (strstr($strPosition, "Mới tốt nghiệp") || strstr($strPosition, "Thực tập sinh")
            || strstr($strPosition, "Hợp đồng")) {
            return ['job_position_name' => "Mới tốt nghiệp/Thực tập sinh", "job_position_id" => 1];
        } elseif (strstr($strPosition, "Nhân viên") || strstr($strPosition, "Kỹ thuật viên/Kỹ sư")
            || strstr($strPosition, "Mới đi làm")) {
            return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
        } elseif (strstr($strPosition, "Trưởng") || strstr($strPosition, "trưởng")) {
            return ['job_position_name' => "Trưởng nhóm/Trưởng phòng", "job_position_id" => 3];
        } elseif (strstr($strPosition, "Giám") || strstr($strPosition, "giám") || strstr($strPosition, "cấp cao")) {
            return ['job_position_name' => "Giám đốc và cấp cao hơn", "job_position_id" => 4];
        } else {
            return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
        }
    }

    public function getJobCrawlSalary($strSalary)
    {

    }

    public function getJobCrawlType($strType)
    {
        if (strstr($strType, "Toàn thời gian") || strstr($strType, "hợp đồng") || strstr($strType, "Hợp đồng")) {
            return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
        } elseif (strstr($strType, "Bán thời gian")) {
            return ['job_type_name' => "Bán thời gian", "job_type_id" => 2];
        } elseif (strstr($strType, "Thực tập")) {
            return ['job_type_name' => "Thực tập", "job_type_id" => 5];
        } else {
            return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
        }
    }

    public function getJobCrawlYearExp($strYearExp)
    {
        return ["minYearExp" => 0, "maxYearExp" => 0];
    }

    public function insertJobPlace($strPlace, $job_id, $employer_id, $crawlSource)
    {

    }

    // Insert vào bảng job
    public function insertToJobTable($jobCrawlModel)
    {

        // Lấy các thông tin job, job_place, salary, degree, position, exp, category
        $placeValue = $this->getJobCrawlPlace($jobCrawlModel->job_place, $jobCrawlModel->employer_id);
        $degreeValue = $this->getJobCrawlDegree($jobCrawlModel->job_degree);
        $jobTypeValue = $this->getJobCrawlType($jobCrawlModel->job_type);
        $positionValue = $this->getJobCrawlPosition($jobCrawlModel->job_position);
        $experimentValue = $this->getJobCrawlYearExp(NULL);
        $categoryValue = $this->getJobCrawlCategory($jobCrawlModel);

        // Insert vào bảng job
        try {
            $jobExist = $this->allowToInsertJobTable($jobCrawlModel);

            if (!$jobExist) {
                $job = new Job();
                $job->addError('error', "Job tồn tại");
                return $job;
            }

            $job = new Job();
            $job->employer_id = $jobCrawlModel->employer_id;
            $job->job_title = $jobCrawlModel->job_title;
            $job->crawl_detail_id = $jobCrawlModel->id;
            $job->crawl_source = $jobCrawlModel->crawl_source;
            $job->degree = $degreeValue['degree_name'];
            $job->degree_id = $degreeValue['degree_id'];
            $job->min_expect_salary = $jobCrawlModel->min_salary > 1000 ? floor($jobCrawlModel->min_salary / 1000000) : $jobCrawlModel->min_salary;
            $job->max_expect_salary = $jobCrawlModel->max_salary > 1000 ? floor($jobCrawlModel->max_salary / 1000000) : $jobCrawlModel->max_salary;
            $job->exp_min_require_year = $experimentValue['minYearExp'];
            $job->exp_max_require_year = $experimentValue['maxYearExp'];
            $job->job_type = $jobTypeValue['job_type_name'];
            $job->job_type_id = $jobTypeValue['job_type_id'];
            $job->job_position = $positionValue['job_position_name'];
            $job->job_position_id = $positionValue['job_position_id'];
            $job->job_category = implode(",", $categoryValue);
            $job->job_description = $jobCrawlModel->job_description;
            $job->job_requirement = $jobCrawlModel->job_requirement;
            $job->job_benefit = $jobCrawlModel->job_benefit;
            $job->deadline = $jobCrawlModel->deadline ? $jobCrawlModel->deadline : HopeTimeHelper::getDayPlus(30);
            $job->created = HopeTimeHelper::getNow();
            $job->updated = HopeTimeHelper::getNow();
            $job->status = 0;
            $job->admin_id = Yii::$app->user->identity->user_id;
            $job->save();

            if ($job->errors) {
                return $job;
            }
            $employerModel = Employer::find()
                ->where(['employer_id' => $jobCrawlModel->employer_id])
                ->one();
            // Insert vào bảng place assoc
            foreach ($placeValue as $place) {
                $placeAssocModel = new JobPlaceAssoc();
                $placeAssocModel->job_id = $job->job_id;
                $placeAssocModel->place_id = $place['place_id'];
                $placeAssocModel->detail_address = $place['place_name'];
                $placeAssocModel->geo_latitude = $place['geo_latitude'];
                $placeAssocModel->geo_longitude = $place['geo_longitude'];
                $placeAssocModel->province_name = $place['place_name'];
                $placeAssocModel->district_name = '';
                $placeAssocModel->address_google = $place['place_name'];
                $placeAssocModel->save();
                if ($placeAssocModel->errors) {
                    return $job;
                }
            }
            // Insert vào bảng category_assoc
            foreach ($categoryValue as $categoryId) {
                $categoryAssocModel = new JobCategoryAssoc();
                $categoryAssocModel->job_id = $job->job_id;
                $categoryAssocModel->job_category_id = $categoryId;
                $categoryAssocModel->save();
                if ($categoryAssocModel->errors) {
                    return $job;
                }
            }

            return $job;
        } catch (\Exception $ex) {
            print_r($ex->getMessage());
            exit;
            return $job;
        }
    }

    public static function getJobBenefit($arrBenefit)
    {
        $strBenefit = '';
        foreach ($arrBenefit as $benefit) {
            $strBenefit .= "\r\n<p>$benefit</p>";
        }
        return $strBenefit;
    }

    /**
     * Insert vào bảng job_crawl để duyệt
     *
     * @param type $jobCrawlData
     * @param type $keyword
     * @param type $employerId
     */
    public function insertToJobCrawlTable($jobCrawlData, $keyword, $employerId = NULL)
    {
        $jobCrawlModel = new XJobCrawl();
        if ($employerId) {
            $jobCrawlModel->employer_id = $employerId;
        }
        $jobCrawlModel->keyword = $keyword;
        $jobCrawlModel->job_title = $jobCrawlData->jobTitle;
        $jobCrawlModel->job_category = implode(",", $jobCrawlData->categories);
        $jobCrawlModel->job_skill = implode(",", $jobCrawlData->skills);
        $jobCrawlModel->job_place = implode(",", $jobCrawlData->locationVIs);
        $jobCrawlModel->job_position = $jobCrawlData->jobLevelVI;
        $jobCrawlModel->company_name = $jobCrawlData->company;
        $jobCrawlModel->company_logo = $jobCrawlData->companyLogo;
        $jobCrawlModel->deadline = date('Y-m-d', $jobCrawlData->expiredDate);
        $jobCrawlModel->posted_time = date('Y-m-d', $jobCrawlData->onlineDate);
        $jobCrawlModel->job_description = $jobCrawlData->jobDescription;
        $jobCrawlModel->job_benefit = self::getJobBenefit(ArrayHelper::getColumn($jobCrawlData->benefits, 'benefitValue'));
        $jobCrawlModel->job_requirement = $jobCrawlData->jobRequirement;
        $jobCrawlModel->created = date('Y-m-d H:i:s');
        $jobCrawlModel->updated = date('Y-m-d H:i:s');
        $jobCrawlModel->max_salary = $jobCrawlData->salaryMax < 100000 ? (floor($jobCrawlData->salaryMax * 22700 / 1000000)) * 1000000 : $jobCrawlData->salaryMax;
        $jobCrawlModel->min_salary = $jobCrawlData->salaryMin ? (floor($jobCrawlData->salaryMin * 22700 / 1000000)) * 1000000 : $jobCrawlData->salaryMin;
        $jobCrawlModel->crawl_source = $this->crawlSource;
        $jobCrawlModel->crawl_job_url = $this->crawlDomain . "/" . $jobCrawlData->alias . "-" . $jobCrawlData->jobId . "-jv";
        $jobCrawlModel->crawl_url = $jobCrawlModel->crawl_job_url;
        $jobCrawlModel->job_level = $jobCrawlData->jobLevel;
        // Check nếu cty crawl không khớp với cty jobsgo (đã từng bị loại) thì cũng ko insert
        $notMatchEmployer = Yii::$app->db->createCommand(
            "SELECT count(*) FROM x_job_crawl"
            . " WHERE employer_id = :employer_id"
            . " AND company_name = :employer_name"
            . " AND crawl_status = -1"
        )
            ->bindValue(':employer_id', $jobCrawlModel->employer_id)
            ->bindValue(':employer_name', $jobCrawlModel->company_name)
            ->queryScalar();

        if ($notMatchEmployer) {
            return false;
        }

        // Check $jobCrawlModel->crawl_job_url chưa tồn tại thì mới insert
        $jobExist = XJobCrawl::find()
            ->where(['job_title' => $jobCrawlModel->job_title,
                'employer_id' => $employerId
            ])
            ->andWhere([">", 'employer_id', 0])
            ->orWhere(['and',
                ['job_title' => $jobCrawlModel->job_title],
                ['company_name' => $jobCrawlModel->company_name],
            ])
            ->one();
        if ($jobExist) {
            $jobExist->crawl_url = $jobCrawlModel->crawl_job_url;
            $jobExist->updated = date('Y-m-d H:i:s');
            $jobExist->save();
            // Update cả vào bảng company
            Yii::$app->db->createCommand(
                "UPDATE x_employer_crawl
                    SET updated = :updated
                    WHERE company_name = :company_name"
            )
                ->bindValue(':company_name', $jobCrawlModel->company_name)
                ->bindValue(':updated', date('Y-m-d H:i:s'))
                ->execute();
            return false;
        }

        // Check xem NTD đã có job_title này trong bảng job chưa
        // nếu có rồi thì không insert lại nữa
        $jobExistInJobTable = false;
        if ($employerId) {
            $jobExistInJobTable = Yii::$app->db->createCommand(
                "SELECT * FROM
                    (
                        SELECT job_id, j.employer_id
                        FROM job AS j, employer AS e
                        WHERE 
                        j.employer_id = e.employer_id
                        AND j.job_title = :job_title
                        AND (e.employer_id = :employer_id or e.name LIKE :employer_name)
                        ) AS j1
                        UNION
                        (
                        SELECT job_id, j.employer_id
                        FROM job_product AS j, employer_product AS e
                        WHERE 
                        j.employer_id = e.employer_id
                        AND j.job_title = :job_title
                        AND (e.employer_id = :employer_id or e.name LIKE :employer_name)
                    )"
            )
                ->bindValue(':job_title', $jobCrawlModel->job_title)
                ->bindValue(':employer_id', $employerId)
                ->bindValue(':employer_name', "%$keyword%")
                ->queryAll();
        }
        if ($jobExistInJobTable) {
            return false;
        }
        return $jobCrawlModel->save();

    }

    /**
     * Tạo link search từ domain tương ứng
     * VD: https://www.vietnamworks.com/Công-ty-TNHH-ANS-Asia-kv
     *
     * @param type $keyword
     */
    public function createSearchUrl($keyword)
    {
        // Encode keywork 
        $keyword = str_replace(" ", "-", $keyword);
        // Domain
        $domainSearch = $this->crawlDomain;
        // Link search theo format của từng trang
        // VD: https://www.vietnamworks.com/Công-ty-TNHH-ANS-Asia-kv
        $searchUrl = $domainSearch . "/$keyword-kv";

        return $searchUrl;
    }

    /**
     * Search trên trang Vietnamwork : https://www.vietnamworks.com/C%C3%B4ng-ty-TNHH-ANS-Asia-kv
     * Từ kết quả trả về insert vào bảng x_job_crawl
     * @param type $keyword
     * @return array
     */
    public function searchJob($keyword, $employerId = NULL)
    {
        // Url search từ VNW
        $searchUrl = $this->createSearchUrl($keyword);
        $searchUrl = 'https://jf8q26wwud-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20vanilla%20JavaScript%20(lite)%203.24.5%3Binstantsearch.js%201.6.0%3BJS%20Helper%202.21.2&x-algolia-application-id=JF8Q26WWUD&x-algolia-api-key=MTJjNmRkNzg4YTI1ZGU4MDgyNDU0NDcwMThmZWM2YzRlZDNhYTAyMjk0NWM2NTU1MzBiM2Y0MjJjZGQ3NDdlOXRhZ0ZpbHRlcnM9JnVzZXJUb2tlbj01MjBmYzQ1M2UyYmU5YzRlMDg2YTA1NTI3ZmFmNmZiMQ%3D%3D';

        // Lấy kết quả trả về
        // create curl resource 
        $ch = curl_init();
        // set url 
        curl_setopt($ch, CURLOPT_URL, $searchUrl);
        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $postData = '{"requests":[{"indexName":"vnw_job_v2_rel","params":"query=' . $keyword . '&hitsPerPage=20&maxValuesPerFacet=20&page=0&facets=%5B%22categoryIds%22%2C%22locationIds%22%2C%22categories%22%2C%22locations%22%2C%22skills%22%2C%22jobLevel%22%2C%22company%22%5D&tagFilters="}]}';
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        // $output contains the output string 
        $html = curl_exec($ch);
        // close curl resource to free up system resources 
        curl_close($ch);
        // Lấy mảng job trả về
        $arrJobData = $this->getSearchJobResult($html);
        // Insert vào bảng x_job_crawl
        foreach ($arrJobData as $jobData) {
            $this->insertToJobCrawlTable($jobData, $keyword, $employerId);
        }

        return $arrJobData;
    }

    /**
     * Xử lý html lấy về thành mảng data
     * [job_title, job_url,employer_name, place, deadline, salary, posted_date]
     *
     * @param $html
     * @return array
     */
    public function getSearchJobResult($html)
    {
        $decodeResult = json_decode($html);
        $arrResult = $decodeResult->results;
        if ($arrResult[0]->hits) {
            return $arrResult[0]->hits;
        }
        return [];


        // Dom object
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);

        // Các job trả về
        $arrJobItem = $finder->query("//div[@id='job-list']/div[@class='job-item']");
        $crawlData = [];
        // Lấy thông tin của từng job
        foreach ($arrJobItem as $index => $jobNode) {

            $arrJobUrlNode = $finder->query("div[@class='job-item-info']/h3[@class='bold-red']/a", $jobNode);
            foreach ($arrJobUrlNode as $index => $jobUrlNode) {
                $jobUrl = $this->crawlDomain . $jobUrlNode->getAttribute('href');
                $tmp_dom = new \DOMDocument();
                $tmp_dom->appendChild($tmp_dom->importNode($jobUrlNode, true));
                $jobTitle = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $jobData['job_title'] = $jobTitle;
                $jobData['job_url'] = $jobUrl;
            }

            $companyNode = $finder->query("div[@class='company text-clip']", $jobNode);
            // Company
            foreach ($companyNode as $index => $company) {
                // Company name
                $companyNameNode = $finder->query("span[@class='job-search__company']", $company);
                $companyName = $companyNameNode[0]->getAttribute('title');
                // Place name
                $placeNameNode = $finder->query("span[@class='job-search__location']", $company);
                $tmp_dom = new \DOMDocument();
                $tmp_dom->appendChild($tmp_dom->importNode($placeNameNode[0], true));
                $placeName = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $jobData['employer_name'] = $companyName;
                $jobData['place'] = $placeName;
            }
            // Deadline
            $postedDateNode = $finder->query("div[@class='extra-info salary text-clip']/span[@class='job-search__date-posted gray-light']", $jobNode);
            foreach ($postedDateNode as $index => $postedDate) {
                $tmp_dom = new \DOMDocument();
                $tmp_dom->appendChild($tmp_dom->importNode($postedDate, true));
                $postedDate = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $postedDate = str_replace("/", "-", $postedDate);
                $postedDate = date('Y-m-d', strtotime($postedDate));
                $jobData['posted_date'] = $postedDate;
            }

            $crawlData[] = $jobData;

        }
        return $crawlData;
    }

    /**
     * Search trên trang Vietnamwork : https://www.vietnamworks.com/C%C3%B4ng-ty-TNHH-ANS-Asia-kv
     * Từ kết quả trả về insert vào bảng x_job_crawl
     * @param null $cursor
     * @return array
     */
    public function getBrowseJob($cursor = NULL)
    {
        // Url browse từ VNW
        $browseUrl = "https://jf8q26wwud-dsn.algolia.net/1/indexes/vnw_job_v2/browse?x-algolia-agent=Algolia%20for%20vanilla%20JavaScript%20(lite)%203.30.0%3Binstantsearch.js%201.6.0%3BJS%20Helper%202.26.1&x-algolia-application-id=JF8Q26WWUD&x-algolia-api-key=MTYxMjJlZmFmNTg5NTA1NjQ5YWUxMjYwZmI3ZjFlMzI3ZjRkMjU3N2U0ZDQ0MDVlZjVjYzM0YmQ3ZGRkNzY0NXRhZ0ZpbHRlcnM9JnVzZXJUb2tlbj1lZWI5NGNjZThkZDVjODVhODdlZTE0MjY4YjFlODAwNg%3D%3D";

        // Lấy kết quả trả về
        // create curl resource 
        $ch = curl_init();
        // set url 
        curl_setopt($ch, CURLOPT_URL, $browseUrl);
        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if ($cursor) {
            $postData = '{"cursor":"' . $cursor . '"}';
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        }
        // $output contains the output string 
        $html = curl_exec($ch);
        // close curl resource to free up system resources 
        curl_close($ch);
        // Lấy mảng job trả về
        $arrJobData = $this->getBrowseJobResult($html);
        $arrBrowseParams = $this->getBrowseJobParams($html);
        // Insert vào bảng x_job_crawl
        foreach ($arrJobData as $jobData) {
            $this->insertToJobCrawlTable($jobData, 'browse_job', 0);
            break;
        }

        return ['data' => $arrJobData, 'params' => $arrBrowseParams];
    }

    /**
     * Xử lý html lấy về thành mảng data
     * [job_title, job_url,employer_name, place, deadline, salary, posted_date]
     *
     * @param $html
     * @return array
     */
    public function getBrowseJobResult($html)
    {
        $decodeResult = json_decode($html);
        $arrResult['hits'] = $decodeResult->hits;
        if ($arrResult) {
            return $arrResult;
        }
        return [];
    }

    /**
     * Lấy các params kết quả trả về
     * @param $html
     * @return array|mixed|object|null
     */
    public function getBrowseJobParams($html)
    {
        $decodeResult = json_decode($html);
        unset($decodeResult->hits);
        return $decodeResult;
    }

}
