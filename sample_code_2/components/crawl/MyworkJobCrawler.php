<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\crawl;

use common\extensions\extra\TextFormatHelper;
use common\models\JobCategoryAssoc;
use DateTime;
use Yii;
use common\models\Employer;
use common\models\XJobCrawl;
use yii\helpers\ArrayHelper;
use common\components\HopeTimeHelper;
use common\models\Job;
use common\models\Place;
use common\models\JobPlaceAssoc;
use common\components\HopeUrlHelper;

/**
 * Description of HopeGeoHelper
 *
 * @author Luongtn
 * @modified QuyenNV
 */
class MyworkJobCrawler extends JobCrawler
{

    public $crawlSource = "mywork";
    public $crawlDomain = "https://mywork.com.vn";

    /**
     * Crawl job data thành 1 mảng data
     * @param $url
     * @return mixed
     */
    public function crawlJob($url)
    {
        $html = file_get_contents($url);
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        //company info
        $jobCompanyAddressNodes = $finder->query("//div[@id='detail-el']//div[@class='row row-standard']//div[@class='col-md-9']//p[@class='address']//span");
        $jobTitleNode = $finder->query("//h1[@class='main-title']");

        foreach ($jobTitleNode as $index => $node) {
            $tmp_dom = new \DOMDocument();
            $tmp_dom->appendChild($tmp_dom->importNode($node, true));
            $innerHTML = trim($tmp_dom->saveHTML());
            $crawlData['job_crawl_title'] = html_entity_decode(strip_tags($innerHTML));
        }

        foreach ($jobCompanyAddressNodes as $index => $node) {
            $tmp_dom = new \DOMDocument();
            $tmp_dom->appendChild($tmp_dom->importNode($node, true));
            $innerHTML = trim($tmp_dom->saveHTML());
            $crawlData['company_crawl_address'] = html_entity_decode(strip_tags($innerHTML));
        }

        $jobInfoNode = $finder->query("//div[@class='box_main_info_job_left']//div[@class='row']");
        foreach ($jobInfoNode as $index => $node) {
            if ($index == 0) {
                // salary + deadline
                $salaryDeadlineNode = $finder->query("div[@class='col-md-6']", $node);
                foreach ($salaryDeadlineNode as $key => $value) {
                    if ($key == 0) {
                        // salary
                        $strSalary = $this->getInnerHtmlValue($value);
                        $strSalary = str_replace("Mức lương", "", html_entity_decode(strip_tags($strSalary)));
                        $strSalary = preg_replace('/[^0-9-]/', '', $strSalary);
                        $arrSalary = explode("-", $strSalary);
                        if (isset($arrSalary[1])) {
                            $crawlData['min_salary'] = $arrSalary[0] * 1000000;
                            $crawlData['max_salary'] = $arrSalary[1] * 1000000;
                        } elseif (isset($arrSalary[0])) {
                            $crawlData['min_salary'] = $arrSalary[0] * 1000000;
                            $crawlData['max_salary'] = 0;
                        } else {
                            $crawlData['min_salary'] = 0;
                            $crawlData['max_salary'] = 0;
                        }
                    } elseif ($key == 1) {
                        // deadline
                        $deadline = $this->getInnerHtmlValue($value);
                        $deadline = str_replace("Hạn nộp hồ sơ", "", html_entity_decode(strip_tags($deadline)));
                        $deadline = str_replace("/", "-", $deadline);// replace / with -
                        $deadline = str_replace('/\s+/', '', $deadline); // remove spaces
                        $deadline = trim(preg_replace('~[^\d:.]~', ' ', $deadline));
                        $deadline = str_replace(" ", "-", $deadline);// replace space with -
                        $date = date_create_from_format("d-m-Y", trim($deadline)); // format time
                        $deadline = date_format($date, "Y-m-d");
                        $crawlData['job_crawl_deadline'] = trim($deadline);
                    } else {
                        // do nothing
                    }
                }

            } elseif ($index == 1) {
                // place + category
                $placeCategoryNode = $finder->query("div[@class='col-md-6']", $node);
                foreach ($placeCategoryNode as $key => $value) {
                    if ($key == 0) {
                        // place
                        $strPlace = $this->getInnerHtmlValue($value);
                        $strPlace = str_replace(["nơi làm việc:", "địa điểm tuyển dụng:", "nơi làm việc", "địa điểm tuyển dụng"], "", mb_strtolower(html_entity_decode(strip_tags($strPlace))));
                        $crawlData['job_crawl_place'] = ucwords(trim($strPlace));
                    } elseif ($key == 1) {
                        // category/skills
                        $skillNode = $finder->query("div//span//div[@class='txt_ellipsis']", $value);
                        foreach ($skillNode as $skills) {
                            $strSkills = $this->getInnerHtmlValue($skills);
                            $strSkills = mb_strtolower(html_entity_decode(strip_tags($strSkills)));
                            $crawlData['job_crawl_skills'] = ucwords(trim($strSkills));
                        }
                    } else {
                        // do nothing
                    }
                }
            } elseif ($index == 2) {
                // position + exp
                $positionExpNode = $finder->query("div[@class='col-md-6']", $node);
                foreach ($positionExpNode as $key => $value) {
                    if ($key == 0) {
                        // position
                        $strPosition = $this->getInnerHtmlValue($value);
                        $strPosition = str_replace(["chức vụ:", "chức vụ"], "", mb_strtolower(html_entity_decode(strip_tags($strPosition))));
                        $crawlData['job_crawl_position'] = ucwords(trim($strPosition));
                    } elseif ($key == 1) {
                        // exp
                        $strExp = $this->getInnerHtmlValue($value);
                        $strExp = str_replace(["kinh nghiệm:", "kinh nghiệm"], "", mb_strtolower(html_entity_decode(strip_tags($strExp))));
                        $crawlData['job_crawl_experience'] = ucwords(trim($strExp));
                    } else {
                        // do nothing
                    }
                }
            } elseif ($index == 3) {
                // type + degree
                $typeDegreeNode = $finder->query("div[@class='col-md-6']", $node);
                foreach ($typeDegreeNode as $key => $value) {
                    if ($key == 0) {
                        //type
                        $strType = $this->getInnerHtmlValue($value);
                        $strType = str_replace(["hình thức làm việc:", "hình thức làm việc"], "", mb_strtolower(html_entity_decode(strip_tags($strType))));
                        $crawlData['job_crawl_type'] = ucwords(trim($strType));
                    } elseif ($key == 1) {
                        // degree
                        $strDegree = $this->getInnerHtmlValue($value);
                        $strDegree = str_replace(["yêu cầu bằng cấp:", "yêu cầu bằng cấp"], "", mb_strtolower(html_entity_decode(strip_tags($strDegree))));
                        $crawlData['job_crawl_degree'] = ucwords(trim($strDegree));
                    } else {
                        // do nothing
                    }
                }
            } elseif ($index == 4) {
                // maxRecruit + gender
                $maxRecGenderNode = $finder->query("div[@class='col-md-6']", $node);
                foreach ($maxRecGenderNode as $key => $value) {
                    if ($key == 0) {
                        // max recruit
                        $strMaxRec = $this->getInnerHtmlValue($value);
                        $strMaxRec = preg_replace('[^0-9]+', "", mb_strtolower(html_entity_decode(strip_tags($strMaxRec))));
                        $crawlData['company_crawl_number_people'] = ucwords(trim($strMaxRec));
                    } elseif ($key == 1) {
                        // gender
                        $strGenderReq = $this->getInnerHtmlValue($value);
                        $strGenderReq = str_replace(["yêu cầu giới tính", "yêu cầu giới tính:"], "", mb_strtolower(html_entity_decode(strip_tags($strGenderReq))));
                        $crawlData['job_crawl_gender_req'] = ucwords(trim($strGenderReq));
                    } else {
                        // do nothing
                    }
                }
            }
        }

        // job description, benefit, requirement,
        $jobDetailHeadingNodes = $finder->query("//div[@class='detail_job']//div[@class='row']//div[@class='col-md-8 col-lg-8 col-left']//div[@class='box multiple']//div[@class='col-md-9 col-lg-9']//h3[@class='heading-title']");
        $jobDetailContentNodes = $finder->query("//div[@class='detail_job']//div[@class='row']//div[@class='col-md-8 col-lg-8 col-left']//div[@class='box multiple']//div[@class='col-md-9 col-lg-9']//div[@class='mw-box-item']");
        foreach ($jobDetailHeadingNodes as $index => $node) {
            $strHeading = $this->getInnerHtmlValue($node);
            $strHeading = trim(mb_strtolower(html_entity_decode(strip_tags($strHeading))));
            if (strpos($strHeading,'mô tả công việc') !== false) {
                $strDesc = $this->getInnerHtmlValue($jobDetailContentNodes[$index]);
                $crawlData['job_crawl_description'] = html_entity_decode(strip_tags($strDesc));
            } elseif (strpos($strHeading,'quyền lợi được hưởng') !== false) {
                $strBenefit = $this->getInnerHtmlValue($jobDetailContentNodes[$index]);
                $crawlData['job_crawl_benefit'] = html_entity_decode(strip_tags($strBenefit));
            } elseif (strpos($strHeading,'yêu cầu công việc') !== false) {
                $strReq = $this->getInnerHtmlValue($jobDetailContentNodes[$index]);
                $crawlData['job_crawl_requirement'] = html_entity_decode(strip_tags($strReq));
            } elseif (strpos($strHeading,'yêu cầu khác') !== false || strpos('yêu cầu hồ sơ', $strHeading) !== false) {
                $strOtherReq = $this->getInnerHtmlValue($jobDetailContentNodes[$index]);
                $crawlData['job_crawl_other_requirement'] = html_entity_decode(strip_tags($strOtherReq));
            } else {
                // do nothing
            }
        }

        // company name
        $companyNameNodes = $finder->query("//div[@id='detail-el']//div[@class='row row-standard']//div[@class='col-md-9']//p[@class='company-name']");
        foreach ($companyNameNodes as $index => $node) {
            $strCompanyName = $this->getInnerHtmlValue($node);
            $crawlData['company_crawl_name'] = ucwords(trim(html_entity_decode(strip_tags($strCompanyName))));
        }

        // company logo
        $companyLogoNodes = $finder->query("//head//meta[@property='og:image']");
        foreach ($companyLogoNodes as $index => $node) {
            $innerHTML = $node->getAttribute('content');
            $crawlData['company_crawl_logo'] .= $innerHTML;
        }

        // ngày duyệt
        $approvedDate = $finder->query("//div[@class='box_main_info_job_right']//div[last()]//span[last()]");
        foreach ($approvedDate as $index => $node) {
            $strPosted = $this->getInnerHtmlValue($node);
            $posted = str_replace(["ngày duyệt:", "ngày duyệt"], "", mb_strtolower(html_entity_decode(strip_tags($strPosted))));
            $posted = str_replace("/", "-", $posted);
            $posted = date('Y-m-d', strtotime($posted));
            $crawlData['job_crawl_posted'] = $posted;
        }

        array_walk_recursive($crawlData, function (&$value) {
            $value = trim($value);
        });

        return $crawlData;
    }

    /**
     * @param $node
     * @return string
     */
    private function getInnerHtmlValue($node)
    {
        $tmp_dom = new \DOMDocument();
        $tmp_dom->appendChild($tmp_dom->importNode($node, true));
        return $tmp_dom->saveHTML();
    }

    public function getJobCrawlCategory($jobCrawlModel)
    {
        return [];
    }

    /**
     * Lấy job degree
     * @param $strDegree
     * @return array
     */
    public function getJobCrawlDegree($strDegree)
    {
        if ($strDegree == "Trung cấp") {
            return ['degree_name' => "Trung cấp - Nghề", "degree_id" => 1];
        } elseif ($strDegree == "Cao đẳng") {
            return ['degree_name' => "Cao Đẳng", "degree_id" => 2];
        } elseif ($strDegree == "Đại học" || $strDegree == "Kỹ sư" || $strDegree == "Cử nhân") {
            return ['degree_name' => "Đại Học", "degree_id" => 3];
        } elseif ($strDegree == "Trên đại học" || $strDegree == "Thạc sĩ" || $strDegree == "Thạc sĩ Y học") {
            return ['degree_name' => "Thạc sỹ", "degree_id" => 4];
        } else {
            return ['degree_name' => "Cao Đẳng", "degree_id" => 2];
        }
    }

    /**
     * Lấy job place
     * @param $strPlace
     * @param $employerId
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getJobCrawlPlace($strPlace, $employerId)
    {

        // Lấy thông tin employer
//        $employerModel = \Yii::$app->db->createCommand(
//                    "SELECT ep.* "
//                . " FROM employer AS e, employer_product AS ep"
//                . " WHERE e.transfer_ref_id = ep.transfer_ref_id"
//                . " AND e.employer_id = $employerId"
//                )->queryOne();

        $arrPlace = explode(",", $strPlace);
        $arrPlaceResult = Place::find()
            ->select('place_name, place_id')
            ->where(['place_name' => $arrPlace])
            ->asArray()
            ->all();

        return $arrPlaceResult;

        $withEmployerAddress = false;
        // Check xem các place có nằm trong address của employer thì đưa vào
        foreach ($arrPlace as $index => $place) {
            $lowerPlaceName = strtolower($place['place_name']);
            $lowerAddress = strtolower($employerModel['address']);
            if (strstr($lowerAddress, $lowerPlaceName) !== FALSE && !empty($employerModel['geo_latitude'])) {
                $arrPlace[$index]['place_name'] = $employerModel['address'];
                $arrPlace[$index]['geo_latitude'] = $employerModel['geo_latitude'];
                $arrPlace[$index]['geo_longitude'] = $employerModel['geo_longitude'];
                $withEmployerAddress = true;
            }
        }

        return $arrPlaceResult;
    }

    /**
     * Lấy job position
     * @param $strPosition
     * @return array
     */
    public function getJobCrawlPosition($strPosition)
    {

        if (strstr($strPosition, "Mới tốt nghiệp") || strstr($strPosition, "Thực tập sinh")
            || strstr($strPosition, "Hợp đồng")) {
            return ['job_position_name' => "Mới tốt nghiệp/Thực tập sinh", "job_position_id" => 1];
        } elseif (strstr($strPosition, "Nhân viên") || strstr($strPosition, "Kỹ thuật viên/Kỹ sư")
            || strstr($strPosition, "Mới đi làm")) {
            return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
        } elseif (strstr($strPosition, "Trưởng") || strstr($strPosition, "trưởng")) {
            return ['job_position_name' => "Trưởng nhóm/Trưởng phòng", "job_position_id" => 3];
        } elseif (strstr($strPosition, "Giám") || strstr($strPosition, "giám") || strstr($strPosition, "cấp cao")) {
            return ['job_position_name' => "Giám đốc và cấp cao hơn", "job_position_id" => 4];
        } else {
            return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
        }
    }

    /**
     * Lấy job salary
     * @param $strSalary
     * @return mixed
     */
    public function getJobCrawlSalary($strSalary)
    {
        $arrSalaryPiece = explode("-", $strSalary);
        $data['max_salary'] = 0;
        $data['min_salary'] = 0;
        if (count($arrSalaryPiece) == 2) {
            $data['min_salary'] = preg_replace('/[^\d]/', '', $arrSalaryPiece[0]) * 1000000;
            $data['max_salary'] = preg_replace('/[^\d]/', '', $arrSalaryPiece[1]) * 1000000;
        }
        return $data;
    }

    /**
     * @param $strType
     * @return array|void
     */
    public function getJobCrawlType($strType)
    {
        if (strstr($strType, "Toàn thời gian") || strstr($strType, "hợp đồng") || strstr($strType, "Hợp đồng")) {
            return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
        } elseif (strstr($strType, "Bán thời gian")) {
            return ['job_type_name' => "Bán thời gian", "job_type_id" => 2];
        } elseif (strstr($strType, "Thực tập")) {
            return ['job_type_name' => "Thực tập", "job_type_id" => 5];
        } else {
            return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
        }
    }

    /**
     * Lấy job experiment
     * @param $strYearExp
     * @return array
     */
    public function getJobCrawlYearExp($strYearExp)
    {
        return ["minYearExp" => 0, "maxYearExp" => 0];
    }

    /**
     * Insert job place
     *
     * @param $strPlace
     * @param $job_id
     * @param $employer_id
     * @param $crawlSource
     */
    public function insertJobPlace($strPlace, $job_id, $employer_id, $crawlSource)
    {

    }

    /**
     * Insert vào bảng job
     * @param $jobCrawlModel
     * @return Job
     */
    public function insertToJobTable($jobCrawlModel)
    {
        // Lấy các thông tin job, job_place, salary, degree, position, exp, category
        $placeValue = $this->getJobCrawlPlace($jobCrawlModel->job_place, $jobCrawlModel->employer_id);
        $degreeValue = $this->getJobCrawlDegree($jobCrawlModel->job_degree);
        $jobTypeValue = $this->getJobCrawlType($jobCrawlModel->job_type);
        $positionValue = $this->getJobCrawlPosition($jobCrawlModel->job_position);
        $experimentValue = $this->getJobCrawlYearExp($jobCrawlModel->job_year_experience);
        $categoryValue = $this->getJobCrawlCategory($jobCrawlModel);

        // Insert vào bảng job
        try {
            $jobExist = $this->allowToInsertJobTable($jobCrawlModel);

            if (!$jobExist) {
                $job = new Job();
                $job->addError('error', "Job tồn tại");
                return $job;
            }

            $job = new Job();
            $job->employer_id = $jobCrawlModel->employer_id;
            $job->job_title = $jobCrawlModel->job_title;
            $job->crawl_detail_id = $jobCrawlModel->id;
            $job->crawl_source = $jobCrawlModel->crawl_source;
            $job->degree = $degreeValue['degree_name'];
            $job->degree_id = $degreeValue['degree_id'];
            $job->min_expect_salary = $jobCrawlModel->min_salary > 1000 ? floor($jobCrawlModel->min_salary / 1000000) : $jobCrawlModel->min_salary;
            $job->max_expect_salary = $jobCrawlModel->max_salary > 1000 ? floor($jobCrawlModel->max_salary / 1000000) : $jobCrawlModel->max_salary;
            $job->exp_min_require_year = $experimentValue['minYearExp'];
            $job->exp_max_require_year = $experimentValue['maxYearExp'];
            $job->job_type = $jobTypeValue['job_type_name'];
            $job->job_type_id = $jobTypeValue['job_type_id'];
            $job->job_position = $positionValue['job_position_name'];
            $job->job_position_id = $positionValue['job_position_id'];
            $job->job_category = implode(",", $categoryValue);
            $job->job_description = strip_tags($jobCrawlModel->job_description);
            $job->job_requirement = strip_tags($jobCrawlModel->job_requirement);
            $job->job_benefit = strip_tags($jobCrawlModel->job_benefit);
            $job->deadline = $jobCrawlModel->deadline ? $jobCrawlModel->deadline : HopeTimeHelper::getDayPlus(30);
            $job->created = HopeTimeHelper::getNow();
            $job->updated = HopeTimeHelper::getNow();
            $job->status = 0;
            $job->admin_id = Yii::$app->user->identity->user_id;
            $job->save();

            if ($job->errors) {
                return $job;
            }
            $employerModel = Employer::find()
                ->where(['employer_id' => $jobCrawlModel->employer_id])
                ->one();
            // Insert vào bảng place assoc
            foreach ($placeValue as $place) {
                $placeAssocModel = new JobPlaceAssoc();
                $placeAssocModel->job_id = $job->job_id;
                $placeAssocModel->place_id = $place['place_id'];
                $placeAssocModel->detail_address = $place['place_name'];
                $placeAssocModel->geo_latitude = $place['geo_latitude'];
                $placeAssocModel->geo_longitude = $place['geo_longitude'];
                $placeAssocModel->province_name = $place['place_name'];
                $placeAssocModel->district_name = '';
                $placeAssocModel->address_google = $place['place_name'];
                $placeAssocModel->save();
                if ($placeAssocModel->errors) {
                    return $job;
                }
            }
            // Insert vào bảng category_assoc
            foreach ($categoryValue as $categoryId) {
                $categoryAssocModel = new JobCategoryAssoc();
                $categoryAssocModel->job_id = $job->job_id;
                $categoryAssocModel->job_category_id = $categoryId;
                $categoryAssocModel->save();
                if ($categoryAssocModel->errors) {
                    return $job;
                }
            }

            return $job;
        } catch (\Exception $ex) {
            print_r($ex->getMessage());
            exit;
            return $job;
        }
    }

    /**
     * Lấy job benefit
     * @param $arrBenefit
     * @return string
     */
    public static function getJobBenefit($arrBenefit)
    {
        $strBenefit = '';
        foreach ($arrBenefit as $benefit) {
            $strBenefit .= "\r\n<p>$benefit</p>";
        }
        return $strBenefit;
    }

    /**
     * Insert vào bảng job_crawl để duyệt
     *
     * @param $jobCrawlData
     * @param $keyword
     * @param $employerId
     * @return bool
     * @throws \yii\db\Exception
     */
    public function insertToJobCrawlTable($jobCrawlData, $keyword, $employerId = NULL)
    {
        // get all crawl job url in last 45 days
        $arrCrawlJobUrl = $this->getAllJobCrawlUrl($this->crawlSource);
        $crawlJobUrl = $jobCrawlData['job_url'];
        $companyLink = $jobCrawlData['company_link'];
        // if this job url in the list then return false
        if (in_array($crawlJobUrl, $arrCrawlJobUrl)) {
            return false;
        }

        // Phải crawl job về để lấy thêm data
        $jobCrawlData = $this->crawlJob($crawlJobUrl);

        // Nếu không crawl được
        if (!$jobCrawlData) {
            return false;
        }

        $jobCrawlModel = new XJobCrawl();
        if ($employerId) {
            $jobCrawlModel->employer_id = $employerId;
        }
        $jobCrawlModel->keyword = $keyword;
        $jobCrawlModel->job_title = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_title']);
        $jobCrawlModel->job_category = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_skills']);
        $jobCrawlModel->job_skill = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_skills']);
        $jobCrawlModel->job_place = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_place']);
        $jobCrawlModel->job_position = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_position']);
        $jobCrawlModel->job_year_experience = $jobCrawlData['job_crawl_experience'];
        $jobCrawlModel->company_name = TextFormatHelper::replaceMultipleSpace($jobCrawlData['company_crawl_name']);
        $jobCrawlModel->company_logo = $jobCrawlData['company_crawl_logo'];
        $jobCrawlModel->deadline = $jobCrawlData['job_crawl_deadline'];
        $jobCrawlModel->posted_time = $jobCrawlData['job_crawl_posted'];
        $jobCrawlModel->job_description = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_description']);
        $jobCrawlModel->job_benefit = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_benefit']);
        $jobCrawlModel->job_requirement = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_requirement']);
        $jobCrawlModel->created = date('Y-m-d H:i:s');
        $jobCrawlModel->updated = date('Y-m-d H:i:s');
        $jobCrawlModel->max_salary = $jobCrawlData['max_salary'] < 100000 ? (floor($jobCrawlData['max_salary'] * 22700 / 1000000)) * 1000000 : $jobCrawlData['max_salary'];
        $jobCrawlModel->min_salary = $jobCrawlData['min_salary'] < 100000 ? (floor($jobCrawlData['min_salary'] * 22700 / 1000000)) * 1000000 : $jobCrawlData['min_salary'];
        $jobCrawlModel->crawl_source = $this->crawlSource;
        $jobCrawlModel->crawl_job_url = $crawlJobUrl;
        $jobCrawlModel->crawl_url = $crawlJobUrl;
        $jobCrawlModel->job_level = trim($jobCrawlData['job_crawl_position']);
        $jobCrawlModel->job_other_requirement = TextFormatHelper::replaceMultipleSpace($jobCrawlData['job_crawl_requirement']);
        $jobCrawlModel->company_link = $companyLink;
        $jobCrawlModel->company_address = $jobCrawlData['company_crawl_address'];
        return $jobCrawlModel->save();
    }

    /**
     * Tạo link search từ domain tương ứng
     * VD: https://mywork.com.vn/tim-viec-lam/tong-cong-ty-co-phan-buu-chinh-viettel.html
     *
     * @param $keyword
     * @return string
     */
    public function createSearchUrl($keyword)
    {
        // Encode keywork
        $keywordSlug = HopeUrlHelper::slug($keyword);
        // Domain
        $domainSearch = $this->crawlDomain;
        // Link search theo format của từng trang
        // VD: https://mywork.com.vn/tim-viec-lam/tong-cong-ty-co-phan-buu-chinh-viettel.html
        $searchUrl = $domainSearch . "/tim-viec-lam/$keywordSlug.html";

        return $searchUrl;
    }

    /**
     * Search trên trang Vietnamwork : https://www.vietnamworks.com/C%C3%B4ng-ty-TNHH-ANS-Asia-kv
     * Từ kết quả trả về insert vào bảng x_job_crawl
     * @param $keyword
     * @return array
     * @throws \yii\db\Exception
     */
    public function searchJob($keyword, $employerId = NULL)
    {
        // Url search từ VNW
        $searchUrl = $this->createSearchUrl($keyword);

        // Lấy kết quả trả về
        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $searchUrl);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // $output contains the output string
        $html = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch);

        // Lấy mảng job trả về
        $arrJobData = $this->getSearchJobResult($html);
        // Insert vào bảng x_job_crawl
        foreach ($arrJobData as $jobData) {
            $this->insertToJobCrawlTable($jobData, $keyword, $employerId);
        }

        return $arrJobData;
    }

    /**
     * Xử lý html lấy về thành mảng data
     * [job_title, job_url,employer_name, place, deadline, salary, posted_date]
     *
     * @param $html
     * @return array
     */
    public function getSearchJobResult($html)
    {
        // Dom object
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);

        // Các job trả về
        $arrJobItem = $finder->query("//div[@class='job-relate tablesearch']/div[@class='panel panel-default']/div[@id='list-jobs']/div[@class='item ']");

        $crawlData = [];
        // Lấy thông tin của từng job
        foreach ($arrJobItem as $index => $jobNode) {

            $arrJobUrlNode = $finder->query("div[@class='title col-8']/a[@class='title']", $jobNode);

            foreach ($arrJobUrlNode as $key => $jobUrlNode) {
                $jobUrl = $this->crawlDomain . $jobUrlNode->getAttribute('href');
                $tmp_dom = new \DOMDocument();
                $tmp_dom->appendChild($tmp_dom->importNode($jobUrlNode, true));
                $jobTitle = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $jobData['job_title'] = $jobTitle;
                $jobData['job_url'] = $jobUrl;
            }

            // Company
            $companyNode = $finder->query("div[@class='bottom']/div[@class='col-5']/p[@class='desc']/a", $jobNode);
            foreach ($companyNode as $key => $company) {
                $companyName = $company->getAttribute('title');
                $jobData['employer_name'] = str_replace("Tìm việc làm của nhà tuyển dụng ", "", $companyName);
            }

            // Deadline
            $deadlineNode = $finder->query("div[@class='bottom']/div[@class='col-3']/p[@class='desc']", $jobNode);
            $tmp_dom = new \DOMDocument();
            foreach ($deadlineNode as $key => $deadlineNd) {
                $tmp_dom->appendChild($tmp_dom->importNode($deadlineNd, true));
                $deadline = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $deadline = str_replace("/", "-", $deadline);
                $deadline = date('Y-m-d', strtotime($deadline));
                $jobData['deadline'] = $deadline;
            }


            // Place
            $placeNode = $finder->query("div[@class='bottom']/div[@class='col-4']/p[@class='desc text-location']", $jobNode);
            foreach ($placeNode as $key => $placeNd) {
                $tmp_dom = new \DOMDocument();
                $tmp_dom->appendChild($tmp_dom->importNode($placeNd, true));
                $placeName = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $jobData['place'] = $placeName;
            }

            // Nếu deadline < tomorrow thì ko insert
            if (strtotime($jobData['deadline']) - time() > 86400) {
                $crawlData[] = $jobData;
            }

        }
        return $crawlData;
    }

    //==================================BROWSE JOB============================//

    /**
     * Search trên trang Mywork : https://mywork.com.vn/tuyen-dung/trang/8
     * Từ kết quả trả về insert vào bảng x_job_crawl
     * @param null $cursor
     * @return array
     * @throws \yii\db\Exception
     */
    public function crawlBrowseJob($cursor = NULL)
    {
        // Url browse từ VNW
        $browseUrl = "https://mywork.com.vn/tuyen-dung";

        if ($cursor) {
            $browseUrl = $browseUrl . "/trang/$cursor";
        }

        // Lấy kết quả trả về
        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $browseUrl);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // $output contains the output string
        $html = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);
        // Lấy mảng job trả về
        $arrJobData = $this->extractAllJobFromHtml($html);
        // count total job crawl
        $this->totalJob += count($arrJobData);
        // check site out of job
        if (count($arrJobData) == 0 || $arrJobData == false) {
            $this->isRunningOutJobs = true;
            exit;
        }
        // Insert vào bảng x_job_crawl
        $count = 0;
        foreach ($arrJobData as $jobData) {
            $resultQuery = $this->insertToJobCrawlTable($jobData, 'browse_job', 0);
            if ($resultQuery) {
                $count++;
                $this->newJob += 1;
            }
        }
        if ($count == count($arrJobData)) {
            return ['data' => $arrJobData];
        } else {
            return ['failed'];
        }


    }

    /**
     * Xử lý html lấy về thành mảng data
     * [job_title, job_url,employer_name, place, deadline, salary, posted_date]
     *
     * @param $html
     * @return array
     */
    public function extractAllJobFromHtml($html)
    {

        // Dom object
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $finder = new \DOMXPath($dom);

        // Các job trả về
        $arrJobItem = $finder->query("//div[@id='idJobNew']//div//div[@class='item-list']//section[@class='item']");

//        $arr = $arrJobItem->item(0)->nodeValue;
//        print($arr);die;

        $crawlData = [];
        // Lấy thông tin của từng job
        foreach ($arrJobItem as $index => $jobNode) {
            $arrJobUrlNode = $finder->query("div[@class='row']//div[@class='col-sm-10']//div[@class='company_name col-md-12 col-lg-12']//div[@class='content']//p[@class='j_title text_ellipsis']//a[@class='el-tooltip item']", $jobNode);

            foreach ($arrJobUrlNode as $jobUrlNode) {
                $jobUrl = $this->crawlDomain . $jobUrlNode->getAttribute('href');
                $tmp_dom = new \DOMDocument();
                $tmp_dom->appendChild($tmp_dom->importNode($jobUrlNode, true));
                $jobTitle = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $jobData['job_title'] = $jobTitle;
                $jobData['job_url'] = $jobUrl;
            }


            // Company
            $companyNode = $finder->query("div[@class='row']//div[@class='col-sm-10']//div[@class='company_name col-md-12 col-lg-12']//div[@class='content']//div[@class='j_company text_ellipsis']//a", $jobNode);
            foreach ($companyNode as $company) {
                $companyLink = $this->crawlDomain . $company->getAttribute('href');
                $tmp_dom = new \DOMDocument();
                $tmp_dom->appendChild($tmp_dom->importNode($company, true));
                $companyName = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $jobData['company_name'] = $companyName;
                $jobData['company_link'] = $companyLink;
            }

            // Deadline
            $deadlineNode = $finder->query("div[@class='row']//div[@class='col-sm-10']//div[@class='col-md-4 mt-10'][2]//span[@class='time ml-20']", $jobNode);

            $tmp_dom = new \DOMDocument();
            foreach ($deadlineNode as $deadlineNd) {
                $tmp_dom->appendChild($tmp_dom->importNode($deadlineNd, true));
                $deadline = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $deadline = str_replace("/", "-", $deadline);
                $deadline = date('Y-m-d', strtotime($deadline));
                $jobData['job_deadline'] = $deadline;
            }

            // Place
            $placeNode = $finder->query("div[@class='row']//div[@class='col-sm-10']//div[@class='col-md-4 mt-10'][1]//div[@class='location']", $jobNode);
            foreach ($placeNode as $placeNd) {
                $tmp_dom = new \DOMDocument();
                $tmp_dom->appendChild($tmp_dom->importNode($placeNd, true));
                $placeName = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $jobData['job_place'] = $placeName;
            }

            // Lương
            $arrSalaryNode = $finder->query("div[@class='row']//div[@class='col-sm-10']//div[@class='col-md-4 mt-10'][0]//span[@class='dollar']", $jobNode);
            foreach ($arrSalaryNode as $salaryNode) {
                $tmp_dom = new \DOMDocument();
                $tmp_dom->appendChild($tmp_dom->importNode($salaryNode, true));
                $salaryDesc = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                $arrSalary = $this->getJobCrawlSalary($salaryDesc);
                $jobData['min_salary'] = $arrSalary['min_salary'];
                $jobData['max_salary'] = $arrSalary['max_salary'];
            }
            // Nếu deadline < tomorrow thì ko insert
            $crawlData[] = $jobData;
            if (strtotime($jobData['deadline']) - time() > 86400) {

            }

        }
        return $crawlData;
    }

    /**
     * Insert vào bảng job_crawl để duyệt
     *
     * @param $jobCrawlData
     * @param $keyword
     * @param $employerId
     * @return bool
     * @throws \yii\db\Exception
     */
    public function insertToJobCrawlTableDirect($jobCrawlData, $keyword, $employerId = NULL)
    {

        // Nếu không cho phép insert thì trả về luôn
        if (!$this->allowToInsert($jobCrawlData, $keyword, $employerId)) {
            return false;
        }
        $crawlJobUrl = $jobCrawlData['job_url'];

        // Nếu không crawl được
        if (!$jobCrawlData) {
            return false;
        }

        $jobCrawlModel = new XJobCrawl();
        if ($employerId) {
            $jobCrawlModel->employer_id = $employerId;
        }
        $jobCrawlModel->keyword = $keyword;
        $jobCrawlModel->job_title = $jobCrawlData['job_title'];
        $jobCrawlModel->job_category = $jobCrawlData['job_skills'];
        $jobCrawlModel->job_skill = $jobCrawlData['job_skills'];
        $jobCrawlModel->job_place = $jobCrawlData['job_place'];
        $jobCrawlModel->job_position = $jobCrawlData['job_position'];
        $jobCrawlModel->job_year_experience = $jobCrawlData['job_experience'];
        $jobCrawlModel->company_name = $jobCrawlData['company_name'];
        $jobCrawlModel->company_logo = $jobCrawlData['company_logo'];
        $jobCrawlModel->deadline = $jobCrawlData['job_deadline'];
        $jobCrawlModel->posted_time = $jobCrawlData['job_posted'];
        $jobCrawlModel->job_description = $jobCrawlData['job_description'];
        $jobCrawlModel->job_benefit = $jobCrawlData['job_benefit'];
        $jobCrawlModel->job_requirement = $jobCrawlData['job_requirement'];
        $jobCrawlModel->created = date('Y-m-d H:i:s');
        $jobCrawlModel->updated = date('Y-m-d H:i:s');
        $jobCrawlModel->max_salary = $jobCrawlData['max_salary'] < 100000 ? (floor($jobCrawlData['max_salary'] * 22700 / 1000000)) * 1000000 : $jobCrawlData['max_salary'];
        $jobCrawlModel->min_salary = $jobCrawlData['min_salary'] < 100000 ? (floor($jobCrawlData['min_salary'] * 22700 / 1000000)) * 1000000 : $jobCrawlData['min_salary'];
        $jobCrawlModel->crawl_source = $this->crawlSource;
        $jobCrawlModel->crawl_job_url = $crawlJobUrl;
        $jobCrawlModel->crawl_url = $crawlJobUrl;
        $jobCrawlModel->job_level = $jobCrawlData['job_position'];

        return $jobCrawlModel->save();

    }

    //==============================END BROWSE JOB============================//

    /**
     * @param null $cursor
     * @return array
     * @throws \yii\db\Exception
     * @todo Browse for jobs by cursor then use the url to get the details
     * @author QuyenNV
     */
    public function myworkCrawler($cursor = NULL)
    {
        $arrJobDetail = [];
        // browse jobs by cursor
        $crawlBrowseJob = $this->crawlBrowseJob($cursor);
        // get details by urls
        foreach ($crawlBrowseJob['data'] as $key => $data) {
            $arrJobDetail[$key] = $this->crawlJob($data['job_url']);
        }
        return $arrJobDetail;
    }

    /**
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Crawl all new jobs from site
     */
    public function crawlAllNewJobs()
    {
        $this->crawlAllNewJobsBySource($this->crawlSource);
    }
}
