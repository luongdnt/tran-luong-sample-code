<?php

namespace common\components\crawl;

use common\extensions\extra\TextFormatHelper;
use common\extensions\mysql\libs\XJobCrawlLogQueries;
use common\extensions\tesseract\libs\StripVN;
use common\models\XJobCrawl;

/**
 * @author QuyenNV
 * @todo Crawl all jobs from Glints.com
 * @created Feb 21, 2020
 * Class GlintsJobCrawler
 * @package common\components\crawl
 * @API
 *      Một số API của Glints
 *           1. Lấy thông tin các jobs
 *              https://glints.com/api/marketplace/jobs?isRemote=false&type[]=INTERNSHIP&type[]=FULL_TIME&type[]=PART_TIME&type[]=PROJECT_BASED&CountryCode[]=VN&limit=30&offset= ($i*30)
 *           2. Lấy job skills
 *               a. https://glints.com/api/jobs/" . $jobID . "/skills (Lấy list các skills với job id)
 *               b. https://glints.com/api/skills/ . $skillID (Lấy thông tin skill với skill ID)
 *           3. Lấy job category
 *               https://glints.com/api/jobCategories/ + category id (Lấy thông tin category)
 *           4. Lấy thông tin chi tiết job
 *               https://glints.com/api/jobs/$jobID?include=Company,+Career,+Skills,+Groups,+JobTitles,+JobSalaries,+City,+Country
 */
class GlintsJobCrawler
{
    private $crawlSource = "Glints";
    private $crawlDomain = "https://glints.com/vn/opportunities/jobs/explore?countries=VN";
    private $page = 1;
    private $totalJob = 0;
    private $newJob = 0;
    private $isRunningOutJobs = false;

    /**
     * @param null $cursor
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    private function crawlBrowseJob($cursor = NULL)
    {
        // Url browse từ VNW
        $browseUrl = "https://glints.com/api/marketplace/jobs?isRemote=false&type[]=INTERNSHIP&type[]=FULL_TIME&type[]=PART_TIME&type[]=PROJECT_BASED&CountryCode[]=VN&limit=30&offset=";

        if ($cursor == NULL) {
            $browseUrl = $browseUrl . "0";
        } else {
            $browseUrl = $browseUrl . "" . ($cursor * 30);
        }

        // Lấy kết quả trả về
        $json = $this->getJsonFromUrl($browseUrl);
        // Lấy mảng job trả về
        $arrJobData = $this->extractJobFromJson($json);
//         count total job crawl
        $this->totalJob += count($arrJobData);
        // check site out of job
        if (count($arrJobData) == 0 || $arrJobData == false) {
            $this->isRunningOutJobs = true;
            exit;
        }
        $count = 0;
        foreach ($arrJobData as $jobData) {
            $resultQuery = $this->insertToJobCrawlTable($jobData, 'browse_job', 0);
            if ($resultQuery) {
                $this->newJob += 1;
            }
            $count++;
        }
        return ($count == count($arrJobData));
    }

    /**
     * @param $jobData
     * @param $keyword
     * @param null $employerId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    private function insertToJobCrawlTable($jobData, $keyword, $employerId = NULL)
    {
        // get all crawl job url in last 45 days
        $arrCrawlJobUrl = $this->getAllJobCrawlUrl($this->crawlSource);
        $crawlJobUrl = $jobData['crawl_job_url'];
        // if this job url in the list then return false
        if (in_array($crawlJobUrl, $arrCrawlJobUrl)) {
            return false;
        }

        // if job title empty, not allow insert
        if (empty($jobData['crawl_job_title'])) {
            return false;
        }

        if (empty($jobData['crawl_job_id'])) {
            return false;
        }

        $jobCrawlModel = new XJobCrawl();
        $jobCrawlModel->keyword = $keyword;
        $jobCrawlModel->job_title = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_title']);
        $jobCrawlModel->job_category = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_category']);
        $jobCrawlModel->job_skill = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_skills']);
        $jobCrawlModel->job_place = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_location']);
        $jobCrawlModel->job_position = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_title']);
        $jobCrawlModel->job_year_experience = $this->getYearExperience($jobData['crawl_job_min_exp'], $jobData['crawl_job_max_exp']);
        $jobCrawlModel->company_name = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_company_name']);
        $jobCrawlModel->company_logo = "https://images.glints.com/unsafe/glints-dashboard.s3.amazonaws.com/company-logo/" . $jobData['crawl_job_company_logo'];
        $jobCrawlModel->deadline = trim($jobData['crawl_job_deadline']);
        $jobCrawlModel->posted_time = $jobData['crawl_job_posted_time'];
        $jobCrawlModel->job_description = TextFormatHelper::replaceMultipleSpace($jobData['crawl_job_description']);
        $jobCrawlModel->created = date('Y-m-d H:i:s');
        $jobCrawlModel->updated = date('Y-m-d H:i:s');
        $jobCrawlModel->max_salary = $jobData['crawl_job_max_salary'];
        $jobCrawlModel->min_salary = $jobData['crawl_job_min_salary'];
        $jobCrawlModel->crawl_source = $this->crawlSource;
        $jobCrawlModel->crawl_job_url = $jobData['crawl_job_url'];
        $jobCrawlModel->crawl_url = $this->crawlDomain;
        $jobCrawlModel->job_type = trim($jobData['crawl_job_type']);
        return $jobCrawlModel->save();
    }

    /**
     * @param $minYearExperience
     * @param $maxYearExperience
     * @return string
     * @todo Get experience by min and max exp
     */
    private function getYearExperience($minYearExperience, $maxYearExperience)
    {
        if ($minYearExperience != null) {
            if ($maxYearExperience != null) {
                return "Từ " . $minYearExperience . " đến " . ($maxYearExperience <= 20 ? $maxYearExperience : (int)($maxYearExperience / 12)) . " năm";
            } else {
                return "Từ " . $minYearExperience . " năm";
            }
        } else {
            if ($maxYearExperience != null) {
                return "Đến " . ($maxYearExperience <= 20 ? $maxYearExperience : (int)($maxYearExperience / 12)) . " năm";
            } else {
                return "Không yêu cầu kinh nghiệm";
            }
        }
    }

    /**
     * @param $jobCrawlData
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Check if job data can be inserted to db
     */
    private function isAllowInsert($jobCrawlData)
    {
        // check if job already exist
        $jobExist = XJobCrawl::find()
            ->where([
                'crawl_job_url' => $jobCrawlData['crawl_job_url'],
            ])->one();
        // if job exist, just update the time then return false
        if ($jobExist) {
            $jobExist->updated = date('Y-m-d H:i:s');
            $jobExist->save();
            return false;
        }
        return true;
    }

    /**
     * @param $json
     * @return array|bool
     * @todo Extract job data from api json
     */
    private function extractJobFromJson($json)
    {
        // decode json
        $arrJob = json_decode($json, true);

        // get data only if have
        if (isset($arrJob['data'])) {
            // store data crawled from json
            $arrJobData = [];
            // get data array only
            $arrRawData = $arrJob['data'];
            // get data from json
            foreach ($arrRawData as $key => $rawDatum) {
                $arrJobData[$key]['crawl_job_id'] = $rawDatum['id'];
                $jobTitle = preg_replace('/(🔥)/', '', $rawDatum['title']);
                $arrJobData[$key]['crawl_job_title'] = trim($jobTitle);
                $arrJobData[$key]['crawl_job_type'] = $rawDatum['type'];
                $arrJobData[$key]['crawl_job_deadline'] = $rawDatum['expiryDate'];
                $arrJobData[$key]['crawl_job_min_exp'] = $rawDatum['minYearsOfExperience'];
                $arrJobData[$key]['crawl_job_max_exp'] = $rawDatum['maxYearsOfExperience'];
                $postedTime = $rawDatum['links']['jobSalaries'][0]['updatedAt'];
                preg_match('/[\d]{4}-[\d]{2}-[\d]{2}/', $postedTime, $output);
                $arrJobData[$key]['crawl_job_posted_time'] = $output[0];
                // get min and max salary in currencies then calculate them
                $minSalary = $rawDatum['links']['jobSalaries'][0]['minAmount'];
                $maxSalary = $rawDatum['links']['jobSalaries'][0]['maxAmount'];
                $salaryCurrency = $rawDatum['links']['jobSalaries'][0]['CurrencyCode'];
                $arrJobData[$key]['crawl_job_min_salary'] = $this->getJobSalaryByCurrency($minSalary, $salaryCurrency);
                $arrJobData[$key]['crawl_job_max_salary'] = $this->getJobSalaryByCurrency($maxSalary, $salaryCurrency);
                $arrJobData[$key]['crawl_job_location'] = $rawDatum['links']['city']['name'];
                $arrJobData[$key]['crawl_job_company_name'] = $rawDatum['links']['company']['name'];
                $arrJobData[$key]['crawl_job_company_logo'] = $rawDatum['links']['company']['logo'];
                $arrJobData[$key]['crawl_job_description'] = $this->getJobDesc($rawDatum['descriptionRaw']['blocks']);
                // job skills
                $arrJobData[$key]['crawl_job_skills'] = implode(', ', $this->getJobSkillsByJobID($rawDatum['id']));
                // job category
                $arrJobData[$key]['crawl_job_category'] = $this->getJobCategoryByItsID($rawDatum['JobCategoryId']);
                // crawl source
                $arrJobData[$key]['crawl_source'] = $this->crawlSource;
                $arrJobData[$key]['crawl_job_url'] = $this->getJobUrl($jobTitle, $rawDatum['id']);
            }
            return $arrJobData;
        } else {
            return false;
        }
    }

    /**
     * @param $salary
     * @param $currency
     * @return float|int
     * @todo Convert currency for 3 types, else, +1
     */
    private function getJobSalaryByCurrency($salary, $currency)
    {
        if ($currency == 'USD') {
            return $salary <= 10e6 ? $salary * 23000 : $salary;
        } elseif ($currency == 'IDR') {
            return $salary * 1.6;
        } elseif ($currency == 'VND') {
            if ($salary <= 100) {
                return $salary * 10e5;
            } else {
                return $salary;
            }
        } else {
            return ($salary == 0 ? $salary : ($salary + 1));
        }
    }

    /**
     * @param $blocks
     * @return string
     * @todo Get job desc from blocks
     */
    private function getJobDesc($blocks)
    {
        $jobDescString = "";
        foreach ($blocks as $index => $rawDesc) {
            $jobDescString .= $rawDesc['text'] . "<br>";
        }
        $jobDescString = preg_replace('/(🔥)/', '', $jobDescString);
        return $jobDescString;
    }

    /**
     * @param $jobTitle
     * @param $jobID
     * @return string
     * @todo Get job url from job title and its id
     */
    private function getJobUrl($jobTitle, $jobID)
    {
        $stripVN = new StripVN();
        $jobTitleSlug = preg_replace('/\s\n\t/', '-', $jobTitle);
        $jobTitleSlug = str_replace('/', '', $jobTitleSlug);
        $jobTitleSlug = str_replace(' ', '-', $stripVN->stripVN($jobTitleSlug));
        return "https://glints.com/vn/opportunities/jobs/" . $jobTitleSlug . "/" . $jobID;
    }


    /**
     * @param $jobID
     * @return array|bool
     * @todo Using JobID to get skill ids list, then use these ids to get its name
     */
    private function getJobSkillsByJobID($jobID)
    {
        // api to get skill ids list
        $browserUrl = "https://glints.com/api/jobs/" . $jobID . "/skills";
        // get json from api
        $jsonSkillList = json_decode($this->getJsonFromUrl($browserUrl), true);
        // get skills
        if ($jsonSkillList['data']) {
            // get all skills data
            $arrSkillID = $jsonSkillList['data'];
            // array to store skills name
            $arrSkillName = [];
            // loop the array to get skill ids
            foreach ($arrSkillID as $key => $skillID) {
                // get skill name
                $isSkillName = $this->getJobSkillByItsID($skillID['SkillId']);
                // check if there is a skill for this id
                if ($isSkillName) {
                    $arrSkillName[$key] = $isSkillName;
                }
            }
            return array_values($arrSkillName);
        } else {
            return false;
        }
    }

    /**
     * @param $skillID
     * @return bool|mixed
     * @todo Using api to get skill names by its id
     */
    private function getJobSkillByItsID($skillID)
    {
        // api to get skill ids list
        $browserUrl = "https://glints.com/api/skills/" . $skillID;
        // get json from api
        $jsonSkillList = json_decode($this->getJsonFromUrl($browserUrl), true);
        if ($jsonSkillList['data']) {
            // get all skills data
            return $jsonSkillList['data']['name'];
        } else {
            return false;
        }
    }

    /**
     * @param $browseUrl
     * @return bool|string
     * @todo Get json from apis
     */
    private function getJsonFromUrl($browseUrl)
    {
        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $browseUrl);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // $output contains the output string
        $json = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);
        return $json;
    }

    /**
     * @param $JobCategoryId
     * @return bool|mixed
     * @todo Get job category from its id
     */
    private function getJobCategoryByItsID($JobCategoryId)
    {
        // api to get skill ids list
        $browserUrl = "https://glints.com/api/jobCategories/" . $JobCategoryId;
        // get json from api
        $jsonCategory = json_decode($this->getJsonFromUrl($browserUrl), true);
        if ($jsonCategory['data']) {
            return $jsonCategory['data']['name'];
        } else {
            return false;
        }
    }

    /**
     * @param $source
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Crawl all new jobs from Glints by source
     */
    private function crawlAllNewJobsBySource($source)
    {
        $xJobCrawlLog = new XJobCrawlLogQueries();
        // check if there is a running process, do not run crawl
        if ($xJobCrawlLog->isRunningProcess($source) == false) {
            // save the first log
            $startLogID = $xJobCrawlLog->saveStartLog($source);
            while (!$this->isRunningOutJobs) {
                // crawl jobs from pages
                $crawlJob = $this->crawlBrowseJob($this->page);
                // count pages
                if ($crawlJob) {
                    $this->page += 1;
                }
                // update end log
                $xJobCrawlLog->saveEndLog($startLogID, $this->totalJob, $this->newJob, $this->page);
            }
            // if running out of jobs, update estimated time
            if ($this->isRunningOutJobs == true) {
                $xJobCrawlLog->updateEstimatedTime($startLogID);
            }
        }
    }

    /**
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @todo Crawl all new jobs from Glints
     */
    public function crawlAllNewJobs()
    {
        $this->crawlAllNewJobsBySource($this->crawlSource);
    }

    /**
     * @param $crawlSource
     * @return array
     */
    private function getAllJobCrawlUrl($crawlSource)
    {
        // get time
        $timeStart = date('Y-m-d', strtotime("-45 days")) . ' 00:00:01';
        // get all crawl job url in last 45 days
        $arrCrawlJobUrl = XJobCrawl::find()->select('crawl_job_url')
            ->where(['crawl_source' => $crawlSource])
            ->andWhere(['>=', 'created', $timeStart])
            ->asArray()->all();
        // return only values
        return array_column($arrCrawlJobUrl, 'crawl_job_url');
    }

}