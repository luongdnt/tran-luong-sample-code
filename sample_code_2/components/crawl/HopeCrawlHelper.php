<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\crawl;

use Yii;
use common\models\Employer;

/**
 * Description of HopeGeoHelper
 *
 * @author Luongtn
 */
class HopeCrawlHelper {

    public static function crawlMywork($url){
         $html = file_get_contents($url);
         $crawlData =[];

         $dom = new \DOMDocument();
         @$dom->loadHTML($html);
         $finder = new \DOMXPath($dom);

         //company info
         $classname="company-info";
         $jobCompanyInfoNodes = $finder->query("//div[@class='{$classname}']/*[@class='job-company-info']/p");
         $jobTitleNode = $finder->query("//div[@class='title-job-info']");

         foreach ($jobTitleNode as $index => $node){
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML=trim($tmp_dom->saveHTML()); 
             $crawlData['job_crawl_title']= html_entity_decode(strip_tags($innerHTML));
         }

         foreach ($jobCompanyInfoNodes as $index => $node) 
         {                        
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML=trim($tmp_dom->saveHTML()); 
             if($index == 0){
                 $crawlData['job_crawl_place']= str_replace("Nơi làm việc:" , "" ,html_entity_decode(strip_tags($innerHTML)));
                 $crawlData['job_crawl_place']= str_replace("Nơi làm việc:" , "" ,$crawlData['job_crawl_place']);
             }elseif($index == 1){
                 $crawlData['job_crawl_type']= str_replace("Loại hình công việc:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif($index == 2){
                 $crawlData['job_crawl_salary']= str_replace("Mức lương:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }
         }

         // job info
         $classname="info_job";
         $jobInfoNodes = $finder->query("//div[@class='{$classname}']/li");
         foreach ($jobInfoNodes as $index => $node) 
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML= $tmp_dom->saveHTML();             
             if($index == 0){
                 $crawlData['job_crawl_experience']= str_replace("Kinh nghiệm" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif($index == 1){
                 $crawlData['job_crawl_position']= str_replace("Cấp bậc" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif($index == 3){
                 $crawlData['job_crawl_degree']= str_replace("Trình độ học vấn" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }
         }        

         // job skill
         $classname="desjob-company";
         $jobSkillNodes = $finder->query("//div[@class='{$classname}']/span");
         foreach ($jobSkillNodes as $index => $node)
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML= $tmp_dom->saveHTML();
             $crawlData['job_crawl_skills'] .= html_entity_decode(strip_tags($innerHTML)) ."|";
         } 

         // job description, benefit, requirement, 
         $classname="desjob-company";
         $jobDetailNodes = $finder->query("//div[@class='block_info contentdef']/div[@class='{$classname}']");
         foreach ($jobDetailNodes as $index => $node)
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML= $tmp_dom->saveHTML();
             if($index == 0){
                 $crawlData['job_crawl_description']= str_replace("Mô tả công việc" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif($index == 1){
                 $crawlData['job_crawl_benefit']= str_replace("Quyền lợi được hưởng" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif($index == 2 || $index == 3){
                 $crawlData['job_crawl_requirement'] .= str_replace("Yêu cầu công việc" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif($index == 4){
                 $crawlData['job_crawl_other_requirement'] .= str_replace("Yêu cầu khác" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }
         }         

         // job deadline
         $classname="job_deadline";
         $jobDeadlineNodes = $finder->query("//div[@class='{$classname}']");
         foreach ($jobDeadlineNodes as $index => $node)
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML= $tmp_dom->saveHTML();
             $crawlData['job_crawl_deadline'] .= str_replace("(Hạn nộp:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             $crawlData['job_crawl_deadline'] = str_replace(")" , "" ,$crawlData['job_crawl_deadline']);
         }        

         // company name
         $companyNameNodes = $finder->query("//h2[@class='fullname-company']");
         foreach ($companyNameNodes as $index => $node)
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML= $tmp_dom->saveHTML();
             $crawlData['company_crawl_name'] .= html_entity_decode(strip_tags($innerHTML));
         }  

         // company logo
         $companyLogoNodes = $finder->query("//div[@class='logo-company']/img");
         foreach ($companyLogoNodes as $index => $node)
         {
             $innerHTML= $node->getAttribute('src');
             $crawlData['company_crawl_logo'] .= $innerHTML;
         }  

         // company detail
         $classname="info-lienhe";
         $companyDetailNodes = $finder->query("//div[@class='{$classname}']/p");
         foreach ($companyDetailNodes as $index => $node)
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML= $tmp_dom->saveHTML();
             if($index == 0){
                 $crawlData['company_crawl_address']= html_entity_decode(strip_tags($innerHTML));
             }elseif(strpos(html_entity_decode(strip_tags($innerHTML)), 'Điện thoại:') !== false){
                 $crawlData['company_crawl_phone']= str_replace("Điện thoại:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif(strpos(html_entity_decode(strip_tags($innerHTML)), 'Website:') !== false){
                 $crawlData['company_crawl_website'] .= str_replace("Website:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif(strpos(html_entity_decode(strip_tags($innerHTML)), 'Quy mô công ty:') !== false){
                 $crawlData['company_crawl_number_people'] .= str_replace("Quy mô công ty:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }
         }      

         array_walk_recursive($crawlData, function(&$value) {
             $value = trim($value);
         });

         return $crawlData;
    }

    public static function crawlCareerLinkVN($url){
         $html = file_get_contents($url);
         $crawlData =[];

         $dom = new \DOMDocument();
         @$dom->loadHTML('<?xml encoding="utf-8" ?>' . $html);
         $finder = new \DOMXPath($dom);
         //job_title
         $classname="page-header job-header";        
         $jobTitleNode = $finder->query("//div[@class='$classname']/h1");

         foreach ($jobTitleNode as $index => $node){
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML=trim($tmp_dom->saveHTML()); 
             $crawlData['job_crawl_title']= html_entity_decode(strip_tags($innerHTML));
         }

         // company name
         $classname="list-unstyled critical-job-data";        
         $arrCompanyNameNodes = $finder->query("//div[@class='job-data']/ul[@class='$classname']/li");
         foreach ($arrCompanyNameNodes as $index => $node)
         {                        
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML=trim($tmp_dom->saveHTML()); 
             if($index == 0){
                 $crawlData['company_crawl_name']= str_replace("Nơi làm việc:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif($index == 1){
                 $crawlData['company_crawl_address']= str_replace("Loại hình công việc:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif($index == 2){
                 $crawlData['job_crawl_salary']= str_replace("Lương:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }
         }

         // job description, benefit, requirement
         $classname="job-data";
         $jobDetailNodes = $finder->query("//div[@class='$classname']/div");
         foreach ($jobDetailNodes as $index => $node)
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML= $tmp_dom->saveHTML();
             if($index == 1){
                 $crawlData['job_crawl_description']= str_replace("Mô tả công việc" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif($index == 2){
                 $crawlData['job_crawl_requirement'] .= str_replace("Yêu cầu công việc" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }

             if(strpos($crawlData['job_crawl_description'], 'Quyền lợi:')){
                 $startBenefit = strpos($crawlData['job_crawl_description'], 'Quyền lợi:');

                 $crawlData['job_crawl_benefit']= substr($crawlData['job_crawl_description'], $startBenefit);
                 $crawlData['job_crawl_description'] = substr($crawlData['job_crawl_description'], 0, $startBenefit);
             }
         }   

         // job info
         $classname="job-data";
         $jobDetailNodes = $finder->query("//div[@class='$classname']/ul[@class='list-unstyled']/li");
         foreach ($jobDetailNodes as $index => $node)
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML= $tmp_dom->saveHTML();             
             if(strpos(html_entity_decode(strip_tags($innerHTML)), 'Cấp bậc') != false){
                 $crawlData['job_crawl_position']= str_replace("Cấp bậc:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif(strpos(html_entity_decode(strip_tags($innerHTML)), 'Nơi làm việc') != false){
                 $crawlData['job_crawl_place']= str_replace("Nơi làm việc:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif(strpos(html_entity_decode(strip_tags($innerHTML)), 'Trình độ học vấn') != false){
                 $crawlData['job_crawl_degree']= str_replace("Trình độ học vấn:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif(strpos(html_entity_decode(strip_tags($innerHTML)), 'Mức kinh nghiệm') != false){
                 $crawlData['job_crawl_experience']= str_replace("Mức kinh nghiệm:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif(strpos(html_entity_decode(strip_tags($innerHTML)), 'Loại công việc') != false){
                 $crawlData['job_crawl_type']= str_replace("Loại công việc:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }
         }    

         // job category
         $classname="job-data";
         $jobCategoryNodes = $finder->query("//div[@class='$classname']/ul[@class='list-unstyled']/li[1]/ul/li");
         foreach ($jobCategoryNodes as $index => $node)
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML= $tmp_dom->saveHTML();             
             $crawlData['job_crawl_category'] .= str_replace("Ngành nghề việc làm:" , "" ,html_entity_decode(strip_tags($innerHTML))) ."|";
         }    

         // job deadline
         $classname="job-data";
         $jobCategoryNodes = $finder->query("//div[@class='$classname']/dl/dd[2]");
         foreach ($jobCategoryNodes as $index => $node)
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML= $tmp_dom->saveHTML();             
             $crawlData['job_crawl_deadline'] = str_replace("Ngành nghề việc làm:" , "" ,html_entity_decode(strip_tags($innerHTML)));
         }    

         // company logo
         $companyLogoNodes = $finder->query("//div[@class='job-side-data']/p[@class='text-center']/img");
         foreach ($companyLogoNodes as $index => $node)
         {
             $innerHTML= $node->getAttribute('src');
             $crawlData['company_crawl_logo'] .= "https://www.careerlink.vn" . $innerHTML;
         }  

         // company website
         $classname="job-side-data";
         $companyWebsiteNodes = $finder->query("//div[@class='{$classname}']/dl/dd");
         foreach ($companyWebsiteNodes as $index => $node)
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML= $tmp_dom->saveHTML();             
             $crawlData['company_crawl_website'] = str_replace("Ngành nghề việc làm:" , "" ,html_entity_decode(strip_tags($innerHTML)));            
         }      

         // company number people
         $classname="job-side-data";
         $companyWebsiteNodes = $finder->query("//div[@class='{$classname}']/ul/li");
         foreach ($companyWebsiteNodes as $index => $node)
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML= $tmp_dom->saveHTML();    
             if($index==0){
                 $crawlData['company_crawl_number_people'] = str_replace("Số nhân viên:" , "" ,html_entity_decode(strip_tags($innerHTML)));            
             }            
         }      

         // company description
         $classname="job-side-data";
         $companyWebsiteNodes = $finder->query("//div[@class='{$classname}']/div");
         foreach ($companyWebsiteNodes as $index => $node)
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML= $tmp_dom->saveHTML();    
             if($index==0){
                 $crawlData['company_crawl_description'] = str_replace("Số nhân viên:" , "" ,html_entity_decode(strip_tags($innerHTML)));            
             }            
         }   

         array_walk_recursive($crawlData, function(&$value) {
             $value = trim($value);
         });

         return $crawlData;
    }

    /**
     * Lấy các thông tin job từ website về để insert
     * @param type $url
     * @return type
     */
    public static function crawlVieclamtuoitre($url){
         $html = file_get_contents($url);
         $crawlData =[];

         $dom = new \DOMDocument();
         @$dom->loadHTML($html);
         $finder = new \DOMXPath($dom);

         //job_title
         $classname="top-job-info";
         
         $jobTitleNode = $finder->query("//div[@class='{$classname}']/h1");
         foreach ($jobTitleNode as $index => $node){
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML=trim($tmp_dom->saveHTML()); 
             $crawlData['job_crawl_title']= html_entity_decode(strip_tags($innerHTML));
         }
         
         // job_detail
         $classname = "MyJobDetail";
         $jobInfoNodes = $finder->query("//div[@class='{$classname}']//ul[@class='DetailJobNew']//p");
         foreach ($jobInfoNodes as $index => $node) 
         {                        
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML=trim($tmp_dom->saveHTML());
             
             $info = html_entity_decode(strip_tags($innerHTML));
             
             if(strpos($info, "Nơi làm việc:") !== false){
                 $crawlData['job_crawl_place']= str_replace("Nơi làm việc:" , "" ,html_entity_decode(strip_tags($innerHTML)));
                 $crawlData['job_crawl_place']= str_replace("Nơi làm việc:" , "" ,$crawlData['job_crawl_place']);
             }elseif(strpos($info, "Cấp bậc:") !== false){
                 $crawlData['job_crawl_position']= str_replace("Cấp bậc:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif(strpos($info, "Kinh nghiệm:") !== false){
                 $crawlData['job_crawl_experience']= str_replace("Kinh nghiệm:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif(strpos($info, "Lương:") !== false){
                 $crawlData['job_crawl_salary']= str_replace("Lương:" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif(strpos($info, "Ngành nghề:") !== false){
                 $crawlData['job_crawl_category']= str_replace("Ngành nghề:" , "" ,html_entity_decode(strip_tags($innerHTML)));   
             }elseif(strpos($info, "Hết hạn nộp:") !== false){
                 $crawlData['job_crawl_deadline']= str_replace("Hết hạn nộp:" , "" ,html_entity_decode(strip_tags($innerHTML)));     
                 $crawlData['job_crawl_deadline'] = str_replace("/","-", $crawlData['job_crawl_deadline']);   
             }                                                    
         }

         // job info
         $classname="MyJobDetail";
         $jobInfoNodes = $finder->query("//div[@class='{$classname}']//div[@class='MarBot20']");
         foreach ($jobInfoNodes as $index => $node) 
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML=trim($tmp_dom->saveHTML());
             $info = html_entity_decode(strip_tags($innerHTML));
             if(strpos($info, "Mô tả Công việc") !== false){
                 $crawlData['job_crawl_description']= str_replace("Mô tả Công việc" , "" ,html_entity_decode(strip_tags($innerHTML)));
             }elseif(strpos($info, "Yêu Cầu Công Việc") !== false){
                 $requirement = str_replace("Yêu Cầu Công Việc" , "" ,html_entity_decode(strip_tags($innerHTML)));
                 $arrReuirement = explode("QUYỀN LỢI", $requirement);
                 $crawlData['job_crawl_requirement'] = $arrReuirement[0];
                 $crawlData['job_crawl_benefit'] = $arrReuirement[1];
             }elseif(strpos($info, "Thông tin khác") !== false){
                 $otherInfoNode = $finder->query("div[@class='content_fck']/ul/li", $node);
                foreach($otherInfoNode AS $index => $otherInfo){                       
                    $tmp_dom = new \DOMDocument(); 
                    $tmp_dom->appendChild($tmp_dom->importNode($otherInfo,true));
                    $info = html_entity_decode(strip_tags($tmp_dom->saveHTML()));
                    if(strpos($info, "Bằng cấp") !== false){
                        $crawlData['job_crawl_degree'] = str_replace("Bằng cấp:", "", $info);
                    }elseif(strpos($info, "Hình thức") !== false){
                        $crawlData['job_crawl_type'] = str_replace("Hình thức:", "", $info);
                    }else{
                        $crawlData['job_crawl_other_requirement'] .= $info;
                    }
                }         
             }
         }                     

         // company name
         $companyNameNodes = $finder->query("//div[@class='box1Detail']/p[@class='TitleDetailNew']/span");
         foreach ($companyNameNodes as $index => $node)
         {
             $tmp_dom = new \DOMDocument(); 
             $tmp_dom->appendChild($tmp_dom->importNode($node,true));
             $innerHTML= $tmp_dom->saveHTML();
             $crawlData['company_crawl_name'] .= html_entity_decode(strip_tags($innerHTML));
         }  

         // company logo
         $companyLogoNodes = $finder->query("//div[@class='box1Detail']/div[@class='align_center logocompany']//img");
         foreach ($companyLogoNodes as $index => $node)
         {
             $innerHTML= $node->getAttribute('src');
             $crawlData['company_crawl_logo'] .= $innerHTML;
         }           

         array_walk_recursive($crawlData, function(&$value) {
             $value = trim($value);
         });

         return $crawlData;
    }
    
    /**
     * Lấy danh sách các place
     * @param type $strPlace
     * @param type $crawlSource
     * @return type
     */
    public static function getJobCrawlPlace($strPlace, $crawlSource){
         if($crawlSource == "My Work")
         {
            $arrPlace = explode(",", $strPlace);
            $arrPlaceResult = \common\models\Place::find()
                    ->select('place_name, place_id')
                    ->where(['place_name' => $arrPlace])
                    ->asArray()
                    ->all();

            return $arrPlaceResult;

        }elseif($crawlSource == "Careerlink VN")
         {
            $arrPlace = explode(",", trim($strPlace));
            foreach ($arrPlace AS $index => $place){
                $arrPlace[$index] = trim($place);
            }
            $arrPlaceResult = \common\models\Place::find()
                    ->select('place_name, place_id')
                    ->where(['place_name' => $arrPlace])
                    ->asArray()
                    ->all();
            return $arrPlaceResult;

        }elseif($crawlSource == "Vietnam Works")
         {
            $arrPlace = explode(",", $strPlace);
            $arrPlaceResult = \common\models\Place::find()
                    ->select('place_name, place_id')
                    ->where(['place_name' => $arrPlace])
                    ->asArray()
                    ->all();

            return $arrPlaceResult;

        }else
         {
            $arrPlace = explode(",", trim($strPlace));
            foreach ($arrPlace AS $index => $place){
                $arrPlace[$index] = trim($place);
            }
            $arrPlaceResult = \common\models\Place::find()
                    ->select('place_name, place_id')
                    ->where(['place_name' => $arrPlace])
                    ->asArray()
                    ->all();
            return $arrPlaceResult;

        }

         return [['place_name' => "Hà Nội", "place_id" => 1]];
    }

    /**
     * Lấy danh sách các category
     * @param type $strCategory
     * @param type $crawlSource
     * @return type
     */
    public static function getJobCrawlCategory($jobTitle, $companyName, $crawlSource){

         if($crawlSource == "My Work")
         {
            try{
                $arrCategoryId = \common\models\XJobCrawlBasic::find()
                    ->select('x_keyword_to_crawl.job_category_id')                   
                    ->leftJoin('x_keyword_to_crawl','x_keyword_to_crawl.keyword = x_job_crawl_basic.keyword')
                    ->distinct()
                    ->where(['x_job_crawl_basic.job_title'=>$jobTitle
                            , 'x_job_crawl_basic.company'=> $companyName
                            , 'x_job_crawl_basic.source' => "My Work"])
                    ->asArray()
                    ->all();
                 $arrCategoryId = \yii\helpers\ArrayHelper::getColumn($arrCategoryId, 'job_category_id');

                 $arrCategoryIdVn = \common\models\XJobCrawlBasic::find()
                    ->select('x_keyword_to_crawl.job_category_id')                   
                    ->leftJoin('x_keyword_to_crawl','x_keyword_to_crawl.keyword_vn = x_job_crawl_basic.keyword')
                    ->distinct()
                    ->where(['x_job_crawl_basic.job_title'=>$jobTitle
                            , 'x_job_crawl_basic.company'=> $companyName
                            , 'x_job_crawl_basic.source' => "My Work"])
                    ->asArray()
                    ->all();
                 $arrCategoryIdVn = \yii\helpers\ArrayHelper::getColumn($arrCategoryIdVn, 'job_category_id');

                 $arrCategoryId = array_unique (array_merge ($arrCategoryId, $arrCategoryIdVn));

                 if(empty($arrCategoryId)){
                     return [9999];
                 }
            } catch (\Exception $ex) {
                print_r($ex->getMessage());exit;
            }

            return $arrCategoryId;           
         }elseif($crawlSource == "Careerlink VN")
         {
            try{
                $arrCategoryId = \common\models\XJobCrawlBasic::find()
                    ->select('x_keyword_to_crawl.job_category_id')                   
                    ->leftJoin('x_keyword_to_crawl','x_keyword_to_crawl.keyword = x_job_crawl_basic.keyword')
                    ->distinct()
                    ->where(['x_job_crawl_basic.job_title'=>$jobTitle
                            , 'x_job_crawl_basic.company'=> $companyName
                            , 'x_job_crawl_basic.source' => $crawlSource])
                    ->asArray()
                    ->all();
                 $arrCategoryId = \yii\helpers\ArrayHelper::getColumn($arrCategoryId, 'job_category_id');

                 $arrCategoryIdVn = \common\models\XJobCrawlBasic::find()
                    ->select('x_keyword_to_crawl.job_category_id')                   
                    ->leftJoin('x_keyword_to_crawl','x_keyword_to_crawl.keyword_vn = x_job_crawl_basic.keyword')
                    ->distinct()
                    ->where(['x_job_crawl_basic.job_title'=>$jobTitle
                            , 'x_job_crawl_basic.company'=> $companyName
                            , 'x_job_crawl_basic.source' => $crawlSource])
                    ->asArray()
                    ->all();
                 $arrCategoryIdVn = \yii\helpers\ArrayHelper::getColumn($arrCategoryIdVn, 'job_category_id');

                 $arrCategoryId = array_unique (array_merge ($arrCategoryId, $arrCategoryIdVn));

                 if(empty($arrCategoryId)){
                     return [9999];
                 }
            } catch (\Exception $ex) {
                print_r($ex->getMessage());exit;
            }


            return $arrCategoryId;

         }elseif($crawlSource == "Vietnam Works")
         {
            try{
                $arrCategoryId = \common\models\XJobCrawlBasic::find()
                    ->select('x_keyword_to_crawl.job_category_id')                   
                    ->leftJoin('x_keyword_to_crawl','x_keyword_to_crawl.keyword = x_job_crawl_basic.keyword')
                    ->distinct()
                    ->where(['x_job_crawl_basic.job_title'=>$jobTitle
                            , 'x_job_crawl_basic.company'=> $companyName
                            , 'x_job_crawl_basic.source' => $crawlSource])
                    ->asArray()
                    ->all();
                 $arrCategoryId = \yii\helpers\ArrayHelper::getColumn($arrCategoryId, 'job_category_id');

                 $arrCategoryIdVn = \common\models\XJobCrawlBasic::find()
                    ->select('x_keyword_to_crawl.job_category_id')                   
                    ->leftJoin('x_keyword_to_crawl','x_keyword_to_crawl.keyword_vn = x_job_crawl_basic.keyword')
                    ->distinct()
                    ->where(['x_job_crawl_basic.job_title'=>$jobTitle
                            , 'x_job_crawl_basic.company'=> $companyName
                            , 'x_job_crawl_basic.source' => $crawlSource])
                    ->asArray()
                    ->all();
                 $arrCategoryIdVn = \yii\helpers\ArrayHelper::getColumn($arrCategoryIdVn, 'job_category_id');

                 $arrCategoryId = array_unique (array_merge ($arrCategoryId, $arrCategoryIdVn));               

                 if(empty($arrCategoryId)){
                     return [9999];
                 }
            } catch (\Exception $ex) {
                print_r($ex->getMessage());exit;
            }


            return $arrCategoryId;

         }else
         {
            try{
                $arrCategoryId = \common\models\XJobCrawlBasic::find()
                    ->select('x_keyword_to_crawl.job_category_id')                   
                    ->leftJoin('x_keyword_to_crawl','x_keyword_to_crawl.keyword = x_job_crawl_basic.keyword')
                    ->distinct()
                    ->where(['x_job_crawl_basic.job_title'=>$jobTitle
                            , 'x_job_crawl_basic.company'=> $companyName
                            , 'x_job_crawl_basic.source' => $crawlSource])
                    ->asArray()
                    ->all();
                 $arrCategoryId = \yii\helpers\ArrayHelper::getColumn($arrCategoryId, 'job_category_id');

                 $arrCategoryIdVn = \common\models\XJobCrawlBasic::find()
                    ->select('x_keyword_to_crawl.job_category_id')                   
                    ->leftJoin('x_keyword_to_crawl','x_keyword_to_crawl.keyword_vn = x_job_crawl_basic.keyword')
                    ->distinct()
                    ->where(['x_job_crawl_basic.job_title'=>$jobTitle
                            , 'x_job_crawl_basic.company'=> $companyName
                            , 'x_job_crawl_basic.source' => $crawlSource])
                    ->asArray()
                    ->all();
                 $arrCategoryIdVn = \yii\helpers\ArrayHelper::getColumn($arrCategoryIdVn, 'job_category_id');

                 $arrCategoryId = array_unique (array_merge ($arrCategoryId, $arrCategoryIdVn));               

                 if(empty($arrCategoryId)){
                     return [9999];
                 }
            } catch (\Exception $ex) {
                print_r($ex->getMessage());exit;
            }


            return $arrCategoryId;

         }

         return [9999];
    }

    /**
     * Lấy degree
     * @param type $strDegree
     * @param type $crawlSource
     * @return type
     */
    public static function getJobCrawlDegree($strDegree, $crawlSource){
        
        $strDegree = strtolower($strDegree);
        
         if($crawlSource == "My Work")
         {
            if($strDegree == "Trung cấp"){
                return ['degree_name' => "Trung cấp - Nghề", "degree_id" => 1];
            }elseif($strDegree == "Cao đẳng"){
                return ['degree_name' => "Cao đẳng", "degree_id" => 2];
            }elseif($strDegree == "Đại học" || $strDegree == "Kỹ sư" || $strDegree == "Cử nhân"){
                return ['degree_name' => "Đại học", "degree_id" => 3];
            }elseif($strDegree == "Trên đại học" || $strDegree == "Thạc sĩ" || $strDegree == "Thạc sĩ Y học"){
                return ['degree_name' => "Sau Đại Học", "degree_id" => 4];
            }else{
                return ['degree_name' => "Không yêu cầu", "degree_id" => 0];
            }
        }elseif($crawlSource == "Careerlink VN")
         {
            if($strDegree == "Trung cấp"){
                return ['degree_name' => "Trung cấp - Nghề", "degree_id" => 1];
            }elseif($strDegree == "Cao đẳng"){
                return ['degree_name' => "Cao đẳng", "degree_id" => 2];
            }elseif($strDegree == "Đại học" || $strDegree == "Kỹ sư" || $strDegree == "Cử nhân"){
                return ['degree_name' => "Đại học", "degree_id" => 3];
            }elseif($strDegree == "Trên đại học" || $strDegree == "Thạc sĩ" || $strDegree == "Thạc sĩ Y học"){
                return ['degree_name' => "Sau Đại Học", "degree_id" => 4];
            }else{
                return ['degree_name' => "Trung cấp - Nghề", "degree_id" => 1];
            }
         }elseif($crawlSource == "Vietnam Works")
         {
            if($strDegree == "Trung cấp"){
                return ['degree_name' => "Trung cấp - Nghề", "degree_id" => 1];
            }elseif($strDegree == "Cao đẳng"){
                return ['degree_name' => "Cao đẳng", "degree_id" => 2];
            }elseif($strDegree == "Đại học" || $strDegree == "Kỹ sư" || $strDegree == "Cử nhân"){
                return ['degree_name' => "Đại học", "degree_id" => 3];
            }elseif($strDegree == "Trên đại học" || $strDegree == "Thạc sĩ" || $strDegree == "Thạc sĩ Y học"){
                return ['degree_name' => "Sau Đại Học", "degree_id" => 4];
            }else{
                return ['degree_name' => "Không yêu cầu", "degree_id" => 0];
            }
         }else
         {
            if($strDegree == "trung cấp"){
                return ['degree_name' => "Trung cấp - Nghề", "degree_id" => 1];
            }elseif($strDegree == "cao đẳng"){
                return ['degree_name' => "Cao đẳng", "degree_id" => 2];
            }elseif($strDegree == "đại học" || $strDegree == "Kỹ sư" || $strDegree == "cử nhân"){
                return ['degree_name' => "Đại học", "degree_id" => 3];
            }elseif($strDegree == "trên đại học" || $strDegree == "thạc sĩ" || $strDegree == "thạc sĩ y học"){
                return ['degree_name' => "Sau Đại Học", "degree_id" => 4];
            }else{
                return ['degree_name' => "Trung cấp - Nghề", "degree_id" => 1];
            }
         }

        return ['degree_name' => "Cao đẳng", "degree_id" => 2];
    }

    /**
     * Lấy mức lương
     * @param type $strSalary
     * @param type $crawlSource
     * @return type
     */
    public static function getJobCrawlSalary($strSalary, $crawlSource){
         if($crawlSource == "My Work")
         {
            // Loại bỏ đơn vị
            $strSalary = str_replace(["VND", "VNĐ",",","(Có phần trăm hoa hồng)"], "", $strSalary);

            if(strstr($strSalary,"Cạnh tranh") || strstr($strSalary,"Thương lượng")){
                return ["minSalary" => 0, "maxSalary" => 0];
            }elseif(strstr($strSalary,"Hơn") || strstr($strSalary,"Tối thiểu")){
                preg_match('/.*[0-9]/', $strSalary, $matches); // Loại các phần đằng sau
                preg_match('/[0-9].*/', $matches[0], $matches); // Loại các phần đằng trước
                $salaryValue = $matches[0];
                if(strstr($strSalary, 'USD')){
                    $salaryValue = $salaryValue * 22800;
                }
                return ['minSalary' => $salaryValue/1000000, "maxSalary" => 0];
            }else{
                $arrSalary = explode("-" , $strSalary);
                if(trim($arrSalary[0]) && trim($arrSalary[1])){
                    $minSalary = strstr($arrSalary[0],"USD") ? str_replace("USD", "", trim($arrSalary[0])) * 22000 :  trim($arrSalary[0]);
                    $maxSalary = strstr($arrSalary[1],"USD") ? str_replace("USD", "", trim($arrSalary[1])) * 22000 :  trim($arrSalary[1]);
                }elseif(trim($arrSalary[0])){
                     $minSalary = strstr($arrSalary[0],"USD") ? str_replace("USD", "", trim($arrSalary[0])) * 22000 :  trim($arrSalary[0]);
                     $maxSalary = 0; 
                }else{
                    $minSalary = 0;
                    $maxSalary = 0;

                }
                return ['minSalary' => $minSalary/1000000, "maxSalary" => $maxSalary/1000000];
            }

         }elseif($crawlSource == "Careerlink VN")
         {
            // Loại bỏ đơn vị
            $strSalary = str_replace(["VND", "VNĐ",",","(Có phần trăm hoa hồng)"], "", $strSalary);

            if(strstr($strSalary,"Cạnh tranh") || strstr($strSalary,"Thương lượng")){
                return ["minSalary" => 0, "maxSalary" => 0];
            }elseif(strstr($strSalary,"Hơn") || strstr($strSalary,"Tối thiểu")){
                preg_match('/.*[0-9]/', $strSalary, $matches); // Loại các phần đằng sau
                preg_match('/[0-9].*/', $matches[0], $matches); // Loại các phần đằng trước
                $salaryValue = $matches[0];
                if(strstr($strSalary, 'USD')){
                    $salaryValue = $salaryValue * 22800;
                }
                return ['minSalary' => $salaryValue/1000000, "maxSalary" => 0];
            }else{
                $arrSalary = explode("-" , $strSalary);
                if(trim($arrSalary[0]) && trim($arrSalary[1])){
                    $minSalary = strstr($arrSalary[0],"USD") ? str_replace("USD", "", trim($arrSalary[0])) * 22000 :  trim($arrSalary[0]);
                    $maxSalary = strstr($arrSalary[1],"USD") ? str_replace("USD", "", trim($arrSalary[1])) * 22000 :  trim($arrSalary[1]);
                }elseif(trim($arrSalary[0])){
                     $minSalary = strstr($arrSalary[0],"USD") ? str_replace("USD", "", trim($arrSalary[0])) * 22000 :  trim($arrSalary[0]);
                     $maxSalary = 0; 
                }else{
                    $minSalary = 0;
                    $maxSalary = 0;

                }
                return ['minSalary' => $minSalary/1000000, "maxSalary" => $maxSalary/1000000];
            }

         }elseif($crawlSource == "Vietnam Works")
         {
            // Loại bỏ đơn vị
            $strSalary = str_replace(["VND", "VNĐ",",","(Có phần trăm hoa hồng)"], "", $strSalary);

            if(strstr($strSalary,"Cạnh tranh") || strstr($strSalary,"Thương lượng")){
                return ["minSalary" => 0, "maxSalary" => 0];
            }elseif(strstr($strSalary,"Hơn") || strstr($strSalary,"Tối thiểu")){
                preg_match('/.*[0-9]/', $strSalary, $matches); // Loại các phần đằng sau
                preg_match('/[0-9].*/', $matches[0], $matches); // Loại các phần đằng trước
                $salaryValue = $matches[0];
                if(strstr($strSalary, 'USD')){
                    $salaryValue = $salaryValue * 22800;
                }
                return ['minSalary' => $salaryValue/1000000, "maxSalary" => 0];
            }else{
                $arrSalary = explode("-" , $strSalary);
                if(trim($arrSalary[0]) && trim($arrSalary[1])){
                    $minSalary = strstr($arrSalary[0],"USD") ? str_replace("USD", "", trim($arrSalary[0])) * 22000 :  trim($arrSalary[0]);
                    $maxSalary = strstr($arrSalary[1],"USD") ? str_replace("USD", "", trim($arrSalary[1])) * 22000 :  trim($arrSalary[1]);
                }elseif(trim($arrSalary[0])){
                     $minSalary = strstr($arrSalary[0],"USD") ? str_replace("USD", "", trim($arrSalary[0])) * 22000 :  trim($arrSalary[0]);
                     $maxSalary = 0; 
                }else{
                    $minSalary = 0;
                    $maxSalary = 0;

                }
                return ['minSalary' => $minSalary/1000000, "maxSalary" => $maxSalary/1000000];
            }

         }else{
            // Loại bỏ đơn vị
            $strSalary = str_replace(["VND", "VNĐ",",","(Có phần trăm hoa hồng)"], "", $strSalary);

            if(strstr($strSalary,"Cạnh tranh") || strstr($strSalary,"Thương lượng")){
                return ["minSalary" => 0, "maxSalary" => 0];
            }elseif(strstr($strSalary,"Hơn") || strstr($strSalary,"Tối thiểu")){
                preg_match('/.*[0-9]/', $strSalary, $matches); // Loại các phần đằng sau
                preg_match('/[0-9].*/', $matches[0], $matches); // Loại các phần đằng trước
                $salaryValue = $matches[0];
                if(strstr($strSalary, 'USD')){
                    $salaryValue = $salaryValue * 22800;
                }
                return ['minSalary' => $salaryValue/1000000, "maxSalary" => 0];
            }else{
                $arrSalary = explode("-" , $strSalary);
                if(trim($arrSalary[0]) && trim($arrSalary[1])){
                    $minSalary = strstr($arrSalary[0],"USD") ? str_replace("USD", "", trim($arrSalary[0])) * 22000 :  trim($arrSalary[0]);
                    $maxSalary = strstr($arrSalary[1],"USD") ? str_replace("USD", "", trim($arrSalary[1])) * 22000 :  trim($arrSalary[1]);
                }elseif(trim($arrSalary[0])){
                     $minSalary = strstr($arrSalary[0],"USD") ? str_replace("USD", "", trim($arrSalary[0])) * 22000 :  trim($arrSalary[0]);
                     $maxSalary = 0; 
                }else{
                    $minSalary = 0;
                    $maxSalary = 0;

                }
                return ['minSalary' => $minSalary/1000000, "maxSalary" => $maxSalary/1000000];
            }

         }

         return ['minSalary' => 0, "maxSalary" => 0];
    }

    /**
     * Lấy loại công việc (toàn thời gian, bán thời gian...)
     * @param type $strType
     * @param type $crawlSource
     * @return type
     */
    public static function getJobCrawlType($strType, $crawlSource){
         if($crawlSource == "My Work")
         {
             if(strstr($strType, "Toàn thời gian") || strstr($strType, "hợp đồng") || strstr($strType, "Hợp đồng")){
                 return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
             }elseif(strstr($strType, "Bán thời gian")){
                 return ['job_type_name' => "Bán thời gian", "job_type_id" => 2];
             }elseif(strstr($strType, "Thực tập")){
                 return ['job_type_name' => "Thực tập", "job_type_id" => 5];
             }else{
                 return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
             }
         }elseif($crawlSource == "Careerlink VN")
         {
            if(strstr($strType, "Toàn thời gian") || strstr($strType, "hợp đồng") || strstr($strType, "Hợp đồng")){
                return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
            }elseif(strstr($strType, "Bán thời gian")){
                return ['job_type_name' => "Bán thời gian", "job_type_id" => 2];
            }elseif(strstr($strType, "Thực tập")){
                return ['job_type_name' => "Thực tập", "job_type_id" => 5];
            }else{
                return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
            }
         }elseif($crawlSource == "Vietnam Works")
         {
            if(strstr($strType, "Toàn thời gian") || strstr($strType, "hợp đồng") || strstr($strType, "Hợp đồng")){
                return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
            }elseif(strstr($strType, "Bán thời gian")){
                return ['job_type_name' => "Bán thời gian", "job_type_id" => 2];
            }elseif(strstr($strType, "Thực tập")){
                return ['job_type_name' => "Thực tập", "job_type_id" => 5];
            }else{
                return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
            }
         }else
         {
            if(strstr($strType, "Toàn thời gian") || strstr($strType, "hợp đồng") || strstr($strType, "Hợp đồng")){
                return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
            }elseif(strstr($strType, "Bán thời gian")){
                return ['job_type_name' => "Bán thời gian", "job_type_id" => 2];
            }elseif(strstr($strType, "Thực tập")){
                return ['job_type_name' => "Thực tập", "job_type_id" => 5];
            }else{
                return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
            }
         }

         return ['job_type_name' => "Toàn thời gian", "job_type_id" => 1];
    }

    /**
     * Lấy năm kinh nghiệm
     * @param type $strYearExp
     * @param type $crawlSource
     * @return type
     */
    public static function getJobCrawlYearExp($strYearExp, $crawlSource){
         if($crawlSource == "My Work")
         {
            // Loại bỏ đơn vị
            $strYearExp = str_replace(["năm", "kinh nghiệm"], "", $strYearExp);
            if(strstr($strYearExp,"Không yêu cầu") || strstr($strYearExp,"Chưa có")){
                return ["minYearExp" => 0, "maxYearExp" => 0];
            }elseif(strstr($strYearExp,"Dưới") || strstr($strYearExp,"Tối thiểu")){
                preg_match('/.*[0-9]/', $strYearExp, $matches); // Loại các phần đằng sau
                preg_match('/[0-9].*/', $matches[0], $matches); // Loại các phần đằng trước
                $yearValue = $matches[0];
                return ['minYearExp' => 0, "maxYearExp" => $yearValue];
            }elseif(strstr($strYearExp,"trở lên") || strstr($strYearExp,"Hơn")){
                preg_match('/.*[0-9]/', $strYearExp, $matches); // Loại các phần đằng sau
                preg_match('/[0-9].*/', $matches[0], $matches); // Loại các phần đằng trước

                $yearValue = $matches[0];
                return ['minYearExp' => $yearValue, "maxYearExp" => 0];
            }else{
                $arrYearExp = explode("-" , $strYearExp);
                if(trim($arrYearExp[0]) && trim($arrYearExp[1])){
                    $minYearExp = trim($arrYearExp[0]);
                    $maxYearExp =  trim($arrYearExp[1]);
                }elseif(trim($arrYearExp[0])){
                     $minYearExp = trim($arrYearExp[0]);
                     $maxYearExp = 0; 
                }else{
                    $minYearExp = 0;
                    $minYearExp = 0;

                }
                return ['minYearExp' => $minYearExp, "maxYearExp" => $maxYearExp];
            }

         }elseif($crawlSource == "Careerlink VN")
         {
            // Loại bỏ đơn vị
            $strYearExp = str_replace(["năm", "kinh nghiệm"], "", $strYearExp);
            if(strstr($strYearExp,"Không yêu cầu") || strstr($strYearExp,"Chưa có")){
                return ["minYearExp" => 0, "maxYearExp" => 0];
            }elseif(strstr($strYearExp,"Dưới") || strstr($strYearExp,"Tối thiểu")){
                preg_match('/.*[0-9]/', $strYearExp, $matches); // Loại các phần đằng sau
                preg_match('/[0-9].*/', $matches[0], $matches); // Loại các phần đằng trước
                $yearValue = $matches[0];
                return ['minYearExp' => 0, "maxYearExp" => $yearValue];
            }elseif(strstr($strYearExp,"trở lên") || strstr($strYearExp,"Hơn")){
                preg_match('/.*[0-9]/', $strYearExp, $matches); // Loại các phần đằng sau
                preg_match('/[0-9].*/', $matches[0], $matches); // Loại các phần đằng trước

                $yearValue = $matches[0];
                return ['minYearExp' => $yearValue, "maxYearExp" => 0];
            }else{
                $arrYearExp = explode("-" , $strYearExp);
                if(trim($arrYearExp[0]) && trim($arrYearExp[1])){
                    $minYearExp = trim($arrYearExp[0]);
                    $maxYearExp =  trim($arrYearExp[1]);
                }elseif(trim($arrYearExp[0])){
                     $minYearExp = trim($arrYearExp[0]);
                     $maxYearExp = 0; 
                }else{
                    $minYearExp = 0;
                    $minYearExp = 0;

                }
                return ['minYearExp' => $minYearExp, "maxYearExp" => $maxYearExp];
            }

        }elseif($crawlSource == "Vietnam Works")
         {
            // Loại bỏ đơn vị
            $strYearExp = str_replace(["năm", "kinh nghiệm"], "", $strYearExp);
            if(strstr($strYearExp,"Không yêu cầu") || strstr($strYearExp,"Chưa có")){
                return ["minYearExp" => 0, "maxYearExp" => 0];
            }elseif(strstr($strYearExp,"Dưới") || strstr($strYearExp,"Tối thiểu")){
                preg_match('/.*[0-9]/', $strYearExp, $matches); // Loại các phần đằng sau
                preg_match('/[0-9].*/', $matches[0], $matches); // Loại các phần đằng trước
                $yearValue = $matches[0];
                return ['minYearExp' => 0, "maxYearExp" => $yearValue];
            }elseif(strstr($strYearExp,"trở lên") || strstr($strYearExp,"Hơn")){
                preg_match('/.*[0-9]/', $strYearExp, $matches); // Loại các phần đằng sau
                preg_match('/[0-9].*/', $matches[0], $matches); // Loại các phần đằng trước

                $yearValue = $matches[0];
                return ['minYearExp' => $yearValue, "maxYearExp" => 0];
            }else{
                $arrYearExp = explode("-" , $strYearExp);
                if(trim($arrYearExp[0]) && trim($arrYearExp[1])){
                    $minYearExp = trim($arrYearExp[0]);
                    $maxYearExp =  trim($arrYearExp[1]);
                }elseif(trim($arrYearExp[0])){
                     $minYearExp = trim($arrYearExp[0]);
                     $maxYearExp = 0; 
                }else{
                    $minYearExp = 0;
                    $minYearExp = 0;

                }
                return ['minYearExp' => $minYearExp, "maxYearExp" => $maxYearExp];
            }

        }else
         {
            // Loại bỏ đơn vị
            $strYearExp = str_replace(["năm", "kinh nghiệm"], "", $strYearExp);
            if(strstr($strYearExp,"Không yêu cầu") || strstr($strYearExp,"Chưa có")){
                return ["minYearExp" => 0, "maxYearExp" => 0];
            }elseif(strstr($strYearExp,"Dưới") || strstr($strYearExp,"Tối thiểu")){
                preg_match('/.*[0-9]/', $strYearExp, $matches); // Loại các phần đằng sau
                preg_match('/[0-9].*/', $matches[0], $matches); // Loại các phần đằng trước
                $yearValue = $matches[0];
                return ['minYearExp' => 0, "maxYearExp" => $yearValue];
            }elseif(strstr($strYearExp,"trở lên") || strstr($strYearExp,"Hơn")){
                preg_match('/.*[0-9]/', $strYearExp, $matches); // Loại các phần đằng sau
                preg_match('/[0-9].*/', $matches[0], $matches); // Loại các phần đằng trước

                $yearValue = $matches[0];
                return ['minYearExp' => $yearValue, "maxYearExp" => 0];
            }else{
                $arrYearExp = explode("-" , $strYearExp);
                if(trim($arrYearExp[0]) && trim($arrYearExp[1])){
                    $minYearExp = trim($arrYearExp[0]);
                    $maxYearExp =  trim($arrYearExp[1]);
                }elseif(trim($arrYearExp[0])){
                     $minYearExp = trim($arrYearExp[0]);
                     $maxYearExp = 0; 
                }else{
                    $minYearExp = 0;
                    $minYearExp = 0;

                }
                return ['minYearExp' => $minYearExp, "maxYearExp" => $maxYearExp];
            }

        }

         return ['minYearExp' => 0, "maxYearExp" => 0];
    }

    /**
     * Lấy vị trí mong muốn
     * @param type $strPosition
     * @param type $crawlSource
     * @return type
     */
    public static function getJobCrawlPosition($strPosition, $crawlSource){
         if($crawlSource == "My Work")
         {
            if(strstr($strPosition, "Mới tốt nghiệp") || strstr($strPosition, "Thực tập sinh") 
                    || strstr($strPosition, "Hợp đồng")){
                return ['job_position_name' => "Mới tốt nghiệp/Thực tập sinh", "job_position_id" => 1];
            }elseif(strstr($strPosition, "Nhân viên") || strstr($strPosition, "Kỹ thuật viên/Kỹ sư") 
                    || strstr($strPosition, "Mới đi làm")){
                return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
            }elseif(strstr($strPosition, "Trưởng") || strstr($strPosition, "trưởng")){
                return ['job_position_name' => "Trưởng nhóm/Trưởng phòng", "job_position_id" => 3];
            }elseif(strstr($strPosition, "Giám") || strstr($strPosition, "giám") || strstr($strPosition, "cấp cao")){
                return ['job_position_name' => "Trưởng nhóm/Trưởng phòng", "job_position_id" => 3];
            }
         }elseif($crawlSource == "Careerlink VN")
         {
            if(strstr($strPosition, "Mới tốt nghiệp") || strstr($strPosition, "Thực tập sinh") 
                    || strstr($strPosition, "Hợp đồng")){
                return ['job_position_name' => "Mới tốt nghiệp/Thực tập sinh", "job_position_id" => 1];
            }elseif(strstr($strPosition, "Nhân viên") || strstr($strPosition, "Kỹ thuật viên/Kỹ sư") 
                    || strstr($strPosition, "Mới đi làm")){
                return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
            }elseif(strstr($strPosition, "Trưởng") || strstr($strPosition, "trưởng")){
                return ['job_position_name' => "Trưởng nhóm/Trưởng phòng", "job_position_id" => 3];
            }elseif(strstr($strPosition, "Giám") || strstr($strPosition, "giám") || strstr($strPosition, "cấp cao")){
                return ['job_position_name' => "Trưởng nhóm/Trưởng phòng", "job_position_id" => 3];
            }
         }elseif($crawlSource == "Vietnam Works")
         {
            if(strstr($strPosition, "Mới tốt nghiệp") || strstr($strPosition, "Thực tập sinh") 
                    || strstr($strPosition, "Hợp đồng")){
                return ['job_position_name' => "Mới tốt nghiệp/Thực tập sinh", "job_position_id" => 1];
            }elseif(strstr($strPosition, "Nhân viên") || strstr($strPosition, "Kỹ thuật viên/Kỹ sư") 
                    || strstr($strPosition, "Mới đi làm")){
                return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
            }elseif(strstr($strPosition, "Trưởng") || strstr($strPosition, "trưởng")){
                return ['job_position_name' => "Trưởng nhóm/Trưởng phòng", "job_position_id" => 3];
            }elseif(strstr($strPosition, "Giám") || strstr($strPosition, "giám") || strstr($strPosition, "cấp cao")){
                return ['job_position_name' => "Trưởng nhóm/Trưởng phòng", "job_position_id" => 3];
            }
         }else
         {
            if(strstr($strPosition, "Mới tốt nghiệp") || strstr($strPosition, "Thực tập sinh") 
                    || strstr($strPosition, "Hợp đồng")){
                return ['job_position_name' => "Mới tốt nghiệp/Thực tập sinh", "job_position_id" => 1];
            }elseif(strstr($strPosition, "Nhân viên") || strstr($strPosition, "Kỹ thuật viên/Kỹ sư") 
                    || strstr($strPosition, "Mới đi làm")){
                return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
            }elseif(strstr($strPosition, "Trưởng") || strstr($strPosition, "trưởng")){
                return ['job_position_name' => "Trưởng nhóm/Trưởng phòng", "job_position_id" => 3];
            }elseif(strstr($strPosition, "Giám") || strstr($strPosition, "giám") || strstr($strPosition, "cấp cao")){
                return ['job_position_name' => "Trưởng nhóm/Trưởng phòng", "job_position_id" => 3];
            }
         }

         return ['job_position_name' => "Nhân viên/Chuyên viên", "job_position_id" => 2];
    }

    /**
     * Insert job ở bảng crawl vào bảng job
     * @param type $jobCrawlModel
     */
    public static function insertToJobTable($jobCrawlModel, $jobCrawlSource){
         // Lấy các thông tin job, job_place, salary, degree, position, exp, category
         $placeValue = HopeCrawlHelper::getJobCrawlPlace($jobCrawlModel->job_crawl_place, $jobCrawlSource);
         $degreeValue = HopeCrawlHelper::getJobCrawlDegree($jobCrawlModel->job_crawl_degree, $jobCrawlSource);
         $salaryValue = HopeCrawlHelper::getJobCrawlSalary($jobCrawlModel->job_crawl_salary, $jobCrawlSource);
         $jobTypeValue = HopeCrawlHelper::getJobCrawlType($jobCrawlModel->job_crawl_type, $jobCrawlSource);
         $positionValue = HopeCrawlHelper::getJobCrawlPosition($jobCrawlModel->job_crawl_position, $jobCrawlSource);
         $expValue = HopeCrawlHelper::getJobCrawlYearExp($jobCrawlModel->job_crawl_experience, $jobCrawlSource);
         $jobCrawlIndeed = \common\models\XJobCrawlBasic::find()
                 ->where(['id' => $jobCrawlModel->company_job_crawl_id])
                 ->one();

         $categoryValue = HopeCrawlHelper::getJobCrawlCategory($jobCrawlIndeed->job_title, $jobCrawlIndeed->company, $jobCrawlSource);    
         
         // Insert vào bảng job
         try{   

             $jobExist = \common\models\Job::find()
                     ->where(['employer_id' => $jobCrawlModel->employer_id])
                     ->andWhere(['job_title' => $jobCrawlModel->job_crawl_title])
                     ->one();

             if(!empty($jobExist)){
                 return true;
             }

             $job = new \common\models\Job();
             $job->employer_id = $jobCrawlModel->employer_id;
             $job->job_title = $jobCrawlModel->job_crawl_title;
             $job->crawl = $jobCrawlModel->id.'-'.$jobCrawlSource;
             $job->crawl_detail_id = $jobCrawlModel->id;
             $job->crawl_source = $jobCrawlSource;
             $job->degree = $degreeValue['degree_name'];
             $job->degree_id = $degreeValue['degree_id'];
             $job->min_expect_salary = $salaryValue['minSalary'];
             $job->max_expect_salary = $salaryValue['maxSalary'];
             $job->job_type = $jobTypeValue['job_type_name'];
             $job->job_type_id = $jobTypeValue['job_type_id'];
             $job->job_position = $positionValue['job_position_name'];
             $job->job_position_id = $positionValue['job_position_id'];
             $job->exp_min_require_year = $expValue['minYearExp'];
             $job->exp_max_require_year = $expValue['maxYearExp'];
             $job->job_category = implode(",", $categoryValue);            
             $job->job_description = strip_tags($jobCrawlModel->job_crawl_description);
             $job->job_requirement = strip_tags($jobCrawlModel->job_crawl_requirement);
             $job->job_benefit = strip_tags($jobCrawlModel->job_crawl_benefit);
             $job->deadline = HopeTimeHelper::getDayPlus(30);
             $job->created = HopeTimeHelper::getNow();
             $job->updated = HopeTimeHelper::getNow();
             $job->status = 1;
             $job->save();

             if($job->errors){
                 print_r($jobCrawlModel->id . "<br>");
                 print_r($job->errors);
                 return false;
             }
             $employerModel = \common\models\Employer::find()
                     ->where(['employer_id' => $jobCrawlModel->employer_id])
                     ->one();
             // Insert vào bảng place assoc
             foreach($placeValue AS $place){
                 $placeAssocModel = new \common\models\JobPlaceAssoc();
                 $placeAssocModel->job_id = $job->job_id;
                 $placeAssocModel->place_id = $place['place_id'];
                 $placeAssocModel->detail_address = $employerModel->address;
                 $placeAssocModel->geo_latitude = $employerModel->geo_longitude;
                 $placeAssocModel->geo_longitude = $employerModel->geo_longitude;
                 $placeAssocModel->province_name = $place['place_name'];
                 $placeAssocModel->district_name = '';
                 $placeAssocModel->address_google = $employerModel->address;
                 $placeAssocModel->save();
                 if($placeAssocModel->errors){
                     print_r($jobCrawlModel->id . "<br>");
                     print_r($placeAssocModel->errors);

                 }
             }
             // Insert vào bảng category_assoc
             foreach($categoryValue AS $categoryId){
                 $categoryAssocModel = new \common\models\JobCategoryAssoc();
                 $categoryAssocModel->job_id = $job->job_id;
                 $categoryAssocModel->job_category_id = $categoryId;
                 $categoryAssocModel->save();
                 if($categoryAssocModel->errors){
                     print_r($jobCrawlModel->id . "<br>");
                     print_r($categoryAssocModel->errors);

                 }
             }

             return true;
         } catch (\Exception $ex) {
             print_r($ex->getMessage()); exit;
             return false;

         }

    }

    public static function insertJobPlace($strPlace, $job_id, $employer_id, $crawlSource){

         $placeValue = HopeCrawlHelper::getJobCrawlPlace($strPlace, $crawlSource);
         $employerModel = \common\models\Employer::findOne($employer_id);

         // Insert vào bảng place assoc
         foreach($placeValue AS $place){
             $placeAssocModel = new \common\models\JobPlaceAssoc();
             $placeAssocModel->job_id = $job_id;
             $placeAssocModel->place_id = $place['place_id'];
             $placeAssocModel->detail_address = $employerModel->address;
             $placeAssocModel->geo_latitude = $employerModel->geo_longitude;
             $placeAssocModel->geo_longitude = $employerModel->geo_longitude;
             $placeAssocModel->province_name = $place['place_name'];
             $placeAssocModel->district_name = '';
             $placeAssocModel->address_google = $employerModel->address;
             $placeAssocModel->save();
             if($placeAssocModel->errors){
                 print_r($job_id . "<br>");
                 print_r($placeAssocModel->errors);
             }
         }       
    }
    
    /**
     * Số ngày từ ngày gần nhất crawl
     */
    public static function getDaySinceLastCrawl(){        
        $maxCrawlDate = \Yii::$app->db->createCommand(""
                . "SELECT MAX(DATE(updated)) FROM x_job_crawl_basic")
                ->queryScalar();
        $numOfDaySinceLastCrawl = floor((time() - strtotime($maxCrawlDate))/86400);
        
        return $numOfDaySinceLastCrawl;
    }
    
    /**
     * Lấy keyword và place để crawl
     * Tiêu chí : chưa tồn tại cặp keyword, place trong bảng crawl_turn
     */
    public static function getKwAndPlaceToCrawl(){
        $keywordModel = \Yii::$app->db->createCommand(
                        "SELECT * 
                            FROM 
                            (
                            select k.id AS keyword_id, k.keyword, p.place_name 
                            from x_keyword_to_crawl AS k, x_place_to_crawl AS p
                            where k.status >= 0
                            ) AS t
                            WHERE not EXISTS (
                                    select * from x_crawl_turn where keyword = t.keyword AND crawl_place = t.place_name
                            )
                            limit 0,1
                            "
                )
                ->queryOne();
        
        return $keywordModel;
    }
    
    /**
     * Full url của indeed để crawl
     * 
     * @param type $keyword
     * @param type $start
     * @param type $location
     * @param type $fromAge
     * @return string
     */
    public static function getIndeedCrawlUrl($keyword, $start, $location, $fromAge ){
         $api = 'http://api.indeed.com/ads/apisearch?publisher=6959891807748278'
                . '&latlong=1&co=vn&chnl=&userip=1.2.3.4&useragent=Mozilla/%2F4.0%28Firefox%29&v=2&format=json'
                . '&highlight=1&jt=full_time&limit=25';

        // quét api
        $fullApi = $api . '&q="' . $keyword . '"&start=' . $start . '&l=' . urlencode($location) . '&fromage=' . urlencode($fromAge);
        
        return $fullApi;
    }
    
    /**
     * Lấy website từ bing search
     * 
     * @param type $companyName
     * @return type
     */
    public static function getCompanyWebsiteFromBing($companyName) {
        
        # Use the Curl extension to query Google and get back a page of results
        $companyName = urlencode("$companyName");
        $url = "https://www.bing.com/search?q=$companyName";

        $html = file_get_contents($url);
        # Create a DOM parser object
        $dom = new \DOMDocument();

        # Parse the HTML from Google.
        # The @ before the method call suppresses any warnings that
        # loadHTML might throw because of invalid HTML in the page.
        @$dom->loadHTML($html);

        # Iterate over all the <a> tags
        foreach ($dom->getElementsByTagName('a') as $link) {
//            print_r($link->getAttribute('href'));
//            echo "<br/>";
            # Show the <a href>
            if ((strpos($link->getAttribute('href'), 'http://') !== false || strpos($link->getAttribute('href'), 'https://') !== false ) 
                    && strpos($link->getAttribute('href'), 'choice.microsoft.com') === false 
                    && strpos($link->getAttribute('href'), 'bing.com') === false 
                    && strpos($link->getAttribute('href'), 'timviecnhanh') === false 
                    && strpos($link->getAttribute('href'), 'jora.com') === false 
                    && strpos($link->getAttribute('href'), 'vietnamworks') === false 
                    && strpos($link->getAttribute('href'), 'mywork') === false 
                    && strpos($link->getAttribute('href'), 'itviec') === false 
                    && strpos($link->getAttribute('href'), 'viec.me') === false 
                    && strpos($link->getAttribute('href'), 'thongtincongty') === false
                    && strpos($link->getAttribute('href'), 'hosocongty') === false
                    && strpos($link->getAttribute('href'), 'jobstreet.vn') === false
                    && strpos($link->getAttribute('href'), 'topcv.vn') === false
                    && strpos($link->getAttribute('href'), 'careerlink.vn') === false
                    && strpos($link->getAttribute('href'), 'news.asiaone.com') === false
                    && strpos($link->getAttribute('href'), 'careerbuilder.vn') === false
                    && strpos($link->getAttribute('href'), 'trangvangvietnam.com') === false
                    && strpos($link->getAttribute('href'), 'vinabiz.org') === false
                    && strpos($link->getAttribute('href'), 'vieclam24h.vn') === false
                    && strpos($link->getAttribute('href'), 'jellyfishhr.com') === false
                    && strpos($link->getAttribute('href'), 'doanhnghiepmoi.vn') === false
                    && strpos($link->getAttribute('href'), 'first-viec-lam.com') === false
                    && strpos($link->getAttribute('href'), 'megaceo.com') === false
                    && strpos($link->getAttribute('href'), 'vn.indeed.com') === false
                    && strpos($link->getAttribute('href'), 'facebook.com') === false
                    && strpos($link->getAttribute('href'), 'viecoi.vn') === false
                    && strpos($link->getAttribute('href'), 'linkedin.com') === false
                    && strpos($link->getAttribute('href'), '1viec.com') === false
                    && strpos($link->getAttribute('href'), 'vncareer.com.vn') === false
                    && strpos($link->getAttribute('href'), 'jellyfishhr.jp') === false
                    && strpos($link->getAttribute('href'), 'fa.net.vn') === false
                    && strpos($link->getAttribute('href'), 'hrc.vn') === false
                    && strpos($link->getAttribute('href'), 'hrchannels.com') === false
                    && strpos($link->getAttribute('href'), 'vieclam.tuoitre.vn') === false
                    && strpos($link->getAttribute('href'), 'vieclam.') === false
                    && strpos($link->getAttribute('href'), 'hrvietnam.com') === false
            ) {
                $website = $link->getAttribute('href');
                $website = str_replace('/url?q=', '', $website);
                if(preg_match("/http:\/\/[^\/]*/", $website, $match)){
                    return $match[0];
                }                
                if(preg_match("/https:\/\/[^\/]*/", $website, $match)){
                    return $match[0];
                }
                
            }
        }
        
        return -1   ;
    }
    
    /**
     * Match company website với employer
     */
    public static function getIdOfEmployerMatchByWebsite($website){
        // Lấy domain ( loại subdomain)
        $domain = parse_url($website, PHP_URL_HOST);
//        $host_names = explode(".", $domain);
//        $bottom_host_name = $host_names[count($host_names)-2] . "." . $host_names[count($host_names)-1];
        
        // Lấy employer có website match với domain
        $matchEmployer = Employer::find()
                ->where(['like', 'website' , $domain])
                ->one();
        
        if(!empty($matchEmployer)){
            return $matchEmployer->employer_id;
        }
        
        return -1;
    }
    
    /**
     * Match company name với employer
     */
    public static function getIdOfEmployerMatchByName($companyName){
        // Lấy domain ( loại subdomain)
        $domain = parse_url($companyName, PHP_URL_HOST);
//        $host_names = explode(".", $domain);
//        $bottom_host_name = $host_names[count($host_names)-2] . "." . $host_names[count($host_names)-1];
        
        // Lấy employer có website match với domain
        $matchEmployer = Employer::find()
                ->where(['like', 'website' , $domain])
                ->one();
        
        if(!empty($matchEmployer)){
            return $matchEmployer->employer_id;
        }
        
        return -1;
    }
    
    /**
     * Tạo crawler dựa vào crawlsource
     * @param type $crawlSource
     * @return \common\components\crawl\HopeCrawlVietnamwork
     */
    public static function getCrawlerBySource($crawlSource){
        switch ($crawlSource)
        {
            case 'vietnamwork' : 
                return new HopeCrawlVietnamwork();
                break;
            case 'mywork' : 
                return new HopeCrawlMywork();
                break;
            case 'careerbuilder' : 
                return new HopeCrawlCareerbuilder();
                break;
            case 'timviecnhanh' : 
                return new HopeCrawlTimviecnhanh();
                break;
            case 'itviec' : 
                return new HopeCrawlTimviecnhanh();
                break;
            default:
                return new HopeCrawlVietnamwork();
        }
    }
}
