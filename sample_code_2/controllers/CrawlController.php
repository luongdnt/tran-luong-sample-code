<?php

namespace backend\controllers;

use backend\models\DateRangeForm;
use backend\models\EmployerJobsExportForm;
use backend\models\XEmployerCrawlSearch;
use common\components\crawl\CareerbuilderJobCrawler;
use common\components\crawl\CareerlinkJobCrawler;
use common\components\crawl\GlintsJobCrawler;
use common\components\crawl\HopeCrawlVietnamwork;
use common\components\crawl\HoteljobJobCrawler;
use common\components\crawl\ItviecJobCrawler;
use common\components\crawl\jobsposting\JobMyworkCrawler;
use common\components\crawl\MyworkJobCrawler;
use common\components\crawl\TimviecnhanhJobCrawler;
use common\components\crawl\TopcvJobCrawler;
use common\components\crawl\Vieclam24hJobCrawler;
use common\components\crawl\ViectotnhatJobCrawler;
use common\components\crawl\VietnamworkJobCrawler;
use common\extensions\mysql\libs\DatabaseMigrationQuery;
use common\extensions\mysql\libs\XCrawlCandidateQueries;
use common\extensions\mysql\libs\XEmployerCrawlQueries;
use common\extensions\mysql\libs\XJobCrawlQueries;
use common\extensions\tesseract\libs\StripVN;
use common\models\Employer;
use common\models\XEmployerCrawl;
use Yii;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\db\Query;
use yii\web\Controller;
use common\components\RequestParamRetriever;
use backend\models\crawl\CrawlJobSearch;
use common\models\XJobCrawl;

/**
 * Dashboard controller
 */
class CrawlController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
////                    [
////                        'actions' => ['save-apply-pc'],
////                        'allow' => true,
////                    ],
////                    [
////                        'allow' => true,
////                        'roles' => ['@'],
////                    ]
//                ],
//            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function beforeAction($action)
    {

        $this->layout = 'v2';

        if (!parent::beforeAction($action)) {
            return false;
        }

        // force logout
        //$this->redirect(Url::to(['site/logout'], true), 302);
        return true; // or false to not run the action

    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        // Lấy action-id
        $actionName = $this->action->id;
        $adminId = Yii::$app->user->identity->user_id;

        // Các action lưu log

        $arrActionToLog = ['employer-manage', 'company-profile', 'job-manage', 'job-form', 'candidate-manage'];
        $arrApiActionToLog = ['api-human-resource-sort', 'api-human-resource', 'api-remove-company-history', 'api-company-history', 'api-company-profile', 'api-candidate-manage'];

        // Check action name nằm trong list lưu log
        if (in_array($actionName, $arrApiActionToLog)) {

            try {
                // Save log
                $logModel = new \common\models\LogAction();
                $logModel->user_id = $adminId;
                $logModel->action = $actionName;
                $logModel->account_type = 'admin';
                $logModel->parameter = json_encode($_POST);
                $logModel->save();
            } catch (\Exception $ex) {
                // TODO
            }
        }


        if (in_array($actionName, $arrActionToLog)) {
            // Lấy candidateId
            $parameter = $_SERVER['QUERY_STRING'];
            $allowToLog = false;

            if ($actionName == 'employer-manage') {
                // Có form submit
                if (array_key_exists('action', $_GET)) {
                    $allowToLog = true;
                }
            }

            if ($actionName == 'company-profile') {
                // Có form submit
                if (array_key_exists('_csrf-backend', $_POST)) {
                    $allowToLog = true;
                }
            }

            if ($actionName == 'job-manage') {
                // Có action
                if (array_key_exists('action', $_GET)) {
                    $allowToLog = true;
                }
            }


            if ($actionName == 'job-form') {
                // Có form submit
                if (array_key_exists('_csrf-backend', $_POST)) {
                    $allowToLog = true;
                }
            }

            if ($actionName == 'company-profile') {
                // Có form submit
                if (array_key_exists('action', $_GET)) {
                    $allowToLog = true;
                }
            }

            if ($actionName == 'company-profile') {
                // Có form submit
                if (array_key_exists('action', $_GET)) {
                    $allowToLog = true;
                }
            }

            if (!empty($adminId) && $allowToLog) {
                try {
                    // Save log
                    $logModel = new \common\models\LogAction();
                    $logModel->user_id = $adminId;
                    $logModel->action = $actionName;
                    $logModel->account_type = 'admin';
                    $logModel->parameter = $parameter;
                    $logModel->save();
                } catch (\Exception $ex) {
                    // TODO
                }
            }
        }

        return $result;
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        // Decrale

        return $this->render('index', [

        ]);
    }

    /**
     * Hiển thị danh sach các job đã crawl về của các NTD
     */
    public function actionListCrawlJob()
    {
        $newEmployerName = $_REQUEST['new_employer_name'];
        // Get data provider
        $searchModel = new CrawlJobSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        // Render view 
        return $this->render('list_crawl_job', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'statusEmployerNew' => $newEmployerName
        ]);

    }

    /**
     * Save job crawl vào bảng job
     */
    public function actionSaveToJobTable()
    {

        $crawlJobId = RequestParamRetriever::getParamValue('id');

        if (empty($crawlJobId)) {
            echo "empty";
            exit;
        }

        // Lấy model
        $crawlJobModel = XJobCrawl::find()
            ->where(['id' => $crawlJobId])
            ->one();

        // save by html format
        $search = ['•', '.-', '*', ':', '. -', '+', '-', '–'];
        $replace = ['<br>•', '.<br>-', '<br>*', ':<br>', '.<br>-', '<br>+', '<br>-', '<br>–'];
        $jobDescription = $crawlJobModel['job_description'];
        $jobDescription = str_replace($search, $replace, $jobDescription);
        $jobRequirement = $crawlJobModel['job_requirement'];
        $jobRequirement = str_replace($search, $replace, $jobRequirement);
        $jobBenefit = $crawlJobModel['job_benefit'];
        $jobBenefit = str_replace($search, $replace, $jobBenefit);
        $crawlJobModel->job_description = $jobDescription;
        $crawlJobModel->job_requirement = $jobRequirement;
        $crawlJobModel->job_benefit = $jobBenefit;
        if ($crawlJobModel->save()){
            XEmployerCrawl::updateAll(['manual_confirm'=>1],['company_name'=>$crawlJobModel['company_name']]);
        }


        $this->layout = 'dashboard_candidate';
        // Render view 
        return $this->render('save_to_job_table', [
            'model' => $crawlJobModel
        ]);

    }

    /**
     * @return string
     * @todo Display all crawled candidates from x_crawl_candidate table
     * @author QuyenNV
     */
    public function actionCandidateList()
    {
        // get all necessary fields from x_crawl_candidate
        $xCrawlCandidateQueries = new XCrawlCandidateQueries();
        $allFields = $xCrawlCandidateQueries->getAllFields();
        // what to display on Gridview
        $dataProvider = new ActiveDataProvider([
            'query' => $allFields,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('candidate_list', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @throws Exception
     * @todo Matching employer from column company table x_job_crawl with employer table
     *      If company_name not found or null, employer_id = -1
     *      else update employer_id to x_job_crawl table
     * @author QuyenNV
     */
    public function actionCheckMatchingCrawledEmployer()
    {
        $stripVN = new StripVN();
        // get start time
        $time_start = microtime(true);
        $xJobCrawl = new XJobCrawlQueries();
        $xEmployerCrawlQueries = new XEmployerCrawlQueries();
        // get all crawl employer group by name from x_job_crawl table
        $arrCrawlEmployers = $xJobCrawl->getCrawlEmployerName();
//        print_r($arrCrawlEmployers);die;
        // search and update employer_id for each company_name
        foreach ($arrCrawlEmployers as $key => $crawlEmployer) {
            $arrPattern = ['/(công ty cổ phần)/', '/(công ty cp)/', '/(công ty)/', '/(công ty trách nhiệm hữu hạn)/', '/(công ty tnhh)/',
                '/(cong ty co phan)/', '/(cong ty cp)/', '/(cong ty trach nhiem huu han)/', '/(cong ty tnhh)/', '/(cty )/'];
            $searchingKeywords = trim(mb_strtolower($crawlEmployer['company_name']));
            // get searching params
            $searchingKeywords = preg_replace($arrPattern, '', trim($searchingKeywords));
            // get search result
            $apiSearchResult = file_get_contents('https://jobsgo.vn/search?k=' . $stripVN->stripVN(trim($searchingKeywords)));
            // decode result json to array
            $arrSearchResult = json_decode($apiSearchResult, true);
            $arrSearchResult = array_values($arrSearchResult);

            if (count($arrSearchResult) >= 1) {
                $resultEmployerIdUpdate = $xJobCrawl->updateCrawlEmployerIDByEmployerName($crawlEmployer['company_name'], $arrSearchResult[0]['id']);
                // inform the results
//                if ($resultEmployerIdUpdate) {
//                    echo "<p style='color: blue'>All " . $crawlEmployer['company_name'] . " marked " . $arrSearchResult[0]['id'] . "</p>";
//                }
            } else {
                $resultEmployerIdUpdate = $xJobCrawl->updateCrawlEmployerIDByEmployerName($crawlEmployer['company_name'], -1);
                // inform the results
//                if ($resultEmployerIdUpdate) {
//                    echo "<p style='color: red'>All " . $crawlEmployer['company_name'] . " marked -1</p>";
//                }
            }
        }
        // get end time
        $time_end = microtime(true);
        // calculate the processing time
        $execution_time = $time_end - $time_start;
        // inform processing time
        echo '<b>Total Execution Time:</b> ' . $execution_time . ' secs';
        // avoid Headers already sent
        exit;
    }

    /**
     * @throws Exception
     * @todo Crawl all new jobs from mywork.com.vn
     * @author QuyenNV
     */
    public function actionMyworkJobCrawler()
    {
        $jobMyworkCrawler = new MyworkJobCrawler();
        $jobMyworkCrawler->crawlAllNewJobs();
        exit;
    }

    /**
     * @throws Exception
     * @author QuyenNV
     * @todo Crawl new jobs from topcv
     */
    public function actionTopcvJobCrawler()
    {
        $topcvJobCrawler = new TopcvJobCrawler();
        $topcvJobCrawler->crawlAllNewJobs();
        exit;
    }

    /**
     * @return string
     * @throws Exception
     * @author QuyenNV
     * @todo Update employers list from x_job_crawl table to x_employer_crawl table
     *      List all employers from x_employer_crawl table
     */
    public function actionCrawledEmployersList()
    {
        $searchModel = new XEmployerCrawlSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('crawled_employers_list',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider
            ]);
    }

    /**
     * @throws Exception
     * @todo Update online jobs counted from x_job_crawl table to x_employer_crawl table
     * @author QuyenNV
     */
    public function actionUpdateOnlineJobs()
    {
        $xEmployerCrawlQueries = new XEmployerCrawlQueries();
        $xEmployerCrawlQueries->migrateEmployersList();
        $xEmployerCrawlQueries->updateEmployerOnlineJobs();
        exit;
    }

    /**
     * @throws Exception
     * @todo Crawl all new jobs from career builder site
     * @author QuyenNV
     */
    public function actionCareerbuilderJobCrawler()
    {
        $careerbuilderJobCrawler = new CareerbuilderJobCrawler();
        $careerbuilderJobCrawler->crawlAllNewJobs();
        exit;
    }

    /**
     * @throws Exception
     * @author QuyenNV
     * @todo Crawl all new jobs from careerlink site
     */
    public function actionCareerlinkJobCrawler()
    {
        $careerlinkJobCrawler = new CareerlinkJobCrawler();
        $careerlinkJobCrawler->crawlAllNewJobs();
        exit;
    }

    /**
     * @throws Exception
     * @todo Crawl all new jobs from hoteljob site
     * @author QuyenNV
     */
    public function actionHoteljobJobCrawler()
    {
        $hoteljobJobCrawler = new HoteljobJobCrawler();
        $hoteljobJobCrawler->crawlAllNewJobs();
        exit;
    }

    /**
     * @throws Exception
     * @todo Crawl all new jobs from itviec site
     * @author QuyenNV
     */
    public function actionItviecJobCrawler()
    {
        $itviecJobCrawler = new ItviecJobCrawler();
        $itviecJobCrawler->crawlAllNewJobs();
        exit;
    }

    /**
     * @throws Exception
     * @todo Crawl all new jobs from timviecnhanh site
     * @author QuyenNV
     */
    public function actionTimviecnhanhJobCrawler()
    {
        $timviecnhanhJobCrawler = new TimviecnhanhJobCrawler();
        $timviecnhanhJobCrawler->crawlAllNewJobs();
        exit;
    }

    /**
     * @throws Exception
     * @todo Crawl all new jobs from vieclam24h site
     * @author QuyenNV
     */
    public function actionVieclam24hJobCrawler()
    {
        $vieclam24hJobCrawler = new Vieclam24hJobCrawler();
        $vieclam24hJobCrawler->crawlAllNewJobs();
        exit;
    }

    /**
     * @throws Exception
     * @todo Crawl all new jobs from viectotnhat site
     * @author QuyenNV
     */
    public function actionViectotnhatJobCrawler()
    {
        $viectotnhatJobCrawler = new ViectotnhatJobCrawler();
        $viectotnhatJobCrawler->crawlAllNewJobs();
        exit;
    }

    /**
     * @throws Exception
     * @throws \yii\db\StaleObjectException
     * @author QuyenNV
     * @todo Crawl all new jobs from Glints site
     */
    public function actionGlintsJobCrawler()
    {
        $glintsJobCrawler = new GlintsJobCrawler();
        $glintsJobCrawler->crawlAllNewJobs();
        exit();
    }

    /**
     * @throws Exception
     * @throws \yii\db\StaleObjectException
     * @author QuyenNV
     * @todo Crawl all new jobs from vietnamwork site
     */
    public function actionVietnamworkJobCrawler()
    {
        $vietnamworkJobCrawl = new VietnamworkJobCrawler();
        $vietnamworkJobCrawl->crawlAllNewJobs();
        exit();
    }

    /**
     * @return string
     * @author QuyenNV
     * @todo Show a report of crawled jobs
     */
    public function actionReport()
    {
        $model = new DateRangeForm();
        return $this->render('report', ['model' => $model]);
    }

    /**
     * @author QuyenNV
     * @todo Export new records from x_job_crawl table
     */
    public function actionExportXJobCrawl()
    {
        $database = new DatabaseMigrationQuery();
        // start time
        $startTime = microtime(true);
        // export records from x_job_crawl
        $record = $database->exportXJobCrawlToSqlFile('x_job_crawl');
        // if it has new records
        if ($record) {
            // import to db by command line
            $import = $database->importDataToRemote($record['filePath']);
            // end time
            $endTime = microtime(true);
            // duration to run whole process
            $duration = $endTime - $startTime;
            // how many jobs inserted to x_job_crawl
            $countInserted = empty($record['startID']) ? $record['stopID'] : (intval($record['stopID']) - intval($record['startID']));
            echo "Inserted " . ($countInserted + 1) . " new jobs in " . $duration . " seconds\n";
            // if importing fails, send email to inform the error
            if ($import != null) {
                $mailSubject = 'MIGRATE X_JOB_CRAWL TABLE FAILED';
                $mailBody = '<p><b>Thời gian: </b> ' . date('Y-m-d H:i:s') . '</p>';
                $mailBody .= '<p><b>Nguyên nhân: </b>Lỗi khi import data với command line</p>';
                Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['contactEmail'] => Yii::$app->name])
                    ->setTo(['luong.tran@jobsgo.vn', 'quyen.ngo@jobsgo.vn'])
                    ->setSubject($mailSubject)
                    ->setHtmlBody($mailBody)->send();
            }
        } // if there is no new records, send email to inform error
        else {
            $mailSubject = 'MIGRATE X_JOB_CRAWL TABLE FAILED';
            $mailBody = '<p><b>Thời gian: </b> ' . date('Y-m-d H:i:s') . '</p>';
            $mailBody .= '<p><b>Nguyên nhân: </b>Bảng không có dữ liệu mới (Theo giá trị meta table)</p>';
            Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['contactEmail'] => Yii::$app->name])
                ->setTo(['luong.tran@jobsgo.vn', 'quyen.ngo@jobsgo.vn'])
                ->setSubject($mailSubject)
                ->setHtmlBody($mailBody)->send();
        }
    }

    /**
     * @author QuyenNV
     * @todo Export new records from x_employer_crawl table
     */
    public function actionExportXEmployerCrawl()
    {
        // get max id before insert
        $maxIDBeforeInsert = XEmployerCrawl::find()->orderBy('id DESC')->one()['id'];
//        $maxIDBeforeInsert = (new Query())->select('max(id)')->from('x_employer_crawl')->scalar();
        $database = new DatabaseMigrationQuery();
        // start time
        $startTime = microtime(true);
        // export records from x_employer_crawl table
        $record = $database->exportXEmployerCrawlToSqlFile('x_employer_crawl');
        // if there is records
        if ($record) {
            // import to db
            $import = $database->importDataToRemote($record);
            $endTime = microtime(true);
            $duration = $endTime - $startTime;
            // if importing fails, send email to inform the error
            if ($import != null) {
                $mailSubject = 'MIGRATE X_EMPLOYER_CRAWL TABLE FAILED';
                $mailBody = '<p><b>Thời gian: </b> ' . date('Y-m-d H:i:s') . '</p>';
                $mailBody .= '<p><b>Nguyên nhân: </b>Lỗi khi import data với command line</p>';
                Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['contactEmail'] => Yii::$app->name])
                    ->setTo(['luong.tran@jobsgo.vn', 'quyen.ngo@jobsgo.vn'])
                    ->setSubject($mailSubject)
                    ->setHtmlBody($mailBody)->send();
            } // if import successfully
            else {
                // get max id after insert
                $maxIDAfterInsert = XEmployerCrawl::find()->orderBy('id DESC')->one()['id'];
//                $maxIDAfterInsert = (new Query())->select('max(id)')->from('x_employer_crawl')->scalar();
                // count updated records
                $countUpdateRecords = (new Query())->select('count(*) as count_updated')->from('x_employer_crawl')->where(['>=', 'updated', date('Y-m-d H:i:s', strtotime("-2 hours"))])->scalar();
                // count inserted records
                $countInserted = empty($maxIDBeforeInsert) ? $maxIDAfterInsert : ($maxIDAfterInsert - $maxIDBeforeInsert);
                // if there is no inserted and updated
                if (intval($countUpdateRecords) == 0 && intval($countInserted) == 0) {
                    $mailSubject = 'MIGRATE X_EMPLOYER_CRAWL TABLE FAILED';
                    $mailBody = '<p><b>Thời gian: </b> ' . date('Y-m-d H:i:s') . '</p>';
                    $mailBody .= '<p><b>Số lượng insert mới: </b> ' . $countInserted . '</p>';
                    $mailBody .= '<p><b>Số lượng update: </b> ' . $countUpdateRecords . '</p>';
                    $mailBody .= '<p><b>Thời lượng: </b> ' . $duration . ' giây</p>';
                    Yii::$app->mailer->compose()
                        ->setFrom([Yii::$app->params['contactEmail'] => 'JobsGO'])
                        ->setTo(['luong.tran@jobsgo.vn', 'quyen.ngo@jobsgo.vn'])
                        ->setSubject($mailSubject)
                        ->setHtmlBody($mailBody)->send();
                }
            }
        } else {
            $mailSubject = 'MIGRATE X_EMPLOYER_CRAWL TABLE FAILED';
            $mailBody = '<p><b>Thời gian: </b> ' . date('Y-m-d H:i:s') . '</p>';
            $mailBody .= '<p><b>Nguyên nhân: </b>Bảng không có dữ liệu mới</p>';
            Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['contactEmail'] => 'JobsGO'])
                ->setTo(['luong.tran@jobsgo.vn', 'quyen.ngo@jobsgo.vn'])
                ->setSubject($mailSubject)
                ->setHtmlBody($mailBody)->send();
        }
    }

}