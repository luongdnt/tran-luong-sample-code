<?php

namespace candidate\controllers;

use common\components\HopeTimeHelper;
use yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\BaseStringHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Candidate;
use common\models\Language;
use common\models\Hobby;
use common\models\CandidateEducation;
use common\models\CandidateHobby;
use common\models\CandidateJobCategory;
use common\models\CandidateJobHistory;
use common\models\CandidateJobHistoryDetail;
use common\models\CandidateLanguage;
use common\models\CandidateSoftSkill;
use common\models\JobCategory;
use common\models\Cv;
use common\models\CvTheme;
use common\models\SoftSkill;
use common\models\Award;
use common\models\CandidateActivity;
use common\models\Certificates;
use common\models\CvGoFeedback;
use common\models\CandidateCvHistory;
use common\models\CandidateCvReferences;
use common\models\CandidateSkill;
use common\components\HopeFileHelper;
use common\components\HopeChromeHelper;
use common\components\HopeShareHelper;
use common\components\HopeConstant;
use common\components\RequestParamRetriever;
use common\components\HopeTrimHelper;
use common\extensions\extra\TextFormatHelper;
use common\colorgb\ColorgbHelper;
use Google\Cloud\Translate\V2\TranslateClient;

class CreateResumeController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index', 'save-cv', 'get-job-history-template', 'get-education-template', 'update-avatar', 'change-theme', 'download-resume', 'get-award-template', 'get-references-template', 'get-activity-template', 'get-certificates-template', 'get-pdf', 'unica-gif', 'update-color', 'send-feedback', 'save-draft','update-font-family', 'update-language', 'manage', 'add-new-cv', 'delete-cv', 'update-cv-name', 'update-font-size', 'update-line-height', 'test-gg-translate', 'translate-cv'],
						'roles' => ['@'],
					],
					[
						'allow' => true,
						'actions' => ['render', 'hide-elements'],
						'roles' => ['?', '@'],
					],
				],
			],
		];
	}

	/*
	 *
	 */
	public function beforeAction($action)
	{
		if (Yii::$app->user->isGuest) {
			$currentUrl = Yii::$app->request->getUrl();
			$actionName = $this->action->id;
			if ($actionName !== "render" && $actionName !== "hide-elements") {
				return $this->redirect('/site/login?ref=' . $currentUrl);
			}
		}

		$candidateId = Yii::$app->request->get('candidate_id');
		if (!empty($candidateId)) {
			$candidateId = HopeShareHelper::decryptString($candidateId);
		} else {
			$candidateId = Yii::$app->user->identity->candidate_id;
		}

		if (!empty(Yii::$app->request->get('id'))) {
			$resumeId = Yii::$app->request->get('id');
			$resumeId = HopeShareHelper::decryptString($resumeId);
		} elseif (!empty(Yii::$app->request->post('cvId'))) {
			$resumeId = Yii::$app->request->post('cvId');
			$resumeId = HopeShareHelper::decryptString($resumeId);
		} else {
			$resumeId = null;
		}

		$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $resumeId])->with('theme')->one();
		if (!empty($cv) && !empty($cv->language)) {
			Yii::$app->language = $cv->language;
		} else {
			Yii::$app->language = 'vi-VN';
		}		

		$this->layout = false;
		$this->enableCsrfValidation = false;

		if (!parent::beforeAction($action)) {
			return false;
		}

		return true; // or false to not run the action

	}

	/*index function*/
	public function actionIndex($id = null)
	{
		if (Yii::$app->user->isGuest) {
			$currentUrl = Yii::$app->request->getUrl();
			$actionName = $this->action->id;
			if ($actionName !== "render" && $actionName !== "hide-elements") {
				return $this->redirect('/site/login?ref=' . $currentUrl);
			}
		}

		$candidateId = Yii::$app->user->identity->candidate_id;
		if (empty($id)) {
			$cv = Cv::find()->where(['cv_type' => 1, 'candidate_id' => $candidateId])->with('theme')->one();
			$theme = 1;
			$defaultActiveColor = HopeConstant::CV_GO_DEFAULT_COLOR;
			$defaultActiveFontFamily = HopeConstant::CV_GO_DEFAULT_FONT_FAMILY;
			if (!empty(Yii::$app->request->get('theme'))) {
				$isThemeExist = CvTheme::findOne(Yii::$app->request->get('theme'));
				if (!empty($isThemeExist)) {
					$theme = Yii::$app->request->get('theme');
				}
			}

			if (empty($cv)) {
				//chưa có cv thì tạo cái mới
				$cv = new Cv();
				$cv->candidate_id = $candidateId;
				$cv->active_theme = $theme;
				$cv->active_color = $defaultActiveColor;
				$cv->active_font_family = $defaultActiveFontFamily;
				$cv->created_at = date('Y-m-d H:i:s');
				$cv->updated_at = date('Y-m-d H:i:s');
				$cv->language = 'vi-VN';
				$cv->cv_name = 'CV mặc định';
				$cv->cv_type = 1;
				$cv->save();
				if (empty($cv)) {
					return $this->redirect('/');
				}
			} else {
				//có rồi thì update
				if (!empty(Yii::$app->request->get('theme'))) {
					$cv->active_theme = $theme;
				}

				if (empty($cv->active_color)) {
					$cv->active_color = $defaultActiveColor;
				} else {
					$arrAciveColor = json_decode($cv->active_color, true);
					$arrDefaultActiveColor = json_decode($defaultActiveColor, true);
					foreach ($arrDefaultActiveColor as $key => $value) {
						if (empty($arrAciveColor[$key])) {
							$arrAciveColor[$key] = $value;
						}
					}
					$cv->active_color = json_encode($arrAciveColor, true);
				}

				if (empty($cv->active_font_family)) {
					$cv->active_font_family = $defaultActiveFontFamily;
				} else {
					$arrActiveFontFamily = json_decode($cv->active_font_family, true);
					$arrDefaultActiveFontFamily = json_decode($defaultActiveFontFamily, true);
					foreach ($arrDefaultActiveFontFamily as $key => $value) {
						if (empty($arrActiveFontFamily[$key])) {
							$arrActiveFontFamily[$key] = $value;
						}
					}
					$cv->active_font_family = json_encode($arrActiveFontFamily, true);
				}

				if (empty($cv->language)) {
					$cv->language = 'vi-VN';
				}
				$cv->save();
			}

			$cv = Cv::find()->where(['cv_type' => 1, 'candidate_id' => $candidateId])->with('theme')->one();
			return $this->redirect('/cv-go/' . HopeShareHelper::encryptString($cv->id));
		} else {
			$cvId = HopeShareHelper::decryptString($id);
			$cv = Cv::find()->where(['id' => $cvId, 'candidate_id' => $candidateId])->with('theme')->one();
			if (empty($cv)) {
				return $this->redirect('/cv-go/quan-ly');
			} else {
				//có rồi thì update
				$theme = 1;
				$defaultActiveColor = HopeConstant::CV_GO_DEFAULT_COLOR;
				$defaultActiveFontFamily = HopeConstant::CV_GO_DEFAULT_FONT_FAMILY;
				if (!empty(Yii::$app->request->get('theme'))) {
					$isThemeExist = CvTheme::findOne(Yii::$app->request->get('theme'));
					if (!empty($isThemeExist)) {
						$theme = Yii::$app->request->get('theme');
					}
				}

				if (!empty(Yii::$app->request->get('theme'))) {
					// echo $theme; die;
					$cv->active_theme = $theme;
				}

				if (empty($cv->active_color)) {
					$cv->active_color = $defaultActiveColor;
				} else {
					$arrAciveColor = json_decode($cv->active_color, true);
					$arrDefaultActiveColor = json_decode($defaultActiveColor, true);
					foreach ($arrDefaultActiveColor as $key => $value) {
						if (empty($arrAciveColor[$key])) {
							$arrAciveColor[$key] = $value;
						}
					}
					$cv->active_color = json_encode($arrAciveColor, true);
				}

				if (empty($cv->active_font_family)) {
					$cv->active_font_family = $defaultActiveFontFamily;
				} else {
					$arrActiveFontFamily = json_decode($cv->active_font_family, true);
					$arrDefaultActiveFontFamily = json_decode($defaultActiveFontFamily, true);
					foreach ($arrDefaultActiveFontFamily as $key => $value) {
						if (empty($arrActiveFontFamily[$key])) {
							$arrActiveFontFamily[$key] = $value;
						}
					}
					$cv->active_font_family = json_encode($arrActiveFontFamily, true);
				}

				if (empty($cv->language)) {
					$cv->language = 'vi-VN';
				}
				$cv->save();
				$cv = Cv::find()->where(['id' => $cvId, 'candidate_id' => $candidateId])->with('theme')->one();
			}
		}

		$activeColor = json_decode($cv->active_color, true);
		$activeColor = $activeColor[$cv['theme']['id']];
		$color = json_decode($cv['theme']['color'], true);
		$activeFontFamily = json_decode($cv->active_font_family, true);
		$activeFontFamily = $activeFontFamily[$cv['theme']['id']];
		$fontFamily = json_decode($cv['theme']['font_family'], true);

		$cvType = $cv->cv_type;
		if ($cvType == 1 || ($cvType == 2 && empty($cv->secondary_cv_data))) {
			$candidate = Candidate::find()->where(['candidate_id' => $candidateId])->one();
			$jobHistory = CandidateJobHistory::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC', 'date_start' => 'DESC'])->all();
			$education = CandidateEducation::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->all();
			$language = Language::find()->all();
			$candidateLanguage = CandidateLanguage::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->with('language')->all();
			$hobby = Hobby::find()->all();
			$candidateHobby = CandidateHobby::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->with('hobby')->all();
			$jobCategory = JobCategory::find()->orderBy(['job_category_name' => 'ASC'])->all();
			self::syncCandidateJobCategory();
			$candidateJobCategory = CandidateSkill::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->with('category')->all();
			if (empty($candidateJobCategory)) {
				$candidateJobCategory = CandidateJobCategory::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->with('category')->all();
			}
			$softSkill = SoftSkill::find()->where(['status' => 0])->all();
			$candidateSoftSkill = CandidateSoftSkill::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->with('softskill')->all();
			$award = Award::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->all();
			$activity = CandidateActivity::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->all();
			$certificates = Certificates::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->all();
			$references = CandidateCvReferences::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->all();
		} else {
			$secondaryCvData = $cv->secondary_cv_data;
			$secondaryCvData = json_decode($secondaryCvData);
			
			//get candidate
			$candidate = null;
			$candidateInfo = $secondaryCvData->candidateInfo;
			if (!empty($candidateInfo)) {
				foreach ($candidateInfo as $key => $value) {
					if ($value->name == 'date_of_birth') {
						$date = str_replace('/', '-', $value->value);
						$candidate->{$value->name} = date('Y-m-d H:i:s', strtotime($date));
					} else {
						$candidate->{$value->name} = $value->value;
					}
				}
				$dataCandidate = Candidate::findOne($candidateId);
				$candidate->avatar = $dataCandidate->avatar;
			}

			//get job history
			$jobHistory = null;
			$tmpJobHistory = $secondaryCvData->jobHistory;
			if (!empty($tmpJobHistory)) {
				foreach ($tmpJobHistory as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$jobHistory[$k1]->{$v2->name} = $v2->value;
					}
				}
			}
			
			//get education
			$education = null;
			$tmpEducation = $secondaryCvData->education;
			if (!empty($tmpEducation)) {
				foreach ($tmpEducation as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$education[$k1]->{$v2->name} = $v2->value;
					}
				}
			}

			//get language
			$language = Language::find()->all();
			$candidateLanguage = null;
			$tmpCandidateLanguage = $secondaryCvData->language;
			if (!empty($tmpCandidateLanguage)) {
				foreach ($tmpCandidateLanguage as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$candidateLanguage[$k1]->{$v2->name} = $v2->value;
					}
					$candidateLanguage[$k1]->language = Language::find()->where(['language_id' => $candidateLanguage[$k1]->language_id])->one();
				}
			}
			
			//get hobby
			$hobby = Hobby::find()->all();
			$candidateHobby = null;
			$tmpCandidateHobby = $secondaryCvData->hobby;
			if (!empty($tmpCandidateHobby)) {
				foreach ($tmpCandidateHobby as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$candidateHobby[$k1]->{$v2->name} = $v2->value;
					}
					$candidateHobby[$k1]->hobby = Hobby::find()->where(['hobby_id' => $candidateHobby[$k1]->hobby_id])->one();
				}
			}
			
			//get job category
			$jobCategory = JobCategory::find()->orderBy(['job_category_name' => 'ASC'])->all();
			$candidateJobCategory = null;
			$tmpCandidateJobCategory = $secondaryCvData->skill;
			if (!empty($tmpCandidateJobCategory)) {
				foreach ($tmpCandidateJobCategory as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$candidateJobCategory[$k1]->{$v2->name} = $v2->value;
					}
					$candidateJobCategory[$k1]->category = JobCategory::find()->where(['job_category_id' => $candidateJobCategory[$k1]->job_category_id])->one();
				}
			}

			//get soft skill
			$softSkill = SoftSkill::find()->where(['status' => 0])->all();
			$candidateSoftSkill = null;
			$tmpCandidateSoftSkill = $secondaryCvData->softSkill;
			if (!empty($tmpCandidateSoftSkill)) {
				foreach ($tmpCandidateSoftSkill as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$candidateSoftSkill[$k1]->{$v2->name} = $v2->value;
					}
					$candidateSoftSkill[$k1]->softskill = SoftSkill::find()->where(['soft_skill_id' => $candidateSoftSkill[$k1]->soft_skill_id])->one();
				}
			}
			
			//get award
			$award = null;
			$tmpAward = $secondaryCvData->award;
			if (!empty($tmpAward)) {
				foreach ($tmpAward as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$award[$k1]->{$v2->name} = $v2->value;
					}
				}
			}

			//get activity
			$activity = null;
			$tmpActivity = $secondaryCvData->activity;
			if (!empty($tmpActivity)) {
				foreach ($tmpActivity as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$activity[$k1]->{$v2->name} = $v2->value;
					}
				}
			}

			//get certificates
			$certificates = null;
			$tmpCertificates = $secondaryCvData->certificates;
			if (!empty($tmpCertificates)) {
				foreach ($tmpCertificates as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$certificates[$k1]->{$v2->name} = $v2->value;
					}
				}
			}

			//get references
			$references = null;
			$tmpReferences = $secondaryCvData->references;
			if (!empty($tmpReferences)) {
				foreach ($tmpReferences as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$references[$k1]->{$v2->name} = $v2->value;
					}
				}
			}
		}

		$theme = CvTheme::find()->where(['status' => 1])->all();

		$view = $cv['theme']->alias . '/' . 'index';
		$title = 'JobsGO – Hồ sơ ứng viên - ' . $cv['theme']->name;
		return $this->render($view, compact('candidate', 'jobHistory', 'education', 'language', 'candidateLanguage', 'hobby', 'candidateHobby', 'jobCategory', 'candidateJobCategory', 'softSkill', 'candidateSoftSkill', 'theme', 'cv', 'title', 'award', 'activity', 'certificates', 'references', 'color', 'activeColor', 'fontFamily', 'activeFontFamily'));
	}

	public function actionDownloadResume()
	{
		// Url cv
		if (Yii::$app->request->isPost) {
			$candidateId = Yii::$app->user->identity->candidate_id;
			$encryptCandidateId = HopeShareHelper::encryptString($candidateId);
			$data = Yii::$app->request->post();
			$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
			$renderUrl = Yii::$app->params['ntv_url'] . "/create-resume/render?cv_id=$cvId";

			//cv name
			$cvId = HopeShareHelper::decryptString($cvId);
			$candidateName = Yii::$app->user->identity->name;
			$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->one();
			$cvName = HopeFileHelper::createCVGoName($candidateName, $cv->active_theme, $cv->language, $cv->id);

			//cv folder name
			$uploadFolder = HopeFileHelper::createCandCvFolder();
			$folderName = HopeFileHelper::prepareFolder($uploadFolder);

			// Full path of file on server
			$cvPath = $folderName . '/' . $cvName;

			// File url after saving
			$cvUrl = HopeFileHelper::createCandCvFullUrl($cvName);

			$options = [
				'headless', //must have
				'disable-gpu', //must have
				'run-all-compositor-stages-before-draw',
				'print-to-pdf-no-header', //bỏ dấu slash ở đuôi file pdf
				// 'virtual-time-budget' => '1000', //optional by millisecond
				// 'allow-insecure-localhost',
				// 'ignore-ssl-errors',
				// 'purge_hint_cache_store',
				'print-to-pdf' => $cvPath, //must have
				'window-size' => '1920,1080'
			];
			$execStr = HopeChromeHelper::getRenderPdfCommand($options, $renderUrl, $cvPath);
			// echo $execStr; die;
			$result = shell_exec($execStr);

			//save cv history
			$cvHistory = new CandidateCvHistory();
			$cvHistory->candidate_id = $candidateId;
			$cvHistory->cv_template_id = $cv->active_theme;
			$cvHistory->created = date('Y-m-d H:i:s');
			$cvHistory->status = 0;
			$cvHistory->cv_file_url = $cvUrl;
			$cvHistory->language = $cv->language;
			$cvHistory->save();

			//save cv_path
			$cv->cv_path = $cvPath;
			$savingResult = $cv->save();
			if (!empty($savingResult)) {
				$msg = ColorgbHelper::encryptInt($cv->id);
				$status = HopeConstant::HOPE_API_SUCCESS_STATUS;
			} else {
				$msg = "Có lỗi xuất hiện trong quá trình xuất file.";
				$status = HopeConstant::HOPE_API_UNSUCCESS_STATUS;
			}

			die(json_encode(['status' => $status, 'msg' => $msg]));
		}
	}

	public function actionGetPdf()
	{
		$cvId = Yii::$app->request->get('cv_id');
		$cvId = ColorgbHelper::decryptInt($cvId);
		$cv = Cv::find()->where(['id' => $cvId])->one();
		if (!empty($cv->cv_path)) {
			return Yii::$app->response->sendFile($cv->cv_path);
		} else {
			die;
		}
	}

	public function actionRender()
	{
		$cvId = Yii::$app->request->get('cv_id');
		$cvId = HopeShareHelper::decryptString($cvId);
		$cv = Cv::find()->where(['id' => $cvId])->with('theme')->one();

		if (empty($cv)) {
			echo 'CV not found'; die;
		}

		$candidateId = $cv->candidate_id;
		$activeColor = json_decode($cv->active_color, true);
		$activeColor = $activeColor[$cv['theme']['id']];
		$color = json_decode($cv['theme']['color'], true);
		$activeFontFamily = json_decode($cv->active_font_family, true);
		$activeFontFamily = $activeFontFamily[$cv['theme']['id']];
		$fontFamily = json_decode($cv['theme']['font_family'], true);

		$cvType = $cv->cv_type;
		if ($cvType == 1  || ($cvType == 2 && empty($cv->secondary_cv_data))) {
			$candidate = Candidate::find()->where(['candidate_id' => $candidateId])->one();
			$jobHistory = CandidateJobHistory::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->all();
			$education = CandidateEducation::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->all();
			$language = Language::find()->all();
			$candidateLanguage = CandidateLanguage::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->with('language')->all();
			$hobby = Hobby::find()->all();
			$candidateHobby = CandidateHobby::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->with('hobby')->all();
			$jobCategory = JobCategory::find()->orderBy(['job_category_name' => 'ASC'])->all();
			self::syncCandidateJobCategory();
			$candidateJobCategory = CandidateSkill::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->with('category')->all();
			$softSkill = SoftSkill::find()->where(['status' => 0])->all();
			$candidateSoftSkill = CandidateSoftSkill::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->with('softskill')->all();
			$award = Award::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->all();
			$activity = CandidateActivity::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->all();
			$certificates = Certificates::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->all();
			$references = CandidateCvReferences::find()->where(['candidate_id' => $candidateId])->orderBy(['cv_order' => 'ASC'])->all();
		} else {
			$secondaryCvData = $cv->secondary_cv_data;
			$secondaryCvData = json_decode($secondaryCvData);
			
			//get candidate
			$candidate = null;
			$candidateInfo = $secondaryCvData->candidateInfo;
			foreach ($candidateInfo as $key => $value) {
				if ($value->name == 'date_of_birth') {
					$date = str_replace('/', '-', $value->value);
					$candidate->{$value->name} = date('Y-m-d H:i:s', strtotime($date));
				} else {
					$candidate->{$value->name} = $value->value;
				}
				$dataCandidate = Candidate::findOne($candidateId);
				$candidate->avatar = $dataCandidate->avatar;
			}

			//get job history
			$jobHistory = null;
			$tmpJobHistory = $secondaryCvData->jobHistory;
			if (!empty($tmpJobHistory)) {
				foreach ($tmpJobHistory as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$jobHistory[$k1]->{$v2->name} = $v2->value;
					}
				}
			}
			
			//get education
			$education = null;
			$tmpEducation = $secondaryCvData->education;
			if (!empty($tmpEducation)) {
				foreach ($tmpEducation as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$education[$k1]->{$v2->name} = $v2->value;
					}
				}
			}

			//get language
			$language = Language::find()->all();
			$candidateLanguage = null;
			$tmpCandidateLanguage = $secondaryCvData->language;
			if (!empty($tmpCandidateLanguage)) {
				foreach ($tmpCandidateLanguage as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$candidateLanguage[$k1]->{$v2->name} = $v2->value;
					}
					$candidateLanguage[$k1]->language = Language::find()->where(['language_id' => $candidateLanguage[$k1]->language_id])->one();
				}
			}
			
			//get hobby
			$hobby = Hobby::find()->all();
			$candidateHobby = null;
			$tmpCandidateHobby = $secondaryCvData->hobby;
			if (!empty($tmpCandidateHobby)) {
				foreach ($tmpCandidateHobby as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$candidateHobby[$k1]->{$v2->name} = $v2->value;
					}
					$candidateHobby[$k1]->hobby = Hobby::find()->where(['hobby_id' => $candidateHobby[$k1]->hobby_id])->one();
				}
			}
			
			//get job category
			$jobCategory = JobCategory::find()->orderBy(['job_category_name' => 'ASC'])->all();
			$candidateJobCategory = null;
			$tmpCandidateJobCategory = $secondaryCvData->skill;
			if (!empty($tmpCandidateJobCategory)) {
				foreach ($tmpCandidateJobCategory as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$candidateJobCategory[$k1]->{$v2->name} = $v2->value;
					}
					$candidateJobCategory[$k1]->category = JobCategory::find()->where(['job_category_id' => $candidateJobCategory[$k1]->job_category_id])->one();
				}
			}

			//get soft skill
			$softSkill = SoftSkill::find()->where(['status' => 0])->all();
			$candidateSoftSkill = null;
			$tmpCandidateSoftSkill = $secondaryCvData->softSkill;
			if (!empty($tmpCandidateSoftSkill)) {
				foreach ($tmpCandidateSoftSkill as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$candidateSoftSkill[$k1]->{$v2->name} = $v2->value;
					}
					$candidateSoftSkill[$k1]->softskill = SoftSkill::find()->where(['soft_skill_id' => $candidateSoftSkill[$k1]->soft_skill_id])->one();
				}
			}
			
			//get award
			$award = null;
			$tmpAward = $secondaryCvData->award;
			if (!empty($tmpAward)) {
				foreach ($tmpAward as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$award[$k1]->{$v2->name} = $v2->value;
					}
				}
			}

			//get activity
			$activity = null;
			$tmpActivity = $secondaryCvData->activity;
			if (!empty($tmpActivity)) {
				foreach ($tmpActivity as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$activity[$k1]->{$v2->name} = $v2->value;
					}
				}
			}

			//get certificates
			$certificates = null;
			$tmpCertificates = $secondaryCvData->certificates;
			if (!empty($tmpCertificates)) {
				foreach ($tmpCertificates as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$certificates[$k1]->{$v2->name} = $v2->value;
					}
				}
			}

			//get references
			$references = null;
			$tmpReferences = $secondaryCvData->references;
			if (!empty($tmpReferences)) {
				foreach ($tmpReferences as $k1 => $v1) {
					foreach ($v1 as $k2 => $v2) {
						$references[$k1]->{$v2->name} = $v2->value;
					}
				}
			}
		}

		$theme = CvTheme::find()->all();

		if (!empty(Yii::$app->request->get('theme'))) {
			$view = Yii::$app->request->get('theme') . '/' . 'index';
		} else {
			$view = $cv['theme']->alias . '/' . 'index';
		}

		$view = $cv['theme']->alias . '/' . 'index';

		$title = 'JobsGO - Hồ sơ ứng viên - ' . $cv['theme']->name;
		$isRendered = 1;
		if (!empty($cv) && !empty($cv->language)) {
			Yii::$app->language = $cv->language;
		} else {
			Yii::$app->language = 'vi-VN';
		}
		return $this->render($view, compact('candidate', 'jobHistory', 'education', 'language', 'candidateLanguage', 'hobby', 'candidateHobby', 'jobCategory', 'candidateJobCategory', 'softSkill', 'candidateSoftSkill', 'theme', 'cv', 'title', 'award', 'activity', 'certificates', 'references', 'color', 'activeColor', 'isRendered', 'fontFamily', 'activeFontFamily'));
	}

	/**
	 * @param $arrCertificate
	 * @return array
	 *  Format year field of candidate certificate
	 */
	private function getCertificateWithFormattedTime($arrCertificate){
		$arrTimeFormattedCertificate = [];
		foreach ($arrCertificate as $key => $certificate){
			$arrTimeFormattedCertificate[$key] = $certificate;
			$arrTimeFormattedCertificate[$key]['year'] = HopeTimeHelper::getFormattedTime($certificate['year'],"Y");
		}
		return $arrTimeFormattedCertificate;
	}

	/**
	 * @param $arrActivity
	 * @return array
	 * Format start and end time of candidate activities
	 */
	private function getActivityWithFormattedTime($arrActivity){
		$arrTimeFormattedActivity = [];
		foreach ($arrActivity as $key => $activity){
			$arrTimeFormattedActivity[$key] = $activity;
			$arrTimeFormattedActivity[$key]['from'] = HopeTimeHelper::getFormattedTime($activity['from'],"d/m/Y");
			$arrTimeFormattedActivity[$key]['to'] = HopeTimeHelper::getFormattedTime($activity['to'],"d/m/Y");
		}
		return $arrTimeFormattedActivity;
	}

	/**
	 * @param $arrAwards
	 * @return array
	 * Format year field of candidate award
	 */
	private function getAwardWithFormattedTime($arrAwards){
		$arrTimeFormattedAward = [];
		foreach ($arrAwards as $key => $award){
			$arrTimeFormattedAward[$key] = $award;
			$arrTimeFormattedAward[$key]['year'] = HopeTimeHelper::getFormattedTime($award['year'],"Y");
		}
		return $arrTimeFormattedAward;
	}

	/**
	 * @param $candidateEducation
	 * @return array
	 * Format start and end time of candidate education
	 */
	private function getEducationWithFormattedTime($candidateEducation){
		$arrTimeFormattedEducation = [];
		foreach ($candidateEducation as $key => $education){
			$arrTimeFormattedEducation[$key] = $education;
			$arrTimeFormattedEducation[$key]['date_start'] = HopeTimeHelper::getFormattedTime($education['date_start'],"m/Y");
			$arrTimeFormattedEducation[$key]['date_end'] = HopeTimeHelper::getFormattedTime($education['date_end'],"m/Y");
		}
		return $arrTimeFormattedEducation;
	}

	/**
	 * @param $candidateJobHistory
	 * @return array
	 * Format start and end time of candidate job history
	 */
	private function getJobHistoryWithFormattedTime($candidateJobHistory){
		$arrTimeFormattedJobHistory = [];
		foreach ($candidateJobHistory as $key => $jobHistory){
			$arrTimeFormattedJobHistory[$key] = $jobHistory;
			$arrTimeFormattedJobHistory[$key]['date_start'] = HopeTimeHelper::getFormattedTime($jobHistory['date_start'],"m/Y");
			$arrTimeFormattedJobHistory[$key]['date_end'] = HopeTimeHelper::getFormattedTime($jobHistory['date_end'],"m/Y");
		}
		return $arrTimeFormattedJobHistory;
	}

	/**
	 * save cv
	 * @return json
	 */
	public function actionSaveCv()
	{
		if (Yii::$app->request->isPost) {
			$data = Yii::$app->request->post();
			$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
			$cvId = HopeShareHelper::decryptString($cvId);
			$cvName = !empty($data['cvName']) ? $data['cvName'] : '';
			$candidateId = Yii::$app->user->identity->candidate_id;
			$cv = Cv::find()->where(['id' => $cvId, 'candidate_id' => $candidateId])->one();
			if (!empty($cv)) {
				if ($cv->cv_type == 1) {
					//cv chính
					$candidateInfo = !empty($data['candidateInfo']) ? $data['candidateInfo'] : [];
					$jobHistory = !empty($data['jobHistory']) ? $data['jobHistory'] : [];
					$education = !empty($data['education']) ? $data['education'] : [];
					$skill = !empty($data['skill']) ? $data['skill'] : [];
					$language = !empty($data['language']) ? $data['language'] : [];
					$hobby = !empty($data['hobby']) ? $data['hobby'] : [];
					$hiddenElements = !empty($data['hiddenElements']) ? $data['hiddenElements'] : [];
					$softSkill = !empty($data['softSkill']) ? $data['softSkill'] : [];
					$award = !empty($data['award']) ? $data['award'] : [];
					$activity = !empty($data['activity']) ? $data['activity'] : [];
					$certificates = !empty($data['certificates']) ? $data['certificates'] : [];
					$references = !empty($data['references']) ? $data['references'] : [];

					$savingCandidate = self::updateCandidateInfo($candidateInfo);
					$savingJobHistory = self::updateJobHistory($jobHistory);
					$savingEducation = self::updateEducation($education);
					$savingSkill = self::updateSkill($skill);
					$savingLanguage = self::updateLanguage($language);
					$savingHobby = self::updateHobby($hobby);
					$savingHidden = self::updateHidden($hiddenElements, $cvId);
					$savingSoftSkill = self::updateSoftSkill($softSkill);
					$savingAward = self::updateAward($award);
					$savingActivity = self::updateAcitvity($activity);
					$savingCertificates = self::updateCertificates($certificates);
					$savingReferences = self::updateReferences($references);

					if ($savingCandidate && $savingJobHistory && $savingEducation && $savingSkill && $savingLanguage && $savingHobby && $savingHidden && $savingSoftSkill && $savingAward && $savingActivity && $savingCertificates && $savingReferences) {
						$candidateId = Yii::$app->user->identity->candidate_id;
						if ($cv->send_mail_unica == 0) {
							//send mail unica
							self::sendMailUnica();
							$cv->send_mail_unica = 1;
						}

						//update saving_success
						$cv->secondary_cv_data = json_encode($data, JSON_UNESCAPED_UNICODE);
						$cv->saving_success += 1;
						$cv->save();

						die(json_encode(['status' => HopeConstant::HOPE_API_SUCCESS_STATUS, 'msg' => 'Lưu hồ sơ thành công!']));
					} else {
						die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Có lỗi xuất hiện trong quá trình lưu hồ sơ.']));
					}
				} elseif ($cv->cv_type == 2) {
					//cv phụ
					$hiddenElements = !empty($data['hiddenElements']) ? $data['hiddenElements'] : [];
					$savingHidden = self::updateHidden($hiddenElements, $cvId);

					$cv->secondary_cv_data = json_encode($data, JSON_UNESCAPED_UNICODE);
					$cv->saving_success += 1;
					$cv->cv_name = $cvName;

					if ($cv->save()) {
						die(json_encode(['status' => HopeConstant::HOPE_API_SUCCESS_STATUS, 'msg' => 'Lưu hồ sơ thành công!']));
					} else {
						die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Có lỗi xuất hiện trong quá trình lưu hồ sơ.']));
					}
				} else {
					die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Loại CV không xác định']));
				}
			} else {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Không tìm thấy CV']));
			}
		}
	}

	protected static function sendMailUnica()
	{
		$candidateId = Yii::$app->user->identity->candidate_id;
		$candidateModel = Candidate::find()->where(['candidate_id' => $candidateId])->one();
		$mailModel = new \common\components\mail\HopeMailCandidate();
		$emailType = HopeConstant::HOPE_MAIL_CANDIDATE_GO_CV_UNICA;
		$receiverEmail = $candidateModel->email;
		$employerModel = null;
		$jobModel = null;
		$subEmployerModel = null;
		$customEmailSubject = ''; // add custom để gửi nội dung custom
		$customEmailBody = ''; // add custom để gửi nội dung custom
		$fromEmail = '';
		$setupResult = $mailModel->setupDataToSendMail($emailType, $receiverEmail, $employerModel,
			$candidateModel, $jobModel, $subEmployerModel, $customEmailSubject, $customEmailBody, $fromEmail);
	}

	/**
	 * update candidate info
	 * @param array $candidateInfo
	 * @return bool
	 */
	protected static function updateCandidateInfo($candidateInfo)
	{
		$candidateId = Yii::$app->user->identity->candidate_id;
		$candidate = Candidate::find()->where(['candidate_id' => $candidateId])->one();
		if (!empty($candidate)) {
			$arrData = [];
			foreach ($candidateInfo as $key => $value) {
				if ($value['name'] == 'date_of_birth') {
					$date = str_replace('/', '-', $value['value']);
					$value['value'] = date('Y-m-d', strtotime($date));
				}

				if ($value['name'] == 'name') {
					$value['value'] = mb_convert_case($value['value'], MB_CASE_TITLE, "UTF-8");
				}
				$arrData[$value['name']] = HopeTrimHelper::trimXssChar($value['value']);
			}

			if (!empty($arrData['short_bio_html'])) {
				$arrData['short_bio_html'] = trim($arrData['short_bio_html']);
				$arrData['short_bio'] = TextFormatHelper::convertHtmlToText($arrData['short_bio_html']);
			} else {
				$arrData['short_bio_html'] = "";
				$arrData['short_bio'] = "";
			}

			$arrData['updated'] = date('Y-m-d H:i:s');

			$savingResult = $candidate->updateAll($arrData, ['candidate_id' => $candidateId]);
			if (!empty($savingResult)) {
				return true;
			} else {
				return false;
			}
		}

		return false;
	}

	/**
	 * update job history
	 * @param array $jobHistory
	 * @return bool
	 */
	protected static function updateJobHistory($jobHistory)
	{
		$result = true;
		$candidateId = Yii::$app->user->identity->candidate_id;

		//get deleted item, currently is all, but will remove when detect updated item.
		$deleteItem = self::getCurrentJobHistory($candidateId);

		if (!empty($jobHistory)) {
			$arrData = [];
			$i = 0;
			foreach ($jobHistory as $item) {
				//transform input data
				foreach ($item as $key => $value) {
					$arrData[$i][$value['name']] = HopeTrimHelper::trimXssChar($value['value']);
				}
				$i++;
			}

			$cv_order = 1;
			foreach ($arrData as $key => $value) {
				$value['cv_order'] = $cv_order;
				if (!empty($value['job_description_html'])) {
					$value['job_description_html'] = trim($value['job_description_html']);
					$value['job_description'] = TextFormatHelper::convertHtmlToText($value['job_description_html']);
				} else {
					$value['job_description_html'] = "";
					$value['job_description'] = "";
				}

				if (!empty($value['candidate_job_history_id'])) {
					$id = $value['candidate_job_history_id'];
					//get deleted item
					if (($k = array_search($id, $deleteItem)) !== false) {
						unset($deleteItem[$k]);
					}

					$value['updated'] = date('Y-m-d H:i:s');

					//update existed job history
					$updatedJobHistory = CandidateJobHistory::find()->where(['candidate_job_history_id' => $id, 'candidate_id' => $candidateId])->one();
					if (!empty($updatedJobHistory)) {
						$updateResult = CandidateJobHistory::updateAll($value, ['candidate_job_history_id' => $id]);
						if (empty($updateResult)) {
							$result = false;
						}
					}
				} else {
					//new job history
					$newJobHistory = new CandidateJobHistory($value);
					$newJobHistory->candidate_id = $candidateId;
					$newResult = $newJobHistory->save();
					if (empty($newResult)) {
						$result = false;
					}
				}
				$cv_order++;
			}

			//delete deleted item
			if (!empty($deleteItem)) {
				$deleteResult = CandidateJobHistory::deleteAll(['candidate_job_history_id' => $deleteItem]);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		} else {
			//clear all job history of candidate by candidate_id
			if (!empty($deleteItem)) {
				$deleteResult = CandidateJobHistory::deleteAll('candidate_id = ' . $candidateId);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		}

		return $result;
	}

	/**
	 * get current job history
	 * @param integer $candidateID
	 * @return array
	 */
	protected static function getCurrentJobHistory($candidateId)
	{
		$currentJobHistory = CandidateJobHistory::find()->select(['candidate_job_history_id'])->where(['candidate_id' => $candidateId])->asArray()->all();
		$arrCurrentJobHistory = [];
		if (!empty($currentJobHistory)) {
			foreach ($currentJobHistory as $key => $value) {
				$arrCurrentJobHistory[] = $value['candidate_job_history_id'];
			}
		}

		return $arrCurrentJobHistory;
	}

	/**
	 * update candidate education
	 * @param array $education
	 * @return bool
	 */
	protected static function updateEducation($education)
	{
		$result = true;
		$candidateId = Yii::$app->user->identity->candidate_id;

		//get deleted item, currently is all, but will remove when detect updated item.
		$deleteItem = self::getCurrentEducation($candidateId);

		if (!empty($education)) {
			$arrData = [];
			$i = 0;
			foreach ($education as $item) {
				//transform input data
				foreach ($item as $key => $value) {
					$arrData[$i][$value['name']] = HopeTrimHelper::trimXssChar($value['value']);
				}
				$i++;
			}

			$cv_order = 1;
			foreach ($arrData as $key => $value) {
				$value['cv_order'] = $cv_order;
				if (!empty($value['candidate_education_id'])) {
					$id = $value['candidate_education_id'];

					//get deleted item
					if (($k = array_search($id, $deleteItem)) !== false) {
						unset($deleteItem[$k]);
					}

					$value['updated'] = date('Y-m-d H:i:s');

					//update existed education
					$updatedEducation = CandidateEducation::find()->where(['candidate_education_id' => $id, 'candidate_id' => $candidateId])->one();
					if (!empty($updatedEducation)) {
						$updateResult = CandidateEducation::updateAll($value, ['candidate_education_id' => $id]);
						if (empty($updateResult)) {
							$result = false;
						}
					}
				} else {
					//new job education
					$newEducation = new CandidateEducation($value);
					$newEducation->candidate_id = $candidateId;
					$newResult = $newEducation->save();
					if (empty($newResult)) {
						$result = false;
					}
				}
				$cv_order++;
			}

			//delete deleted item
			if (!empty($deleteItem)) {
				$deleteResult = CandidateEducation::deleteAll(['candidate_education_id' => $deleteItem]);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		} else {
			//clear all job education of candidate by candidate_id
			if (!empty($deleteItem)) {
				$deleteResult = CandidateEducation::deleteAll('candidate_id = ' . $candidateId);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		}

		return $result;
	}

	/**
	 * get current education
	 * @param integer $candidateID
	 * @return array
	 */
	protected static function getCurrentEducation($candidateId)
	{
		$current = CandidateEducation::find()->select(['candidate_education_id'])->where(['candidate_id' => $candidateId])->asArray()->all();
		$arrCurrent = [];
		if (!empty($current)) {
			foreach ($current as $key => $value) {
				$arrCurrent[] = $value['candidate_education_id'];
			}
		}

		return $arrCurrent;
	}

	/**
	 * update skill
	 * @param array $skill
	 * @return bool
	 */
	protected static function updateSkill($skill)
	{
		$result = true;
		$candidateId = Yii::$app->user->identity->candidate_id;

		//get deleted item, currently is all, but will remove when detect updated item.
		$deleteItem = self::getCurrentSkill($candidateId);

		if (!empty($skill)) {
			$arrData = [];
			$i = 0;
			foreach ($skill as $item) {
				//transform input data
				foreach ($item as $key => $value) {
					$arrData[$i][$value['name']] = HopeTrimHelper::trimXssChar($value['value']);
				}
				$i++;
			}

			$cv_order = 1;
			foreach ($arrData as $key => $value) {
				$value['cv_order'] = $cv_order;
				if (!empty($value['candidate_job_category_id'])) {
					$id = $value['candidate_job_category_id'];
					//get deleted item
					if (($k = array_search($id, $deleteItem)) !== false) {
						unset($deleteItem[$k]);
					}

					if (empty($value['job_category_id'])) {
						$value['job_category_id'] = 0;
					}

					//update existed category
					$updatedCandidateJobCategory = CandidateSkill::find()->where(['candidate_job_category_id' => $id, 'candidate_id' => $candidateId])->one();
					if (!empty($updatedCandidateJobCategory)) {
						$updateResult = CandidateSkill::updateAll($value, ['candidate_job_category_id' => $id]);
						if (!empty($updateResult || $updateResult == 0)) {
							$result = true;
						} else {
							$result = false;
						}
					} else {
						if (empty($updatedCandidateSkill)) {
							$checkNewSkill = CandidateSkill::find()->where(['job_category_id' => $value['job_category_id'], 'candidate_id' => $candidateId])->one();
							if (empty($checkNewSkill)) {
								unset($value['candidate_job_category_id']);
								$newCandidateSkill = new CandidateSkill($value);
								$newCandidateSkill->candidate_id = $candidateId;
								$newResult = $newCandidateSkill->save();
							}
						}
					}
				} else {
					//new job category
					$checkJobCategory = JobCategory::findOne($value['job_category_id']);
					if (empty($checkJobCategory)) {
						$value['job_category_id'] = 0;
					}
					$newCandidateJobCategory = new CandidateSkill($value);
					$newCandidateJobCategory->candidate_id = $candidateId;
					$newResult = $newCandidateJobCategory->save();
					if (empty($newResult)) {
						$result = false;
					}
				}
				$cv_order++;
			}

			//delete deleted item
			if (!empty($deleteItem)) {
				$deleteResult = CandidateSkill::deleteAll(['candidate_job_category_id' => $deleteItem]);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		} else {
			// clear all job category of candidate by candidate_id
			if (!empty($deleteItem)) {
				$deleteResult = CandidateSkill::deleteAll('candidate_id = ' . $candidateId);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		}

		self::syncCandidateSkill();
		return $result;
	}

	/**
	 * sync candidate_skill into candidate_job_category
	 * @return void
	 */
	protected function syncCandidateSkill()
	{
		$candidateId = Yii::$app->user->identity->candidate_id;
		$candidateSkills = CandidateSkill::find()->where(['candidate_id' => $candidateId])->all();
		$candidateJobCategory = CandidateJobCategory::find()->where(['candidate_id' => $candidateId])->all();
		
		//sync candidate_skill into candidate_job_category
		if (!empty($candidateSkills)) {
			foreach ($candidateSkills as $key => $value) {
				if (!empty($value->job_category_id)) {
					$checkCategory = CandidateJobCategory::find()->where(['candidate_id' => $candidateId, 'job_category_id' => $value->job_category_id])->one();
					if (!empty($checkCategory)) {
						//nếu có thì update
						$checkCategory->experiment_duration = $value->experiment_duration;
						$checkCategory->save();
					} else {
						//nếu chưa có thì thêm mới
						$newCandidateJobCategory = new CandidateJobCategory();
						$newCandidateJobCategory->candidate_id = $candidateId;
						$newCandidateJobCategory->job_category_id = $value->job_category_id;
						$newCandidateJobCategory->experiment_duration = $value->experiment_duration;
						$newCandidateJobCategory->cv_order = 0;
						$newCandidateJobCategory->type = 1;
						$newCandidateJobCategory->save();
					}
				}
			}
		}
		
		if (!empty($candidateJobCategory)) {
			foreach ($candidateJobCategory as $key => $value) {
				$checkCategory = CandidateSkill::find()->where(['candidate_id' => $candidateId, 'job_category_id' => $value->job_category_id])->one();
				if (empty($checkCategory)) {
					//xóa
					$value->delete();
				}
			}
		}
	}

	/**
	 * sync candidate_job_category into candidate_skill
	 * @return void
	 */
	protected function syncCandidateJobCategory()
	{
		$candidateId = Yii::$app->user->identity->candidate_id;
		$candidateSkills = CandidateSkill::find()->where(['candidate_id' => $candidateId])->all();
		$candidateJobCategory = CandidateJobCategory::find()->where(['candidate_id' => $candidateId])->all();
		
		//sync candidate_skill into candidate_job_category
		if (!empty($candidateJobCategory)) {
			foreach ($candidateJobCategory as $key => $value) {
				if (!empty($value->job_category_id)) {
					$checkSkill = CandidateSkill::find()->where(['candidate_id' => $candidateId, 'job_category_id' => $value->job_category_id])->one();
					if (!empty($checkSkill)) {
						//nếu có thì update
						$checkSkill->experiment_duration = $value->experiment_duration;
						$checkSkill->save();
					} else {
						//nếu chưa có thì thêm mới
						$newCandidateSkill = new CandidateSkill();
						$newCandidateSkill->candidate_id = $candidateId;
						$newCandidateSkill->job_category_id = $value->job_category_id;
						$newCandidateSkill->experiment_duration = $value->experiment_duration;
						$newCandidateSkill->cv_order = 0;
						$newCandidateSkill->save();
					}
				}
			}
		}
		
		if (!empty($candidateSkills)) {
			foreach ($candidateSkills as $key => $value) {
				if (!empty($value->job_category_id)) {
					$checkCategory = CandidateJobCategory::find()->where(['candidate_id' => $candidateId, 'job_category_id' => $value->job_category_id])->one();
					if (empty($checkCategory)) {
						//xóa
						$value->delete();
					}
				}
			}
		}
	}

	/**
	 * get current skill
	 * @param integer $candidateID
	 * @return array
	 */
	protected static function getCurrentSkill($candidateId)
	{
		$current = CandidateSkill::find()->select(['candidate_job_category_id'])->where(['candidate_id' => $candidateId])->asArray()->all();
		$arrCurrent = [];
		if (!empty($current)) {
			foreach ($current as $key => $value) {
				$arrCurrent[] = $value['candidate_job_category_id'];
			}
		}

		return $arrCurrent;
	}

	/**
	 * update language
	 * @param array $language
	 * @return bool
	 */
	protected static function updateLanguage($language)
	{
		$result = true;
		$candidateId = Yii::$app->user->identity->candidate_id;

		//get deleted item, currently is all, but will remove when detect updated item.
		$deleteItem = self::getCurrentLanguage($candidateId);

		if (!empty($language)) {
			$arrData = [];
			$i = 0;
			foreach ($language as $item) {
				//transform input data
				foreach ($item as $key => $value) {
					$arrData[$i][$value['name']] = HopeTrimHelper::trimXssChar($value['value']);
				}
				$i++;
			}

			$cv_order = 1;
			foreach ($arrData as $key => $value) {
				$value['cv_order'] = $cv_order;
				if (!empty($value['candidate_language_id'])) {
					$id = $value['candidate_language_id'];
					//get deleted item
					if (($k = array_search($id, $deleteItem)) !== false) {
						unset($deleteItem[$k]);
					}

					//update existed language
					$updatedCandidateLanguage = CandidateLanguage::find()->where(['candidate_language_id' => $id, 'candidate_id' => $candidateId])->one();
					if (!empty($updatedCandidateLanguage)) {
						$updateResult = CandidateLanguage::updateAll($value, ['candidate_language_id' => $id]);
						if (!empty($updateResult || $updateResult == 0)) {
							$result = true;
						} else {
							$result = false;
						}
					}
				} else {
					//new candidate language
					$newCandidateLanguage = new CandidateLanguage($value);
					$newCandidateLanguage->candidate_id = $candidateId;
					$newResult = $newCandidateLanguage->save();

					if (empty($newResult)) {
						$result = false;
					}
				}
				$cv_order++;
			}

			//delete deleted item
			if (!empty($deleteItem)) {
				$deleteResult = CandidateLanguage::deleteAll(['candidate_language_id' => $deleteItem]);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		} else {
			// clear all language of candidate by candidate_id
			if (!empty($deleteItem)) {
				$deleteResult = CandidateLanguage::deleteAll('candidate_id = ' . $candidateId);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		}

		return $result;
	}

	/**
	 * get current language
	 * @param integer $candidateID
	 * @return array
	 */
	protected static function getCurrentLanguage($candidateId)
	{
		$current = CandidateLanguage::find()->select(['candidate_language_id'])->where(['candidate_id' => $candidateId])->asArray()->all();
		$arrCurrent = [];
		if (!empty($current)) {
			foreach ($current as $key => $value) {
				$arrCurrent[] = $value['candidate_language_id'];
			}
		}

		return $arrCurrent;
	}

	/**
	 * update hobby
	 * @param array $hobby
	 * @return bool
	 */
	protected static function updateHobby($hobby)
	{
		$result = true;
		$candidateId = Yii::$app->user->identity->candidate_id;

		//get deleted item, currently is all, but will remove when detect updated item.
		$deleteItem = self::getCurrentHobby($candidateId);

		if (!empty($hobby)) {
			$arrData = [];
			$i = 0;
			foreach ($hobby as $item) {
				//transform input data
				foreach ($item as $key => $value) {
					$arrData[$i][$value['name']] = HopeTrimHelper::trimXssChar($value['value']);
				}
				$i++;
			}

			$cv_order = 1;
			foreach ($arrData as $key => $value) {
				$value['cv_order'] = $cv_order;
				if (!empty($value['candidate_hobby_id'])) {
					$id = $value['candidate_hobby_id'];
					//get deleted item
					if (($k = array_search($id, $deleteItem)) !== false) {
						unset($deleteItem[$k]);
					}

					//update existed hobby
					$updatedCandidateHobby = CandidateHobby::find()->where(['candidate_hobby_id' => $id, 'candidate_id' => $candidateId])->one();
					if (!empty($updatedCandidateHobby)) {
						$updateResult = CandidateHobby::updateAll($value, ['candidate_hobby_id' => $id]);
						if (!empty($updateResult || $updateResult == 0)) {
							$result = true;
						} else {
							$result = false;
						}
					}
				} else {
					//new candidate hobby
					$newCandidateHobby = new CandidateHobby($value);
					$newCandidateHobby->candidate_id = $candidateId;
					$newResult = $newCandidateHobby->save();

					if (empty($newResult)) {
						$result = false;
					}
				}
				$cv_order++;
			}

			//delete deleted item
			if (!empty($deleteItem)) {
				$deleteResult = CandidateHobby::deleteAll(['candidate_hobby_id' => $deleteItem]);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		} else {
			// clear all hobby of candidate by candidate_id
			if (!empty($deleteItem)) {
				$deleteResult = CandidateHobby::deleteAll('candidate_id = ' . $candidateId);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		}

		return $result;
	}

	/**
	 * get current hobby
	 * @param integer $candidateID
	 * @return array
	 */
	protected static function getCurrentHobby($candidateId)
	{
		$current = CandidateHobby::find()->select(['candidate_hobby_id'])->where(['candidate_id' => $candidateId])->asArray()->all();
		$arrCurrent = [];
		if (!empty($current)) {
			foreach ($current as $key => $value) {
				$arrCurrent[] = $value['candidate_hobby_id'];
			}
		}

		return $arrCurrent;
	}

	/**
	 * update hidden elements
	 * @param array $hiddenElements
	 * @return mixed
	 */
	protected static function updateHidden($hiddenElements, $cvId)
	{
		$strHidden = '';
		$candidateId = Yii::$app->user->identity->candidate_id;
		$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->one();
		$cv->hidden_element = $strHidden;

		if (!empty($hiddenElements)) {
			$len = count($hiddenElements);
			foreach ($hiddenElements as $key => $value) {
				if ($key == $len - 1) {
					$strHidden .= $value;
				} else {
					$strHidden .= $value . ',';
				}
			}
			$cv->hidden_element = $strHidden;
		}
		$cv->updated_at = date('Y-m-d H:i:s');
		$savingResult = $cv->save();
		if (!empty($savingResult)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * update soft skill
	 * @param array $softSkill
	 * @return mixed
	 */
	protected static function updateSoftSkill($softSkill)
	{
		$result = true;
		$candidateId = Yii::$app->user->identity->candidate_id;

		//get deleted item, currently is all, but will remove when detect updated item.
		$deleteItem = self::getCurrentSoftSkill($candidateId);

		if (!empty($softSkill)) {
			$arrData = [];
			$i = 0;
			foreach ($softSkill as $item) {
				//transform input data
				foreach ($item as $key => $value) {
					$arrData[$i][$value['name']] = HopeTrimHelper::trimXssChar($value['value']);
				}
				$i++;
			}

			$cv_order = 1;
			foreach ($arrData as $key => $value) {
				$value['cv_order'] = $cv_order;
				if (!empty($value['candidate_soft_skill_id'])) {
					$id = $value['candidate_soft_skill_id'];
					//get deleted item
					if (($k = array_search($id, $deleteItem)) !== false) {
						unset($deleteItem[$k]);
					}

					//update existed soft skill
					$updatedCandidateSoftSkill = CandidateSoftSkill::find()->where(['candidate_soft_skill_id' => $id, 'candidate_id' => $candidateId])->one();
					if (!empty($updatedCandidateSoftSkill)) {
						$updateResult = CandidateSoftSkill::updateAll($value, ['candidate_soft_skill_id' => $id]);
						if (!empty($updateResult || $updateResult == 0)) {
							$result = true;
						} else {
							$result = false;
						}
					}
				} else {
					//new candidate soft skill
					$newCandidateSoftSkill = new CandidateSoftSkill($value);
					$newCandidateSoftSkill->candidate_id = $candidateId;
					$newResult = $newCandidateSoftSkill->save();

					if (empty($newResult)) {
						$result = false;
					}
				}
				$cv_order++;
			}

			//delete deleted item
			if (!empty($deleteItem)) {
				$deleteResult = CandidateSoftSkill::deleteAll(['candidate_soft_skill_id' => $deleteItem]);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		} else {
			// clear all soft skill of candidate by candidate_id
			if (!empty($deleteItem)) {
				$deleteResult = CandidateSoftSkill::deleteAll('candidate_id = ' . $candidateId);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		}

		return $result;
	}

	/**
	 * get current soft skill
	 * @param integer $candidateID
	 * @return array
	 */
	protected static function getCurrentSoftSkill($candidateId)
	{
		$current = CandidateSoftSkill::find()->select(['candidate_soft_skill_id'])->where(['candidate_id' => $candidateId])->asArray()->all();
		$arrCurrent = [];
		if (!empty($current)) {
			foreach ($current as $key => $value) {
				$arrCurrent[] = $value['candidate_soft_skill_id'];
			}
		}

		return $arrCurrent;
	}

	/**
	 * update award
	 * @param array $award
	 * @return mixed
	 */
	protected static function updateAward($award)
	{
		$result = true;
		$candidateId = Yii::$app->user->identity->candidate_id;

		//get deleted item, currently is all, but will remove when detect updated item.
		$deleteItem = self::getCurentAward($candidateId);

		if (!empty($award)) {
			$arrData = [];
			$i = 0;
			foreach ($award as $item) {
				//transform input data
				foreach ($item as $key => $value) {
					$arrData[$i][$value['name']] = HopeTrimHelper::trimXssChar($value['value']);
				}
				$i++;
			}

			$cv_order = 1;
			foreach ($arrData as $key => $value) {
				$value['cv_order'] = $cv_order;
				if (!empty($value['id'])) {
					$id = $value['id'];
					//get deleted item
					if (($k = array_search($id, $deleteItem)) !== false) {
						unset($deleteItem[$k]);
					}

					$value['updated_at'] = date('Y-m-d H:i:s');

					//update existed award
					$updatedAward = Award::find()->where(['id' => $id, 'candidate_id' => $candidateId])->one();
					if (!empty($updatedAward)) {
						$updateResult = Award::updateAll($value, ['id' => $id]);
						if (empty($updateResult)) {
							$result = false;
						}
					}
				} else {
					//new candidate award
					$newAward = new Award($value);
					$newAward->candidate_id = $candidateId;
					$newResult = $newAward->save();

					if (empty($newResult)) {
						$result = false;
					}
				}
				$cv_order++;
			}

			//delete deleted item
			if (!empty($deleteItem)) {
				$deleteResult = Award::deleteAll(['id' => $deleteItem]);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		} else {
			// clear all award of candidate by candidate_id
			if (!empty($deleteItem)) {
				$deleteResult = Award::deleteAll('candidate_id = ' . $candidateId);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		}

		return $result;
	}

	/**
	 * get current award
	 * @param integer $candidateID
	 * @return array
	 */
	protected static function getCurentAward($candidateId)
	{
		$current = Award::find()->select(['id'])->where(['candidate_id' => $candidateId])->asArray()->all();
		$arrCurrent = [];
		if (!empty($current)) {
			foreach ($current as $key => $value) {
				$arrCurrent[] = $value['id'];
			}
		}

		return $arrCurrent;
	}

	/**
	 * update references
	 * @param array $references
	 * @return mixed
	 */
	protected static function updateReferences($references)
	{
		$result = true;
		$candidateId = Yii::$app->user->identity->candidate_id;

		//get deleted item, currently is all, but will remove when detect updated item.
		$deleteItem = self::getCurentReferences($candidateId);

		if (!empty($references)) {
			$arrData = [];
			$i = 0;
			foreach ($references as $item) {
				//transform input data
				foreach ($item as $key => $value) {
					$arrData[$i][$value['name']] = HopeTrimHelper::trimXssChar($value['value']);
				}
				$i++;
			}

			$cv_order = 1;
			foreach ($arrData as $key => $value) {
				$value['cv_order'] = $cv_order;
				if (!empty($value['id'])) {
					$id = $value['id'];
					//get deleted item
					if (($k = array_search($id, $deleteItem)) !== false) {
						unset($deleteItem[$k]);
					}

					$value['updated'] = date('Y-m-d H:i:s');

					//update existed references
					$updatedReferences = CandidateCvReferences::find()->where(['id' => $id, 'candidate_id' => $candidateId])->one();
					if (!empty($updatedReferences)) {
						$updateResult = CandidateCvReferences::updateAll($value, ['id' => $id]);
						if (empty($updateResult)) {
							$result = false;
						}
					}
				} else {
					//new candidate references
					$newReferences = new CandidateCvReferences($value);
					$newReferences->candidate_id = $candidateId;
					$newResult = $newReferences->save();

					if (empty($newResult)) {
						$result = false;
					}
				}
				$cv_order++;
			}

			//delete deleted item
			if (!empty($deleteItem)) {
				$deleteResult = CandidateCvReferences::deleteAll(['id' => $deleteItem]);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		} else {
			// clear all references of candidate by candidate_id
			if (!empty($deleteItem)) {
				$deleteResult = CandidateCvReferences::deleteAll('candidate_id = ' . $candidateId);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		}

		return $result;
	}

	/**
	 * get current references
	 * @param integer $candidateID
	 * @return array
	 */
	protected static function getCurentReferences($candidateId)
	{
		$current = CandidateCvReferences::find()->select(['id'])->where(['candidate_id' => $candidateId])->asArray()->all();
		$arrCurrent = [];
		if (!empty($current)) {
			foreach ($current as $key => $value) {
				$arrCurrent[] = $value['id'];
			}
		}

		return $arrCurrent;
	}

	/**
	 * update activity
	 * @param array $activity
	 * @return mixed
	 */
	protected static function updateAcitvity($activity)
	{
		$result = true;
		$candidateId = Yii::$app->user->identity->candidate_id;

		//get deleted item, currently is all, but will remove when detect updated item.
		$deleteItem = self::getCurentActivity($candidateId);

		if (!empty($activity)) {
			$arrData = [];
			$i = 0;
			foreach ($activity as $item) {
				//transform input data
				foreach ($item as $key => $value) {
					$arrData[$i][$value['name']] = HopeTrimHelper::trimXssChar($value['value']);
				}
				$i++;
			}

			$cv_order = 1;
			foreach ($arrData as $key => $value) {
				$value['cv_order'] = $cv_order;
				if (!empty($value['id'])) {
					$id = $value['id'];
					//get deleted item
					if (($k = array_search($id, $deleteItem)) !== false) {
						unset($deleteItem[$k]);
					}

					$value['updated_at'] = date('Y-m-d H:i:s');

					//update existed activity
					$updatedActivity = CandidateActivity::find()->where(['id' => $id, 'candidate_id' => $candidateId])->one();
					if (!empty($updatedActivity)) {
						$updateResult = CandidateActivity::updateAll($value, ['id' => $id]);
						if (empty($updateResult)) {
							$result = false;
						}
					}
				} else {
					//new candidate activity
					$newActivity = new CandidateActivity($value);
					$newActivity->candidate_id = $candidateId;
					$newResult = $newActivity->save();

					if (empty($newResult)) {
						$result = false;
					}
				}
				$cv_order++;
			}

			//delete deleted item
			if (!empty($deleteItem)) {
				$deleteResult = CandidateActivity::deleteAll(['id' => $deleteItem]);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		} else {
			// clear all activity of candidate by candidate_id
			if (!empty($deleteItem)) {
				$deleteResult = CandidateActivity::deleteAll('candidate_id = ' . $candidateId);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		}

		return $result;
	}

	/**
	 * get current activity
	 * @param integer $candidateID
	 * @return array
	 */
	protected static function getCurentActivity($candidateId)
	{
		$current = CandidateActivity::find()->select(['id'])->where(['candidate_id' => $candidateId])->asArray()->all();
		$arrCurrent = [];
		if (!empty($current)) {
			foreach ($current as $key => $value) {
				$arrCurrent[] = $value['id'];
			}
		}

		return $arrCurrent;
	}

	/**
	 * update certificates
	 * @param array $certificates
	 * @return mixed
	 */
	protected static function updateCertificates($certificates)
	{
		$result = true;
		$candidateId = Yii::$app->user->identity->candidate_id;

		//get deleted item, currently is all, but will remove when detect updated item.
		$deleteItem = self::getCurentCertificate($candidateId);

		if (!empty($certificates)) {
			$arrData = [];
			$i = 0;
			foreach ($certificates as $item) {
				//transform input data
				foreach ($item as $key => $value) {
					$arrData[$i][$value['name']] = HopeTrimHelper::trimXssChar($value['value']);
				}
				$i++;
			}

			$cv_order = 1;
			foreach ($arrData as $key => $value) {
				$value['cv_order'] = $cv_order;
				if (!empty($value['id'])) {
					$id = $value['id'];
					//get deleted item
					if (($k = array_search($id, $deleteItem)) !== false) {
						unset($deleteItem[$k]);
					}

					$value['updated_at'] = date('Y-m-d H:i:s');

					//update existed certificates
					$updatedCertificate = Certificates::find()->where(['id' => $id, 'candidate_id' => $candidateId])->one();
					if (!empty($updatedCertificate)) {
						$updateResult = Certificates::updateAll($value, ['id' => $id]);
						if (empty($updateResult)) {
							$result = false;
						}
					}
				} else {
					//new candidate certificates
					$newCertificate = new Certificates($value);
					$newCertificate->candidate_id = $candidateId;
					$newResult = $newCertificate->save();

					if (empty($newResult)) {
						$result = false;
					}
				}
				$cv_order++;
			}

			//delete deleted item
			if (!empty($deleteItem)) {
				$deleteResult = Certificates::deleteAll(['id' => $deleteItem]);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		} else {
			// clear all certificates of candidate by candidate_id
			if (!empty($deleteItem)) {
				$deleteResult = Certificates::deleteAll('candidate_id = ' . $candidateId);
				if (empty($deleteResult)) {
					$result = false;
				}
			}
		}

		return $result;
	}

	/**
	 * get current certificates
	 * @param integer $candidateID
	 * @return array
	 */
	protected static function getCurentCertificate($candidateId)
	{
		$current = Certificates::find()->select(['id'])->where(['candidate_id' => $candidateId])->asArray()->all();
		$arrCurrent = [];
		if (!empty($current)) {
			foreach ($current as $key => $value) {
				$arrCurrent[] = $value['id'];
			}
		}

		return $arrCurrent;
	}

	/**
	 * hide elements
	 * @param array $hiddenElements
	 * @return mixed
	 */
	public function actionHideElements()
	{
		$cvId = Yii::$app->request->post('cvId');
		if (!empty($cvId)) {
			$cvId = HopeShareHelper::decryptString($cvId);
		} else {
			$cvId = '';
		}

		$cv = Cv::find()->where(['id' => $cvId])->one();
		if (!empty($cv)) {
			if (!empty($cv->hidden_element)) {
				$strHidden = $cv->hidden_element;
				$arrHidden = explode(',', $strHidden);
				die(json_encode($arrHidden));
			}
		}

	}

	/**
	 * load job history template
	 * @return mixed
	 */
	public function actionGetJobHistoryTemplate()
	{
		$data = Yii::$app->request->post();
		$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
		$cvId = HopeShareHelper::decryptString($cvId);
		$candidateId = Yii::$app->user->identity->candidate_id;
		$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->with('theme')->one();
		$view = $cv['theme']->alias . '/' . 'job_history_template';
		return $this->render($view);
	}

	/**
	 * load education template
	 * @return mixed
	 */
	public function actionGetEducationTemplate()
	{
		$data = Yii::$app->request->post();
		$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
		$cvId = HopeShareHelper::decryptString($cvId);
		$candidateId = Yii::$app->user->identity->candidate_id;
		$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->with('theme')->one();
		$view = $cv['theme']->alias . '/' . 'education_template';
		return $this->render($view);
	}

	/**
	 * load award template
	 * @return mixed
	 */
	public function actionGetAwardTemplate()
	{
		$data = Yii::$app->request->post();
		$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
		$cvId = HopeShareHelper::decryptString($cvId);
		$candidateId = Yii::$app->user->identity->candidate_id;
		$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->with('theme')->one();
		$view = $cv['theme']->alias . '/' . 'award_template';
		return $this->render($view);
	}

	/**
	 * load references tempalte
	 * @return mixed
	 */
	public function actionGetReferencesTemplate()
	{
		$data = Yii::$app->request->post();
		$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
		$cvId = HopeShareHelper::decryptString($cvId);
		$candidateId = Yii::$app->user->identity->candidate_id;
		$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->with('theme')->one();
		$view = $cv['theme']->alias . '/' . 'references_template';
		return $this->render($view);
	}

	/**
	 * load activity template
	 * @return mixed
	 */
	public function actionGetActivityTemplate()
	{
		$data = Yii::$app->request->post();
		$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
		$cvId = HopeShareHelper::decryptString($cvId);
		$candidateId = Yii::$app->user->identity->candidate_id;
		$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->with('theme')->one();
		$view = $cv['theme']->alias . '/' . 'activity_template';
		return $this->render($view);
	}

	/**
	 * load education template
	 * @return mixed
	 */
	public function actionGetCertificatesTemplate()
	{
		$data = Yii::$app->request->post();
		$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
		$cvId = HopeShareHelper::decryptString($cvId);
		$candidateId = Yii::$app->user->identity->candidate_id;
		$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->with('theme')->one();
		$view = $cv['theme']->alias . '/' . 'certificates_template';
		return $this->render($view);
	}

	/**
	 * change theme
	 * @return json
	 */
	public function actionChangeTheme()
	{
		if (Yii::$app->request->isPost) {
			$data = Yii::$app->request->post();
			$themeId = !empty($data['theme_id']) ? $data['theme_id'] : null;
			$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
			$cvId = HopeShareHelper::decryptString($cvId);
			$theme = CvTheme::findOne($themeId);
			if (empty($theme)) {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Không tìm thấy biểu mẫu. Vui lòng thử lại.']));
			}

			$candidateId = Yii::$app->user->identity->candidate_id;
			$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->one();
			if (empty($cv)) {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Không tìm thấy hồ sơ. Vui lòng thử lại.']));
			}

			if ($cv->active_theme == $theme->id) {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Biểu mẫu đã được kích hoạt.']));
			}

			$cv->active_theme = $theme->id;
			$savingResult = $cv->save();
			if (!empty($savingResult)) {
				die(json_encode(['status' => HopeConstant::HOPE_API_SUCCESS_STATUS, 'msg' => 'Thành công!']));
			} else {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Xuất hiện lỗi trong quá trình lưu hồ sơ. Vui lòng thử lại.']));
			}
		}
	}

	/**
	 * Redirect to unica gift messenger page
	 * @return mixed
	 */
	public function actionUnicaGif()
	{
		$encryptId = Yii::$app->request->get('token');
		$candidateId = Yii::$app->user->identity->candidate_id;
		$id = ColorgbHelper::decryptInt($encryptId);
		$unicaUrl = 'https://m.me/1651541728492826?ref=jobsgo';
                return $this->redirect($unicaUrl);
//		if ($id == $candidateId) {
//			return $this->redirect($unicaUrl);
//		} else {
//			return $this->redirect('/');
//		}
	}

	/**
	 * Update color for cv
	 * @return json
	 */
	public function actionUpdateColor()
	{
		if (Yii::$app->request->isPost) {
			$data = Yii::$app->request->post();
			$styleSheet = !empty($data['styleSheet']) ? $data['styleSheet'] : null;

			if (!in_array($styleSheet, HopeConstant::CV_GO_COLOR)) {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Màu đã chọn không hợp lệ.']));
			}

			$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
			$cvId = HopeShareHelper::decryptString($cvId);
			$candidateId = Yii::$app->user->identity->candidate_id;
			$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->with('theme')->one();
			$activeColor = json_decode($cv->active_color, true);
			$activeColor[$cv['theme']['id']] = $styleSheet;
			$cv->active_color = json_encode($activeColor, true);
			$savingResult = $cv->save();

			if (!empty($savingResult)) {
				die(json_encode(['status' => HopeConstant::HOPE_API_SUCCESS_STATUS, 'msg' => 'Cập nhật màu thành công.']));
			} else {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Có lỗi trong quá trình cấu hình màu sắc.']));
			}
		}
	}

	/**
	 * Send feedback
	 * @return json
	 */
	public function actionSendFeedback()
	{
		if (Yii::$app->request->isPost) {
			$data = Yii::$app->request->post();
			$easy = !empty($data['easy']) ? $data['easy'] : 0;
			$back = !empty($data['back']) ? $data['back'] : 0;
			$satisfy = !empty($data['satisfy']) ? $data['satisfy'] : 0;
			$additional = !empty($data['additional']) ? $data['additional'] : null;

			//init new feedback
			$candidateId = Yii::$app->user->identity->candidate_id;
			$feedback = new CvGoFeedback();
			$feedback->candidate_id = $candidateId;
			$feedback->easy = $easy;
			$feedback->back = $back;
			$feedback->satisfy = $satisfy;
			$feedback->additional = HopeTrimHelper::trimXssChar($additional);
			$feedback->created_at = date('Y-m-d H:i:s');
			$feedback->updated_at = date('Y-m-d H:i:s');
			$savingResult = $feedback->save();

			if (!empty($savingResult)) {
				die(json_encode(['status' => HopeConstant::HOPE_API_SUCCESS_STATUS, 'msg' => 'Gửi feedback thành công']));
			} else {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Có lỗi trong quá trình feedback']));
			}
		}
	}

	/**
	 * Auto save draft
	 * @return json
	 */
	public function actionSaveDraft()
	{
		if (Yii::$app->request->isPost) {
			$data = Yii::$app->request->post();
			$candidateId = Yii::$app->user->identity->candidate_id;
			$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
			$cvId = HopeShareHelper::decryptString($cvId);
			$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->one();
			//update saving_success
			$cv->draft = json_encode($data, JSON_UNESCAPED_UNICODE);
			if ($cv->save()) {
				die(json_encode(['status' => HopeConstant::HOPE_API_SUCCESS_STATUS, 'msg' => 'Saving draft successfully']));
			} else {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'An error occured when trying to save draft']));
			}
		}
	}

	/**
	 * Update font family for cvgo themes
	 */
	public function actionUpdateFontFamily()
	{
		if (Yii::$app->request->isPost) {
			// get param font-family from js/setting.js
			$fontFamily = RequestParamRetriever::getParamValue('font_family');
			$cvId = RequestParamRetriever::getParamValue('cvId');
			$cvId = HopeShareHelper::decryptString($cvId);
			
			// check if font-family is allowed
			if (!in_array($fontFamily, HopeConstant::$allowedCvGOFontFamily)) {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Font chữ đã chọn không hợp lệ!']));
			}
			// get active font family
			$candidateId = Yii::$app->user->identity->candidate_id;
			$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->with('theme')->one();
			$activeFontFamily = json_decode($cv->active_font_family, true);
			$activeFontFamily[$cv['theme']['id']] = $fontFamily;
			// update active font family
			$cv->active_font_family = json_encode($activeFontFamily, true);
			$savingResult = $cv->save();
			if (!empty($savingResult)) {
				die(json_encode(['status' => HopeConstant::HOPE_API_SUCCESS_STATUS, 'msg' => 'Cập nhật Font chữ thành công.']));
			} else {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Có lỗi trong quá trình đổi Font chữ.']));
			}
		}
	}

	/**
	 * Update cv language
	 * @return json
	 */
	public function actionUpdateLanguage()
	{
		if (Yii::$app->request->isPost) {
			// get param
			$data = Yii::$app->request->post();
			$candidateId = Yii::$app->user->identity->candidate_id;
			$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
			$cvId = HopeShareHelper::decryptString($cvId);
			$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->one();
			//update saving_success
			$cv->language = $data['language'];
			
			if ($cv->save()) {
				die(json_encode(['status' => HopeConstant::HOPE_API_SUCCESS_STATUS, 'msg' => 'Saving language successfully']));
			} else {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'An error occured when trying to save language']));
			}
		}
	}

	/**
	 * get list cv of candidate
	 * @return mixed
	 */
	public function actionManage()
	{
		$this->layout = 'page';
		$candidateId = Yii::$app->user->identity->candidate_id;
		$mainCv = Cv::find()->where(['candidate_id' => $candidateId, 'cv_type' => 1])->one();
		$defaultActiveColor = HopeConstant::CV_GO_DEFAULT_COLOR;
		$defaultActiveFontFamily = HopeConstant::CV_GO_DEFAULT_FONT_FAMILY;
		if (empty($mainCv)) {
			$mainCv = new Cv();
			$mainCv->candidate_id = $candidateId;
			$mainCv->active_theme = 1;
			$mainCv->active_color = $defaultActiveColor;
			$mainCv->active_font_family = $defaultActiveFontFamily;
			$mainCv->created_at = date('Y-m-d H:i:s');
			$mainCv->updated_at = date('Y-m-d H:i:s');
			$mainCv->language = 'vi-VN';
			$mainCv->cv_name = 'CV mặc định';
			$mainCv->cv_type = 1;
			$mainCv->save();
		} else {
			//có rồi thì update
			if (!empty(Yii::$app->request->get('theme'))) {
				$mainCv->active_theme = $theme;
			}
			if (empty($mainCv->active_color)) {
				$mainCv->active_color = $defaultActiveColor;
			} else {
				$arrAciveColor = json_decode($mainCv->active_color, true);
				$arrDefaultActiveColor = json_decode($defaultActiveColor, true);
				foreach ($arrDefaultActiveColor as $key => $value) {
					if (empty($arrAciveColor[$key])) {
						$arrAciveColor[$key] = $value;
					}
				}
				$mainCv->active_color = json_encode($arrAciveColor, true);
			}
			if (empty($mainCv->active_font_family)) {
				$mainCv->active_font_family = $defaultActiveFontFamily;
			} else {
				$arrActiveFontFamily = json_decode($mainCv->active_font_family, true);
				$arrDefaultActiveFontFamily = json_decode($defaultActiveFontFamily, true);
				foreach ($arrDefaultActiveFontFamily as $key => $value) {
					if (empty($arrActiveFontFamily[$key])) {
						$arrActiveFontFamily[$key] = $value;
					}
				}
				$mainCv->active_font_family = json_encode($arrActiveFontFamily, true);
			}

			if (empty($mainCv->language)) {
				$mainCv->language = 'vi-VN';
			}
			$mainCv->save();
		}
		
		$cv = Cv::find()->where(['candidate_id' => $candidateId])->with('theme')->all();
		return $this->render('manage', ['cv' => $cv]);
	}

	public function actionAddNewCv()
	{
		if (Yii::$app->request->isPost) {
			$candidateId = Yii::$app->user->identity->candidate_id;
			$cv = Cv::find()->where(['candidate_id' => $candidateId])->all();
			$countCv = count($cv);

			if ($countCv >= Cv::LIMIT_CV) {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Số lượng CV đã đạt giới hạn.']));
			}

			$defaultActiveColor = HopeConstant::CV_GO_DEFAULT_COLOR;
			$defaultActiveFontFamily = HopeConstant::CV_GO_DEFAULT_FONT_FAMILY;
			$cv = new Cv();
			$cv->candidate_id = $candidateId;
			$cv->active_theme = 1;
			$cv->active_color = $defaultActiveColor;
			$cv->active_font_family = $defaultActiveFontFamily;
			$cv->created_at = date('Y-m-d H:i:s');
			$cv->updated_at = date('Y-m-d H:i:s');
			$cv->language = 'vi-VN';
			$cv->cv_name = 'CV mới';
			$cv->cv_type = 2;
			$savingResult = $cv->save();
			if (!empty($savingResult)) {
				$cvId = HopeShareHelper::encryptString($cv->id);
				die(json_encode(['status' => HopeConstant::HOPE_API_SUCCESS_STATUS, 'msg' => '/cv-go/' . $cvId]));
			} else {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Có lỗi xuất hiện trong quá trình tạo CV Go']));
			}
		}
	}

	public function actionDeleteCv()
	{
		if (Yii::$app->request->isPost) {
			$id = Yii::$app->request->post('id');
			$id = HopeShareHelper::decryptString($id);
			$cv = Cv::findOne($id);
			if (empty($cv)) {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Không tìm thấy CV']));
			}

			if ($cv->cv_type == 1) {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Không thể xóa CV mặc định']));
			}

			if ($cv->delete()) {
				die(json_encode(['status' => HopeConstant::HOPE_API_SUCCESS_STATUS, 'msg' => 'Xóa CV thành công']));	
			} else {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Có lỗi xuất hiện trong quá trình xóa CV']));	
			}
		}
	}

	public function actionUpdateCvName()
	{
		if (Yii::$app->request->isPost) {
			$id = Yii::$app->request->post('id');
			$name = Yii::$app->request->post('name');
			$id = HopeShareHelper::decryptString($id);
			$cv = Cv::findOne($id);
			if (empty($cv)) {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Không tìm thấy CV']));
			}

			if ($cv->cv_type == 1) {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Không thể đổi tên CV mặc định']));
			}

			$cv->cv_name = $name;
			if ($cv->save()) {
				die(json_encode(['status' => HopeConstant::HOPE_API_SUCCESS_STATUS, 'msg' => 'Đổi tên CV thành công']));	
			} else {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Có lỗi xuất hiện trong quá trình đổi tên CV']));	
			}
		}
	}

	public function actionUpdateFontSize()
	{
		if (Yii::$app->request->isPost) {
			// get param
			$data = Yii::$app->request->post();
			$candidateId = Yii::$app->user->identity->candidate_id;
			$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
			$cvId = HopeShareHelper::decryptString($cvId);
			$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->one();
			//update saving_success
			$cv->font_size = $data['font_size'];
			
			if ($cv->save()) {
				die(json_encode(['status' => HopeConstant::HOPE_API_SUCCESS_STATUS, 'msg' => 'Saving font size successfully']));
			} else {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'An error occured when trying to save font size']));
			}
		}
	}

	public function actionUpdateLineHeight()
	{
		if (Yii::$app->request->isPost) {
			// get param
			$data = Yii::$app->request->post();
			$candidateId = Yii::$app->user->identity->candidate_id;
			$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
			$cvId = HopeShareHelper::decryptString($cvId);
			$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->one();
			//update saving_success
			$cv->line_height = $data['line_height'];
			
			if ($cv->save()) {
				die(json_encode(['status' => HopeConstant::HOPE_API_SUCCESS_STATUS, 'msg' => 'Saving line height successfully']));
			} else {
				die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'An error occured when trying to save line height']));
			}
		}
	}

	/**
	 * Use Google Translate Api to translate CV content
	 * @return json
	 */
	public function actionTranslateCv()
	{
		$translate = new TranslateClient([
			'key' => Yii::$app->params['GG_TRANSLATE_API_KEY']
		]);

		$data = Yii::$app->request->post();
		$candidateId = Yii::$app->user->identity->candidate_id;
		$cvId = !empty($data['cvId']) ? $data['cvId'] : '';
		$cvId = HopeShareHelper::decryptString($cvId);
		$cv = Cv::find()->where(['candidate_id' => $candidateId, 'id' => $cvId])->one();
		$cvs = Cv::find()->where(['candidate_id' => $candidateId])->all();
		$countCv = count($cvs);

		if ($countCv >= Cv::LIMIT_CV) {
			die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => 'Số lượng CV đã đạt giới hạn <strong>(' . Cv::LIMIT_CV .'/' . Cv::LIMIT_CV . ')</strong>. Quay lại trang <a href="/cv-go/quan-ly">Quản lý CV</a> để xóa bớt CV trước khi sử dụng.']));
		}

		$secondaryCvData = $cv->secondary_cv_data;
		$secondaryCvData = json_decode($secondaryCvData);
			
		//get candidate info
		$candidateInfo = $secondaryCvData->candidateInfo;
		if (!empty($candidateInfo)) {
			foreach ($candidateInfo as $key => $value) {
				$result = $translate->translate($candidateInfo[$key]->value, [
					'target' => 'en'
				]);
				$candidateInfo[$key]->value = $result['text'];
			}
		}
		$secondaryCvData->candidateInfo = $candidateInfo;

		//get job history
		$tmpJobHistory = $secondaryCvData->jobHistory;
		if (!empty($tmpJobHistory)) {
			foreach ($tmpJobHistory as $k1 => $v1) {
				foreach ($v1 as $k2 => $v2) {
					$result = $translate->translate($tmpJobHistory[$k1][$k2]->value, [
						'target' => 'en'
					]);
					$tmpJobHistory[$k1][$k2]->value = $result['text'];
				}
			}
		}
		$secondaryCvData->jobHistory = $tmpJobHistory;

		//get education
		$tmpEducation = $secondaryCvData->education;
		if (!empty($tmpEducation)) {
			foreach ($tmpEducation as $k1 => $v1) {
				foreach ($v1 as $k2 => $v2) {
					$result = $translate->translate($tmpEducation[$k1][$k2]->value, [
						'target' => 'en'
					]);
					$tmpEducation[$k1][$k2]->value = $result['text'];
				}
			}
		}
		$secondaryCvData->education = $tmpEducation;

		//get award
		$tmpAward = $secondaryCvData->award;
		if (!empty($tmpAward)) {
			foreach ($tmpAward as $k1 => $v1) {
				foreach ($v1 as $k2 => $v2) {
					$result = $translate->translate($tmpAward[$k1][$k2]->value, [
						'target' => 'en'
					]);
					$tmpAward[$k1][$k2]->value = $result['text'];
				}
			}
		}
		$secondaryCvData->award = $tmpAward;

		//get activity
		$tmpActivity = $secondaryCvData->activity;
		if (!empty($tmpActivity)) {
			foreach ($tmpActivity as $k1 => $v1) {
				foreach ($v1 as $k2 => $v2) {
					$result = $translate->translate($tmpActivity[$k1][$k2]->value, [
						'target' => 'en'
					]);
					$tmpActivity[$k1][$k2]->value = $result['text'];
				}
			}
		}
		$secondaryCvData->activity = $tmpActivity;

		//get certificates
		$tmpCertificates = $secondaryCvData->certificates;
		if (!empty($tmpCertificates)) {
			foreach ($tmpCertificates as $k1 => $v1) {
				foreach ($v1 as $k2 => $v2) {
					$result = $translate->translate($tmpCertificates[$k1][$k2]->value, [
						'target' => 'en'
					]);
					$tmpCertificates[$k1][$k2]->value = $result['text'];
				}
			}
		}
		$secondaryCvData->certificates = $tmpCertificates;

		//get references
		$tmpReferences = $secondaryCvData->references;
		if (!empty($tmpReferences)) {
			foreach ($tmpReferences as $k1 => $v1) {
				foreach ($v1 as $k2 => $v2) {
					$result = $translate->translate($tmpReferences[$k1][$k2]->value, [
						'target' => 'en'
					]);
					$tmpReferences[$k1][$k2]->value = $result['text'];
				}
			}
		}
		$secondaryCvData->references = $tmpReferences;

		//change again
		$cv->secondary_cv_data = json_encode($secondaryCvData);
		$cv->cv_name = "Translated from: " . $cv->cv_name;
		$cv->language = 'en-US';
		$cv->cv_type = 2;

		//create new translated cv
		$newTranslatedCv = new Cv;
		$newTranslatedCv->attributes = $cv->attributes;
		if ($newTranslatedCv->save()) {
			$newCVId = $newTranslatedCv->id;
			$msg = '/cv-go/' . HopeShareHelper::encryptString($newCVId);
			die(json_encode(['status' => HopeConstant::HOPE_API_SUCCESS_STATUS, 'msg' => $msg]));
		} else {
			$msg = 'Xuất hiện lỗi trong quá trình dịch CV';
			die(json_encode(['status' => HopeConstant::HOPE_API_UNSUCCESS_STATUS, 'msg' => $msg]));
		}
	}
}
