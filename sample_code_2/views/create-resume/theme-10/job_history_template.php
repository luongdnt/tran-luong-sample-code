<div class="item mb-0 mt-3 cv-child-elem history-item">
    <h3 class="item-title mb-2 spec-heading mb-md-0 cv-editable-elem required"
        data-placeholder="Vị trí/công việc" info-name="job_title"
        info-group="job_history"><?= Yii::t('cvgo', 'Software developer') ?></h3>
    <p class="cv-editable-elem d-inline required"
       data-placeholder="Năm bắt đầu"
       info-group="job_history"
       info-name="date_start"><?= date('m/Y',strtotime('-1 years')) ?></p>
    -
    <p class="cv-editable-elem d-inline required"
       data-placeholder="Năm kết thúc" info-group="job_history"
       info-name="date_end"><?= date('m/Y') ?></p>
    <div class="cv-editable-elem mt-2 spec-heading required"
         data-placeholder="Tên công ty"
         info-group="job_history"
         info-name="job_company"><?= Yii::t('cvgo', '... JSC') ?></div>
    <div class="item-content cv-editable-elem required"
         data-placeholder="Mô tả công việc"
         info-name="job_description_html"
         info-group="job_history">
        <ul>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', "Participating in building a system of website systems for the Company's products") ?></li>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Participating in software development projects of the Company') ?></li>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Build management tools, develop system features') ?></li>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Optimize the system, propose to perform the work for the system to operate effectively') ?></li>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Researching and developing new products features') ?></li>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Other jobs under the direction of the division head') ?></li>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Support other technical issues') ?></li>
        </ul>
    </div>
</div><!--//item-->