<div class="item mt-3 cv-child-elem activity-item mb-2">
    <h3 class="item-content mb-0 spec-heading cv-editable-elem required"
        data-placeholder="Vị trí/vai trò" info-name="role"
        info-group="activity">
        <?= Yii::t('cvgo', 'Speakers') ?>
    </h3>
    <p class="cv-editable-elem d-inline required"
       data-placeholder="Từ ngày"
       info-group="activity" info-name="from">02/02/<?= date("Y",strtotime("-1 years"))?></p>
    -
    <p class="cv-editable-elem d-inline required"
       data-placeholder="Đến ngày"
       info-group="activity" info-name="to">20/02/<?= date("Y",strtotime("-1 years"))?></p>
    <div class="mb-0 spec-heading mt-2 cv-editable-elem required"
         data-placeholder="Tên công ty/tổ chức/sự kiện"
         info-name="organization"
         info-group="activity"><?= Yii::t('cvgo', 'WORKSHOP: A day in game programming') ?></div>
    <div class="item-content cv-editable-elem"
         data-placeholder="Mô tả hoạt động" info-name="description"
         info-group="activity">
        <ul>
            <li><?= Yii::t('cvgo', 'Introduction about game programming, game vision now & future in the game industry in general and game development companies in Vietnam in particular.') ?></li>
            <li><?= Yii::t('cvgo', 'History of the game and the development of gaming technology') ?></li>
            <li><?= Yii::t('cvgo', 'Game development process') ?></li>
            <li><?= Yii::t('cvgo', 'Future interactive technologies of the industry such as: Virtual reality game, Multi-platform game') ?></li>
            <li><?= Yii::t('cvgo', 'Realistic demo of a team game with 3 employees (Designer, Coder, Tester), experience game manipulation practice on the machine') ?></li>
        </ul>
    </div>
</div><!--//item-->