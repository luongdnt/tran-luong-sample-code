<li class="mb-0 mt-3 cv-child-elem certificates-item">
    <div class="resume-degree spec-heading d-inline cv-editable-elem required "
         data-placeholder="Tên chứng chỉ"
         info-group="certificates"
         info-name="name"><?= Yii::t('cvgo', 'IELTS 7.0') ?></div>
    <p class="cv-editable-elem d-inline required"
       data-placeholder="Năm bắt đầu"
       info-group="certificates"
       info-name="year"><?= date('Y', strtotime("-1 year", time())) ?></p>
</li>