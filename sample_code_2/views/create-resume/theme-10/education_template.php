<div class="item mt-3 mb-5 cv-child-elem education-item">
    <h3 class=" mb-0 spec-heading cv-editable-elem required"
        data-placeholder="Công nghệ thông tin"
        info-name="specialization"
        info-group="education"><?= Yii::t('cvgo', 'Information Technology'); ?></h3>
    <p class="cv-editable-elem text-black d-inline required"
       data-placeholder="Năm bắt đầu"
       info-group="education"
       info-name="date_start"><?= date('m/Y') ?></p>
    -
    <p class="cv-editable-elem d-inline required"
       data-placeholder="Năm kết thúc" info-group="education"
       info-name="date_end"><?= date('m/Y') ?></p>
    <div class="item-content spec-heading mt-2 cv-editable-elem required"
         data-placeholder="Tên trường" info-name="school_name"
         info-group="education">
        <?= Yii::t('cvgo', '... University'); ?>
    </div>
    <div class="item-content cv-editable-elem"
         data-placeholder="Mô tả học vấn" info-name="hightlight"
         info-group="education">
        <?= Yii::t('cvgo', 'Good graduate'); ?>
    </div>
</div><!--//item-->