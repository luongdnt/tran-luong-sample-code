<div class="item mt-3 cv-child-elem award-item">
    <p class="mb-0 cv-editable-elem spec-heading d-inline required"
       data-placeholder="Tên thành tích" info-name="name"
       info-group="award"><?= Yii::t('cvgo', 'Excellent employees at ...') ?></p>
    <p class="cv-editable-elem d-inline required"
       data-placeholder="Năm"
       info-group="award"
       info-name="year"><?= date("Y", strtotime("-1 years")) ?></p>
</div><!--//item-->