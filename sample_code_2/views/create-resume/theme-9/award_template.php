<div class="item mt-3 cv-child-elem award-item">
    <div class="row">
        <div class="col-12 col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 p-0">
            <p class="cv-editable-elem text-black d-inline required"
               data-placeholder="Năm"
               info-group="award"
               info-name="year"><?= date('Y') ?></p>
        </div>
        <div class="col-12 col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 p-0">
            <p class="mb-0 cv-editable-elem text-black required"
               data-placeholder="Tên thành tích" info-name="name"
               info-group="award"><?= Yii::t('cvgo', 'Excellent employees at ...') ?></p>
        </div>
    </div>
</div><!--//item-->