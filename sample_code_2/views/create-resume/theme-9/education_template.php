<div class="item mt-3 cv-child-elem education-item">
    <div class="row">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 p-0">
            <p class="cv-editable-elem text-bold-year text-black d-inline required"
               data-placeholder="Năm bắt đầu"
               info-group="education"
               info-name="date_start"><?= date('Y', strtotime("-1 year", time())) ?></p>
            -
            <p class="cv-editable-elem text-black text-bold-year d-inline required"
               data-placeholder="Năm kết thúc" info-group="education"
               info-name="date_end"><?= date('Y') ?></p>
        </div>
        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 col-xl-9 p-0">
            <h3 class=" mb-0 spec-heading cv-editable-elem required"
                data-placeholder="Công nghệ thông tin"
                info-name="specialization"
                info-group="education"><?= Yii::t('cvgo', 'Marketing manager') ?></h3>
            <div class="item-content job-his-company-name m-0 cv-editable-elem required"
                 data-placeholder="Tên trường" info-name="school_name"
                 info-group="education">
                <?= Yii::t('cvgo', '... University'); ?>
            </div>
            <div class="item-content cv-editable-elem"
                 data-placeholder="Mô tả học vấn" info-name="hightlight"
                 info-group="education">
                <?= Yii::t('cvgo', 'Good graduate'); ?>
            </div>
        </div>
    </div>
</div><!--//item-->