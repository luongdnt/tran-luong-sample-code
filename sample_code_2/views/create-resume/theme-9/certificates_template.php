<li class="mb-0 mt-3 cv-child-elem certificates-item">
    <div class="row">
        <div class="col-12 col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 p-0">
            <p class="cv-editable-elem d-inline text-black required"
               data-placeholder="Năm bắt đầu"
               info-group="certificates"
               info-name="year"><?= date('Y', strtotime("-1 year", time())) ?></p>
        </div>
        <div class="col-12 col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 p-0">
            <div class="resume-degree text-black cv-editable-elem required "
                 data-placeholder="Tên chứng chỉ"
                 info-group="certificates"
                 info-name="name"><?= Yii::t('cvgo', 'IELTS 7.0') ?></div>
        </div>
    </div>
</li>