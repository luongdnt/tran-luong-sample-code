<div class="item mt-3 cv-child-elem activity-item mb-2">
    <div class="row">
        <div class="col-12 col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 p-0">
            <p class="cv-editable-elem text-bold-year d-inline required"
               data-placeholder="Từ ngày"
               info-group="activity" info-name="from"><?= date("d/m/Y") ?></p>
            -
            <p class="cv-editable-elem text-bold-year d-inline required"
               data-placeholder="Đến ngày"
               info-group="activity" info-name="to"><?= date("d/m/Y") ?></p>
        </div>
        <div class="col-12 col-xs-9 col-sm-9 col-md-9 col-lg-9 col-xl-9 p-0">
            <h3 class="item-content mb-0 spec-heading cv-editable-elem required"
                data-placeholder="Vị trí/vai trò" info-name="role"
                info-group="activity">
                <?= Yii::t('cvgo', 'Speakers') ?>
            </h3>
            <div class="mb-0 job-his-company-name cv-editable-elem required"
                 data-placeholder="Tên công ty/tổ chức/sự kiện"
                 info-name="organization"
                 info-group="activity"><?= Yii::t('cvgo', 'Workshop: Content Marketing Trends after COVID-19') ?></div>
            <div class="item-content cv-editable-elem"
                 data-placeholder="Mô tả hoạt động" info-name="description"
                 info-group="activity">
                <ul>
                    <li><?= Yii::t('cvgo', 'Review Content trends in the first half of 2020, a period of economic volatility before the Covid-19 influence') ?></li>
                    <li><?= Yii::t('cvgo', 'Solution for content for the “trạng thái bình thường mới”') ?></li>
                    <li><?= Yii::t('cvgo', 'KOLs market context after the epidemic') ?></li>
                    <li><?= Yii::t('cvgo', 'How to deploy content and use KOLs as marketing') ?></li>
                </ul>
            </div>
        </div>
    </div>
</div><!--//item-->