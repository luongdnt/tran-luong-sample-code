<div class="item mb-0 mt-3 cv-child-elem history-item">
    <div class="row">
        <div class="col-12 col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 p-0">
            <p class="cv-editable-elem text-bold-year text-black d-inline required"
               data-placeholder="Năm bắt đầu"
               info-group="job_history"
               info-name="date_start">04/<?= date('Y', strtotime("-1 year", time())) ?></p>
            -
            <p class="cv-editable-elem text-bold-year text-black d-inline required"
               data-placeholder="Năm kết thúc" info-group="job_history"
               info-name="date_end"><?= date('m/Y') ?></p>
        </div>
        <div class="col-12 col-xs-9 col-sm-9 col-md-9 col-lg-9 col-xl-9 p-0">
            <h3 class="item-title mb-2 spec-heading mb-md-0 cv-editable-elem required"
                data-placeholder="Vị trí/công việc" info-name="job_title"
                info-group="job_history"><?= Yii::t('cvgo', 'Marketer') ?></h3>
            <p class="cv-editable-elem job-his-company-name d-inline required"
               data-placeholder="Tên công ty"
               info-group="job_history"
               info-name="job_company"><?= Yii::t('cvgo', '... JSC'); ?></p>
            <div class="item-content cv-editable-elem required"
                 data-placeholder="Mô tả công việc"
                 info-name="job_description_html"
                 info-group="job_history">
                <ul>
                    <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Develop an SEO strategy') ?></li>
                    <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Evaluate marketing effectiveness from given budget') ?></li>
                    <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Work with managers on making lists') ?></li>
                    <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Optimize targets, budget over time and geography') ?></li>
                    <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Keep up with the improvements and changes of technology and online media channels') ?></li>
                    <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Foresee business trends to deploy Digital Marketing activities') ?></li>
                    <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Management and skills training for Digital Marketing staff') ?></li>
                    <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Measure KPIs and ROI within the allowed budget') ?></li>
                    <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Building a marketing strategy on social networks and social media') ?></li>
                    <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Manage digital marketing channels such as (website, blog, social networks and email) to ensure brand image consistency.') ?></li>
                    <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Planning, solutions and organizing the implementation of Digital Marketing activities for businesses') ?></li>
                </ul>
            </div>
        </div>
    </div>
</div><!--//item-->