<div class="item mb-3 cv-child-elem education-item">
	<div class="item-heading row align-items-center">
		<h6 class="col-12 col-md-6 col-lg-8 cv-editable-elem required" data-placeholder="Chuyên ngành" info-name="specialization" info-group="education"><?= Yii::t('cvgo', 'Business Administration'); ?></h6>
		<div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-left text-md-right">
		 	<p class="cv-editable-elem d-inline required" data-placeholder="Năm bắt đầu" info-group="education" info-name="date_start"><?= date('Y', strtotime("-1 year", time())) ?></p>
		 	-
		 	<p class="cv-editable-elem d-inline required" data-placeholder="Năm kết thúc" info-group="education" info-name="date_end"><?= date('Y') ?></p>
		</div>
	</div>
	<div class="item-content cv-editable-elem required" data-placeholder="Tên trường" info-name="school_name" info-group="education">
		<?= Yii::t('cvgo', '... University'); ?>
	</div>
	<div class="item-content cv-editable-elem" data-placeholder="Mô tả học vấn" info-name="hightlight" info-group="education">
		<?= Yii::t('cvgo', 'Good graduate'); ?>
	</div>
</div><!--//item-->