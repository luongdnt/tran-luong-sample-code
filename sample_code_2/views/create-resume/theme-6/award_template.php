<div class="item mb-3 cv-child-elem award-item">
	<div class="item-heading row align-items-center">
		<p class="col-12 col-md-6 col-lg-8 cv-editable-elem required" data-placeholder="Tên thành tích" info-name="name" info-group="award"><?= Yii::t('cvgo', 'Excellent employees at ...') ?></p>
		<div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-left text-md-right">
		 	<p class="cv-editable-elem d-inline required" data-placeholder="Năm" info-group="award" info-name="year"><?= date('Y') ?></p>
		</div>
	</div>
</div><!--//item-->