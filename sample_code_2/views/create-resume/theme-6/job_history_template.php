<div class="item mb-3 cv-child-elem history-item">
	<div class="item-heading row align-items-center mb-2">
		<h4 class="item-title col-12 col-md-6 col-lg-8 mb-2 mb-md-0 cv-editable-elem required" data-placeholder="Vị trí/công việc" info-name="job_title" info-group="job_history"><?= Yii::t('cvgo', 'Sale'); ?></h4>
		<div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-left text-md-right">
			<p class="cv-editable-elem d-inline required" data-placeholder="Tên công ty" info-group="job_history" info-name="job_company"><?= Yii::t('cvgo', '... JSC'); ?></p>
		 	|
		 	<p class="cv-editable-elem d-inline required" data-placeholder="Năm bắt đầu" info-group="job_history" info-name="date_start"><?= date('Y', strtotime("-1 year", time())) ?></p>
		 	-
		 	<p class="cv-editable-elem d-inline required" data-placeholder="Năm kết thúc" info-group="job_history" info-name="date_end"><?= date('Y') ?></p>
		</div>
	</div>
	<div class="item-content cv-editable-elem required" data-placeholder="Mô tả công việc" info-name="job_description_html" info-group="job_history">
		<p class="resume-timeline-item-desc-heading font-weight-bold cv-editable-elem"><?= Yii::t('cvgo', 'Mission'); ?>:</p>
        <p class="cv-editable-elem">- <?= Yii::t('cvgo', 'Carry out business plans: find customers, introduce products and services, consult products and applications, take care and manage customer relationships.'); ?></p>
		<p class="cv-editable-elem">- <?= Yii::t('cvgo', 'Make annual, quarterly, monthly, and weekly work plans to achieve assigned goals.'); ?></p>
		<p class="cv-editable-elem">- <?= Yii::t('cvgo', 'Survey, study, evaluate expected revenue in the management area. Develop and propose appropriate business plans.') ?></p>
        <p class="resume-timeline-item-desc-heading font-weight-bold cv-editable-elem"><?= Yii::t('cvgo', 'Achievements'); ?>:</p>
        <p class="cv-editable-elem"><?= Yii::t('cvgo', 'Through the process of working at ... Joint Stock Company, I have achieved some of the following achievements'); ?>:</p>
        <ul>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', '... Best seller') ?> <?= date('Y', strtotime("-1 year", time())) ?></li>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Excellent staff') ?> <?= date('Y', strtotime("-1 year", time())) ?></li>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Creative staff') ?></li>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Excellent sale project') ?></li>
        </ul>
	</div>
</div><!--//item-->