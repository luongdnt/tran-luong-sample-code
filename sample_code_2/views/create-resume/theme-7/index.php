<?php

use yii\widgets\ActiveForm;

?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <title><?= $title ?></title>

    <!-- Meta -->
    <?= $this->render('../layout/meta') ?>

    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="<?= $activeColor ?>">
    <?= $this->render('../layout/assets') ?>
    <link rel="stylesheet" href="/cv_template/theme_7/css/theme-5.css">
    <link rel="stylesheet" href="/cv_template/theme_7/css/theme-7.css">
    <style>
        .jobsgo-watermark, .font-wrapper {
            font-family: <?= $activeFontFamily ?>;
        }

        <?php if ($activeFontFamily == 'Arial, sans-serif'): ?>
        .resume-name {
            font-weight: 600;
        }

        <?php else: ?>
        .resume-name {
            font-weight: 900;
        }

        <?php endif; ?>
    </style>
</head>
<body>
<?php if (empty($isRendered)): ?>
    <div class="loader-container">
        <div class="loader"></div>
    </div>
    <?= $this->render('../layout/header') ?>
    <?= $this->render('../layout/sidebar', [
        'theme' => $theme,
        'cv' => $cv
    ]) ?>
<?php endif; ?>
<div class="main-wrapper">
    <div class="container px-3 px-lg-5">
        <article class="resume-wrapper-inner font-wrapper mx-auto theme-bg-light p-5 mb-5 shadow-lg">
            <div class="resume-header remove-bg-color text-center">
                <!-- cv profile name -->
                <h2 class="resume-name mb-0 text-uppercase cv-editable-elem candidate-name arial-text-font-weight"
                    data-placeholder="Tên"
                    info-group="candidate" info-name="name"><?= $candidate->name ?></h2>
                <!-- current address -->
                <div class="social-container">
                    <p class="d-inline cv-editable-elem" data-placeholder="Tỉnh/thành phố"
                       info-group="candidate"
                       info-name="current_address"><?= $candidate->current_address ?></p>
                    <br>
                </div>

                <p class="d-inline">
                    <i class="fas fa-phone-square fa-fw fa-lg"></i>
                <p class="cv-editable-elem d-inline" data-placeholder="Số điện thoại"
                   info-group="candidate" info-name="tel"><?= $candidate->tel ?></p>
                <i class="fas fa-envelope-square fa-fw fa-lg"></i>
                <p class="cv-editable-elem d-inline" data-placeholder="Email" info-group="candidate"
                   info-name="email"><?= $candidate->email ?></p>
                <?php if (empty($isRendered)): ?>
                    <input id="dp" class="datepicker" data-date-format="dd/mm/yyyy" style="display: none;">
                    <i class="fas fa-birthday-cake fa-fw fa-lg"></i>
                    <p class="d-inline date-of-birth" info-group="candidate"
                       info-name="date_of_birth"><?= date('d/m/Y', strtotime($candidate->date_of_birth)) ?></p>
                <?php else: ?>
                    <?php if ($candidate->date_of_birth != '1970-01-01' && !empty($candidate->date_of_birth)): ?>
                        <input id="dp" class="datepicker" data-date-format="dd/mm/yyyy" style="display: none;">
                        <i class="fas fa-birthday-cake fa-fw fa-lg"></i>
                        <p class="d-inline date-of-birth" info-group="candidate"
                           info-name="date_of_birth"><?= date('d/m/Y', strtotime($candidate->date_of_birth)) ?></p>
                    <?php endif; ?>
                <?php endif; ?>
                </p>
            </div><!--//resume-header-->
            <br>
            <div class="resume-body">
                <!-- Mục tiêu nghề nghiệp -->
                <section class="work-section py-3 cv-block cv-section-event" id="career-summary-block"
                         my-title="Mục tiêu nghề nghiệp">
                    <div class="cvo-block-title">
                        <h3><?= Yii::t('cvgo', 'Career goals'); ?></h3>
                    </div>
                    <div class="cvo-block-content cv-editable-elem"
                         data-placeholder="Mục tiêu nghề nghiệp: ngắn hạn, dài hạn."
                         info-group="candidate" info-name="short_bio_html">
                        <?php if (empty($candidate->short_bio_html)): ?>
                            <?= $candidate->short_bio ?>
                        <?php else: ?>
                            <?= $candidate->short_bio_html ?>
                        <?php endif; ?>
                    </div>
                </section>
                <!-- End Mục tiêu nghề nghiệp -->
                <!-- Học vấn -->
                <section class="skills-section py-3 cv-block cv-section-event" id="education-block" my-title="Học vấn">
                    <div class="cvo-block-title">
                        <h3><?= Yii::t('cvgo', 'Education'); ?></h3>
                    </div>
                    <div class="cvo-block-content">
                        <div id="education-container" block="education">
                            <?php if (!empty($education)): ?>
                                <?php foreach ($education as $item): ?>
                                    <div class="item cv-child-elem education-item mb-0">
                                        <p class="d-none item-id" info-name="candidate_education_id"
                                           info-group="education"><?= $item->candidate_education_id ?></p>
                                        <div class="row">
                                            <div class="col-9 col-md-9 col-lg-9 col-xl-9">
                                                <h4 class="m-0 item-content cv-editable-elem required"
                                                    data-placeholder="Tên trường" info-name="school_name"
                                                    info-group="education">
                                                    <?= $item->school_name ?>
                                                </h4>
                                            </div>
                                            <div class="col-3 col-md-3 col-lg-3 col-xl-3 text-right">
                                                <?php if (empty($isRendered)): ?>
                                                    <p class="cv-editable-elem d-inline required"
                                                       data-placeholder="Năm bắt đầu" info-group="education"
                                                       info-name="date_start"><?= $item->date_start ?></p>
                                                    -
                                                    <p class="cv-editable-elem d-inline required"
                                                       data-placeholder="Năm kết thúc" info-group="education"
                                                       info-name="date_end"><?= $item->date_end ?></p>
                                                <?php else: ?>
                                                    <?php if (!empty($item->date_start)): ?>
                                                        <p class="cv-editable-elem d-inline required"
                                                           data-placeholder="Năm bắt đầu" info-group="education"
                                                           info-name="date_start"><?= $item->date_start ?></p>
                                                    <?php endif; ?>
                                                    <?php if (!empty($item->date_start) && !empty($item->date_end)): ?>
                                                        -
                                                    <?php endif; ?>
                                                    <?php if (!empty($item->date_end)): ?>
                                                        <p class="cv-editable-elem d-inline required"
                                                           data-placeholder="Năm kết thúc" info-group="education"
                                                           info-name="date_end"><?= $item->date_end ?></p>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                            <div class="pl-3">
                                                <div class="mb-0 italic-title cv-editable-elem required"
                                                     data-placeholder="Vị trí/công việc" info-name="specialization"
                                                     info-group="education"><?= $item->specialization ?></div>
                                                <div class="item-content cv-editable-elem"
                                                     data-placeholder="Mô tả học vấn" info-name="hightlight"
                                                     info-group="education">
                                                    <?= $item->hightlight ?>
                                                </div>
                                            </div>
                                        </div>
                                        <hr style="color: grey" class="mt-0 mb-0">
                                    </div><!--//item-->
                                <?php endforeach; ?>
                            <?php else: ?>
                                <?php if (empty($isRendered)): ?>
                                    <div class="add-new-btn  text-center" id="add-education-template"
                                         data-toggle="tooltip"
                                         data-placement="top" title="Thêm biểu mẫu">
                                        <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
                <!-- End học vấn -->
                <!-- Kinh nghiệm làm việc -->
                <section class="work-section py-3 cv-block cv-section-event" id="experience-block"
                         my-title="Kinh nghiệm làm việc">
                    <div class="cvo-block-title">
                        <h3><?= Yii::t('cvgo', 'Work experience'); ?></h3>
                    </div>
                    <div class="cvo-block-content">
                        <div id="job-history-container" block="job-history">
                            <?php if (!empty($jobHistory)): ?>
                                <?php foreach ($jobHistory as $item): ?>
                                    <div class="item m-0 cv-child-elem history-item">
                                        <p class="d-none item-id" info-name="candidate_job_history_id"
                                           info-group="job_history"><?= $item->candidate_job_history_id ?></p>
                                        <div class="row">
                                            <div class="col-9 col-md-9 col-lg-9 col-xl-9">
                                                <h4 class="cv-editable-elem m-0 required"
                                                    data-placeholder="Tên công ty"
                                                    info-group="job_history"
                                                    info-name="job_company"><?= $item->job_company ?></h4>
                                            </div>
                                            <div class="col-3 col-md-3 col-lg-3 col-xl-3 text-right">
                                                <?php if (empty($isRendered)): ?>
                                                    <p class="cv-editable-elem d-inline required"
                                                       data-placeholder="Năm bắt đầu" info-group="job_history"
                                                       info-name="date_start"><?= $item->date_start ?></p>
                                                    -
                                                    <p class="cv-editable-elem d-inline required"
                                                       data-placeholder="Năm kết thúc" info-group="job_history"
                                                       info-name="date_end"><?= $item->date_end ?></p>
                                                <?php else: ?>
                                                    <?php if (!empty($item->date_start)): ?>
                                                        <p class="cv-editable-elem d-inline required"
                                                           data-placeholder="Năm bắt đầu" info-group="job_history"
                                                           info-name="date_start"><?= $item->date_start ?></p>
                                                    <?php endif; ?>
                                                    <?php if (!empty($item->date_start) && !empty($item->date_end)): ?>
                                                        -
                                                    <?php endif; ?>
                                                    <?php if (!empty($item->date_end)): ?>
                                                        <p class="cv-editable-elem d-inline required"
                                                           data-placeholder="Năm kết thúc" info-group="job_history"
                                                           info-name="date_end"><?= $item->date_end ?></p>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                            <div class="pl-3">
                                                <div class="item-title italic-title cv-editable-elem required"
                                                     data-placeholder="Vị trí/công việc" info-name="job_title"
                                                     info-group="job_history"><?= $item->job_title ?></div>
                                                <div class="item-content m-0 cv-editable-elem required"
                                                     data-placeholder="Mô tả công việc"
                                                     info-name="job_description_html"
                                                     info-group="job_history">
                                                    <?php if (empty($item->job_description_html)): ?>
                                                        <?= $item->job_description ?>
                                                    <?php else: ?>
                                                        <?= $item->job_description_html ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="mt-0 mb-0" style="color: grey">
                                    </div><!--//item-->
                                <?php endforeach; ?>
                            <?php else: ?>
                                <?php if (empty($isRendered)): ?>
                                    <div class="add-new-btn  text-center" id="add-job-history-template"
                                         data-toggle="tooltip"
                                         data-placement="top" title="Thêm biểu mẫu">
                                        <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
                <!-- End Kinh nghiệm làm việc -->
                <!-- Hoạt động -->
                <section class="work-section py-3 cv-block cv-section-event" id="activity-block" my-title="Hoạt động">
                    <div class="cvo-block-title">
                        <h3><?= Yii::t('cvgo', 'Activity'); ?></h3>
                    </div>
                    <div class="cvo-block-content">
                        <div block="activity" id="activity-container">
                            <?php if (!empty($activity)): ?>
                                <?php foreach ($activity as $item): ?>
                                    <div class="item cv-child-elem activity-item mb-2">
                                        <p class="d-none item-id" info-name="id"
                                           info-group="activity"><?= $item->id ?></p>
                                        <div class="row">
                                            <div class="col-7 col-md-7 col-lg-7 col-xl-7">
                                                <h4 class="mb-0 cv-editable-elem required"
                                                    data-placeholder="Tên công ty/tổ chức/sự kiện"
                                                    info-name="organization"
                                                    info-group="activity"><?= $item->organization ?></h4>
                                            </div>
                                            <div class="col-5 col-md-5 col-lg-5 col-xl-5 text-right">
                                                <?php if (empty($isRendered)): ?>
                                                    <p class="cv-editable-elem d-inline required"
                                                       data-placeholder="Từ ngày" info-group="activity"
                                                       info-name="from"><?= $item->from ?></p>
                                                    -
                                                    <p class="cv-editable-elem d-inline required"
                                                       data-placeholder="Đến ngày" info-group="activity"
                                                       info-name="to"><?= $item->to ?></p>
                                                <?php else: ?>
                                                    <?php if (!empty($item->from)): ?>
                                                        <p class="cv-editable-elem d-inline required"
                                                           data-placeholder="Từ ngày" info-group="activity"
                                                           info-name="from"><?= $item->from ?></p>
                                                    <?php endif; ?>
                                                    <?php if (!empty($item->from) && !empty($item->to)): ?>
                                                        -
                                                    <?php endif; ?>
                                                    <?php if (!empty($item->to)): ?>
                                                        <p class="cv-editable-elem d-inline required"
                                                           data-placeholder="Đến ngày" info-group="activity"
                                                           info-name="to"><?= $item->to ?></p>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                            <div class="pl-3">
                                                <p class="item-content italic-title cv-editable-elem required"
                                                     data-placeholder="Vị trí/vai trò" info-name="role"
                                                     info-group="activity">
                                                    <?= $item->role ?>
                                                </p>
                                                <p class="item-content cv-editable-elem"
                                                     data-placeholder="Mô tả hoạt động" info-name="description"
                                                     info-group="activity">
                                                    <?= $item->description ?>
                                                </p>
                                            </div>
                                        </div>
                                        <hr class="mt-0 mb-0" style="color: grey;">
                                    </div><!--//item-->
                                <?php endforeach; ?>
                            <?php else: ?>
                                <?php if (empty($isRendered)): ?>
                                    <div class="add-new-btn  text-center" id="add-activity-template"
                                         data-toggle="tooltip"
                                         data-placement="top" title="Thêm biểu mẫu">
                                        <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
                <!-- End Hoạt động -->
                <!-- Chứng chỉ -->
                <section class="work-section py-3 cv-block cv-section-event" id="certificates-block"
                         my-title="Chứng chỉ">
                    <div class="cvo-block-title">
                        <h3><?= $candidate->candidate_id == 971281 ? "Skills" :  Yii::t('cvgo', 'Certificates'); ?></h3>
                    </div>
                    <div class="cvo-block-content">
                        <div class="item">
                            <ul class="list-unstyled resume-skills-list" id="certificates-container"
                                block="certificates">
                                <?php if (!empty($certificates)): ?>
                                    <?php foreach ($certificates as $item): ?>
                                        <li class="mb-3 cv-child-elem certificates-item">
                                            <p class="d-none item-id" info-name="id"
                                               info-group="certificates"><?= $item->id ?></p>
                                            <div class="row">
                                                <div class="col-9 col-md-9 col-lg-9 col-xl-9">
                                                    <div class="resume-degree cv-editable-elem required"
                                                         data-placeholder="Chuyên ngành" info-group="certificates"
                                                         info-name="name"><?= $item->name ?></div>
                                                </div>
                                                <div class="col-3 col-md-3 col-lg-3 col-xl-3 text-right">
                                                    <?php if (empty($isRendered)): ?>
                                                        <p class="cv-editable-elem d-inline required"
                                                           data-placeholder="Năm" info-group="certificates"
                                                           info-name="year"><?= $item->year ?></p>
                                                    <?php else: ?>
                                                        <?php if (!empty($item->year)): ?>
                                                            <p class="cv-editable-elem d-inline required"
                                                               data-placeholder="Năm" info-group="certificates"
                                                               info-name="year"><?= $item->year ?></p>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <hr class="mt-0 mb-0" style="color: grey">
                                        </li>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <?php if (empty($isRendered)): ?>
                                        <div class="add-new-btn  text-center" id="add-certificates-template"
                                             data-toggle="tooltip" data-placement="top" title="Thêm biểu mẫu">
                                            <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </ul>
                        </div><!--//item-->
                    </div>
                </section>
                <!-- End Chứng chỉ -->
                <!-- Giải thưởng -->
                <section class="work-section py-3 cv-block cv-section-event" id="award-block" my-title="Giải thưởng">
                    <div class="cvo-block-title">
                        <h3><?= Yii::t('cvgo', 'Award'); ?></h3>
                    </div>
                    <div class="cvo-block-content">
                        <div block="award" id="award-container">
                            <?php if (!empty($award)): ?>
                                <?php foreach ($award as $item): ?>
                                    <div class="item cv-child-elem award-item">
                                        <p class="d-none item-id" info-name="id" info-group="award"><?= $item->id ?></p>
                                        <div class="row">
                                            <div class="col-9 col-md-9 col-lg-9 col-xl-9">
                                                <p class="mb-0 cv-editable-elem required"
                                                   data-placeholder="Tên thành tích" info-name="name"
                                                   info-group="award"><?= $item->name ?></p>
                                            </div>
                                            <div class="col-3 col-md-3 col-lg-3 col-xl-3 text-right">
                                                <?php if (empty($isRendered)): ?>
                                                    <p class="cv-editable-elem d-inline required" data-placeholder="Năm"
                                                       info-group="award" info-name="year"><?= $item->year ?></p>
                                                <?php else: ?>
                                                    <?php if (!empty($item->year)): ?>
                                                        <p class="cv-editable-elem d-inline required"
                                                           data-placeholder="Năm"
                                                           info-group="award" info-name="year"><?= $item->year ?></p>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <hr class="mb-1 mt-1" style="color:grey;">
                                    </div><!--//item-->
                                <?php endforeach; ?>
                            <?php else: ?>
                                <?php if (empty($isRendered)): ?>
                                    <div class="add-new-btn  text-center" id="add-award-template" data-toggle="tooltip"
                                         data-placement="top" title="Thêm biểu mẫu">
                                        <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
                <!-- End Giải thưởng -->
                <!-- Kỹ năng bản thân -->
                <section class="work-section py-3 cv-block cv-section-event" id="skills-block"
                         my-title="Kỹ năng bản thân">
                    <div class="cvo-block-title">
                        <h3><?= Yii::t('cvgo', 'Skills'); ?></h3>
                    </div>
                    <div class="cvo-block-content">
                        <div class="item">
                            <ul class="list-unstyled resume-skills-list" id="list-skill" block="skill">
                                <?php if (!empty($candidateJobCategory)): ?>
                                    <?php foreach ($candidateJobCategory as $item): ?>
                                        <li class="mb-2 cv-child-elem skill-item no-copy-controls"
                                            myId="<?= $item->category->job_category_id ?>">
                                            <p class="d-none item-id" info-name="candidate_job_category_id"
                                               info-group="skill"><?= $item->candidate_job_category_id ?></p>
                                            <p class="d-none item-id" info-name="job_category_id"
                                               info-group="skill"><?= $item->category->job_category_id ?></p>
                                            <div class="row">
                                                <div class="col-8 col-md-8 col-lg-8 col-xl-8">
                                                    <div class="resume-skill-name">
                                                        <?php if (!empty($item->skill_title)): ?>
                                                            <strong info-name="skill_title" info-group="skill"><?= $item->skill_title ?></strong>
                                                        <?php else: ?>
                                                            <?php if ($cv->language == 'en-US'): ?>
                                                            <strong info-name="skill_title" info-group="skill"><?= $item->category->job_category_name_en ?></strong>
                                                            <?php else: ?>
                                                            <strong info-name="skill_title" info-group="skill"><?= $item->category->job_category_name_vn ?></strong>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="col-4 col-md-4 col-lg-4 col-xl-4 text-right">
                                                    <span class="d-none" info-name="experiment_duration" info-group="skill"><?= $item->experiment_duration ?></span>
                                                    <?php if ($item->experiment_duration == 0) : ?>
                                                        <span>
                                                            <span class="exp-year-title"><?= Yii::t('cvgo', 'below') ?> 1</span> 
                                                            <span class="exp-year-text"><?= Yii::t('cvgo', 'year of experience'); ?></span>
                                                        </span>
                                                    <?php else: ?>
                                                        <span>
                                                            <span class="exp-year-title"><?= $item->experiment_duration ?></span> 
                                                            <span class="exp-year-text"><?= Yii::t('cvgo', 'year(s) of experience'); ?></span>
                                                        </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <hr class="mt-0 mb-0" style="color: grey">
                                        </li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ul>
                            <?php if (empty($isRendered)): ?>
                                <select class="form-control select-personal-skill select-skill-select2" id="select-personal-skill">
                                    <option></option>
                                    <?php if (!empty($jobCategory)) : ?>
                                        <?php foreach ($jobCategory as $item) : ?>
                                            <?php if ($item->status >= 0) : ?>
                                                <?php if ($cv->language == 'en-US'): ?>
                                                <option value="<?= $item->job_category_id ?>"><?= $item->job_category_name_en ?></option>
                                                <?php else: ?>
                                                <option value="<?= $item->job_category_id ?>"><?= $item->job_category_name_vn ?></option>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                                <div class="add-new-btn text-center" id="add-skill-template">
                                    <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                </div>
                            <?php endif; ?>
                        </div><!--//item-->
                    </div>
                </section>
                <!-- End Kỹ năng bản thân -->
                <!-- Others -->
                <div class="row remove-padding">
                    <!-- Kỹ năng mềm -->
                    <section class="col-5 col-sm-5 col-md-5 col-lg-5 col-xl-5 skills-section cv-block cv-section-event"
                             id="soft-skill-block"
                             my-title="Kỹ năng mềm">
                        <div class="cvo-block-title">
                            <h3><?= Yii::t('cvgo', 'Soft skills'); ?></h3>
                        </div>
                        <div class="cvo-block-content mt-3">
                            <div class="item">
                                <ul class="list-unstyled resume-skills-list" id="list-soft-skill"
                                    block="soft-skill">
                                    <?php if (!empty($candidateSoftSkill)): ?>
                                        <?php foreach ($candidateSoftSkill as $item): ?>
                                            <li class="mb-2 cv-child-elem soft-skill-item no-copy-controls"
                                                myId="<?= $item->soft_skill_id ?>">
                                                <p class="d-none item-id" info-name="candidate_soft_skill_id"
                                                   info-group="soft_skill"><?= $item->candidate_soft_skill_id ?></p>
                                                <p class="d-none item-id" info-name="soft_skill_id"
                                                   info-group="soft_skill"><?= $item->soft_skill_id ?></p>
                                                <div class="progress resume-progress">
                                                    <div class="progress-bar soft-skill-progress-bar theme-progress-bar-dark"
                                                         role="progressbar" style="width: <?= $item->level ?>%"
                                                         aria-valuenow="25" aria-valuemin="0"
                                                         aria-valuemax="100" value="<?= $item->level ?>">
                                                            <span><span
                                                                        info-group="soft_skill"
                                                                        info-name="level"><?= $item->level ?></span>%</span>
                                                    </div>
                                                </div>
                                                <?php if ($cv->language == 'en-US'): ?>
                                                <div class="resume-skill-name" info-group="soft_skill" info-name="soft_skill_name"><?= $item->softskill->soft_skill_name_eng ?></div>
                                                <?php else: ?>
                                                <div class="resume-skill-name" info-group="soft_skill" info-name="soft_skill_name"><?= $item->softskill->soft_skill_name ?></div>   
                                                <?php endif; ?>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                                <?php if (empty($isRendered)): ?>
                                    <select class="form-control select-soft-skill-new select2"
                                            id="select-soft-skill-new">
                                        <option></option>
                                        <?php if (!empty($softSkill)) : ?>
                                            <?php foreach ($softSkill as $item) : ?>
                                                <?php if ($cv->language == 'en-US'): ?>
                                                <option value="<?= $item->soft_skill_id ?>"><?= $item->soft_skill_name_eng ?></option>    
                                                <?php else: ?>
                                                <option value="<?= $item->soft_skill_id ?>"><?= $item->soft_skill_name ?></option>    
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                    <div class="add-new-btn text-center" id="add-soft-skill-template">
                                        <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                    </div>
                                <?php endif; ?>
                            </div><!--//item-->
                        </div>
                    </section>
                    <!-- End Kỹ năng mềm -->
                    <?php if (empty($isRendered)): ?>
                        <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
                    <?php else: ?>
                        <?php if (!empty($candidateSoftSkill)): ?>
                            <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <!-- Ngôn ngữ -->
                    <section class="col-5 col-sm-5 col-md-5 col-lg-5 col-xl-5 skills-section cv-block cv-section-event"
                             id="language-block"
                             my-title="Ngôn ngữ">
                        <div class="cvo-block-title">
                            <h3><?= Yii::t('cvgo', 'Language'); ?></h3>
                        </div>
                        <div class="cvo-block-content mt-3">
                            <ul class="list-unstyled resume-skills-list" block="language"
                                id="language-container">
                                <?php if (!empty($candidateLanguage)): ?>
                                    <?php foreach ($candidateLanguage as $item): ?>
                                        <li class="mb-2 cv-child-elem language-item no-copy-controls"
                                            myId="<?= $item->language_id ?>">
                                            <p class="d-none item-id" info-name="candidate_language_id"
                                               info-group="language"><?= $item->candidate_language_id ?></p>
                                            <p class="d-none item-id" info-name="language_id"
                                               info-group="language"><?= $item->language_id ?></p>
                                            <div class="progress left-print-progress-bar">
                                                <div class="progress-bar" role="progressbar"
                                                     style="width: <?= $item->level ?>%;" aria-valuenow="25"
                                                     aria-valuemin="0" aria-valuemax="100"><span><span
                                                                info-group="language"
                                                                info-name="level"><?= $item->level ?></span>%</span>
                                                </div>
                                            </div>
                                            <?php if ($cv->language == 'en-US'): ?>
                                            <p class="resume-lang-name" info-group="language" info-name="language_name"><?= $item->language->language_name_en ?></p>
                                            <?php else: ?>
                                            <p class="resume-lang-name" info-group="language" info-name="language_name"><?= $item->language->language_name ?></p>
                                            <?php endif; ?>
                                        </li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ul>
                            <?php if (empty($isRendered)): ?>
                                <select class="form-control select-skill select2" id="select-language">
                                    <option></option>
                                    <?php if (!empty($language)) : ?>
                                        <?php foreach ($language as $item) : ?>
                                            <?php if ($cv->language == 'en-US'): ?>
                                            <option value="<?= $item->language_id ?>"><?= $item->language_name_en ?></option>
                                            <?php else: ?>
                                            <option value="<?= $item->language_id ?>"><?= $item->language_name ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                                <div class="add-new-btn  text-center" id="add-language-template">
                                    <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </section>
                    <!-- End Ngôn ngữ -->
                </div>
                <!-- End others -->
                <!-- Sở thích -->
                <section class="skills-section py-3 cv-block cv-section-event" id="interests-block" my-title="Sở thích">
                    <div class="cvo-block-title">
                        <h3><?= Yii::t('cvgo', 'Hobby'); ?></h3>
                    </div>
                    <div class="cv-block-content">
                        <div class="item">
                            <ul class="list-unstyled resume-interests-list" block="hobby" id="hobby-container">
                                <?php if (!empty($candidateHobby)): ?>
                                    <div class="row">
                                        <?php foreach ($candidateHobby as $item): ?>
                                            <div class="col-sm-4 col-md-4">
                                                <li class="mb-2 cv-child-elem hobby-item only-remove-controls"
                                                    myId="<?= $item->hobby_id ?>">
                                                    <p class="d-none item-id" info-name="candidate_hobby_id"
                                                       info-group="hobby"><?= $item->candidate_hobby_id ?></p>
                                                    <p class="d-none item-id" info-name="hobby_id"
                                                       info-group="hobby"><?= $item->hobby_id ?></p>
                                                    <?php if ($cv->language == 'en-US'): ?>
                                                    <p class="resume-lang-name" info-group="hobby" info-name="hobby_name"><?= $item->hobby->hobby_name_en ?></p>
                                                    <?php else: ?>
                                                    <p class="resume-lang-name" info-group="hobby" info-name="hobby_name"><?= $item->hobby->hobby_name ?></p>
                                                    <?php endif; ?>
                                                </li>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </ul>
                            <?php if (empty($isRendered)): ?>
                                <select class="form-control select-skill select2" id="select-hobby-new">
                                    <option></option>
                                    <?php if (!empty($hobby)) : ?>
                                        <?php foreach ($hobby as $item) : ?>
                                            <?php if ($cv->language == 'en-US'): ?>
                                            <option value="<?= $item->hobby_id ?>"><?= $item->hobby_name_en ?></option>
                                            <?php else: ?>
                                            <option value="<?= $item->hobby_id ?>"><?= $item->hobby_name ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                                <div class="add-new-btn text-center" id="add-hobby-template">
                                    <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </section><!--// hobby-section-->
                <!-- End Sở thích -->
                <!-- References -->
                <section class="resume-section reference-section mb-5 cv-block cv-section-event" id="references-block"
                         my-title="Người tham chiếu">
                    <div class="cvo-block-title">
                        <h3><?= Yii::t('cvgo', 'References'); ?></h3>
                    </div>
                    <div class="cv-block-content">
                        <div class="item">
                            <ul class="list-unstyled resume-skills-list" block="references" id="references-container">
                                <?php if (!empty($references)): ?>
                                    <?php foreach ($references as $item): ?>
                                        <li class="mb-2 cv-child-elem references-item">
                                            <p class="d-none item-id" info-name="id"
                                               info-group="references"><?= $item->id ?></p>
                                            <div class="cv-editable-elem required"
                                                 data-placeholder="Người tham chiếu" info-name="references_text"
                                                 info-group="references"><?= $item->references_text ?></div>
                                        </li>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <?php if (empty($isRendered)): ?>
                                        <div class="add-new-btn text-center" id="add-references-template"
                                             data-toggle="tooltip" data-placement="top" title="Thêm biểu mẫu">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </section><!--//references-section-->
                <!-- End References -->
            </div><!--//resume-body-->
            <hr class="mt-0 mb-0">
            <div class="resume-footer text-center" style="line-height: 0;">
                <ul class="resume-social-list list-inline mx-auto mb-0 d-inline-block text-muted">
                    <li class="list-inline-item mb-lg-0 mr-3 social-container">
                        <i class="fab fa-facebook-f fa-2x mr-2" data-fa-transform="down-4"></i>
                        <span class="d-lg-inline-block text-muted">
								<p class="cv-editable-elem d-inline" data-placeholder="fb.com/username"
                                   info-group="candidate" info-name="facebook"><?= $candidate->facebook ?></p>
							</span>
                    </li>
                    <li class="list-inline-item mb-lg-0 mr-3 social-container">
                        <i class="fab fa-linkedin fa-2x mr-2" data-fa-transform="down-4"></i>
                        <span class="d-lg-inline-block text-muted">
								<p class="cv-editable-elem d-inline" data-placeholder="linkedin.com/in/username"
                                   info-group="candidate" info-name="linkedin"><?= $candidate->linkedin ?></p>
							</span>
                    </li>
                    <li class="list-inline-item mb-lg-0 mr-lg-3 social-container">
                        <i class="fab fa-skype fa-2x mr-2" data-fa-transform="down-4"></i>
                        <span class="d-lg-inline-block text-muted">
								<p class="cv-editable-elem d-inline" data-placeholder="live:skype_username"
                                   info-group="candidate" info-name="skype"><?= $candidate->skype ?></p>
							</span>
                    </li>
                </ul>
            </div><!--//resume-footer-->
        </article>
    </div><!--//container-->
    <?php if (empty($isRendered)): ?>
        <?= $this->render('../layout/hidden_list') ?>
        <?= $this->render('../layout/modal') ?>
    <?php else: ?>
        <script type="text/javascript" src="/cv_template/assets/js/render.js"></script>
    <?php endif; ?>
    <?= $this->render('../layout/footer', [
        'theme' => $theme,
        'cv' => $cv,
        'color' => $color,
        'activeColor' => $activeColor,
        'fontFamily' => $fontFamily,
        'activeFontFamily' => $activeFontFamily,
        'isRendered' => $isRendered
    ]) ?>
    <?= $this->render('../layout/init_js', ['candidateId' => $candidate->candidate_id]) ?>
    <script type="text/javascript" src="/cv_template/assets/js/script.js"></script>
    <!-- try this if you want to customize skill,soft-skill, language adding layout -->
    <script type="text/javascript" src="/cv_template/theme_7/js/customize.js"></script>
</div><!--//main-wrapper-->
</body>
</html>