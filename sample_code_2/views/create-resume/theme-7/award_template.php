<div class="item cv-child-elem award-item">
    <div class="row">
        <div class="col-12 col-md-9 col-lg-9 col-xl-9">
            <p class="mb-0 cv-editable-elem required"
               data-placeholder="Tên thành tích" info-name="name"
               info-group="award"><?= Yii::t('cvgo', 'Excellent employees at ...') ?></p>
        </div>
        <div class="col-12 col-md-3 col-lg-3 col-xl-3 text-right">
            <p class="cv-editable-elem d-inline required" data-placeholder="Năm"
               info-group="award" info-name="year"><?= date('Y', strtotime("-1 year", time())) ?></p>
        </div>
    </div>
    <hr class="mb-1 mt-1" style="color:grey;">
</div><!--//item-->