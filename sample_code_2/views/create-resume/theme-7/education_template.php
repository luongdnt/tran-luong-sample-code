<div class="item cv-child-elem education-item mb-0">
    <div class="row">
        <div class="col-12 col-md-9 col-lg-9 col-xl-9">
            <h4 class="m-0 item-content cv-editable-elem required"
                data-placeholder="Tên trường" info-name="school_name"
                info-group="education">
                <?= Yii::t('cvgo', '... University'); ?>
            </h4>
        </div>
        <div class="col-12 col-md-3 col-lg-3 col-xl-3 text-right">
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Năm bắt đầu"
               info-group="education"
               info-name="date_start"><?= date('Y', strtotime("-1 year", time())) ?></p>
            -
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Năm kết thúc" info-group="education"
               info-name="date_end"><?= date('Y') ?></p>
        </div>
        <div class="pl-3">
            <div class="mb-0 italic-title cv-editable-elem required"
                 data-placeholder="Chuyên ngành" info-name="specialization"
                 info-group="education"><?= Yii::t('cvgo', 'Information Technology'); ?>
            </div>
            <div class="item-content cv-editable-elem"
                 data-placeholder="Mô tả học vấn" info-name="hightlight"
                 info-group="education">
                <?= Yii::t('cvgo', 'Good graduate'); ?>
            </div>
        </div>
    </div>
    <hr style="color: grey" class="mt-1 mb-1">
</div><!--//item-->