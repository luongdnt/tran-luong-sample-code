<div class="item cv-child-elem activity-item mb-2">
    <div class="row">
        <div class="col-12 col-md-7 col-lg-7 col-xl-7">
            <h4 class="mb-0 cv-editable-elem required"
                data-placeholder="Tên công ty/tổ chức/sự kiện"
                info-name="organization"
                info-group="activity"><?= Yii::t('cvgo', '... volunteer group') ?></h4>
        </div>
        <div class="col-12 col-md-5 col-lg-5 col-xl-5 text-right">
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Từ ngày"
               info-group="activity" info-name="from"><?= date("d/m/Y") ?></p>
            -
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Đến ngày"
               info-group="activity" info-name="to"><?= date("d/m/Y") ?></p>
        </div>
        <div class="pl-3">
            <div class="item-content italic-title cv-editable-elem required"
                 data-placeholder="Vị trí/vai trò" info-name="role"
                 info-group="activity">
                <?= Yii::t('cvgo', 'Volunteer') ?>
            </div>
            <div class="item-content cv-editable-elem"
                 data-placeholder="Mô tả hoạt động" info-name="description"
                 info-group="activity">
                <?= Yii::t('cvgo', 'Gather the gifts and distribute them to the homeless.') ?> 
                <?= Yii::t('cvgo', 'Share, encourage them to overcome difficult times, help them have positive thoughts.') ?>
            </div>
        </div>
    </div>
    <hr class="mt-1 mb-0" style="color: grey;">
</div><!--//item-->