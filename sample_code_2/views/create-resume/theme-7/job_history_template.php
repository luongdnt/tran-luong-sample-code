<div class="item m-0 cv-child-elem history-item">
    <div class="row">
        <div class="col-12 col-md-9 col-lg-9 col-xl-9">
            <h4 class="cv-editable-elem m-0 required"
                data-placeholder="Tên công ty"
                info-group="job_history"
                info-name="job_company"><?= Yii::t('cvgo', '... JSC'); ?></h4>
        </div>
        <div class="col-12 col-md-3 col-lg-3 col-xl-3 text-right">
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Năm bắt đầu"
               info-group="job_history"
               info-name="date_start"><?= date('Y', strtotime("-1 year", time())) ?></p>
            -
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Năm kết thúc" info-group="job_history"
               info-name="date_end"><?= date('Y') ?></p>
        </div>
        <div class="pl-3">
            <div class="item-title italic-title cv-editable-elem required"
                 data-placeholder="Vị trí/công việc" info-name="job_title"
                 info-group="job_history"><?= Yii::t('cvgo', 'Sale'); ?>
            </div>
            <div class="item-content m-0 cv-editable-elem required"
                 data-placeholder="Mô tả công việc"
                 info-name="job_description_html"
                 info-group="job_history">
                <p class="resume-timeline-item-desc-heading font-weight-bold cv-editable-elem"><?= Yii::t('cvgo', 'Mission'); ?>:</p>
                <p class="cv-editable-elem">- <?= Yii::t('cvgo', 'Carry out business plans: find customers, introduce products and services, consult products and applications, take care and manage customer relationships.'); ?></p>
                <p class="cv-editable-elem">- <?= Yii::t('cvgo', 'Make annual, quarterly, monthly, and weekly work plans to achieve assigned goals.'); ?></p>
                <p class="cv-editable-elem">- <?= Yii::t('cvgo', 'Survey, study, evaluate expected revenue in the management area. Develop and propose appropriate business plans.') ?></p>
                <p class="resume-timeline-item-desc-heading font-weight-bold cv-editable-elem"><?= Yii::t('cvgo', 'Achievements'); ?>:</p>
                <p class="cv-editable-elem"><?= Yii::t('cvgo', 'Through the process of working at ... Joint Stock Company, I have achieved some of the following achievements'); ?>:</p>
                <ul>
                    <li class="cv-editable-elem p-text"><?= Yii::t('cvgo', '... Best seller') ?> <?= date('Y', strtotime("-1 year", time())) ?></li>
                    <li class="cv-editable-elem p-text"><?= Yii::t('cvgo', 'Excellent staff') ?> <?= date('Y', strtotime("-1 year", time())) ?></li>
                    <li class="cv-editable-elem p-text"><?= Yii::t('cvgo', 'Creative staff') ?></li>
                    <li class="cv-editable-elem p-text"><?= Yii::t('cvgo', 'Excellent sale project') ?></li>
                </ul>
            </div>
        </div>
    </div>
    <hr class="mt-1 mb-1" style="color: grey">
</div><!--//item-->