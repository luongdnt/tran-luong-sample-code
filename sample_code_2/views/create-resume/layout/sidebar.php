<div id="mySidenav" class="sidenav sidebar-elem" onfocusout="closeNav()">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	<?php if (!empty($theme)): ?>
		<?php foreach ($theme as $item): ?>
			<div class="theme-item">
				<?php if ($item->id == $cv['theme']->id): ?>
					<span class="notify-badge">Đang chọn</span>
				<?php endif; ?>
				<a href="javascript:void(0)" class="select-theme" theme-id=<?= $item->id ?>>
					<img class="rounded img-thumbnail" src="/cv_template/assets/images/theme/<?= $item->thumbnail ?>" alt="<?= $item->alias ?>" width="150">
					<p class="text-center" style="font-size: 13px;"><?= $item->name ?></p>
				</a>
				<?php if ($item->id > 6): ?>
					<span class="hot-badge"><i class="fab fa-hotjar"></i> Hot</span>
				<?php endif; ?>
			</div>
		<?php endforeach;?>
	<?php else:?>
		<a href="#">Không tồn tại theme nào</a>
	<?php endif; ?>
</div>
