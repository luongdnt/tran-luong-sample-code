<div id="top-panel">
    <a target="_blank" title="Trang chủ JobsGO.vn" href="https://jobsgo.vn"><img src="https://jobsgo.vn/img/logo-2019.png" height="33" alt="JobsGO" class="float-left" id="logo"></a>
    <div class="float-right">
        <div class="btn-group header-btn-group" role="group">
            <button class="btn btn-light save-btn" id="manage-btn">
                <i class="fas fa-tasks"></i>  
                <span>Quản lý CV</span>    
            </button>
            <button class="btn btn-light save-btn" id="translate_now">
                <i class="fa fa-language"></i>
                <span>Dịch CV</span>
                <span class="badge badge-danger">N</span>
            </button>
            <button type="button" class="btn btn-light save-btn sidebar-elem" title="Lưu xuống hồ sơ" onclick="openNav(); ga('send', 'event', 'go_cv', 'change_theme');">
                <i class="fas fa-copy"></i> 
                <span class="btn-title">Đổi mẫu</span> 
            </button>
            <button type="button" class="btn btn-light save-btn" title="Lưu xuống hồ sơ" id="save-btn" onclick="ga('send', 'event', 'go_cv', 'save_resume');">
                <i class="fas fa-save"></i> 
                <span class="btn-title">Lưu hồ sơ</span>
            </button>
            <button type="button" class="btn btn-light save-btn" title="Tải xuống hồ sơ" id="download-resume-btn" onclick="ga('send', 'event', 'go_cv', 'download_resume');">
                <i class="fas fa-file-download"></i> 
                <span class="btn-title">Tải xuống hồ sơ</span>
            </button>
        </div>
    </div>
</div>
<div class="bd-example" style="display: none;" id="unica-alert">
    <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
        <i class="fas fa-gift"></i> <i>Sau khi hoàn thành CV, JobsGO sẽ gửi tặng bạn một khóa học Unica tùy chọn, hoàn toàn miễn phí!</i>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
</div>
