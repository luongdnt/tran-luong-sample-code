<script>(function (w, d, s, l, i) { w[l] = w[l] || []; w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'}); var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f); })(window, document, 'script', 'dataLayer', 'GTM-W4XCPHD');</script> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-791019431"></script> <script> window.dataLayer = window.dataLayer || []; function gtag() { dataLayer.push(arguments); } gtag('js', new Date()); gtag('config', 'AW-791019431'); </script>
<footer class="footer jobsgo-watermark">
    <small class="copyright">© jobsgo.vn</small>
</footer>
<style type="text/css">
    .font-wrapper {
        <?php if ($cv->font_size == 1) : ?>
            font-size: 0.9em;
        <?php elseif ($cv->font_size == 2) : ?>
            font-size: 1.0em;
        <?php elseif ($cv->font_size == 3) : ?>
            font-size: 1.1em;
        <?php else : ?>
            font-size: 1.0em;
        <?php endif; ?>
    }
    .cv-editable-elem {
        <?php if ($cv->line_height == 1) : ?>
            line-height: 1.2em;
        <?php elseif ($cv->line_height == 2) : ?>
            line-height: 1.4em;
        <?php elseif ($cv->line_height == 3) : ?>
            line-height: 1.6em;
        <?php else : ?>
            line-height: 1.4em;
        <?php endif; ?>
    }
</style>
<!-- Color panel -->
<?php if (empty($isRendered)): ?>
<div id="config-panel" class="config-panel d-none d-lg-block" style="right: 0px;">
    <div class="panel-inner">
        <a id="config-trigger" class="config-trigger text-center config-panel-open" href="#">
            <i class="fas fa-cog mx-auto" data-fa-transform="down-6"></i>
        </a>
        <div class="mb-3 mt-3">
            <h5 class="panel-title text-white mb-1">Tên CV</h5>
            <div class="mb-2">
                <?php if ($cv->cv_type != 1) : ?>
                    <p class="cv-editable-elem cv-chinh" id="cv-name"><?= $cv->cv_name ?></p>
                <?php else : ?>
                    <p id="cv-name"><?= $cv->cv_name ?></p>
                <?php endif; ?>
            </div>
        </div>
        <!-- Select color -->
        <div class="mb-3">
            <h5 class="panel-title text-white mb-1">Chọn màu</h5>
            <ul id="color-options" class="list-inline mb-2">
                <?php if (!empty($color)) : ?>
                    <?php foreach ($color as $key => $value) : ?>
                        <?php if ($key == $activeColor) : ?>
                            <li class="theme-1 list-inline-item active"><a style="background: <?= $value ?>"
                                                                           data-style="<?= $key ?>"
                                                                           href="javascript:void(0)"></a></li>
                        <?php else : ?>
                            <li class="theme-1 list-inline-item"><a style="background: <?= $value ?>"
                                                                    data-style="<?= $key ?>" href="javascript:void(0)"></a>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
        <!-- End select color -->
        
        <!-- Select font family -->
        <div class="mb-3">
            <h5 class="panel-title text-white mb-1">Chọn Font chữ</h5>
            <div id="font-family-options" class="mb-2">
                <select class="custom-select" id="font-family-select-options">
                    <option selected disabled>Chọn Font chữ</option>
                    <?php if (!empty($fontFamily)) : ?>
                        <?php foreach ($fontFamily as $key => $value) : ?>
                            <?php if ($key == $activeFontFamily) : ?>
                                <option value="<?= $key ?>" selected style="font-family: <?= $key ?>"><?= $value ?></option>
                            <?php else : ?>
                                <option value="<?= $key ?>" style="font-family: <?= $key ?>"><?= $value ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>
        <!-- End select font family -->
        
        <div class="mb-3">
            <h5 class="panel-title text-white mb-1">Cỡ chữ</h5>
            <ul id="font-size-options" class="list-inline mb-2">
                <li class="list-inline-item <?php if ($cv->font_size == 1) : ?> active <?php endif; ?>">
                    <a href="javascript:void(0)" data-size="1" style="font-size: 0.8em">
                        <i class="fa fa-font"></i>
                    </a>
                </li>
                <li class="list-inline-item <?php if ($cv->font_size == 2) : ?> active <?php endif; ?>">
                    <a href="javascript:void(0)" data-size="2" style="font-size: 1.0em">
                        <i class="fa fa-font"></i>
                    </a>
                </li>
                <li class="list-inline-item <?php if ($cv->font_size == 3) : ?> active <?php endif; ?>">
                    <a href="javascript:void(0)" data-size="3" style="font-size: 1.2em">
                        <i class="fa fa-font"></i>
                    </a>
                </li>
            </ul>
        </div>

        <div class="mb-3">
            <h5 class="panel-title text-white mb-1">Cách dòng</h5>
            <ul id="line-height-options" class="list-inline mb-2">
                <li class="list-inline-item <?php if ($cv->line_height == 1) : ?> active <?php endif; ?>">
                    <a href="javascript:void(0)" data-size="1" style="font-size: 0.8em">
                        <i class="fas fa-text-height"></i>
                    </a>
                </li>
                <li class="list-inline-item <?php if ($cv->line_height == 2) : ?> active <?php endif; ?>">
                    <a href="javascript:void(0)" data-size="2" style="font-size: 1.0em">
                        <i class="fas fa-text-height"></i>
                    </a>
                </li>
                <li class="list-inline-item <?php if ($cv->line_height == 3) : ?> active <?php endif; ?>">
                    <a href="javascript:void(0)" data-size="3" style="font-size: 1.2em">
                        <i class="fas fa-text-height"></i>
                    </a>
                </li>
            </ul>
        </div>

        <div class="mb-3">
            <h5 class="panel-title text-white mb-1">Ngôn ngữ</h5>
            <ul id="language-options" class="list-inline mb-2">
                <li class="list-inline-item <?php if ($cv->language == 'vi-VN') : ?> active <?php endif; ?>">
                    <a href="javascript:void(0)" data-language="vi-VN">
                        <img class="language-flag" src="/cv_template/assets/images/vietnam-flag-round-medium.png" width="28px" />
                    </a>
                </li>
                <li class="list-inline-item <?php if ($cv->language == 'en-US') : ?> active <?php endif; ?>">
                    <a href="javascript:void(0)" data-language="en-US">
                        <img class="language-flag" src="/cv_template/assets/images/united-states-of-america-flag-round-medium.png" width="28px" />
                    </a>
                </li>
            </ul>
        </div>
        <a id="config-close" class="close" href="#">
            <i class="far fa-times-circle"></i>
        </a>
    </div><!--//panel-inner-->
</div>
<?php endif; ?>