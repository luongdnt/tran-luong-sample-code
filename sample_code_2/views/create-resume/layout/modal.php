<?php
use Yii;
use common\colorgb\ColorgbHelper;
?>
<!-- Modal -->
<div class="modal fade" id="experience-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-award"></i> Số năm kinh nghiệm</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<div class="range-slider">
						  	<input class="range-slider__range" id="exp-year" type="range" value="0" min="0" max="15">
						  	<span class="range-slider__value" id="exp-year-val">0</span>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="experience-confirm-btn">Xác nhận</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="update-exp-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-award"></i> Cập nhật số năm kinh nghiệm</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p><i class="fas fa-star"></i> Chọn số năm kinh nghiệm cho <strong id="update-exp-name"></strong></p>
				<form>
					<div class="form-group">
						<div class="range-slider">
						  	<input class="range-slider__range" id="update-exp-year" type="range" value="0" min="0" max="15">
						  	<span class="range-slider__value" id="update-exp-year-val">0</span>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="update-exp-confirm-btn">Xác nhận</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="language-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-globe-asia"></i> Thành thạo ngôn ngữ</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<div class="range-slider">
						  	<input class="range-slider__range" id="lang-competently" type="range" step="10" value="50" min="0" max="100">
						  	<span class="range-slider__value" id="lang-competently-val">1</span>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="language-confirm-btn">Xác nhận</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="update-language-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-globe-asia"></i> Cập nhật độ thành thạo ngôn ngữ</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p><i class="fas fa-star"></i> Chọn độ thành thạo cho <strong id="update-lang-name"></strong></p>
				<form>
					<div class="form-group">
						<div class="range-slider">
						  	<input class="range-slider__range" id="update-lang-competently" type="range" step="10" value="50" min="0" max="100">
						  	<span class="range-slider__value" id="update-lang-competently-val">1</span>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="update-language-confirm-btn">Xác nhận</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="soft-skill-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-feather"></i> Thành thạo kỹ năng</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<div class="range-slider">
						  	<input class="range-slider__range" id="soft-skill-competently" type="range" step="10" value="50" min="0" max="100">
						  	<span class="range-slider__value" id="soft-skill-competently-val">1</span>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="soft-skill-confirm-btn">Xác nhận</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="update-soft-skill-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-feather"></i> Cập nhật độ thành thạo kỹ năng</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p><i class="fas fa-star"></i> Chọn độ thành thạo cho <strong id="update-soft-skill-name"></strong></p>
				<form>
					<div class="form-group">
						<div class="range-slider">
						  	<input class="range-slider__range" id="update-soft-skill-competently" type="range" step="10" value="50" min="0" max="100">
						  	<span class="range-slider__value" id="update-soft-skill-competently-val">1</span>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="update-soft-skill-btn">Xác nhận</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="unica-promote" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<img class="img-fluid" src="/cv_template/assets/images/jobsgo-unica.jpg">
				<button type="button" class="btn btn-secondary float-right mt-3" data-dismiss="modal">Đóng</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="unica-success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-award"></i> Viết CV hay, nhận ngay khóa học</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Chúc mừng bạn đã hoàn thành CV cho riêng mình! JobsGO xin gửi tặng bạn phần quà 01 khóa học online trên website UNICA. Để nhận quà, bạn hãy tạo một tài khoản Unica <a href="https://id.unica.vn/register" target="_blank">tại đây</a>, sau đó nhận quà tại nút dưới đây nhé:</p>
				<a class="btn btn-block btn-primary text-white" href="<?=Yii::$app->params['ntv_url']?>/create-resume/unica-gif?token=<?= ColorgbHelper::encryptInt(Yii::$app->user->identity->candidate_id) ?>" target="_blank">Nhận khóa học ngay!</a>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="survey-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><i class="far fa-comment-dots"></i> Feedback</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Đừng quên để lại feedback để chúng tôi cải thiện CVGo trong tương lai nhé!</p>
				<hr/>
				<form>
					<div class="form-group">
						<label for="easy">Bạn có thấy CV Go dễ dùng không?</label>
						<select class="form-control" id="easy">
							<option value="" selected disabled>---Chọn---</option>
							<option value="1">Dễ dùng</option>
							<option value="2">Bình thường</option>
							<option value="3">Khó dùng</option>
							<option value="4">Rất khó dùng</option>
						</select>
					</div>
					<div class="form-group">
						<label for="back">Bạn sẽ quay lại dùng CV Go trong tương lai chứ?</label>
						<select class="form-control" id="back">
							<option value="" selected disabled>---Chọn---</option>
							<option value="1">Sẽ quay lại</option>
							<option value="2">Sẽ cân nhắc</option>
							<option value="3">Không quay lại</option>
						</select>
					</div>
					<div class="form-group">
						<label for="satisfy">Bạn có hài lòng với các mẫu CV hiện tại không?</label>
						<select class="form-control" id="satisfy">
							<option value="" selected disabled>---Chọn---</option>
							<option value="1">Hài Lòng</option>
							<option value="2">Bình thường</option>
							<option value="3">Không hài lòng</option>
						</select>
					</div>
					<div class="form-group">
						<label for="additional">Góp ý thêm</label>
						<input type="email" class="form-control" id="additional" placeholder="Các màu của theme chưa đẹp, thao tác rắc rối...v.v">
					</div>
				</form>
				<a class="btn btn-block btn-primary text-white" id="send-feedback" target="_blank">Gửi feedback</a>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="translate-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><i class="fa fa-language"></i> Dịch CV</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="translate-modal-body">
				<p><i class="fas fa-circle-notch fa-spin"></i> Đang dịch...</p>
			</div>
		</div>
	</div>
</div>