<?php
use Yii;
?>
<script type="text/javascript">
	//remove loading
	$(document).ready(function() {
		$('.loader-container').fadeOut('fast');
		$('.resume-wrapper').css('filter', 'blur(0px)');
	});

	//range slider
	var rangeSlider = function(){
		var slider = $('.range-slider'),
		range = $('.range-slider__range'),
		value = $('.range-slider__value');

		//init range slider
		slider.each(function(){
			value.each(function(){
				var value = $(this).prev().attr('value');
				$(this).html(value);
			});
			range.on('input', function(){
				$(this).next(value).html(this.value);
			});
		});
	};
	rangeSlider();

	$(document).ready(function() {
		// select2
		$('.select2').select2();
		$(".select2").select2().next().hide();

		$(".select-skill-select2").select2({
		    tags: true,
		}).next().hide();

		//tool tip
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		});

		// hide element
		let cvId = "<?= !empty(Yii::$app->request->get('cv_id')) ? Yii::$app->request->get('cv_id') : '' ?>";
		if (cvId.length <= 0) {
			cvId = "<?= !empty(Yii::$app->request->get('id')) ? Yii::$app->request->get('id') : '' ?>";
		}
		$.ajax({
			url: '/create-resume/hide-elements',
			type: 'POST',
			dataType: 'json',
			data: {cvId: cvId},
		})
		.done(function(response) {
			$('.cv-block').each(function() {
				if ($(this).is(":visible")) {
					let hiddenId = $(this).attr('id');
					let thisBlock = $(this);
					let myTitle = $(this).attr("my-title");
					$.each(response, function(i, item) {
						if (hiddenId == item) {
						thisBlock.hide();
						let action = "<?= Yii::$app->controller->action->id ?>";
						if (action === 'render') {
							thisBlock.remove();
						}
						let hidden_elem = '<li class="hidden-item">' + myTitle + ' <a hidden-id="' + hiddenId + '" title="Khôi phục mục này" href="javascript:void(0)" class="recover-hidden-item-btn"><i class="fas fa-redo"></i></a></li>'
							$('.no-hidden-item').remove();
							$('.hidden-item-list').append(hidden_elem).hide().fadeIn('fast');
						}
					});
				}
			});
		});

		//get language
		let cv_language = $('#language-options').find('.active').find('a').attr('data-language');
		localStorage.setItem("cv_language", cv_language);
	});

	//medium editor
	var elements = document.querySelectorAll('.cv-editable-elem'),
	editor = new MediumEditor(elements, {
		placeholder: {
			text: 'Click để sửa mục này',
			hideOnClick: false,
		},
		toolbar: false,
		keyboardCommands: false,
		disableDoubleReturn: true,
		unwrapTags: ['span']
	});

	//sticky header
	window.onscroll = function() {myFunction()};
	var header = document.getElementById("top-panel");
	if (($('#top-panel').length >= 0)) {
		var sticky = header.offsetTop;	
	}

	function myFunction() {
		if ($('#top-panel').length >= 0) {
			if (window.pageYOffset > sticky) {
				$('#top-panel').addClass('sticky');
			} else {
				$('#top-panel').removeClass('sticky');
			}
		} else {

		}
	}

	//date picker
	$('#dp').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	    startView: 2,
	    language: 'vi'
	}).on('changeDate', function(e) {
		let date = $('#dp').val();
		$('.date-of-birth').text(date);
	});

	//START NAV BAR SCRIPT
	function openNav() {
		document.getElementById("mySidenav").style.width = "700px";
	}

	function closeNav() {
		document.getElementById("mySidenav").style.width = "0";
	}

	const $menu = $('.sidebar-elem');

	$(document).click(e => {
		if (!$menu.is(e.target) && $menu.has(e.target).length === 0) {
			// if the target of the click isn't the container nor a descendant of the container
			closeNav();
		}
	});
	//END NAV BAR SCRIPT
	
	//ask before close window
	// window.onbeforeunload = function (e) {
	//     e = e || window.event;

	//     // For IE and Firefox prior to version 4
	//     if (e) {
	//         e.returnValue = 'Sure?';
	//     }

	//     // For Safari
	//     return 'Sure?';
	// };
	
	// naming variables should not same as cropperPreload glob variable
	var avatarPreviewSelector = '#preview-avatar'; // cv avatar preview selector
	var candidateDetailId = '<?php echo \common\colorgb\HopeEncrypt::encryptString(Yii::$app->user->identity->candidate_id); ?>'; //702358 --> canidate_id
	var adminDomain = '<?php echo Yii::$app->params['qt_url']; ?>';
	createCropper(avatarPreviewSelector, candidateDetailId, adminDomain);

	//unica promote
	if (localStorage.getItem('unica_promote') != 'shown') {
		$('#unica-promote').modal('show');
		$('#unica-alert').show();
		localStorage.setItem('unica_promote','shown');
	}
</script>
