<link rel="stylesheet" href="/cv_template/dist/roboto-gh-pages/roboto.css">
<link rel="stylesheet" href="/cv_template/dist/css/medium-editor.css"> <!-- Core -->
<link rel="stylesheet" href="/cv_template/dist/css/themes/beagle.css"> <!-- or any other theme -->
<link rel="stylesheet" href="/cv_template/assets/css/style.css?v=1.2">
<link rel="stylesheet" href="/cv_template/assets/css/slider.css">
<link rel="stylesheet" href="/cv_template/assets/css/sidebar.css">
<link rel="stylesheet" href="/cv_template/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/cv_template/dist/css/notify-metro.css" rel="stylesheet" />
<link rel="stylesheet" href="/cv_template/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/cv_template/dist/css/cropper.css" rel="stylesheet" />
<link rel="stylesheet" href="/cv_template/assets/css/settings.css" rel="stylesheet" />
<link rel="stylesheet" href="/cv_template/dist/fonts-library/montserrat/montserrat.css">
<link rel="stylesheet" href="/cv_template/dist/fonts-library/roboto-condensed/roboto-condensed.css">

<script type="text/javascript" src="/cv_template/dist/js/jquery.js"></script>
<script src="/cv_template/dist/js/medium-editor.js"></script>
<script src="/cv_template/dist/js/select2.min.js"></script>
<script src="/cv_template/dist/js/bootstrap.bundle.min.js"></script>
<script src="/cv_template/dist/fontawesome/js/all.min.js"></script>
<script src="/cv_template/dist/js/notify.min.js"></script>
<script src="/cv_template/dist/js/notify-metro.js"></script>
<script src="/cv_template/dist/js/notifications.js"></script>
<script src="/cv_template/dist/js/bootstrap-datepicker.min.js"></script>
<script src="/cv_template/dist/js/bootstrap-datepicker.vi.js"></script>
<script src="/cv_template/dist/js/cropper.js"></script>
<script src="/cv_template/dist/js/main.js"></script>
<script src="/cv_template/assets/js/settings.js"></script>
