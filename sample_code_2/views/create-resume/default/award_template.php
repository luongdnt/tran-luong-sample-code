<li class="mb-0 pl-4 mb-2 position-relative cv-child-elem award-item">
    <i class="resume-award-icon fas fa-trophy position-absolute" data-fa-transform="shrink-2"></i>
    <div class="d-flex flex-column flex-md-row">
        <div class=" cv-editable-elem required" info-name="name" info-group="award"><?= Yii::t('cvgo', 'Excellent employees at ...') ?></div>
    	<div class="resume-award-desc ml-auto cv-editable-elem required" info-name="year" info-group="award"><?= date('Y') ?></div>
    </div>
</li>