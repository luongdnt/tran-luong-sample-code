<article class="resume-timeline-item position-relative cv-child-elem history-item">
    <div class="resume-timeline-item-header mb-2">
	    <div class="d-flex flex-column flex-md-row">
	        <h3 class="resume-position-title font-weight-bold mb-1 cv-editable-elem required" data-placeholder="Vị trí/công việc" info-name="job_title" info-group="job_history"><?= Yii::t('cvgo', 'Sale'); ?></h3>
	        <p class="resume-company-name ml-auto cv-editable-elem required" data-placeholder="Tên công ty" info-name="job_company" info-group="job_history"><?= Yii::t('cvgo', '... JSC'); ?></p>
	    </div><!--//row-->
	    <div class="resume-position-time">
	    	<p class="cv-editable-elem d-inline required" data-placeholder="Năm bắt đầu" info-group="job_history" info-name="date_start"><?= date('Y', strtotime("-1 year", time())) ?></p> 
	    	- 
	    	<p class="cv-editable-elem d-inline required" data-placeholder="Năm kết thúc" info-group="job_history" info-name="date_end"><?= date('Y') ?></p>
	    </div>
    </div><!--//resume-timeline-item-header-->
    <div class="resume-timeline-item-desc cv-editable-elem required" data-placeholder="Mô tả công việc" info-name="job_description_html" info-group="job_history">
    	<h4 class="resume-timeline-item-desc-heading font-weight-bold cv-editable-elem"><?= Yii::t('cvgo', 'Mission'); ?>:</h4>
	    <p class="cv-editable-elem">- <?= Yii::t('cvgo', 'Carry out business plans: find customers, introduce products and services, consult products and applications, take care and manage customer relationships.'); ?></p>
		<p class="cv-editable-elem">- <?= Yii::t('cvgo', 'Make annual, quarterly, monthly, and weekly work plans to achieve assigned goals.'); ?></p>
		<p class="cv-editable-elem">- <?= Yii::t('cvgo', 'Survey, study, evaluate expected revenue in the management area. Develop and propose appropriate business plans.') ?></p>
	    <h4 class="resume-timeline-item-desc-heading font-weight-bold cv-editable-elem"><?= Yii::t('cvgo', 'Achievements'); ?>:</h4>
	    <p class="cv-editable-elem"><?= Yii::t('cvgo', 'Through the process of working at ... Joint Stock Company, I have achieved some of the following achievements'); ?>:</p>
	    <ul>
		    <li class="cv-editable-elem"><?= Yii::t('cvgo', '... Best seller') ?> <?= date('Y', strtotime("-1 year", time())) ?></li>
		    <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Excellent staff') ?> <?= date('Y', strtotime("-1 year", time())) ?></li>
		    <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Creative staff') ?></li>
		    <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Excellent sale project') ?></li>
	    </ul>
    </div><!--//resume-timeline-item-desc-->
</article><!--//resume-timeline-item-->