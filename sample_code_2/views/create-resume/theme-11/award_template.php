<div class="item cv-child-elem award-item">
    <div class="row">
        <p class="cv-editable-elem d-inline required col-3 right-border-time-col pr-0"
           data-placeholder="Năm"
           info-group="award"
           info-name="year"><?= date("Y", strtotime("-1 years")) ?></p>
        <p class="col-9 mb-0 cv-editable-elem spec-heading d-inline required"
           data-placeholder="Tên thành tích" info-name="name"
           info-group="award"><?= Yii::t('cvgo', 'Excellent employees at ...') ?></p>
    </div>
</div><!--//item-->