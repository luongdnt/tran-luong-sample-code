<div class="item mt-3 cv-child-elem activity-item mb-2">
    <div class="row">
        <div class="col-4 right-border-time-col text-bold spec-heading pr-0" style="letter-spacing: 0.2rem">
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Từ ngày" info-group="activity"
               info-name="from">02/02/<?= date("Y", strtotime("-1 years")) ?></p>
            -
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Đến ngày" info-group="activity"
               info-name="to">20/02/<?= date("Y", strtotime("-1 years")) ?></p>
        </div>
        <div class="col-8">
            <div class="mb-0 spec-heading text-bold cv-editable-elem required"
                 data-placeholder="Tên công ty/tổ chức/sự kiện"
                 info-name="organization"
                 info-group="activity"><?= Yii::t('cvgo', 'Workshop: Application Design - Challenges & Opportunities') ?>
            </div>
            <div class="item-content mb-0 text-bold spec-heading cv-editable-elem required"
                 data-placeholder="Vị trí/vai trò" info-name="role"
                 info-group="activity">
                <?= Yii::t('cvgo', 'Speakers') ?>
            </div>
        </div>
    </div>
    <div class="item-content cv-editable-elem"
         data-placeholder="Mô tả hoạt động" info-name="description"
         info-group="activity">
        <?= Yii::t('cvgo', 'Share ideas on:') ?>
        <ul>
            <li><?= Yii::t('cvgo', 'Difficulties and challenges in Design') ?></li>
            <li><?= Yii::t('cvgo', 'Requirements to become a Designer') ?></li>
            <li><?= Yii::t('cvgo', 'Job opportunities with terrible salary') ?></li>
            <li><?= Yii::t('cvgo', 'Jobs at Jobs with the Design profession') ?></li>
        </ul>
    </div>
</div><!--//item-->