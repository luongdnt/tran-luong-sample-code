<?php

use common\components\HopeTimeHelper;
use yii\widgets\ActiveForm;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= $title ?></title>

    <!-- Meta -->
    <?= $this->render('../layout/meta') ?>

    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="<?= $activeColor ?>">
    <?= $this->render('../layout/assets') ?>
    <link rel="stylesheet" href="/cv_template/theme_11/css/theme-5.css">
    <link rel="stylesheet" href="/cv_template/theme_11/css/theme-11.css">
    <style>
        .jobsgo-watermark, .font-wrapper {
            font-family: <?= $activeFontFamily ?>;
        }
        .resume-wrapper-inner{
            padding-bottom:0px !important;
            margin-bottom:0px !important;
        }
        .resume-social-list{
            display:none !important;
        }
        .ending-hr{
            display : none;
        }

        <?php if ($activeFontFamily == 'Arial, sans-serif'): ?>
        .resume-name {
            font-weight: 600;
        }

        <?php else: ?>
        .resume-name {
            font-weight: 900;
        }

        <?php endif; ?>
        .resume-header {
            background-image: url("/cv_template/theme_11/images/yellow-banner.png");
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
    </style>
</head>
<body>
<?php if (empty($isRendered)): ?>
    <div class="loader-container">
        <div class="loader"></div>
    </div>
    <?= $this->render('../layout/header') ?>
    <?= $this->render('../layout/sidebar', [
        'theme' => $theme,
        'cv' => $cv
    ]) ?>
<?php endif; ?>
<div class="main-wrapper">
    <div class="container px-3 px-lg-5">
        <article class="resume-wrapper-inner font-wrapper pb-5 mt-5 mb-5 mx-auto theme-bg-light shadow-lg">
            <div class="resume-header p-5">
                <div class="row align-items-center pt-5">
                    <div class="resume-contact col-4 col-sm-4 pr-0 pl-1rem2">
                        <h3 class="resume-name cv-editable-elem mb-0 candidate-name arial-text-font-weight"
                            data-placeholder="Tên"
                            info-group="candidate" info-name="name"><?= $candidate->name ?></h3>
                    </div><!--//resume-contact-->
                    <div class="resume-title col-3 col-sm-3 pl-3 pr-0">
                        <div class="img-box mt-0 ">
                            <div class="img__wrap text-center">
                                <div class="avatar-box">
                                    <?php if (!empty($candidate->avatar)): ?>
                                        <img src="<?= $candidate->avatar ?>"
                                             class="img-fluid picture" id="preview-avatar"/>
                                    <?php else: ?>
                                        <img src="/cv_template/assets/images/upload_icon.png"
                                             class="img-fluid picture" id="preview-avatar"/>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="resume-contact col-5 col-sm-5 pr-4 pl-4rem">
                        <div class="cvo-block-title">
                            <h3 class="cvo-section-heading cvo-heading-40">
                                <?= Yii::t('cvgo', 'Contact') ?>
                            </h3>
                        </div>
                        <div class="social-container">
                            <i class="fa fa-phone contact-icons" aria-hidden="true"></i>
                            <p class="d-inline spec-heading cv-editable-elem"
                               data-placeholder="Phone Number"
                               info-group="candidate"
                               info-name="tel"><?= $candidate->tel ?></p>
                        </div>
                        <div class="social-container">
                            <i class="fa fa-map-marker contact-icons"
                               aria-hidden="true"></i>
                            <p class="d-inline spec-heading cv-editable-elem"
                               data-placeholder="Tỉnh/thành phố"
                               info-group="candidate"
                               info-name="current_address"><?= $candidate->current_address ?></p>
                        </div>
                        <?php if (empty($isRendered)): ?>
                            <i class="fa fa-birthday-cake contact-icons"
                               aria-hidden="true"></i>
                            <input id="dp" class="datepicker" data-date-format="dd/mm/yyyy"
                                   style="display: none;">
                            <p class="date-of-birth spec-heading d-inline" info-group="candidate"
                               info-name="date_of_birth"><?= date('d/m/Y', strtotime($candidate->date_of_birth)) ?></p>
                            <br>
                        <?php else: ?>
                            <?php if ($candidate->date_of_birth != '1970-01-01' && !empty($candidate->date_of_birth)): ?>
                                <i class="fa fa-birthday-cake contact-icons"
                                   aria-hidden="true"></i>
                                <input id="dp" class="datepicker" data-date-format="dd/mm/yyyy"
                                       style="display: none;">
                                <p class="date-of-birth spec-heading d-inline" info-group="candidate"
                                   info-name="date_of_birth"><?= date('d/m/Y', strtotime($candidate->date_of_birth)) ?></p>
                                <br>
                            <?php endif; ?>
                        <?php endif; ?>
                        <div class="social-container">
                            <i class="fa fa-envelope contact-icons"
                               aria-hidden="true"></i>
                            <p class="d-inline spec-heading cv-editable-elem" data-placeholder="Email"
                               info-group="candidate"
                               info-name="email"><?= $candidate->email ?></p>
                        </div>
                    </div><!--//resume-contact-->
                </div><!--//row-->
            </div><!--//resume-header-->
            <div class="resume-body mt-5 ml-5 pl-4 mr-5">
                <div class="row">
                    <div class="cvo-left-col col-5 col-sm-5 col-md-5">
                        <!-- Mục tiêu nghề nghiệp -->
                        <section class="work-section cv-block mb-5 cv-section-event" id="career-summary-block"
                                 my-title="Mục tiêu nghề nghiệp">
                            <div class="cvo-block-title">
                                <h3 class="cvo-section-heading mb-0"><?= Yii::t('cvgo', 'Career goals'); ?></h3>
                            </div>
                            <div class="cvo-block-content cv-editable-elem"
                                 data-placeholder="Mục tiêu nghề nghiệp: ngắn hạn, dài hạn."
                                 info-group="candidate" info-name="short_bio_html">
                                <?php if (empty($candidate->short_bio_html)): ?>
                                    <?= $candidate->short_bio ?>
                                <?php else: ?>
                                    <?= $candidate->short_bio_html ?>
                                <?php endif; ?>
                            </div>
                        </section>
                        <!-- End Mục tiêu nghề nghiệp -->
                        <!-- Chứng Chỉ -->
                        <section class="work-section mb-5 cv-block cv-section-event" id="certificates-block"
                                 my-title="Chứng chỉ">
                            <div class="cvo-block-title">
                                <h3 class="cvo-section-heading cvo-heading-left"><?= Yii::t('cvgo', 'Certificates'); ?></h3>
                            </div>
                            <div class="cvo-block-content">
                                <div class="item">
                                    <ul class="list-unstyled resume-skills-list" id="certificates-container"
                                        block="certificates">
                                        <?php if (!empty($certificates)): ?>
                                            <?php foreach ($certificates as $item): ?>
                                                <li class="cv-child-elem certificates-item">
                                                    <p class="d-none item-id" info-name="id"
                                                       info-group="certificates"><?= $item->id ?></p>
                                                    <div class="row">
                                                        <?php if (empty($isRendered)): ?>
                                                            <p class="cv-editable-elem d-inline required col-3 right-border-time-col pr-0"
                                                               data-placeholder="Năm" info-group="certificates"
                                                               info-name="year"><?= $item->year ?></p>
                                                        <?php else: ?>
                                                            <?php if (!empty($item->year)): ?>
                                                                <p class="cv-editable-elem d-inline required col-3 right-border-time-col pr-0"
                                                                   data-placeholder="Năm" info-group="certificates"
                                                                   info-name="year"><?= $item->year ?></p>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                        <div class="resume-degree spec-heading d-inline cv-editable-elem required col-9"
                                                             data-placeholder="Tên chứng chỉ"
                                                             info-group="certificates"
                                                             info-name="name"><?= $item->name ?></div>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <?php if (empty($isRendered)): ?>
                                                <div class="add-new-btn text-center" id="add-certificates-template"
                                                     data-toggle="tooltip" data-placement="top"
                                                     title="Thêm biểu mẫu">
                                                    <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </ul>
                                </div><!--//item-->
                            </div>
                        </section>
                        <!-- End Chứng Chỉ -->
                        <!-- Giải Thưởng -->
                        <section class="work-section mb-5 cv-block cv-section-event" id="award-block"
                                 my-title="Giải thưởng">
                            <div class="cvo-block-title">
                                <h3 class="cvo-section-heading cvo-heading-left"><?= Yii::t('cvgo', 'Award'); ?></h3>
                            </div>
                            <div class="cvo-block-content">
                                <div block="award" id="award-container">
                                    <?php if (!empty($award)): ?>
                                        <?php foreach ($award as $item): ?>
                                            <div class="item cv-child-elem award-item">
                                                <p class="d-none item-id" info-name="id"
                                                   info-group="award"><?= $item->id ?></p>
                                                <div class="row">
                                                    <?php if (empty($isRendered)): ?>
                                                        <p class="cv-editable-elem d-inline required col-3 right-border-time-col pr-0"
                                                           data-placeholder="Năm"
                                                           info-group="award"
                                                           info-name="year"><?= $item->year ?></p>
                                                    <?php else: ?>
                                                        <?php if (!empty($item->year)): ?>
                                                            <p class="cv-editable-elem d-inline required col-3 right-border-time-col pr-0"
                                                               data-placeholder="Năm"
                                                               info-group="award"
                                                               info-name="year"><?= $item->year ?></p>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    <p class="col-9 mb-0 cv-editable-elem spec-heading d-inline required"
                                                       data-placeholder="Tên thành tích" info-name="name"
                                                       info-group="award"><?= $item->name ?></p>
                                                </div>
                                            </div><!--//item-->
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <?php if (empty($isRendered)): ?>
                                            <div class="add-new-btn text-center" id="add-award-template"
                                                 data-toggle="tooltip"
                                                 data-placement="top" title="Thêm biểu mẫu">
                                                <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </section>
                        <!-- End Giải Thưởng -->
                        <!-- Kỹ năng bản thân -->
                        <section class="work-section mb-5 cv-block cv-section-event mb-5" id="skills-block"
                                 my-title="Kỹ năng bản thân">
                            <div class="cvo-block-title">
                                <h3 class="cvo-section-heading"><?= Yii::t('cvgo', 'Skills'); ?></h3>
                            </div>
                            <div class="cvo-block-content">
                                <div class="item">
                                    <ul class="list-unstyled resume-skills-list" id="list-skill" block="skill">
                                        <?php if (!empty($candidateJobCategory)): ?>
                                            <?php foreach ($candidateJobCategory as $key => $item): ?>
                                                <li class="mb-0 mt-2 cv-child-elem skill-item no-copy-controls"
                                                    myId="<?= $item->category->job_category_id ?>">
                                                    <p class="d-none item-id"
                                                       info-name="candidate_job_category_id"
                                                       info-group="skill"><?= $item->candidate_job_category_id ?></p>
                                                    <p class="d-none item-id" info-name="job_category_id"
                                                       info-group="skill"><?= $item->category->job_category_id ?></p>
                                                    <div class="row">
                                                        <div class="col-3 right-border-year pr-0">
                                                            <span class="d-none" info-name="experiment_duration" info-group="skill"><?= $item->experiment_duration ?></span>
                                                            <?php if ($item->experiment_duration == 0) : ?>
                                                                <span>
                                                                    <span class="exp-year-title"><?= Yii::t('cvgo', 'below') ?> 1</span> 
                                                                    <span class="exp-year-text"><?= Yii::t('cvgo', 'year'); ?></span>
                                                                </span>
                                                            <?php else: ?>
                                                                <span>
                                                                    <span class="exp-year-title"><?= $item->experiment_duration ?></span> 
                                                                    <span class="exp-year-text"><?= Yii::t('cvgo', 'year(s)'); ?></span>
                                                                </span>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-9 resume-skill-name d-inline" info-name="skill_title" info-group="skill">
                                                            <?php if (!empty($item->skill_title)): ?>
                                                                <?= $item->skill_title ?>
                                                            <?php else: ?>    
                                                                <?php if ($cv->language == 'en-US'): ?>
                                                                <?= $item->category->job_category_name_en ?>
                                                                <?php else: ?>
                                                                <?= $item->category->job_category_name_vn ?>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                    <?php if (empty($isRendered)): ?>
                                        <select class="form-control select-personal-skill select-skill-select2"
                                                id="select-personal-skill">
                                            <option></option>
                                            <?php if (!empty($jobCategory)) : ?>
                                                <?php foreach ($jobCategory as $item) : ?>
                                                    <?php if ($item->status >= 0) : ?>
                                                        <?php if ($cv->language == 'en-US'): ?>
                                                        <option value="<?= $item->job_category_id ?>"><?= $item->job_category_name_en ?></option>
                                                        <?php else: ?>
                                                        <option value="<?= $item->job_category_id ?>"><?= $item->job_category_name_vn ?></option>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                        <div class="add-new-btn text-center" id="add-skill-template">
                                            <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                        </div>
                                    <?php endif; ?>
                                </div><!--//item-->
                            </div>
                        </section>
                        <!-- End Kỹ năng bản thân -->
                        <!-- Ngôn Ngữ -->
                        <section class="skills-section cv-block cv-section-event mb-5" id="language-block"
                                 my-title="Ngôn ngữ">
                            <div class="cvo-block-title">
                                <h3 class="cvo-section-heading cvo-heading-left"><?= Yii::t('cvgo', 'Language'); ?></h3>
                            </div>
                            <div class="cvo-block-content">
                                <ul class="list-unstyled resume-skills-list" block="language"
                                    id="language-container">
                                    <?php if (!empty($candidateLanguage)): ?>
                                        <?php foreach ($candidateLanguage as $item): ?>
                                            <li class="mb-0 cv-child-elem language-item no-copy-controls"
                                                myId="<?= $item->language_id ?>">
                                                <p class="d-none item-id" info-name="candidate_language_id"
                                                   info-group="language"><?= $item->candidate_language_id ?></p>
                                                <p class="d-none item-id" info-name="language_id"
                                                   info-group="language"><?= $item->language_id ?></p>
                                                <div class="row">
                                                    <div class="col-12 col-sm-4 pr-0">
                                                        <?php if ($cv->language == 'en-US'): ?>
                                                        <p class="resume-lang-name pb-1" info-group="language" info-name="language_name"><?= $item->language->language_name_en ?></p>
                                                        <?php else: ?>
                                                        <p class="resume-lang-name pb-1" info-group="language" info-name="language_name"><?= $item->language->language_name ?></p>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-12 col-sm-8 pl-0">
                                                        <div class="progress left-print-progress-bar mt-2">
                                                            <div class="progress-bar active-color-bg"
                                                                 role="progressbar"
                                                                 style="width: <?= $item->level ?>%;"
                                                                 aria-valuenow="25"
                                                                 aria-valuemin="0" aria-valuemax="100"><span><span
                                                                            info-group="language"
                                                                            info-name="level"><?= $item->level ?></span>%</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                                <?php if (empty($isRendered)): ?>
                                    <select class="form-control select-skill select2" id="select-language-new">
                                        <option></option>
                                        <?php if (!empty($language)) : ?>
                                            <?php foreach ($language as $item) : ?>
                                                <?php if ($cv->language == 'en-US'): ?>
                                                <option value="<?= $item->language_id ?>"><?= $item->language_name_en ?></option>
                                                <?php else: ?>
                                                <option value="<?= $item->language_id ?>"><?= $item->language_name ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                    <div class="add-new-btn text-center" id="add-language-template">
                                        <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </section>
                        <!-- End Ngôn Ngữ -->
                        <!-- Kỹ năng mềm -->
                        <section class="skills-section cv-block cv-section-event mb-5" id="soft-skill-block"
                                 my-title="Kỹ năng mềm">
                            <div class="cvo-block-title">
                                <h3 class="cvo-section-heading cvo-heading-left"><?= Yii::t('cvgo', 'Soft skills'); ?></h3>
                            </div>
                            <div class="cvo-block-content">
                                <div class="item">
                                    <ul class="list-unstyled resume-skills-list" id="list-soft-skill"
                                        block="soft-skill">
                                        <?php if (!empty($candidateSoftSkill)): ?>
                                            <?php foreach ($candidateSoftSkill as $item): ?>
                                                <li class="mb-0 cv-child-elem soft-skill-item no-copy-controls"
                                                    myId="<?= $item->soft_skill_id ?>">
                                                    <p class="d-none item-id" info-name="candidate_soft_skill_id"
                                                       info-group="soft_skill"><?= $item->candidate_soft_skill_id ?></p>
                                                    <p class="d-none item-id" info-name="soft_skill_id"
                                                       info-group="soft_skill"><?= $item->soft_skill_id ?></p>
                                                    <div class="row">
                                                        <div class="col-12 col-sm-4">
                                                            <?php if ($cv->language == 'en-US'): ?>
                                                            <div class="resume-skill-name pb-1" info-group="soft_skill"
                                                                 info-name="soft_skill_name"><?= $item->softskill->soft_skill_name_eng ?></div>
                                                            <?php else: ?>
                                                            <div class="resume-skill-name pb-1" info-group="soft_skill"
                                                                 info-name="soft_skill_name"><?= $item->softskill->soft_skill_name ?></div>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-12 col-sm-8 pl-0">
                                                            <div class="progress resume-progress left-print-progress-bar mt-2">
                                                                <div class="progress-bar soft-skill-progress-bar theme-progress-bar-dark"
                                                                     role="progressbar"
                                                                     style="width: <?= $item->level ?>%"
                                                                     aria-valuenow="25" aria-valuemin="0"
                                                                     aria-valuemax="100"
                                                                     value="<?= $item->level ?>">
                                                                        <span>
                                                                            <span
                                                                                    info-group="soft_skill"
                                                                                    info-name="level"><?= $item->level ?>
                                                                            </span>%
                                                                        </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                    <?php if (empty($isRendered)): ?>
                                        <select class="form-control select-soft-skill-new select2"
                                                id="select-soft-skill-new">
                                            <option></option>
                                            <?php if (!empty($softSkill)) : ?>
                                                <?php foreach ($softSkill as $item) : ?>
                                                    <?php if ($cv->language == 'en-US'): ?>
                                                    <option value="<?= $item->soft_skill_id ?>"><?= $item->soft_skill_name_eng ?></option>
                                                    <?php else: ?>
                                                    <option value="<?= $item->soft_skill_id ?>"><?= $item->soft_skill_name ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                        <div class="add-new-btn text-center" id="add-soft-skill-template">
                                            <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                        </div>
                                    <?php endif; ?>
                                </div><!--//item-->
                            </div>
                        </section>
                        <!-- End Kỹ năng mềm -->
                        <!-- Hobby -->
                        <section class="skills-section cv-block cv-section-event mb-1" id="interests-block"
                                 my-title="Sở thích">
                            <div class="cvo-block-title">
                                <h3 class="cvo-section-heading cvo-heading-left"><?= Yii::t('cvgo', 'Hobby'); ?></h3>
                            </div>
                            <div class="cv-block-content">
                                <div class="item">
                                    <ul class="list-unstyled resume-interests-list" block="hobby"
                                        id="hobby-container">
                                        <?php if (!empty($candidateHobby)): ?>
                                            <div class="row">
                                                <?php foreach ($candidateHobby as $key => $item): ?>
                                                    <div class="col-sm-6 col-md-6 hobby-list">
                                                        <li class="mb-0 mt-2 cv-child-elem hobby-item only-remove-controls"
                                                            myId="<?= $item->hobby_id ?>">
                                                            <p class="d-none item-id" info-name="candidate_hobby_id"
                                                               info-group="hobby"><?= $item->candidate_hobby_id ?></p>
                                                            <p class="d-none item-id" info-name="hobby_id"
                                                               info-group="hobby"><?= $item->hobby_id ?></p>
                                                            <?php if ($cv->language == 'en-US'): ?>
                                                            <p class="resume-lang-name" info-group="hobby" info-name="hobby_name"><?= $item->hobby->hobby_name_en ?></p>
                                                            <?php else: ?>
                                                            <p class="resume-lang-name" info-group="hobby" info-name="hobby_name"><?= $item->hobby->hobby_name ?></p>
                                                            <?php endif; ?>
                                                        </li>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        <?php endif; ?>
                                    </ul>
                                    <?php if (empty($isRendered)): ?>
                                        <select class="form-control select-skill select2" id="select-hobby-new">
                                            <option></option>
                                            <?php if (!empty($hobby)) : ?>
                                                <?php foreach ($hobby as $item) : ?>
                                                    <?php if ($cv->language == 'en-US'): ?>
                                                    <option value="<?= $item->hobby_id ?>"><?= $item->hobby_name_en ?></option>
                                                    <?php else: ?>
                                                    <option value="<?= $item->hobby_id ?>"><?= $item->hobby_name ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                        <div class="add-new-btn text-center" id="add-hobby-template">
                                            <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </section><!--// hobby-section-->
                        <!-- End Hobby -->
                    </div>
                    <div class="cvo-right-col col-7 col-sm-7 col-md-7 pr-4">
                        <!-- Học vấn -->
                        <section class="skills-section cv-block cv-section-event mb-4" id="education-block"
                                 my-title="Học vấn">
                            <div class="cvo-block-title">
                                <h3 class="cvo-section-heading cvo-heading-left"><?= Yii::t('cvgo', 'Education'); ?></h3>
                            </div>
                            <div class="cvo-block-content">
                                <div id="education-container" block="education">
                                    <?php if (!empty($education)): ?>
                                        <?php foreach ($education as $item): ?>
                                            <div class="item cv-child-elem education-item">
                                                <p class="d-none item-id" info-name="candidate_education_id"
                                                   info-group="education"><?= $item->candidate_education_id ?></p>
                                                <div class="row">
                                                    <div class="col-4 right-border-time-col spec-heading text-bold pr-0">
                                                        <?php if (empty($isRendered)): ?>
                                                            <p class="cv-editable-elem d-inline required"
                                                               data-placeholder="Năm bắt đầu" info-group="education"
                                                               info-name="date_start"><?= $item->date_start ?></p>
                                                            -
                                                            <p class="cv-editable-elem d-inline required"
                                                               data-placeholder="Năm kết thúc" info-group="education"
                                                               info-name="date_end"><?= $item->date_end ?></p>
                                                        <?php else: ?>
                                                            <?php if (!empty($item->date_start)): ?>
                                                                <p class="cv-editable-elem d-inline required"
                                                                   data-placeholder="Năm bắt đầu" info-group="education"
                                                                   info-name="date_start"><?= $item->date_start ?></p>
                                                            <?php endif; ?>
                                                            <?php if (!empty($item->date_start) && !empty($item->date_end)): ?>
                                                                -
                                                            <?php endif; ?>
                                                            <?php if (!empty($item->date_end)): ?>
                                                                <p class="cv-editable-elem d-inline required"
                                                                   data-placeholder="Năm kết thúc"
                                                                   info-group="education"
                                                                   info-name="date_end"><?= $item->date_end ?></p>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-8">
                                                        <div class="item-content text-bold spec-heading cv-editable-elem required"
                                                             data-placeholder="Tên trường" info-name="school_name"
                                                             info-group="education">
                                                            <?= $item->school_name ?>
                                                        </div>
                                                        <div class="mb-0 text-bold spec-heading cv-editable-elem required"
                                                             data-placeholder="Chuyên ngành"
                                                             info-name="specialization"
                                                             info-group="education"><?= $item->specialization ?></div>
                                                    </div>
                                                </div>
                                                <div class="item-content cv-editable-elem mb-3"
                                                     data-placeholder="Mô tả học vấn" info-name="hightlight"
                                                     info-group="education">
                                                    <?= $item->hightlight ?>
                                                </div>
                                            </div><!--//item-->
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <?php if (empty($isRendered)): ?>
                                            <div class="add-new-btn text-center" id="add-education-template"
                                                 data-toggle="tooltip"
                                                 data-placement="top" title="Thêm biểu mẫu">
                                                <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </section>
                        <!-- End Học vấn -->
                        <!-- Kinh nghiệm làm việc -->
                        <section class="work-section cv-block cv-section-event mb-5" id="experience-block"
                                 my-title="Kinh nghiệm làm việc">
                            <div class="cvo-block-title">
                                <h3 class="cvo-section-heading">
                                    <div class="arrow-right"><?= Yii::t('cvgo', 'Work experience'); ?></div>
                                </h3>
                            </div>
                            <div class="cvo-block-content">
                                <div id="job-history-container" block="job-history">
                                    <?php if (!empty($jobHistory)): ?>
                                        <?php foreach ($jobHistory as $item): ?>
                                            <div class="item mb-0 cv-child-elem history-item">
                                                <p class="d-none item-id" info-name="candidate_job_history_id"
                                                   info-group="job_history"><?= $item->candidate_job_history_id ?></p>
                                                <div class="row">
                                                    <div class="col-4 right-border-time-col text-bold spec-heading pr-0">
                                                        <?php if (empty($isRendered)): ?>
                                                            <p class="cv-editable-elem d-inline required"
                                                               data-placeholder="Năm bắt đầu" info-group="job_history"
                                                               info-name="date_start"><?= $item->date_start ?></p>
                                                            -
                                                            <p class="cv-editable-elem d-inline required"
                                                               data-placeholder="Năm kết thúc" info-group="job_history"
                                                               info-name="date_end"><?= $item->date_end ?></p>
                                                        <?php else: ?>
                                                            <?php if (!empty($item->date_start)): ?>
                                                                <p class="cv-editable-elem d-inline required"
                                                                   data-placeholder="Năm bắt đầu"
                                                                   info-group="job_history"
                                                                   info-name="date_start"><?= $item->date_start ?></p>
                                                            <?php endif; ?>
                                                            <?php if (!empty($item->date_start) && !empty($item->date_end)): ?>
                                                                -
                                                            <?php endif; ?>
                                                            <?php if (!empty($item->date_end)): ?>
                                                                <p class="cv-editable-elem d-inline required"
                                                                   data-placeholder="Năm kết thúc"
                                                                   info-group="job_history"
                                                                   info-name="date_end"><?= $item->date_end ?></p>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-8">
                                                        <div class="cv-editable-elem text-bold spec-heading required"
                                                             data-placeholder="Tên công ty"
                                                             info-group="job_history"
                                                             info-name="job_company"><?= $item->job_company ?></div>
                                                        <div class="item-title text-bold spec-heading mb-md-0 cv-editable-elem required"
                                                             data-placeholder="Vị trí/công việc" info-name="job_title"
                                                             info-group="job_history"><?= $item->job_title ?></div>
                                                    </div>
                                                </div>
                                                <div class="item-content cv-editable-elem required"
                                                     data-placeholder="Mô tả công việc"
                                                     info-name="job_description_html"
                                                     info-group="job_history">
                                                    <?php if (empty($item->job_description_html)): ?>
                                                        <?= $item->job_description ?>
                                                    <?php else: ?>
                                                        <?= $item->job_description_html ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div><!--//item-->
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <?php if (empty($isRendered)): ?>
                                            <div class="add-new-btn text-center" id="add-job-history-template"
                                                 data-toggle="tooltip"
                                                 data-placement="top" title="Thêm biểu mẫu">
                                                <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </section>
                        <!-- End kinh nghiệm làm việc -->
                        <!-- Hoạt động -->
                        <section class="work-section mb-5 cv-block cv-section-event" id="activity-block"
                                 my-title="Hoạt động">
                            <div class="cvo-block-title">
                                <h3 class="cvo-section-heading cvo-heading-left">
                                    <div class="arrow-right"><?= Yii::t('cvgo', 'Activity'); ?></div>
                                </h3>
                            </div>
                            <div class="cvo-block-content">
                                <div block="activity" id="activity-container">
                                    <?php if (!empty($activity)): ?>
                                        <?php foreach ($activity as $item): ?>
                                            <div class="item mt-3 cv-child-elem activity-item mb-2">
                                                <p class="d-none item-id" info-name="id"
                                                   info-group="activity"><?= $item->id ?></p>
                                                <div class="row">
                                                    <div class="col-4 right-border-time-col text-bold spec-heading pr-0"
                                                         style="letter-spacing: 0.2rem">
                                                        <?php if (empty($isRendered)): ?>
                                                            <p class="cv-editable-elem d-inline required"
                                                               data-placeholder="Từ ngày" info-group="activity"
                                                               info-name="from"><?= $item->from ?></p>
                                                            -
                                                            <p class="cv-editable-elem d-inline required"
                                                               data-placeholder="Đến ngày" info-group="activity"
                                                               info-name="to"><?= $item->to ?></p>
                                                        <?php else: ?>
                                                            <?php if (!empty($item->from)): ?>
                                                                <p class="cv-editable-elem d-inline required"
                                                                   data-placeholder="Từ ngày" info-group="activity"
                                                                   info-name="from"><?= $item->from ?></p>
                                                            <?php endif; ?>
                                                            <?php if (!empty($item->from) && !empty($item->to)): ?>
                                                                -
                                                            <?php endif; ?>
                                                            <?php if (!empty($item->to)): ?>
                                                                <p class="cv-editable-elem d-inline required"
                                                                   data-placeholder="Đến ngày" info-group="activity"
                                                                   info-name="to"><?= $item->to ?></p>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-8">
                                                        <div class="mb-0 spec-heading text-bold cv-editable-elem required"
                                                             data-placeholder="Tên công ty/tổ chức/sự kiện"
                                                             info-name="organization"
                                                             info-group="activity"><?= $item->organization ?></div>
                                                        <div class="item-content mb-0 text-bold spec-heading cv-editable-elem required"
                                                             data-placeholder="Vị trí/vai trò" info-name="role"
                                                             info-group="activity">
                                                            <?= $item->role ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-content cv-editable-elem"
                                                     data-placeholder="Mô tả hoạt động" info-name="description"
                                                     info-group="activity">
                                                    <?= $item->description ?>
                                                </div>
                                            </div><!--//item-->
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <?php if (empty($isRendered)): ?>
                                            <div class="add-new-btn text-center" id="add-activity-template"
                                                 data-toggle="tooltip"
                                                 data-placement="top" title="Thêm biểu mẫu">
                                                <i class="fas fa-plus"></i> <?= Yii::t('cvgo', 'Create new'); ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </section>
                        <!-- End Hoạt động -->
                        <!-- Người tham chiếu -->
                        <section class="resume-section reference-section mb-5 cv-block cv-section-event"
                                 id="references-block"
                                 my-title="Người tham chiếu">
                            <div class="cvo-block-title">
                                <h3 class="cvo-section-heading">
                                    <div class="arrow-right"><?= Yii::t('cvgo', 'References'); ?></div>
                                </h3>
                            </div>
                            <div class="cv-block-content">
                                <div class="item">
                                    <ul class="list-unstyled resume-skills-list" block="references"
                                        id="references-container">
                                        <?php if (!empty($references)): ?>
                                            <?php foreach ($references as $item): ?>
                                                <li class="mb-0 mt-2 cv-child-elem references-item">
                                                    <p class="d-none item-id" info-name="id"
                                                       info-group="references"><?= $item->id ?></p>
                                                    <div class="cv-editable-elem required"
                                                         data-placeholder="Người tham chiếu" info-name="references_text"
                                                         info-group="references"><?= $item->references_text ?></div>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <?php if (empty($isRendered)): ?>
                                                <div class="add-new-btn text-center" id="add-references-template"
                                                     data-toggle="tooltip" data-placement="top" title="Thêm biểu mẫu">
                                                    <i class="fas fa-plus"></i>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </section><!--//references-section-->
                        <!-- End Người tham chiếu -->
                    </div>
                </div>
            </div><!--//resume-body-->
            <hr class="ending-hr">
            <div class="resume-footer text-center">
                <ul class="resume-social-list list-inline mx-auto mb-0 d-inline-block text-muted">
                    <li class="list-inline-item mb-lg-0 mr-3 social-container">
                        <i class="fab fa-facebook-f fa-2x mr-2" data-fa-transform="down-4"></i>
                        <span class="d-lg-inline-block text-muted">
								<p class="cv-editable-elem d-inline" data-placeholder="fb.com/username"
                                   info-group="candidate" info-name="facebook"><?= $candidate->facebook ?></p>
							</span>
                    </li>
                    <li class="list-inline-item mb-lg-0 mr-3 social-container">
                        <i class="fab fa-linkedin fa-2x mr-2" data-fa-transform="down-4"></i>
                        <span class="d-lg-inline-block text-muted">
								<p class="cv-editable-elem d-inline" data-placeholder="linkedin.com/in/username"
                                   info-group="candidate" info-name="linkedin"><?= $candidate->linkedin ?></p>
							</span>
                    </li>
                    <li class="list-inline-item mb-lg-0 mr-lg-3 social-container">
                        <i class="fab fa-skype fa-2x mr-2" data-fa-transform="down-4"></i>
                        <span class="d-lg-inline-block text-muted">
								<p class="cv-editable-elem d-inline" data-placeholder="live:skype_username"
                                   info-group="candidate" info-name="skype"><?= $candidate->skype ?></p>
							</span>
                    </li>
                </ul>
            </div><!--//resume-footer-->
        </article>
    </div>
    <?php if (empty($isRendered)): ?>
        <?= $this->render('../layout/hidden_list') ?>
        <?= $this->render('../layout/modal') ?>
    <?php else: ?>
        <script type="text/javascript" src="/cv_template/assets/js/render.js"></script>
        <script type="text/javascript" src="/cv_template/assets/js/print.js"></script>
        <script>            
            $(document).ready(function(){      
                // header height
                redundantHeaderHeight = 0;
                // mistery 
                adjustHeight = 0;
                fillBlankRenderPage('resume-wrapper-inner', 'resume-wrapper-inner',redundantHeaderHeight,adjustHeight);
            });
        </script>
    <?php endif; ?>
    <?= $this->render('../layout/footer', [
        'theme' => $theme,
        'cv' => $cv,
        'color' => $color,
        'activeColor' => $activeColor,
        'fontFamily' => $fontFamily,
        'activeFontFamily' => $activeFontFamily,
        'isRendered' => $isRendered
    ]) ?>
    <?= $this->render('../layout/init_js', ['candidateId' => $candidate->candidate_id]) ?>
    <script type="text/javascript" src="/cv_template/assets/js/script.js"></script>
    <!-- try this if you want to customize skill,soft-skill, language adding layout -->
    <script type="text/javascript" src="/cv_template/theme_11/js/customize.js"></script>
</div>
</body>
</html> 

