<div class="item mb-0 cv-child-elem history-item">
    <div class="row">
        <div class="col-4 right-border-time-col text-bold spec-heading pr-0">
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Năm bắt đầu" info-group="job_history"
               info-name="date_start"><?= date('m/Y', strtotime('-1 years')) ?></p>
            -
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Năm kết thúc" info-group="job_history"
               info-name="date_end"><?= date('m/Y') ?></p>
        </div>
        <div class="col-8">
            <div class="cv-editable-elem text-bold spec-heading required"
                 data-placeholder="Tên công ty"
                 info-group="job_history"
                 info-name="job_company"><?= Yii::t('cvgo', '... JSC') ?>
            </div>
            <div class="item-title text-bold spec-heading mb-md-0 cv-editable-elem required"
                 data-placeholder="Vị trí/công việc" info-name="job_title"
                 info-group="job_history"><?= Yii::t('cvgo', 'Design specialist') ?>
            </div>
        </div>
    </div>
    <div class="item-content cv-editable-elem required"
         data-placeholder="Mô tả công việc"
         info-name="job_description_html"
         info-group="job_history">
        <?= Yii::t('cvgo', 'Main job:') ?>
        <ul>
            <li><?= Yii::t('cvgo', 'Outline design products in necessary cases') ?></li>
            <li><?= Yii::t('cvgo', 'Research and develop products based on complex design briefs') ?></li>
            <li><?= Yii::t('cvgo', 'Update and upgrade your portfolio to get ready for the next project') ?></li>
            <li><?= Yii::t('cvgo', 'Design products for online advertising and marketing communication channels such as banner, cover photo, flash animation, infographic, email marketing, ...') ?></li>
            <li><?= Yii::t('cvgo', 'Design products for offline communication such as backdrop, standee, banner, invitation, voucher, bandroll, leaflet, brochure, office decoration banners, hanging pictures, etc.') ?></li>
            <li><?= Yii::t('cvgo', 'Create animation videos and sub videos for advertising') ?></li>
            <li><?= Yii::t('cvgo', 'Edit photos for the company after the event') ?></li>
            <li><?= Yii::t('cvgo', 'Perform design of online and offline registration forms') ?></li>
            <li><?= Yii::t('cvgo', 'Design slides, brochures introducing products and services for companies and businesses') ?></li>
            <li><?= Yii::t('cvgo', 'Contribute design ideas for Marketing events and campaigns of the Company') ?></li>
            <li><?= Yii::t('cvgo', 'Perform additional tasks assigned by the Head and Board of Directors') ?></li>
            <li><?= Yii::t('cvgo', 'Coordinate support work with other departments such as human resources, business, communications, marketing, technology, ...') ?></li>
        </ul>
    </div>
</div><!--//item-->