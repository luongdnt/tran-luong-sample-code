<li class="cv-child-elem certificates-item">
    <div class="row">
        <p class="cv-editable-elem d-inline required col-3 right-border-time-col pr-0"
           data-placeholder="Năm" info-group="certificates"
           info-name="year"><?= date('Y', strtotime("-1 year", time())) ?></p>
        <div class="resume-degree spec-heading d-inline cv-editable-elem required col-9"
             data-placeholder="Tên chứng chỉ"
             info-group="certificates"
             info-name="name"><?= Yii::t('cvgo', 'IELTS 7.0') ?>
        </div>
    </div>
</li>