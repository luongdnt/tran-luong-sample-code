<div class="item cv-child-elem award-item">
    <div class="row">
        <p class="resume-award-name col-sm-8 col-md-8 cv-editable-elem d-inline required"
           data-placeholder="Tên thành tích" info-name="name"
           info-group="award"><?= Yii::t('cvgo', 'Excellent employees at ...') ?></p>
        <p class="cv-editable-elem d-inline required col-sm-4 col-md-4 text-right pr-2"
           data-placeholder="Năm"
           info-group="award"
           info-name="year"><?= date("Y", strtotime("-1 years")) ?></p>
    </div>
</div><!--//item-->