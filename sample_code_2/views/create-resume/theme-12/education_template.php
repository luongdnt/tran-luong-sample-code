<div class="item cv-child-elem education-item">
    <div class="row">
        <div class="col-sm-8 col-md-8 spec-heading text-bold">
            <div class="item-content text-bold spec-heading cv-editable-elem required"
                 data-placeholder="Tên trường" info-name="school_name"
                 info-group="education">
                <?= Yii::t('cvgo', '... University'); ?>
            </div>
            <div class="mb-0 text-bold-50 spec-heading cv-editable-elem required"
                 data-placeholder="Chuyên ngành"
                 info-name="specialization"
                 info-group="education"><?= Yii::t('cvgo', 'Business Administration') ?>
            </div>
        </div>
        <div class="col-sm-4 col-md-4 pr-2 text-right">
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Năm bắt đầu" info-group="education"
               info-name="date_start"><?= date('m/Y') ?></p>
            -
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Năm kết thúc" info-group="education"
               info-name="date_end"><?= date('m/Y') ?></p>
        </div>
    </div>
    <div class="item-content cv-editable-elem mb-3"
         data-placeholder="Mô tả học vấn" info-name="hightlight"
         info-group="education">
        <?= Yii::t('cvgo', 'Good graduate'); ?>
    </div>
</div><!--//item-->