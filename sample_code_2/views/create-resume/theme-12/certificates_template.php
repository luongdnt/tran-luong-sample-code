<li class="cv-child-elem certificates-item">
    <div class="row">
        <div class="resume-degree spec-heading d-inline cv-editable-elem required col-sm-8 col-md-8"
             data-placeholder="Tên chứng chỉ"
             info-group="certificates"
             info-name="name"><?= Yii::t('cvgo', 'IELTS 7.0') ?>
        </div>
        <p class="cv-editable-elem d-inline required col-sm-4 col-md-4 text-right pr-2"
           data-placeholder="Năm" info-group="certificates"
           info-name="year"><?= date('Y', strtotime("-1 year", time())) ?></p>
    </div>
</li>