<div class="item mb-0 cv-child-elem history-item">
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <div class="cv-editable-elem text-bold spec-heading required"
                 data-placeholder="Tên công ty"
                 info-group="job_history"
                 info-name="job_company"><?= Yii::t('cvgo', '... JSC') ?>
            </div>
            <div class="item-title text-bold-50 spec-heading mb-md-0 cv-editable-elem required"
                 data-placeholder="Vị trí/công việc" info-name="job_title"
                 info-group="job_history"><?= Yii::t('cvgo', 'Expert of Digital Sale') ?>
            </div>
        </div>
        <div class="col-sm-4 col-md-4 text-right pr-2">
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Năm bắt đầu" info-group="job_history"
               info-name="date_start"><?= date('m/Y', strtotime('-1 years')) ?></p>
            -
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Năm kết thúc" info-group="job_history"
               info-name="date_end"><?= date('m/Y') ?></p>
        </div>
    </div>
    <div class="item-content cv-editable-elem required"
         data-placeholder="Mô tả công việc"
         info-name="job_description_html"
         info-group="job_history">
        <ul>
            <li><?= Yii::t('cvgo', 'Master the information about the products and services the company offers') ?></li>
            <li><?= Yii::t('cvgo', 'Find potential customers: Meet typing or Call contact to introduce customers about products and services, capture the need for advice, give customers a trial of the product, help customers access the products they are buying') ?></li>
            <li><?= Yii::t('cvgo', 'Quotation and price negotiation, negotiation of sales contracts, payment term agreements and delivery') ?></li>
            <li><?= Yii::t('cvgo', 'Inventory: Submit daily sales invoices. Inventory of business support equipment') ?></li>
            <li><?= Yii::t('cvgo', 'Submit business reports to superiors') ?></li>
        </ul>
    </div>
</div><!--//item-->