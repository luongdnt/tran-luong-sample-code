<div class="item cv-child-elem activity-item mb-1">
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <div class="mb-0 spec-heading text-bold cv-editable-elem required"
                 data-placeholder="Tên công ty/tổ chức/sự kiện"
                 info-name="organization"
                 info-group="activity"><?= Yii::t('cvgo', 'Workshop: "Skills for Sales Leader - Methods to lead Sales team to reach sales milestones"') ?>
            </div>
            <div class="item-content mb-0 text-bold-50 spec-heading cv-editable-elem required"
                 data-placeholder="Vị trí/vai trò" info-name="role"
                 info-group="activity">
                <?= Yii::t('cvgo', 'Digital Sales Coach') ?>
            </div>
        </div>
        <div class="col-sm-4 col-md-4 text-right pr-2">
            <p class="cv-editable-elem required"
               data-placeholder="Từ ngày" info-group="activity"
               info-name="from">02/02/<?= date("Y", strtotime("-1 years")) ?></p>
            đến
            <p class="cv-editable-elem required"
               data-placeholder="Đến ngày" info-group="activity"
               info-name="to">20/02/<?= date("Y", strtotime("-1 years")) ?></p>
        </div>
    </div>
    <div class="item-content cv-editable-elem"
         data-placeholder="Mô tả hoạt động" info-name="description"
         info-group="activity">
        <ul>
            <li><?= Yii::t('cvgo', 'Divide the target revenue exactly for the Sales team') ?></li>
            <li><?= Yii::t('cvgo', 'The secret to building an effective Sales Peg script') ?></li>
            <li><?= Yii::t('cvgo', 'The method of training the Sales team helps to increase the closing rate of orders') ?></li>
            <li><?= Yii::t('cvgo', 'Q&A: Answering the problems of Sales Leader') ?></li>
        </ul>
    </div>
</div><!--//item-->