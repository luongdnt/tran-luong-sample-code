<div class="item mt-3 cv-child-elem education-item">
    <div class="row">
        <div class="col-12 col-md-3 col-lg-3 col-xl-3">
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Năm bắt đầu"
               info-group="education"
               info-name="date_start"><?= date('Y', strtotime("-1 year", time())) ?></p>
            -
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Năm kết thúc" info-group="education"
               info-name="date_end"><?= date('Y') ?></p>
        </div>
        <div class="col-12 col-md-9 col-lg-9 col-xl-9">
            <h4 class="item-content m-0 cv-editable-elem required"
                data-placeholder="Tên trường" info-name="school_name"
                info-group="education">
                <?= Yii::t('cvgo', '... University'); ?>
            </h4>
            <div class=" mb-0 cv-editable-elem required"
                 data-placeholder="Vị trí/công việc" info-name="specialization"
                 info-group="education"><?= Yii::t('cvgo', 'Information Technology'); ?></div>
            <div class="item-content cv-editable-elem"
                 data-placeholder="Mô tả học vấn" info-name="hightlight"
                 info-group="education">
                <?= Yii::t('cvgo', 'Good graduate'); ?>
            </div>
        </div>
    </div>
    <hr style="color: grey;">
</div><!--//item-->