<div class="item mt-3 cv-child-elem award-item">
    <div class="row">
        <div class="col-12 col-md-3 col-lg-3 col-xl-3">
            <p class="cv-editable-elem d-inline required" data-placeholder="Năm"
               info-group="award" info-name="year"><?= date('Y') ?></p>
        </div>
        <div class="col-12 col-md-9 col-lg-9 col-xl-9">
            <p class="mb-0 cv-editable-elem required"
               data-placeholder="Tên thành tích" info-name="name"
               info-group="award"><?= Yii::t('cvgo', 'Excellent employees at ...') ?></p>
        </div>
    </div>
    <hr style="color: grey;">
</div><!--//item-->