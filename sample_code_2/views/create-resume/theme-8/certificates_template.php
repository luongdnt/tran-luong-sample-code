<li class="mb-0 mt-3 cv-child-elem certificates-item">
    <div class="row">
        <div class="col-12 col-md-3 col-lg-3 col-xl-3">
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Năm bắt đầu"
               info-group="certificates"
               info-name="year"><?= date('Y') ?></p>
        </div>
        <div class="col-12 col-md-9 col-lg-9 col-xl-9">
            <div class="resume-degree cv-editable-elem required "
                 data-placeholder="Chuyên ngành" info-group="certificates"
                 info-name="name"><?= Yii::t('cvgo', 'IELTS 7.0') ?></div>
        </div>
    </div>
    <hr style="color: grey;">
</li>