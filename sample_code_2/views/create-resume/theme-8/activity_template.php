<div class="item mt-3 cv-child-elem activity-item mb-2">
    <div class="row">
        <div class="col-12 col-md-3 col-lg-3 col-xl-3">
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Từ ngày"
               info-group="activity" info-name="from"><?= date("d/m/Y") ?></p>
            -
            <p class="cv-editable-elem d-inline required"
               data-placeholder="Đến ngày"
               info-group="activity" info-name="to"><?= date("d/m/Y") ?></p>
        </div>
        <div class="col-12 col-md-9 col-lg-9 col-xl-9">
            <h4 class="mb-0 cv-editable-elem required"
                data-placeholder="Tên công ty/tổ chức/sự kiện"
                info-name="organization"
                info-group="activity"><?= Yii::t('cvgo', '... Job Fair') ?></h4>
            <div class="item-content cv-editable-elem required"
                 data-placeholder="Vị trí/vai trò" info-name="role"
                 info-group="activity">
                <?= Yii::t('cvgo', 'Recruitment consultants') ?>
            </div>
            <div class="item-content cv-editable-elem"
                 data-placeholder="Mô tả hoạt động" info-name="description"
                 info-group="activity">
                <?= Yii::t('cvgo', 'Advice and share job opportunities for final year students at ... University') ?>
            </div>
        </div>
    </div>
    <hr style="color: grey;">
</div><!--//item-->