<div class="item cv-child-elem award-item">
    <div class="meta">
        <div class="upper-row">
            <h3 class="job-title cv-editable-elem required" data-placeholder="Tên thành tích" info-name="name" info-group="award"><?= Yii::t('cvgo', 'Excellent employees at ...') ?></h3>
            <div class="time">
                <p class="cv-editable-elem d-inline required" data-placeholder="Năm" info-group="award" info-name="year"><?= date('Y') ?></p>
            </div>
        </div><!--//upper-row-->
    </div><!--//meta-->
</div><!--//item-->