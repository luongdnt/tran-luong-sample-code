<div class="item cv-child-elem education-item">
    <div class="meta">
        <div class="upper-row">
            <h3 class="job-title cv-editable-elem required" data-placeholder="Chuyên ngành" info-name="specialization" info-group="education"><?= Yii::t('cvgo', 'Business Administration'); ?></h3>
            <div class="time">
                <p class="cv-editable-elem d-inline required" data-placeholder="Năm bắt đầu" info-group="education" info-name="date_start"><?= date('Y', strtotime("-1 year", time())) ?></p>
                - 
                <p class="cv-editable-elem d-inline required" data-placeholder="Năm kết thúc" info-group="education" info-name="date_end"><?= date('Y') ?></p>
            </div>
        </div><!--//upper-row-->
        <div class="company cv-editable-elem required" data-placeholder="Tên trường" info-name="school_name" info-group="education"><?= Yii::t('cvgo', '... University'); ?></div>
        <div class="company cv-editable-elem" data-placeholder="Tên trường" info-name="hightlight" info-group="education"><?= Yii::t('cvgo', 'Good graduate'); ?></div>
    </div><!--//meta-->
</div><!--//item-->