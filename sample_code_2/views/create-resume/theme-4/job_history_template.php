<div class="item cv-child-elem history-item">
    <div class="meta">
        <div class="upper-row">
            <h3 class="job-title cv-editable-elem required" data-placeholder="Vị trí/công việc" info-name="job_title" info-group="job_history"><?= Yii::t('cvgo', 'Sale'); ?></h3>
            <div class="time">
                <p class="cv-editable-elem d-inline required" data-placeholder="Năm bắt đầu" info-group="job_history" info-name="date_start"><?= date('Y', strtotime("-1 year", time())) ?></p>
                - 
                <p class="cv-editable-elem d-inline required" data-placeholder="Năm kết thúc" info-group="job_history" info-name="date_end"><?= date('Y') ?></p>
            </div>
        </div><!--//upper-row-->
        <div class="company cv-editable-elem required" data-placeholder="Tên công ty" info-name="job_company" info-group="job_history"><?= Yii::t('cvgo', '... JSC'); ?></div>
    </div><!--//meta-->
    <div class="details cv-editable-elem required" data-placeholder="Mô tả công việc" info-name="job_description_html" info-group="job_history">
        <p class="resume-timeline-item-desc-heading font-weight-bold cv-editable-elem"><?= Yii::t('cvgo', 'Mission'); ?>:</p>
        <p class="cv-editable-elem">- <?= Yii::t('cvgo', 'Carry out business plans: find customers, introduce products and services, consult products and applications, take care and manage customer relationships.'); ?></p>
        <p class="cv-editable-elem">- <?= Yii::t('cvgo', 'Make annual, quarterly, monthly, and weekly work plans to achieve assigned goals.'); ?></p>
        <p class="cv-editable-elem">- <?= Yii::t('cvgo', 'Survey, study, evaluate expected revenue in the management area. Develop and propose appropriate business plans.') ?></p>
        <p class="resume-timeline-item-desc-heading font-weight-bold cv-editable-elem"><?= Yii::t('cvgo', 'Achievements'); ?>:</p>
        <p class="cv-editable-elem"><?= Yii::t('cvgo', 'Through the process of working at ... Joint Stock Company, I have achieved some of the following achievements'); ?>:</p>
        <ul>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', '... Best seller') ?> <?= date('Y', strtotime("-1 year", time())) ?></li>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Excellent staff') ?> <?= date('Y', strtotime("-1 year", time())) ?></li>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Creative staff') ?></li>
            <li class="cv-editable-elem"><?= Yii::t('cvgo', 'Excellent sale project') ?></li>
        </ul>
    </div><!--//details-->
</div><!--//item-->