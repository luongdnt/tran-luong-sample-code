<li class="mb-2 cv-child-elem certificates-item">
	<div class="resume-degree font-weight-bold cv-editable-elem required" data-placeholder="Tên chứng chỉ" info-group="certificates" info-name="name"><?= Yii::t('cvgo', 'IELTS 7.0') ?></div>
	<div class="resume-degree-time">
		<p class="cv-editable-elem d-inline required" data-placeholder="Năm" info-group="certificates" info-name="year"><?= date('Y') ?></p>
	</div>
</li>