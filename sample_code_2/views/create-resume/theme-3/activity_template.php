<div class="item cv-child-elem activity-item">
    <div class="meta">
        <div class="upper-row">
            <h3 class="job-title cv-editable-elem required" data-placeholder="Tên công ty/tổ chức/sự kiện" info-name="organization" info-group="activity"><?= Yii::t('cvgo', 'Vietnam IT Recruitment Day') ?></h3>
            <div class="time">
                <p class="cv-editable-elem d-inline required" data-placeholder="Từ ngày" info-group="activity" info-name="from"><?= date("d/m/Y") ?></p>
                - 
                <p class="cv-editable-elem d-inline required" data-placeholder="Đến ngày" info-group="activity" info-name="to"><?= date("d/m/Y") ?></p>
            </div>
        </div><!--//upper-row-->
        <div class="company cv-editable-elem required" data-placeholder="Vị trí/vai trò" info-name="role" info-group="activity"><?= Yii::t('cvgo', 'Speaker/technical advisor')?></div>
        <div class="item-content cv-editable-elem" data-placeholder="Mô tả hoạt động" info-name="description" info-group="activity"><?= Yii::t('cvgo', 'Answer questions about the positions or organizations which attendees are interested in.')?></div>
    </div><!--//meta-->
</div><!--//item-->