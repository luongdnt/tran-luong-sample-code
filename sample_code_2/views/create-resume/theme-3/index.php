<?php

use common\components\HopeTimeHelper;
use yii\widgets\ActiveForm;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= $title ?></title>

    <!-- Meta -->
    <?= $this->render('../layout/meta') ?>
    <!-- Global CSS -->
    <link rel="stylesheet" href="/cv_template/dist/bootstrap/bootstrap.min.css">
    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="<?= $activeColor ?>">
    <?= $this->render('../layout/assets') ?>
    <link id="theme-style" rel="stylesheet" href="/cv_template/theme_3/css/theme-3.css">
    <style>
        .jobsgo-watermark, .font-wrapper {
            font-family: <?= $activeFontFamily ?>;
        }
        <?php if ($activeFontFamily == 'Arial, sans-serif'): ?>
        .sidebar-wrapper .name{
            font-weight: 600;
        }
        <?php else: ?>
        .sidebar-wrapper .name{
            font-weight: 900;
        }
        <?php endif; ?>
    </style>
</head>

<body>
    <?php if (empty($isRendered)): ?>
    <div class="loader-container">
        <div class="loader"></div>
    </div>
    <?= $this->render('../layout/header') ?>
    <?= $this->render('../layout/sidebar', [
            'theme'=> $theme,
            'cv' => $cv
        ]) ?>
    <?php endif; ?>
    <div class="wrapper resume-wrapper font-wrapper">
        <div class="sidebar-wrapper">
            <div class="profile-container">
                <div class="img-box mx-auto text-center">
                    <div class="img__wrap">
                        <?php if (!empty($candidate->avatar)): ?>
                            <img class="mr-3 img-fluid picture mx-auto" id="preview-avatar" src="<?= $candidate->avatar ?>" alt="">
                        <?php else: ?>
                            <img class="mr-3 img-fluid picture mx-auto" id="preview-avatar" src="/cv_template/assets/images/upload_icon.png" alt="">
                        <?php endif; ?>
                    </div>
                </div>
                <h1 class="name cv-editable-elem arial-text-font-weight" data-placeholder="Tên" info-group="candidate" info-name="name"><?= $candidate->name ?></h1>
                <h3 class="tagline cv-editable-elem required" data-placeholder="Vị trí mong muốn" info-group="candidate" info-name="career_name"><?= $candidate->career_name ?></h3>
            </div><!--//profile-container-->
            <div class="contact-container container-block">
                <ul class="list-unstyled contact-list">
                    <li class="email">
                        <i class="fas fa-envelope"></i>
                        <p class="cv-editable-elem d-inline" data-placeholder="Email" info-group="candidate" info-name="email"><?= $candidate->email ?></p>
                    </li>
                    <li class="phone">
                        <i class="fas fa-phone"></i>
                        <p class="cv-editable-elem d-inline" data-placeholder="Số điện thoại" info-group="candidate" info-name="tel"><?= $candidate->tel ?></p>
                    </li>
                    <?php if (empty($isRendered)): ?>
                        <input id="dp" class="datepicker" data-date-format="dd/mm/yyyy" style="display: none;">
                        <li class="website">
                            <i class="fas fa-birthday-cake"></i>
                            <p class="d-inline date-of-birth" info-group="candidate" info-name="date_of_birth"><?= date('d/m/Y', strtotime($candidate->date_of_birth)) ?></p>
                        </li>
                    <?php else: ?>
                        <?php if ($candidate->date_of_birth != '1970-01-01' && !empty($candidate->date_of_birth)): ?>
                            <input id="dp" class="datepicker" data-date-format="dd/mm/yyyy" style="display: none;">
                            <li class="website">
                                <i class="fas fa-birthday-cake"></i>
                                <p class="d-inline date-of-birth" info-group="candidate" info-name="date_of_birth"><?= date('d/m/Y', strtotime($candidate->date_of_birth)) ?></p>
                            </li>
                        <?php endif; ?>
                    <?php endif; ?>
                    <li class="candidate-address social-container">
                        <i class="fas fa-map-marker-alt"></i>
                        <p class="cv-editable-elem d-inline" data-placeholder="Tỉnh/thành phố" info-group="candidate" info-name="current_address"><?= $candidate->current_address ?></p>
                    </li>
                    <li class="linkedin social-container">
                        <i class="fab fa-linkedin-in"></i>
                        <p class="cv-editable-elem d-inline" data-placeholder="linkedin.com/in/username" info-group="candidate" info-name="linkedin"><?= $candidate->linkedin ?></p>
                    </li>
                    <li class="github social-container">
                        <i class="fab fa-facebook-f"></i>
                        <p class="cv-editable-elem d-inline" data-placeholder="fb.com/username" info-group="candidate" info-name="facebook"><?= $candidate->facebook ?></p>
                    </li>
                    <li class="twitter social-container">
                        <i class="fab fa-skype"></i>
                        <p class="cv-editable-elem d-inline" data-placeholder="live:skype_username" info-group="candidate" info-name="skype"><?= $candidate->skype ?></p>
                    </li>
                </ul>
            </div><!--//contact-container-->
            <div class="languages-container container-block cv-block cv-section-event" id="skill-block" my-title="Kỹ năng">
                <h2 class="container-block-title"><i class="fas fa-tools"></i> <?= Yii::t('cvgo', 'Skills'); ?></h2>
                <ul class="list-unstyled interests-list" id="list-skill" block="skill">
                    <?php if (!empty($candidateJobCategory)): ?>
                        <?php foreach ($candidateJobCategory as $item): ?>
                            <li class="cv-child-elem skill-item no-copy-controls" myId="<?= $item->category->job_category_id ?>">
                                <p class="d-none item-id" info-name="candidate_job_category_id" info-group="skill"><?= $item->candidate_job_category_id ?></p>
                                <p class="d-none item-id" info-name="job_category_id" info-group="skill"><?= $item->category->job_category_id ?></p>
                                <?php if (!empty($item->skill_title)): ?>
                                    <div class="resume-skill-name"><strong info-name="skill_title" info-group="skill"><?= $item->skill_title ?></strong></div>
                                <?php else: ?>
                                    <?php if ($cv->language == 'en-US'): ?>
                                    <div class="resume-skill-name"><strong info-name="skill_title" info-group="skill"><?= $item->category->job_category_name_en ?></strong></div>
                                    <?php else: ?>
                                    <div class="resume-skill-name"><strong info-name="skill_title" info-group="skill"><?= $item->category->job_category_name_vn ?></strong></div>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <p class="exp-container">
                                    <span class="d-none" info-name="experiment_duration" info-group="skill"><?= $item->experiment_duration ?></span>
                                    <?php if ($item->experiment_duration == 0) : ?>
                                    <span>
                                        <i class="fas fa-award"></i> 
                                        <span class="exp-year-title"><?= Yii::t('cvgo', 'below') ?> 1</span> 
                                        <span class="exp-year-text"><?= Yii::t('cvgo', 'year of experience'); ?></span>
                                    </span>
                                    <?php else: ?>
                                        <span>
                                            <i class="fas fa-award"></i> 
                                            <span class="exp-year-title"><?= $item->experiment_duration ?></span> 
                                            <span class="exp-year-text"><?= Yii::t('cvgo', 'year(s) of experience'); ?></span>
                                        </span>
                                    <?php endif; ?>
                                </p>
                            </li>
                        <?php endforeach;?>
                    <?php endif; ?>
                </ul>
                <?php if (empty($isRendered)): ?>
                    <select class="form-control select-skill select-skill-select2" id="select-skill">
                        <option></option>
                        <?php if (!empty($jobCategory)) : ?>
                            <?php foreach ($jobCategory as $item) : ?>
                                <?php if ($item->status >= 0) : ?>
                                    <?php if ($cv->language == 'en-US'): ?>
                                    <option value="<?= $item->job_category_id ?>"><?= $item->job_category_name_en ?></option>
                                    <?php else: ?>
                                    <option value="<?= $item->job_category_id ?>"><?= $item->job_category_name_vn ?></option>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif;?>
                    </select>
                    <div class="add-new-btn text-center" id="add-skill-template">
                        <i class="fas fa-plus"></i>
                    </div>
                <?php endif;?>
            </div><!--//interests-->

            <div class="languages-container container-block cv-block cv-section-event" id="soft-skill-block" my-title="Kỹ năng mềm">
                <h2 class="container-block-title"><i class="fas fa-feather"></i> <?= Yii::t('cvgo', 'Soft skills'); ?></h2>
                <ul class="list-unstyled interests-list" id="list-soft-skill" block="soft-skill">
                    <?php if (!empty($candidateSoftSkill)): ?>
                        <?php foreach ($candidateSoftSkill as $item): ?>
                            <li class="cv-child-elem soft-skill-item no-copy-controls" myId="<?= $item->soft_skill_id ?>">
                                <p class="d-none item-id" info-name="candidate_soft_skill_id" info-group="soft_skill"><?= $item->candidate_soft_skill_id ?></p>
                                <p class="d-none item-id" info-name="soft_skill_id" info-group="soft_skill"><?= $item->soft_skill_id ?></p>
                                <p class="d-none" info-group="soft_skill" info-name="level"><?= $item->level ?></p>
                                <?php if ($cv->language == 'en-US'): ?>
                                <div class="resume-skill-name" info-group="soft_skill" info-name="soft_skill_name"><?= $item->softskill->soft_skill_name_eng ?></div>
                                <?php else: ?>
                                <div class="resume-skill-name" info-group="soft_skill" info-name="soft_skill_name"><?= $item->softskill->soft_skill_name ?></div>
                                <?php endif; ?>
                                <div class="progress resume-progress">
                                    <div class="progress-bar theme-progress-bar-dark" role="progressbar" style="width: <?= $item->level ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                        <?php endforeach;?>
                    <?php endif; ?>
                </ul>
                <?php if (empty($isRendered)): ?>
                    <select class="form-control select-skill select2" id="select-soft-skill">
                        <option></option>
                        <?php if (!empty($softSkill)) : ?>
                            <?php foreach ($softSkill as $item) : ?>
                                <?php if ($cv->language == 'en-US'): ?>
                                <option value="<?= $item->soft_skill_id ?>"><?= $item->soft_skill_name_eng ?></option>
                                <?php else: ?>
                                <option value="<?= $item->soft_skill_id ?>"><?= $item->soft_skill_name ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif;?>
                    </select>
                    <div class="add-new-btn text-center" id="add-soft-skill-template">
                        <i class="fas fa-plus"></i>
                    </div>
                <?php endif;?>
            </div><!--//soft skill-->

            <div class="languages-container container-block cv-block cv-section-event" id="certificates-block" my-title="Chứng chỉ">
                <h2 class="container-block-title"><i class="fas fa-award"></i> <?= Yii::t('cvgo', 'Certificates'); ?></h2>
                <ul class="list-unstyled interests-list" id="certificates-container" block="certificates">
                    <?php if (!empty($certificates)): ?>
                        <?php foreach ($certificates as $item): ?>
                            <li class="mb-2 cv-child-elem certificates-item">
                                <p class="d-none item-id" info-name="id" info-group="certificates"><?= $item->id ?></p>
                                <div class="resume-degree font-weight-bold cv-editable-elem required" data-placeholder="Tên chứng chỉ" info-group="certificates" info-name="name"><?= $item->name ?></div>
                                <div class="resume-degree-time">
                                    <?php if (empty($isRendered)): ?>
                                        <p class="cv-editable-elem d-inline required" data-placeholder="Năm" info-group="certificates" info-name="year"><?= $item->year ?></p>
                                    <?php else: ?>
                                        <?php if (!empty($item->year)): ?>
                                            <p class="cv-editable-elem d-inline required" data-placeholder="Năm" info-group="certificates" info-name="year"><?= $item->year ?></p>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </li>
                        <?php endforeach;?>
                    <?php else: ?>
                        <?php if (empty($isRendered)): ?>
                            <div class="add-new-btn text-center" id="add-certificates-template" data-toggle="tooltip" data-placement="top" title="Thêm biểu mẫu">
                                <i class="fas fa-plus"></i>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </ul>
            </div><!--//certificates-->

            <div class="languages-container container-block cv-block cv-section-event" id="language-block" my-title="Ngôn ngữ">
                <h2 class="container-block-title"><i class="fas fa-language"></i> <?= Yii::t('cvgo', 'Language'); ?></h2>
                <ul class="list-unstyled interests-list" block="language" id="language-container">
                    <?php if (!empty($candidateLanguage)): ?>
                        <?php foreach ($candidateLanguage as $item): ?>
                            <li class="cv-child-elem language-item no-copy-controls" myId="<?= $item->language_id ?>">
                                <p class="d-none item-id" info-name="candidate_language_id" info-group="language"><?= $item->candidate_language_id ?></p>
                                <p class="d-none item-id" info-name="language_id" info-group="language"><?= $item->language_id ?></p>
                                <div class="progress">
                                  <div class="progress-bar" role="progressbar" style="width: <?= $item->level ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><span><span info-group="language" info-name="level"><?= $item->level ?></span>%</span></div>
                                </div>
                                <?php if ($cv->language == 'en-US'): ?>
                                <p class="resume-lang-name" info-group="language" info-name="language_name"><?= $item->language->language_name_en ?></p>
                                <?php else: ?>
                                <p class="resume-lang-name" info-group="language" info-name="language_name"><?= $item->language->language_name ?></p>
                                <?php endif; ?>
                            </li>
                        <?php endforeach;?>
                    <?php endif; ?>
                </ul>
                <?php if (empty($isRendered)): ?>
                    <select class="form-control select-skill select2" id="select-language">
                        <option></option>
                        <?php if (!empty($language)) : ?>
                            <?php foreach ($language as $item) : ?>
                                <?php if ($cv->language == 'en-US'): ?>
                                <option value="<?= $item->language_id ?>"><?= $item->language_name_en ?></option>
                                <?php else: ?>
                                <option value="<?= $item->language_id ?>"><?= $item->language_name ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif;?>
                    </select>
                    <div class="add-new-btn text-center" id="add-language-template">
                        <i class="fas fa-plus"></i>
                    </div>
                <?php endif; ?>
            </div><!--//interests-->

            <div class="interests-container container-block cv-block cv-section-event" id="interests-block" my-title="Sở thích">
                <h2 class="container-block-title"> <i class="fas fa-gamepad"></i> <?= Yii::t('cvgo', 'Hobby'); ?></h2>
                <ul class="list-unstyled interests-list" block="hobby" id="hobby-container">
                    <?php if (!empty($candidateHobby)): ?>
                        <?php foreach ($candidateHobby as $item): ?>
                            <li class="cv-child-elem hobby-item no-copy-controls" myId="<?= $item->hobby_id ?>">
                                <p class="d-none item-id" info-name="candidate_hobby_id" info-group="hobby"><?= $item->candidate_hobby_id ?></p>
                                <p class="d-none item-id" info-name="hobby_id" info-group="hobby"><?= $item->hobby_id ?></p>
                                <?php if ($cv->language == 'en-US'): ?>
                                <p class="resume-lang-name" info-group="hobby" info-name="hobby_name"><?= $item->hobby->hobby_name_en ?></p>
                                <?php else: ?>
                                <p class="resume-lang-name" info-group="hobby" info-name="hobby_name"><?= $item->hobby->hobby_name ?></p>
                                <?php endif; ?>
                            </li>
                        <?php endforeach;?>
                    <?php endif; ?>
                </ul>
                <?php if (empty($isRendered)): ?>
                <select class="form-control select-skill select2" id="select-hobby">
                    <option></option>
                    <?php if (!empty($hobby)) : ?>
                        <?php foreach ($hobby as $item) : ?>
                            <?php if ($cv->language == 'en-US'): ?>
                            <option value="<?= $item->hobby_id ?>"><?= $item->hobby_name_en ?></option>
                            <?php else: ?>
                            <option value="<?= $item->hobby_id ?>"><?= $item->hobby_name ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif;?>
                </select>
                <div class="add-new-btn text-center" id="add-hobby-template">
                    <i class="fas fa-plus"></i>
                </div>
                <?php endif;?>
            </div><!--//interests-->

        </div><!--//sidebar-wrapper-->

        <div class="main-wrapper">

            <section class="section summary-section cv-block cv-section-event" id="career-summary-block" my-title="Mục tiêu nghề nghiệp">
                <h2 class="section-title"><span class="icon-holder"><i class="fas fa-user"></i></span> <?= Yii::t('cvgo', 'Career goals'); ?></h2>
                <div class="summary cv-editable-elem" data-placeholder="Giới thiệu tổng quát bản thân, mục tiêu phấn đấu..." info-group="candidate" info-name="short_bio_html">
                    <?php if (empty($candidate->short_bio_html)): ?>
                        <?= $candidate->short_bio ?>
                    <?php else: ?>
                        <?= $candidate->short_bio_html ?>
                    <?php endif; ?>
                </div><!--//summary-->
            </section><!--//section-->

            <section class="section experiences-section cv-block cv-section-event" id="experience-block" my-title="Kinh nghiệm làm việc">
                <h2 class="section-title"><span class="icon-holder"><i class="fas fa-briefcase"></i></span> <?= Yii::t('cvgo', 'Work experience'); ?></h2>
                <div id="job-history-container" block="job-history">
                <?php if (!empty($jobHistory)): ?>
                    <?php foreach ($jobHistory as $item): ?>
                        <div class="item cv-child-elem history-item">
                            <p class="d-none item-id" info-name="candidate_job_history_id" info-group="job_history"><?= $item->candidate_job_history_id ?></p>
                            <div class="meta">
                                <div class="upper-row">
                                    <h3 class="job-title cv-editable-elem required" data-placeholder="Vị trí/công việc" info-name="job_title" info-group="job_history"><?= $item->job_title ?></h3>
                                    <div class="time">
                                        <?php if (empty($isRendered)): ?>
                                            <p class="cv-editable-elem d-inline required" data-placeholder="Năm bắt đầu" info-group="job_history" info-name="date_start"><?= $item->date_start ?></p>
                                            -
                                            <p class="cv-editable-elem d-inline required" data-placeholder="Năm kết thúc" info-group="job_history" info-name="date_end"><?= $item->date_end ?></p>
                                        <?php else: ?>
                                            <?php if (!empty($item->date_start)): ?>
                                                <p class="cv-editable-elem d-inline required" data-placeholder="Năm bắt đầu" info-group="job_history" info-name="date_start"><?= $item->date_start ?></p>
                                            <?php endif; ?>
                                            <?php if (!empty($item->date_start) && !empty($item->date_end)): ?>
                                                -
                                            <?php endif; ?>
                                            <?php if (!empty($item->date_end)): ?>
                                                <p class="cv-editable-elem d-inline required" data-placeholder="Năm kết thúc" info-group="job_history" info-name="date_end"><?= $item->date_end ?></p>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </div><!--//upper-row-->
                                <div class="company cv-editable-elem required" data-placeholder="Tên công ty" info-name="job_company" info-group="job_history"><?= $item->job_company ?></div>
                            </div><!--//meta-->
                            <div class="details cv-editable-elem required" data-placeholder="Mô tả công việc" info-name="job_description_html" info-group="job_history">
                                <?php if (empty($item->job_description_html)): ?>
                                    <?= $item->job_description ?>
                                <?php else: ?>
                                    <?= $item->job_description_html ?>
                                <?php endif; ?>
                            </div><!--//details-->
                        </div><!--//item-->
                    <?php endforeach;?>
                <?php else:?>
                    <?php if (empty($isRendered)): ?>
                        <div class="add-new-btn text-center" id="add-job-history-template" data-toggle="tooltip" data-placement="top" title="Thêm biểu mẫu">
                            <i class="fas fa-plus"></i>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
                </div>
            </section><!--//section-->

            <section class="section experiences-section cv-block cv-section-event" id="education-block" my-title="Học vấn">
                <h2 class="section-title"><span class="icon-holder"><i class="fas fa-graduation-cap"></i></span></i> <?= Yii::t('cvgo', 'Education'); ?></h2>
                <div id="education-container" block="education">
                <?php if (!empty($education)): ?>
                    <?php foreach ($education as $item): ?>
                        <div class="item cv-child-elem education-item">
                            <p class="d-none item-id" info-name="candidate_education_id" info-group="education"><?= $item->candidate_education_id ?></p>
                            <div class="meta">
                                <div class="upper-row">
                                    <h3 class="job-title cv-editable-elem required" data-placeholder="Chuyên ngành" info-name="specialization" info-group="education"><?= $item->specialization ?></h3>
                                    <div class="time">
                                        <?php if (empty($isRendered)): ?>
                                            <p class="cv-editable-elem d-inline required" data-placeholder="Năm bắt đầu" info-group="education" info-name="date_start"><?= $item->date_start ?></p>
                                            -
                                            <p class="cv-editable-elem d-inline required" data-placeholder="Năm kết thúc" info-group="education" info-name="date_end"><?= $item->date_end ?></p>
                                        <?php else: ?>
                                            <?php if (!empty($item->date_start)): ?>
                                                <p class="cv-editable-elem d-inline required" data-placeholder="Năm bắt đầu" info-group="education" info-name="date_start"><?= $item->date_start ?></p>
                                            <?php endif; ?>
                                            <?php if (!empty($item->date_start) && !empty($item->date_end)): ?>
                                                -
                                            <?php endif; ?>
                                            <?php if (!empty($item->date_end)): ?>
                                                <p class="cv-editable-elem d-inline required" data-placeholder="Năm kết thúc" info-group="education" info-name="date_end"><?= $item->date_end ?></p>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </div><!--//upper-row-->
                                <div class="company cv-editable-elem required" data-placeholder="Tên trường" info-name="school_name" info-group="education"><?= $item->school_name ?></div>
                                <div class="company cv-editable-elem" data-placeholder="Mô tả học vấn" info-name="hightlight" info-group="education"><?= $item->hightlight ?></div>
                            </div><!--//meta-->
                        </div><!--//item-->
                    <?php endforeach;?>
                <?php else:?>
                    <?php if (empty($isRendered)): ?>
                        <div class="add-new-btn text-center" id="add-education-template" data-toggle="tooltip" data-placement="top" title="Thêm biểu mẫu">
                            <i class="fas fa-plus"></i>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
                </div>
            </section><!--//section-->

            <section class="section experiences-section cv-block cv-section-event" id="activity-block" my-title="Hoạt động">
                <h2 class="section-title"><span class="icon-holder"><i class="fab fa-grav"></i></span> <?= Yii::t('cvgo', 'Activity'); ?></h2>
                <div block="activity" id="activity-container">
                <?php if (!empty($activity)): ?>
                    <?php foreach ($activity as $item): ?>
                        <div class="item cv-child-elem activity-item">
                            <p class="d-none item-id" info-name="id" info-group="activity"><?= $item->id ?></p>
                            <div class="meta">
                                <div class="upper-row">
                                    <h3 class="job-title cv-editable-elem required" data-placeholder="Tên công ty/tổ chức/sự kiện" info-name="organization" info-group="activity"><?= $item->organization ?></h3>
                                    <div class="time">
                                        <?php if (empty($isRendered)): ?>
                                            <p class="cv-editable-elem d-inline required" data-placeholder="Từ ngày" info-group="activity" info-name="from"><?= $item->from ?></p>
                                            -
                                            <p class="cv-editable-elem d-inline required" data-placeholder="Đến ngày" info-group="activity" info-name="to"><?= $item->to ?></p>
                                        <?php else: ?>
                                            <?php if (!empty($item->from)): ?>
                                                <p class="cv-editable-elem d-inline required" data-placeholder="Từ ngày" info-group="activity" info-name="from"><?= $item->from ?></p>
                                            <?php endif; ?>
                                            <?php if (!empty($item->from) && !empty($item->to)): ?>
                                                -
                                            <?php endif; ?>
                                            <?php if (!empty($item->to)): ?>
                                                <p class="cv-editable-elem d-inline required" data-placeholder="Đến ngày" info-group="activity" info-name="to"><?= $item->to ?></p>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </div><!--//upper-row-->
                                <div class="company cv-editable-elem required" data-placeholder="Vị trí/vai trò" info-name="role" info-group="activity"><?= $item->role ?></div>
                                <div class="item-content cv-editable-elem" data-placeholder="Mô tả hoạt động" info-name="description" info-group="activity"><?= $item->description ?></div>
                            </div><!--//meta-->
                        </div><!--//item-->
                    <?php endforeach;?>
                <?php else:?>
                    <?php if (empty($isRendered)): ?>
                        <div class="add-new-btn text-center" id="add-activity-template" data-toggle="tooltip" data-placement="top" title="Thêm biểu mẫu">
                            <i class="fas fa-plus"></i>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
                </div>
            </section><!--//section-->

            <section class="section experiences-section cv-block cv-section-event" id="award-block" my-title="Giải thưởng">
                <h2 class="section-title"><span class="icon-holder"><i class="fas fa-crown"></i></span> <?= Yii::t('cvgo', 'Award'); ?></h2>
                <div block="award" id="award-container">
                <?php if (!empty($award)): ?>
                    <?php foreach ($award as $item): ?>
                        <div class="item cv-child-elem award-item">
                            <p class="d-none item-id" info-name="id" info-group="award"><?= $item->id ?></p>
                            <div class="meta">
                                <div class="upper-row">
                                    <h3 class="job-title cv-editable-elem required" data-placeholder="Tên thành tích" info-name="name" info-group="award"><?= $item->name ?></h3>
                                    <div class="time">
                                        <?php if (empty($isRendered)): ?>
                                            <p class="cv-editable-elem d-inline required" data-placeholder="Năm" info-group="award" info-name="year"><?= $item->year ?></p>
                                        <?php else: ?>
                                            <?php if (!empty($item->year)): ?>
                                                <p class="cv-editable-elem d-inline required" data-placeholder="Năm" info-group="award" info-name="year"><?= $item->year ?></p>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </div><!--//upper-row-->
                            </div><!--//meta-->
                        </div><!--//item-->
                    <?php endforeach;?>
                <?php else:?>
                    <?php if (empty($isRendered)): ?>
                        <div class="add-new-btn text-center" id="add-award-template" data-toggle="tooltip" data-placement="top" title="Thêm biểu mẫu">
                            <i class="fas fa-plus"></i>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
                </div>
            </section><!--//section-->
            <section class="section experiences-section cv-block cv-section-event" id="references-block" my-title="Người tham chiếu">
                <h2 class="section-title"><span class="icon-holder"><i class="fas fa-user-circle"></i></span> <?= Yii::t('cvgo', 'References'); ?></h2>
                <div block="references" id="references-container">
                <?php if (!empty($references)): ?>
                    <?php foreach ($references as $item): ?>
                        <li class="mb-3 cv-child-elem references-item">
                            <p class="d-none item-id" info-name="id" info-group="references"><?= $item->id ?></p>
                            <div class="cv-editable-elem required" data-placeholder="Người tham chiếu" info-name="references_text"
                            info-group="references">
                                <?= $item->references_text ?>
                            </div>
                        </li>
                    <?php endforeach;?>
                <?php else:?>
                    <?php if (empty($isRendered)): ?>
                        <div class="add-new-btn text-center" id="add-references-template" data-toggle="tooltip" data-placement="top" title="Thêm biểu mẫu">
                            <i class="fas fa-plus"></i>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
                </div>
            </section><!--//section-->
        </div><!--//main-body-->
    </div>
    <?php if (empty($isRendered)): ?>
    <?= $this->render('../layout/hidden_list') ?>
    <?= $this->render('../layout/modal') ?>
    <?php else: ?>
        <script type="text/javascript" src="/cv_template/assets/js/render.js"></script>
        <script type="text/javascript" src="/cv_template/assets/js/print.js"></script>
        <script>            
            $(document).ready(function(){      
                // header height
                redundantHeaderHeight = 0;
                // mistery 
                adjustHeight = -50;
                fillBlankRenderPage('resume-wrapper', 'resume-wrapper',redundantHeaderHeight,adjustHeight);
            });
        </script>
    <?php endif; ?>
    <?= $this->render('../layout/footer', [
        'theme' => $theme,
        'cv' => $cv,
        'color' => $color,
        'activeColor' => $activeColor,
        'fontFamily' => $fontFamily,
        'activeFontFamily' => $activeFontFamily,
        'isRendered' => $isRendered
        ]) ?>
    <?= $this->render('../layout/init_js', ['candidateId' => $candidate->candidate_id]) ?>
    <script type="text/javascript" src="/cv_template/assets/js/script.js"></script>
</body>
</html> 

