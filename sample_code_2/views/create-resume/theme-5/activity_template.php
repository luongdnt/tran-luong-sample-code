<div class="item cv-child-elem activity-item">
	<div class="item-heading row align-items-center">
		<h6 class="col-12 col-md-6 col-lg-8 cv-editable-elem required" data-placeholder="Tên công ty/tổ chức/sự kiện" info-name="organization" info-group="activity"><?= Yii::t('cvgo', 'Vietnam IT Recruitment Day') ?></h6>
		<div class="item-meta col-12 col-md-6 col-lg-4 text-muted text-left text-md-right">
		 	<p class="cv-editable-elem d-inline required" data-placeholder="Từ ngày" info-group="activity" info-name="from"><?= date("d/m/Y") ?></p>
		 	-
		 	<p class="cv-editable-elem d-inline required" data-placeholder="Đến ngày" info-group="activity" info-name="to"><?= date("d/m/Y") ?></p>
		</div>
	</div>
	<div class="item-content cv-editable-elem required" data-placeholder="Vị trí/vai trò" info-name="role" info-group="activity"><?= Yii::t('cvgo', 'Speaker/technical advisor')?></div>
    <div class="item-content cv-editable-elem" data-placeholder="Mô tả hoạt động" info-name="description" info-group="activity"><?= Yii::t('cvgo', 'Answer questions about the positions or organizations which attendees are interested in.')?></div>
</div><!--//item-->