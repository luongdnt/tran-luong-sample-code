<li class="mb-0 pl-4 mb-2 position-relative cv-child-elem activity-item">
	<i class="resume-award-icon fas fa-bullhorn position-absolute" data-fa-transform="shrink-2"></i>
	<div class="d-flex flex-column flex-md-row">
		<div class="cv-editable-elem required" data-placeholder="Tên công ty/tổ chức/sự kiện" info-name="organization" info-group="activity"><strong><?= Yii::t('cvgo', 'Vietnam IT Recruitment Day') ?></strong></div>
		<div class="resume-activity-desc ml-auto">
			<div class="resume-degree-time">
				<p class="cv-editable-elem d-inline required" data-placeholder="Từ ngày" info-group="activity" info-name="from"><?= date("d/m/Y") ?></p>
				- 
				<p class="cv-editable-elem d-inline required" data-placeholder="Đến ngày" info-group="activity" info-name="to"><?= date("d/m/Y") ?></p>
			</div>
		</div>
	</div>
	<div class="resume-degree-org cv-editable-elem required medium-editor-element" data-placeholder="Vị trí/vai trò" info-group="activity" info-name="role"><?= Yii::t('cvgo', 'Speaker/technical advisor')?></div>
    <div class="resume-degree-org item-content cv-editable-elem" data-placeholder="Mô tả hoạt động" info-name="description" info-group="activity"><?= Yii::t('cvgo', 'Answer questions about the positions or organizations which attendees are interested in.')?></div>
</li>