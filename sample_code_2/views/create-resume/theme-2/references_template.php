<li class="mb-3 cv-child-elem references-item">
	<div class="cv-editable-elem required" data-placeholder="Người tham chiếu" info-name="references_text"
		 info-group="references">
		<p class="font-weight-bold"><?= Yii::t('cvgo', 'Tran Le Nguyen Vu') ?></p>
		<p class=""><?= Yii::t('cvgo', 'Head of IT Department - ... University') ?></p>
		<p class=""><?= Yii::t('cvgo', 'Phone') ?>: 012346789</p>
		<p class=""><?= Yii::t('cvgo', 'Email') ?>: abc@gmail.com</p>
	</div>
</li>