<li class="mb-0 pl-4 mb-2 position-relative cv-child-elem education-item">
	<i class="resume-award-icon fas fa-university position-absolute" data-fa-transform="shrink-2"></i>
	<div class="d-flex flex-column flex-md-row">
		<p class="resume-degree font-weight-bold cv-editable-elem required"  data-placeholder="Chuyên ngành" info-name="specialization" info-group="education"><?= Yii::t('cvgo', 'Business Administration'); ?></p>
		<div class="resume-activity-desc ml-auto">
			<div class="resume-degree-time">
				<p class="cv-editable-elem d-inline required" data-placeholder="Năm bắt đầu" info-group="education" info-name="date_start"><?= date('Y', strtotime("-1 year", time())) ?></p>
				- 
				<p class="cv-editable-elem d-inline required" data-placeholder="Năm kết thúc" info-group="education" info-name="date_end"><?= date('Y') ?></p>
			</div>
		</div>
	</div>
	<div class="resume-degree-org cv-editable-elem required" data-placeholder="Tên trường" info-group="education" info-name="school_name"><?= Yii::t('cvgo', '... University'); ?></div>
	<div class="resume-degree-org cv-editable-elem" data-placeholder="Mô tả học vấn" info-group="education" info-name="hightlight"><?= Yii::t('cvgo', 'Good graduate'); ?></div>
</li>