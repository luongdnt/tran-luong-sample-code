<?php

use Yii;
use common\components\HopeShareHelper;
use common\models\Cv;
$this->title = 'JobsGO - Quản lý CV Go';

?>
<script type="text/javascript" src="/cv_template/dist/js/jquery.js"></script>
<section class="section wrap-1">
	<div class="container">
		<div class="row no-mrg">
			<div class="col-sm-12">
				<div class="sidebar-widget brows-job-category job-list list-view padd-top-0 padd-bot-0">
					<h4 class="mrg-bot-20">
						<span>Quản lý CV Go - <strong>(<?= count($cv) ?>/<?= Cv::LIMIT_CV ?>)</strong></span>
						<a class="pull-right text-capitalize font-13 text-danger" id="add-new-cv" title="Tạo CV mới" href="javascript:void(0)"><span class="glyphicon glyphicon-plus"></span>Tạo CV mới</a>
					</h4>

					<div class="row">
						<div class="col-sm-12">
							<div class="colorgb-carousel-bk v2">
								<div class="item">
									<?php if (!empty($cv)) : ?>
										<?php foreach ($cv as $item): ?>
									<div class="item-click">
										<article class="platinum">
											<div class="brows-job-list">
												<div class="col-sm-12">
													<div class="item-fl-box">
														<div class="brows-job-company-img">
															<a href="/cv-go/<?= HopeShareHelper::encryptString($item->id) ?>"><img onerror="" class="img-responsive" alt="" src="/cv_template/assets/images/theme/<?= $item->theme->thumbnail ?>" /></a>
														</div>
														<div class="brows-job-position">
															<?php if ($item->cv_type != 1) : ?>
															<h3 class="cv-name-title" contenteditable="true" data-id="<?= HopeShareHelper::encryptString($item->id) ?>"><?= $item->cv_name ?></h3>
															<?php else: ?>
															<h3 data-id="<?= HopeShareHelper::encryptString($item->id) ?>"><?= $item->cv_name ?></h3>
															<?php endif; ?>
															<p class="font-13"><?= $item->theme->name?> - <?= $item->language?></p>
															<p class="font-12">
																<a href="/cv-go/<?= HopeShareHelper::encryptString($item->id) ?>">
																	<span><i class="glyphicon glyphicon-pencil"></i> Chỉnh sửa</span>
																</a> 
																<a href="javascript:void(0)" class="download-cv" data-id="<?= HopeShareHelper::encryptString($item->id) ?>" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Đang tải xuống...">
																	<span class="mrg-l-10 hidden-xs"><i class="glyphicon glyphicon-save-file"></i> Tải xuống</span>
																</a>
																<?php if ($item->cv_type != 1) : ?>
																<a href="javascript:void(0)" class="delete-cv" data-id="<?= HopeShareHelper::encryptString($item->id) ?>">
																	<span class="brows-job-sallery"><i class="glyphicon glyphicon-remove"></i> Xóa CV</span>
																</a>
																<?php endif; ?>
															</p>
														</div>
													</div>
												</div>
											</div>
										</article>
									</div>
										<?php endforeach; ?>
									<?php else: ?>
									<p>Không có cv nào</p>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<style type="text/css">
	.cv-name-title {
		border: 1px dashed transparent;
	}
	.cv-name-title:hover {
		border: 1px dashed #00a2ff;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$('#add-new-cv').click(function(event) {
			$.ajax({
				url: '/create-resume/add-new-cv',
				type: 'POST',
				dataType: 'json',
				data: {},
			})
			.done(function(response) {
				if (response.status) {
					window.location.href = response.msg;
				} else {
					alert(response.msg);
				}
			})
			.fail(function() {
			})
			.always(function() {
			});
		});

		$('.delete-cv').click(function(event) {
			let id = $(this).data('id');
			let r = confirm("Bạn có chắc chắn muốn xóa CV này?");
			if (r == true) {
				$.ajax({
					url: '/create-resume/delete-cv',
					type: 'POST',
					dataType: 'json',
					data: {id: id},
				})
				.done(function(response) {
					if (response.status) {
						location.reload();
					} else {
						alert(response.msg);
					}
				})
				.fail(function() {
				})
				.always(function() {
				});
			}
		});

		$(".cv-name-title").keypress(function(e){
			if (e.which == 13) {
				$(this).blur();
			}
		});

		$('.cv-name-title').bind('DOMSubtreeModified', function(){
			let id = $(this).data('id');
			let name = $(this).text();
			$.ajax({
				url: '/create-resume/update-cv-name',
				type: 'POST',
				dataType: 'json',
				data: {id: id, name: name},
			})
			.done(function(response) {
				if (response.status) {
					// location.reload();
				} else {
					alert(response.msg);
				}
			})
			.fail(function() {
			})
			.always(function() {
			});
		});

		$('.download-cv').click(function(event) {
			let cvId = $(this).data('id');
			let thisBtn = $(this);
			$.ajax({
				url: '/create-resume/download-resume',
				type: 'POST',
				dataType: 'json',
				data: {cvId: cvId},
				beforeSend: function() {
					thisBtn.button('loading');
					$('.download-cv').css('pointer-events', 'none');
				}
			})
			.done(function(response) {
				thisBtn.button('reset');
				if (response.status) {
					let cv_url = '/create-resume/get-pdf?cv_id=' + response.msg;
					window.location = cv_url;
				} else {
					alert(response.msg);
				}
				$('.download-cv').css('pointer-events', 'auto');
			})
			.fail(function() {
				alert('Xuất hiện lỗi không mong muốn. Vui lòng tải lại trang và thử lại.');
			});
		});
	});
</script>
