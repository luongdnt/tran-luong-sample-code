// change select skill layout here
$(document).on('select2:select', '#select-personal-skill', function (event) {
    event.preventDefault();
    var val = $(this).val();
    var text = $("#select-personal-skill option:selected").text();
    var selected = $("#list-skill").find('[myId="' + val + '"]').length;
    var html = '';
    if (selected === 0) {
        $("#experience-modal").modal("show");
        $("#experience-confirm-btn").one('click', function (event) {
            event.preventDefault();
            var selected = $("#list-skill").find('[myId="' + val + '"]').length;
            if (selected === 0) {
                var expYear = $('#exp-year').val();
                let cv_language = localStorage.getItem('cv_language');
                let yearText = 'năm';
                if (cv_language == 'en-US') {
                    yearText = 'year(s)';
                }
                var html = '<div class="col-sm-3 mt-3">' +
                    '<li class="mb-0 mt-3 badge mybadge badge-col-1 cv-child-elem skill-item only-remove-controls" myId="' + val + '">' +
                    '<p class="d-none item-id" info-name="job_category_id"' +
                    ' info-group="skill">' + val + '</p>' +
                    '<div class="resume-skill-name" info-name="skill_title" info-group="skill">' +
                    text +
                    '</div>' +
                    '<span>' +
                    '(<span info-name="experiment_duration" info-group="skill">' +
                    expYear +
                    '</span> ' + yearText +
                    ')</span>' +
                    '</li>' +
                    '</div>';
                if ($('#list-skill').children('div .row').length != 0) {
                    $('#list-skill .row').append(html);
                } else {
                    $('#list-skill').append('<div class="row">' + html + '</div>');
                }
                $('#exp-year').val(1);
                $('#exp-year-val').text(1);
                $("#experience-modal").modal("hide");
            }
        });
    }
    $("#select-personal-skill").val([]).trigger('change');
});

$(document).on('select2:close', '#select-personal-skill', function (event) {
    event.preventDefault();
    $('.select2-container').hide();
});
// end change skill

// change select soft skill layout here
$(document).on('select2:select', '#select-soft-skill-new', function (event) {
    event.preventDefault();
    let val = $(this).val();
    let text = $("#select-soft-skill-new option:selected").text();
    let selected = $("#list-soft-skill").find('[myId=' + val + ']').length;
    if (selected === 0) {
        $("#soft-skill-modal").modal("show");
        $("#soft-skill-confirm-btn").one('click', function (event) {
            event.preventDefault();
            let selected = $("#list-soft-skill").find('[myId=' + val + ']').length;
            if (selected === 0) {
                let competently = $('#soft-skill-competently').val();
                let html = '<li class="mb-0 mt-3 cv-child-elem soft-skill-item no-copy-controls" myId="' + val + '">' +
                    '<p class="d-none item-id" info-name="soft_skill_id" info-group="soft_skill">' + val + '</p>' +
                    '<div class="resume-skill-name pb-1" info-group="soft_skill" info-name="soft_skill_name">' + text + '</div>' +
                    '<div class="progress resume-progress">' +
                    '<div class="progress-bar soft-skill-progress-bar theme-progress-bar-dark"' +
                    ' role="progressbar" style="width: ' + competently + '%"' +
                    ' aria-valuenow="25" aria-valuemin="0"' +
                    ' aria-valuemax="100" value="' + competently + '">' +
                    '  <span><span' +
                    ' info-group="soft_skill"' +
                    ' info-name="level">' + competently + '</span>%</span>' +
                    '</div>' +
                    ' </div>' +
                    '</li>';
                $('#list-soft-skill').append(html);
                $('#soft-skill-competently').val(50);
                $('#soft-skill-competently-val').text(50);
                $("#soft-skill-modal").modal("hide");
            }
        });
    }
    $("#select-soft-skill-new").val('').trigger('change');
});
$(document).on('select2:close', '#select-soft-skill-new', function (event) {
    event.preventDefault();
    $('.select2-container').hide();
});
// end select soft skill customize layout

// change select language layout here
$(document).on('select2:select', '#select-language-new', function (event) {
    event.preventDefault();
    let val = $(this).val();
    let text = $("#select-language-new option:selected").text();
    let selected = $("#language-container").find('[myId=' + val + ']').length;
    if (selected === 0) {
        $("#language-modal").modal("show");
        $("#language-confirm-btn").one('click', function (event) {
            event.preventDefault();
            let selected = $("#language-container").find('[myId=' + val + ']').length;
            if (selected === 0) {
                let langCompetently = $('#lang-competently').val();
                let html = '<li class="mb-0 mt-3 cv-child-elem language-item no-copy-controls" myId="' + val + '">' +
                    '<p class="d-none item-id" info-name="language_id" info-group="language">' + val + '</p>' +
                    '<p class="resume-lang-name pb-1" info-group="language" info-name="language_name">' + text + '</p>' +
                    '<div class="progress left-print-progress-bar">' +
                    '<div class="progress-bar" role="progressbar"' +
                    ' style="width: ' + langCompetently + '%;" aria-valuenow="25"' +
                    ' aria-valuemin="0" aria-valuemax="100"><span><span' +
                    ' info-group="language"' +
                    ' info-name="level">' + langCompetently + '</span>%</span>' +
                    '</div>' +
                    '</div>' +
                    '</li>';
                $('#language-container').append(html);
                $('#lang-competently').val(50);
                $('#lang-competently-val').text(50);
                $("#language-modal").modal("hide");
            }
        });
    }
    $("#select-language").val('').trigger('change');
});
$(document).on('select2:close', '#select-language-new', function (event) {
    event.preventDefault();
    $('.select2-container').hide();
});
// end change select language
// show only remove control
const childElemControlsOnlyRemove = '<div class="child-elem-controls">'
    + '<div title="Chuyển mục này lên trên" class="up" style="visibility: hidden">▲</div>'
    + '<div title="Chuyển mục này xuống dưới" class="down" style="visibility: hidden">▼</div>'
    + '<div title="Nhân đôi mục này" class="clone" style="visibility: hidden"><i class="fa fa-plus"></i></div>'
    + '<div title="Xóa mục này" class="remove">−</div>'
    + '</div>';
$(document).on('mouseover', '.cv-child-elem', function (event) {
    event.preventDefault();
    if (!$(this).find('.child-elem-controls').length) {
        if ($(this).hasClass('only-remove-controls')) {
            $(this).append(childElemControlsOnlyRemove);
        } else if ($(this).hasClass('no-copy-controls')) {
            $(this).append(childElemControlsNoCopy);
        } else {
            $(this).append(childElemControls);
        }
    }

    $(this).find('.child-elem-controls').show();
});

// select hobby
$(document).on('select2:select', '#select-hobby-new', function (event) {
    event.preventDefault();
    let val = $(this).val();
    let text = $("#select-hobby-new option:selected").text();
    let selected = $("#hobby-container").find('[myId=' + val + ']').length;
    if (selected === 0) {
        let html = ' <li class="mb-0 mt-3 cv-child-elem hobby-item no-copy-controls" myId="' + val + '">' +
            ' <p class="d-none item-id" info-name="hobby_id" info-group="hobby">' + val + '</p>' +
            ' <p class="resume-lang-name" info-group="hobby" info-name="hobby_name">' + text + '</p>' +
            ' </li>';
        $('#hobby-container').append(html);
        // $(this).parent().parent().find('#hobby-container').append(html);
    }
});

$(document).on('select2:close', '#select-hobby-new', function (event) {
    event.preventDefault();
    $('.select2-container').hide();
});

// remove personal skill
$(document).on('click', '.remove', function () {
    //remove personal skill
    let container = $(this).parent().parent().parent().parent().parent();
    let block = container.attr('block');
    if (block == 'skill') {
        $(this).parent().parent().parent().remove();
    }
});