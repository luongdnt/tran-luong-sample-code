$(document).ready(function() {
	$('.cv-block').each(function() {
		if ($(this).is(":visible")) {
			if ($(this).attr('id') != 'career-summary-block') {
				let childNumber = $(this).find('.cv-child-elem').length;
				if (childNumber <= 0) {
					$(this).hide();
					$(this).remove();
				}
			}
		}
	});

	$('.medium-editor-placeholder').each(function() {
		if ($(this).is(":visible")) {
			if ($(this).parent().attr('id') == 'career-summary-block') {
				$(this).parent().hide();
				$(this).parent().remove();
			} else {
				let infoName = $(this).attr('info-name');
				if (infoName == 'skype' || infoName == 'facebook' || infoName == 'linkedin' || infoName == 'current_address') {
					$(this).closest('.social-container').hide();
					$(this).closest('.social-container').remove();
				}
				$(this).hide();
				$(this).remove();
			}
		}
	});

	if (editor.isActive)  {
		editor.destroy();
	} else {
		editor.setup();
	}

	//remove editor element when in render page
	$('.cv-block').on('mouseover', function () {
	   $(this).css('border', '2px solid transparent');
	});
	$('.cv-editable-elem').on('mouseover', function () {
	   $(this).css('border', '1px solid transparent');
	})
	$('.cv-section-event').removeClass('cv-section-event').css('position', 'relative');
	$('.cv-child-elem').removeClass('cv-child-elem').css('border', '1px solid transparent');
	$('.date-of-birth').css('border', '1px solid transparent');
	$('.skill-item, .language-item, .soft-skill-item, .date-of-birth').css('cursor', 'auto');
	$("[contenteditable='true']").attr('contenteditable', 'false');
	// $('* > :last-child').addClass('end-elem');
});