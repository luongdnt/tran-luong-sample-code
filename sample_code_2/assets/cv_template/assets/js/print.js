/**
 * Fill page blank
 * @param outerWrapperSelector
 * @param selector
 * @param arrMinHeight
 */
function fillBlankRenderPage(outerWrapperSelector, selector, arrMinHeight) {
    // get highest node
    var pageHeight = $('.' + outerWrapperSelector).height();
    // setMinPageHeight(selector, pageHeight, arrMinHeight, 10);
}

// Xóa các giá trị margin-bottom và padding-bottom
// Thay bằng margin-top và padding-top của các thẻ gần nhất phía dưới
function removeBottomGapForPrint(){     
    // Main wrapper
    $(".main-wrapper").attr("style", $(".main-wrapper").attr('style') + ";margin-bottom: 0px !important; padding-bottom: 0px !important");
    
    // Lấy thẻ section cuối cùng
    var lastSection = $('section:visible:last');    
    currCss = lastSection.attr('style');
    console.log(currCss);
    lastSection.attr('style', currCss + ';margin-bottom: 0px !important; padding-bottom: 0px !important');
    
    // Lấy các thẻ visibale có margin-bottom
    var arrHasMarginBtm = $('section:visible:last *:visible').filter(function() {
        return $(this).css('margin-bottom') > '0px';
    });
    // Add css clear margin-bottom và add margin-top vào thẻ ngay sau
    $(arrHasMarginBtm).each(function(index, value ) {
        mBottom = $(this).css('margin-bottom');
        mTop = $(this).css('margin-top');                     

        currCss = $(this).attr('style');
        css = $(this).nextAll("*:visible").first().attr('style');
        $(this).attr('style', currCss + ';margin-bottom: 0px !important');
        $(this).nextAll("*:visible").first().attr('style', css + ';margin-top: ' + mBottom + ' !important');                                                                  
    });
    // Lấy các thẻ visibale có padding-bottom
    var arrHasMarginBtm = $('section:visible:last *:visible').filter(function() {
        return $(this).css('padding-bottom') > '0px';
    });            
    // Add css clear padding-bottom và add padding-top vào thẻ ngay sau
    $(arrHasMarginBtm).each(function(index, value ) {
        mBottom = $(this).css('padding-bottom');
        mTop = $(this).css('padding-top');                     

        currCss = $(this).attr('style');
        css = $(this).nextAll("*:visible").first().attr('style');
        console.log($(this).attr("id"), $(this).nextAll("section:visible").first().attr("id"), css);
        $(this).attr('style', currCss + ';padding-bottom: 0px !important');
        $(this).nextAll("*:visible").first().attr('style', css + ';padding-top: ' + mBottom + ' !important');                                                                  
    });
}

function getMinHeightConfig(redundantHeaderHeight = 0){
    
    arrMinHeight = [1400 - redundantHeaderHeight, 
        2800 - redundantHeaderHeight, 
        4200 - redundantHeaderHeight, 
        5600 - redundantHeaderHeight, 
        7000 - redundantHeaderHeight];
    
    return arrMinHeight;
}

/**
 * Fill page blank theme 3+4 only
 * @param outerWrapperSelector
 * @param selector
 * @param arrMinHeight
 */
function fillBlankRenderPage(outerWrapperSelector, selector, redundantHeaderHeight = 0, adjustHeight = 0 ) {
    removeBottomGapForPrint();
    // Một số theme có phần header nên min height sẽ trừ đi phần header này
    arrMinHeight = getMinHeightConfig(redundantHeaderHeight);
    // get highest node
    var pageHeight = $('.' + outerWrapperSelector).height();    
    // Một số theme bị lệch chưa tìm đc nguyên nhân VD, page đo được là 1444 > 1400 nhưng in ra vẫn vừa 1 page, nên cần 
    // trừ đi khoảng 50 rồi mới so sánh với độ cao trang chuẩn 1400 để tính số page
    // nên cần trừ đi số thừa này để tránh bị thừa trang
    adjustedPageHeight = pageHeight + adjustHeight;    
    console.log(pageHeight);
    $('.cvo-right-side').css('padding-bottom','unset !important');
    $('.cvo-left-side').css('padding-bottom','unset !important');
    setMinPageHeight(selector, adjustedPageHeight, arrMinHeight);
}

/**
 * Set min page height for selectors
 * @param selector
 * @param pageHeight
 * @param arrMinHeight
 * @param minMod
 */
function setMinPageHeight(selector, pageHeight, arrMinHeight) {
    // calculate the number of pages
    let mod = pageHeight % 1420;
    let pageNo = parseInt(pageHeight / 1420);
    if (mod > 0) {
        pageNo = pageNo + 1;
    }
    // set page height
    $('.' + selector).css('min-height', arrMinHeight[pageNo - 1] + 'px');
}