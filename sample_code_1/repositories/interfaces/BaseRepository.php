<?php

namespace app\repositories\interfaces;

use mhndev\yii2Repository\Interfaces\iRepository;

interface BaseRepository extends iRepository
{
    /**
     * Find one row by conditions
     *
     * @param array $conditions
     * @return mixed
     */
    public function findOne($conditions = []);

    public function paginatePage(array $conditions, $page, $limit = 10, $order = [] ,$returnArray = true);

    /**
     * @param array $conditions
     * @param array $orders
     * @param array $relations
     * @return mixed
     */
    public function findAll($conditions = [], $orders = [], $relations = [], $column = ['*']);

    public function updateById($update_data, $id);
}