<?php

namespace app\repositories\interfaces;


interface ProjectMigrateRepository extends BaseRepository
{
    /**
     * @param $user_ids
     * @return mixed
     */
    public function countGroupByUser($user_ids);
}