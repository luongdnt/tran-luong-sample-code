<?php

namespace app\repositories\interfaces;


interface ProjectRepository extends BaseRepository
{
	const LAUNCHER_TYPE_IMAGE = 1;
	const LAUNCHER_TYPE_VIDEO = 2;
	const SHOW_LAUNCHER = 1;
	const NOT_SHOW_LAUNCHER = 0;
	const SHARED_PROJECT = 1;
	const SHOW_WORLD_MAP = 1;
	const NOT_SHOW_WORLD_MAP = 0;

	public function search($key, $page = 0, $filters = [], $limit = 8);

    public function highLightProject($type, $page, $limit = 8, $withArray = false);

}