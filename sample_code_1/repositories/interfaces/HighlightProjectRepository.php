<?php

namespace app\repositories\interfaces;


interface HighlightProjectRepository extends BaseRepository
{
    const HIGHLIGHT_TYPE_BEST = 1;
    const HIGHLIGHT_TYPE_AWESOME = 2;

}