<?php

namespace app\repositories\interfaces;


interface PasswordResetRepository extends BaseRepository
{
	const IS_VERIFIED = 1;
	const IS_NOT_VERIFIED = 0;
	//expired time (minutes)
	const EXPIRED_TIME = 30;
}