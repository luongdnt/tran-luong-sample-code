<?php

namespace app\repositories\interfaces;


interface UserRepository extends BaseRepository
{
    const STATUS_WAITING = 0;
    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const GENDER_UNDEFINED = 0;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    public function authorMostActive($relations = [], $limit = 6, $withArray = false);
}