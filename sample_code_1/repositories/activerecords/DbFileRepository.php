<?php

namespace app\repositories\activerecords;


use app\models\File;

class DbFileRepository extends DbBaseRepository
{
    public function __construct(File $model)
    {
        parent::__construct($model);
    }
}