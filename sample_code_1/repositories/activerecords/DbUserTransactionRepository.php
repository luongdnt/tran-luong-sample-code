<?php

namespace app\repositories\activerecords;


use app\models\UserTransaction;

class DbUserTransactionRepository extends DbBaseRepository
{
    public function __construct(UserTransaction $model)
    {
        parent::__construct($model);
    }
}