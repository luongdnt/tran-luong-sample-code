<?php

namespace app\repositories\activerecords;


use app\models\UserMigrate;
use app\repositories\interfaces\UserMigrateRepository;

class DbUserMigrateRepository extends DbBaseRepository implements UserMigrateRepository
{
    public function __construct(UserMigrate $model)
    {
        $this->model = $model;
    }
}