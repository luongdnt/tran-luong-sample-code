<?php

namespace app\repositories\activerecords;


use app\models\Contact;

class DbContactRepository extends DbBaseRepository
{
    public function __construct(Contact $model)
    {
        parent::__construct($model);
    }
}