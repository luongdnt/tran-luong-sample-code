<?php

namespace app\repositories\activerecords;


use app\models\ProjectMigrate;
use app\repositories\interfaces\ProjectMigrateRepository;
use yii\helpers\ArrayHelper;

class DbProjectMigrateRepository extends DbBaseRepository implements ProjectMigrateRepository
{
    public function __construct(ProjectMigrate $model)
    {
        $this->model = $model;
    }

    /**
     * @param $user_ids
     * @return array
     */
    public function countGroupByUser($user_ids)
    {
        $data = $this->model->find()->select(['user_id', 'COUNT(*) as total'])
            ->where(['user_id' => $user_ids])
            ->groupBy('user_id')
            ->createCommand()
            ->queryAll();
        return ArrayHelper::map($data, 'user_id', 'total');
    }
}