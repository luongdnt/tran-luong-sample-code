<?php

namespace app\repositories\activerecords;

use app\repositories\interfaces\BaseRepository;
use mhndev\yii2Repository\AbstractSqlArRepository;
use yii\db\ActiveRecord;

class DbBaseRepository extends AbstractSqlArRepository implements BaseRepository
{
    /**
     * Find one row by conditions
     *
     * @param array $conditions
     * @return null|static
     */
    public function findOne($conditions = [])
    {
        return $this->model->findOne($conditions);
    }

    public function paginatePage(array $conditions, $page, $limit = 10, $order = [] , $returnArray = true)
    {
        $query = $this->model->find()->where($conditions);
        $countQuery = clone $query;
        $total = (int)$countQuery->count();
        $models = $query->orderBy($order)->offset($page * $limit)->limit($limit);

        if ($returnArray) {
            $models = $models->asArray()->all();
        } else {
            $models = $models->all();
        }

        return [
            'total' => $total,
            'page' => (int)$page,
            'limit' => $limit,
            'is_last' => $page + 1 >= ceil($total/$limit) ? true : false,
            'data' => $models,
        ];
    }

    public function findAll($conditions = [], $orders = [], $relations = [], $column = ['*'])
    {
        $query = $this->model->find()->where($conditions)->select($column)->orderBy($orders);
        if ($relations) {
            $query->with($relations);
        }
        return $query->all();
    }

    /**
     * @param $update_data
     * @param $conditions
     * @return int
     */
    public function updateAll($update_data, $conditions)
    {
        return $this->model->updateAll($update_data, $conditions);
    }

    public function updateById($update_data, $id)
    {
        /* @var $model ActiveRecord */
        $model = $this->model->find()->where(['id' => $id])->one();
        return $model->updateAttributes($update_data);
    }
}