<?php

namespace app\repositories\activerecords;


use app\models\Scene;
use app\repositories\interfaces\SceneRepository;

class DbSceneRepository extends DbBaseRepository implements SceneRepository
{
    public function __construct(Scene $model)
    {
        parent::__construct($model);
    }

}