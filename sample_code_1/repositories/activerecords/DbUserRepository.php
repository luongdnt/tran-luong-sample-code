<?php

namespace app\repositories\activerecords;


use app\models\Project;
use app\models\User;
use yii\db\Expression;

class DbUserRepository extends DbBaseRepository
{
    private $project;

    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function authorMostActive($relations = [], $page = 0, $limit = 6, $withArray = false)
    {
        $query = Project::find()
            ->select([new Expression('SUM(project.view) * COUNT(project.id) as total_view, COUNT(project.id) as total_project'), 'project.user_id', 'user.username'])
            ->join('INNER JOIN', 'user', 'user.id = project.user_id')
            ->where(['project.sharing' => 1])
            ->groupBy('project.user_id');

        $countQuery = clone $query;
        $total = (int)$countQuery->count();

        $query = $query->orderBy('total_view DESC')
            ->offset($limit * $page)
            ->limit($limit)
            ->with($relations);

        if ($withArray) {
            $authors = $query->asArray(true)->all();
        } else {
            $authors = $query->all();
        }

        return [
            'total' => $total,
            'page' => (int)$page,
            'limit' => $limit,
            'is_last' => $page + 1 >= ceil($total/$limit) ? true : false,
            'data' => $authors,
        ];
    }

}