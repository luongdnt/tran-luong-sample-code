<?php

namespace app\repositories\activerecords;


use app\models\Project;
use app\repositories\interfaces\ProjectRepository;
use yii\data\Pagination;
use yii\db\Query;

class DbProjectRepository extends DbBaseRepository implements ProjectRepository
{

    const SHARE_PUBLIC = 1;
    const SHARE_LINK = 2;
    const SHARE_PRIVATE = 3;
    const SHARE_UNPUBLISH = 0;
    
    public function __construct(Project $model)
    {
        parent::__construct($model);
    }

    public function search($key, $page = 0, $filters = [], $limit = 8)
    {
        $query = $this->model
            ->find()
            ->joinWith(['authors']);

        $query->andFilterWhere(['=', 'project.sharing', 1]);

        foreach ($filters as $filter) {
            $query->andFilterWhere($filter);
        }

        $query->andFilterWhere([
            'or',
            ['like', 'project.title', $key],
            ['like', 'project.address', $key],
            ['like', 'project.description', $key],
            ['like', 'user.username', $key],
        ]);

        $query = $query->orderBy(['created_at' => SORT_DESC]);

        $countQuery = clone $query;
        $total = (int)$countQuery->count();

        $result = $query->offset($page * $limit)->limit($limit)->all();

        return [
            'total' => $total,
            'page' => (int)$page,
            'limit' => $limit,
            'is_last' => (int)$page + 1 >= ceil($total/$limit) ? true : false,
            'data' => $result,
        ];
    }

    public function highLightProject($type, $page, $limit = 8, $withArray = false)
    {
        $query = new Query();
        $query = $query
            ->select([
                'project.id',
                'project.title',
                'project.address',
                'project.view',
                'user.username',
                'user.email',
                'user.avatar_url',
                'project.thumb_path'
            ])
            ->from('project')
            ->join('LEFT OUTER JOIN', 'highlight_project',
                'highlight_project.project_id = project.id')
            ->join('LEFT OUTER JOIN', 'user',
                'project.user_id = user.id')
            ->where([
                'and',
                ['=', 'highlight_project.type', 2],
                ['=', 'project.sharing', 1]
            ])
            ->orderBy(['project.created_at' => SORT_DESC]);

        $countQuery = clone $query;
        $total = (int)$countQuery->count();

        $result = $query->offset($page * $limit)->limit($limit)->all();

        return [
            'total' => $total,
            'page' => (int)$page,
            'limit' => $limit,
            'is_last' => (int)$page + 1 >= ceil($total/$limit) ? true : false,
            'data' => $result,
        ];

    }

}