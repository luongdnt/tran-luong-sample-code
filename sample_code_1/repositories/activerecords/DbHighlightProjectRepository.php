<?php

namespace app\repositories\activerecords;


use app\models\HighlightProject;
use app\repositories\interfaces\HighlightProjectRepository;

class DbHighlightProjectRepository extends DbBaseRepository implements HighlightProjectRepository
{
    public function __construct(HighlightProject $model)
    {
        parent::__construct($model);
    }
}