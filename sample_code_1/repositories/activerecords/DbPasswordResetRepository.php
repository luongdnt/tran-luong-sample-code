<?php

namespace app\repositories\activerecords;


use app\models\PasswordReset;

class DbPasswordResetRepository extends DbBaseRepository
{
    public function __construct(PasswordReset $model)
    {
        parent::__construct($model);
    }
}