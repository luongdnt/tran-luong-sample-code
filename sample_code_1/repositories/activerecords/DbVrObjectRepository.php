<?php

namespace app\repositories\activerecords;


use app\models\VrObject;

class DbVrObjectRepository extends DbBaseRepository
{
    public function __construct(VrObject $model)
    {
        parent::__construct($model);
    }
}