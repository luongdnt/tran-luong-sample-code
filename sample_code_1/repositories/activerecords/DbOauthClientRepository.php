<?php

namespace app\repositories\activerecords;


use app\models\OauthClient;

class DbOauthClientRepository extends DbBaseRepository
{
    public function __construct(OauthClient $model)
    {
        parent::__construct($model);
    }
}