<?php

namespace app\repositories\activerecords;


use app\models\Plan;

class DbPlanRepository extends DbBaseRepository
{
    public function __construct(Plan $model)
    {
        parent::__construct($model);
    }
}