<?php

namespace app\repositories\activerecords;


use app\models\Hotspot;
use app\repositories\interfaces\HotspotRepository;

class DbHotspotRepository extends DbBaseRepository implements HotspotRepository
{
    public function __construct(Hotspot $model)
    {
        parent::__construct($model);
    }

}