<?php

namespace app\repositories\activerecords;


use app\models\UserAccount;

class DbUserAccountRepository extends DbBaseRepository
{
    public function __construct(UserAccount $model)
    {
        parent::__construct($model);
    }
}