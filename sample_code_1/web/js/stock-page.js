function stock_multi(){

    var n = 0;
    $('.stock-tour-item').find('.multi-edit-button').on('click', function(){
        n = $(this).index();
        $('.multi-edit-button').hide();
        $('.pano-img').hide();
        $('.multi-edit-step1').show();
        $('.pano-info strong').css({paddingTop:'89px', marginTop:0});
        //console.log(n);
    });

}
stock_multi();

function melti_step2(){

    $('.pano-info, .multi-edit-step1').hide();
    $('.multi-edit-step2').show();

    console.log($(this));
}

function stock_submit(){
    $('.stock_submit_btn').on("click", function(){
        $('.stock-modal-container, .stock-modal1').show();
    });
    $('.stock-modal-head img').on("click", function(){
        $('.stock-modal-container, .stock-modal1, .stock-modal2, .stock-modal3, .stock-modal4, .stock-modal5').hide();
    });
    $('.prev1').on('click', function(){
        $('.stock-modal2').hide();
        $('.stock-modal1').show();
    });
    $('.prev2').on('click', function(){
        $('.stock-modal3').hide();
        $('.stock-modal2').show();
    });
    $('.prev3').on('click', function(){
        $('.stock-modal4').hide();
        $('.stock-modal3').show();
    });
    $('.prev4').on('click', function(){
        $('.stock-modal5').hide();
        $('.stock-modal4').show();
    });
    $('.next-btn').on('click', function(){
        $('.stock-modal1').hide();
        $('.stock-modal2').show();
    });
    $('.next1').on('click', function(){
        $('.stock-modal2').hide();
        $('.stock-modal3').show();
    });
    $('.next2').on('click', function(){
        $('.stock-modal3').hide();
        $('.stock-modal4').show();
    });
    $('.next3').on('click', function(){
        $('.stock-modal4').hide();
        $('.stock-modal5').show();
    });

}
stock_submit();

function stock_item(){

    var isplay = true;

    $('.pano-item').on('click', function(){

        $('.pano-checkbox').css('border','1px solid #fff');
        $('.checkbox').hide();

        $(this).children('.pano-checkbox').css('border','1px solid #00aeef');
        $(this).find('.checkbox').show();
    })

    $('.stock-tour-items').on("click", function(){
        $(this).parent().children('.stock-pano-list').slideToggle();
        if(isplay){
            $(this).children('.pano-btn').children('img').css({transform:"rotate(180deg)"},3000);
        }else{
            $(this).children('.pano-btn').children('img').css({transform:"rotate(0)"},3000);
        }
        isplay = !isplay;        
    });
}
stock_item();



















