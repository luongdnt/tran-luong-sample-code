// window.fbAsyncInit = function() {
//     FB.init({
//         // appId      : '1301252590050529',
//         appId      : face_book_app_id,
//         cookie     : true,  // enable cookies to allow the server to access
//                             // the session
//         xfbml      : true,  // parse social plugins on this page
//         version    : 'v3.3' // The Graph API version to use for the call
//     });
//
//     // FB.AppEvents.logPageView();
//
// };

// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function statusChangeCallback(response) {
    console.log('response', response);
    // return;
    if (response.status === 'connected') {
        var token = response.authResponse.accessToken;
        FB.api('/me', {fields: 'name, email'}, function(response) {
            console.log('Good to see you',response);
            $.ajax({
                url: '/oauth-login/facebook',
                dataType: 'json',
                data: {
                    token : token,
                    _csrf: yii.getCsrfToken(),
                },
                type: 'post',
                success: function (res) {
                    if (res.success) {
                        window.location.href = '/';
                    }

                }, error: function (res) {
                    alert('Error');
                }
            });
        });
    }
}

$('#login-fb').click(function (e) {
    e.preventDefault();
    FB.login(function(response) {
        statusChangeCallback(response);
    }, { scope: 'email', return_scopes: true });

    // FB.getLoginStatus(function(response) {
    //     console.log(response);
    //     statusChangeCallback(response);
    // });
});

// login gg

var googleUser = {};

// var startApp = function() {
//     gapi.load('auth2', function(){
//         // Retrieve the singleton for the GoogleAuth library and set up the client.
//         auth2 = gapi.auth2.init({
//             client_id: '4770862919-eeebmsi9je23amsl42eg7d3g33nifg24.apps.googleusercontent.com',
//             cookiepolicy: 'http://localhost:8000/',
//         });
//         attachSignin(document.getElementById('login-gg'));
//     });
// };

function attachSignin(element) {
    auth2.attachClickHandler(element, {},
        function(googleUser) {
            // console.log(googleUser.getAuthResponse().id_token);
            // return;
            $.ajax({
                url: '/oauth-login/google',
                dataType: 'json',
                data: {
                    token : googleUser.getAuthResponse().id_token,
                    _csrf: yii.getCsrfToken(),
                },
                type: 'post',
                success: function (res) {
                    if (res.success) {
                        window.location.href = '/';
                    }

                }, error: function (res) {
                    alert('Error');
                }
            });

        }, function(error) {
            console.log('error', error);
        });
}

startApp();

// --------------------naver-----------------

// var naver_id_login = new naver_id_login("SdhYTOobFI8FmSwOne_w", "http://localhost:8000/site/login");
// var state = naver_id_login.getUniqState();
// naver_id_login.setButton("white", 2,40);
// naver_id_login.setDomain("/");
// naver_id_login.setState(state);
// naver_id_login.setPopup();
// naver_id_login.init_naver_id_login();

// console.log(naver_id_login.oauthParams.access_token);
// // 네이버 사용자 프로필 조회
// naver_id_login.get_naver_userprofile("naverSignInCallback()");
// // 네이버 사용자 프로필 조회 이후 프로필 정보를 처리할 callback function
// function naverSignInCallback() {
    // var access_token = naver_id_login.oauthParams.access_token;

    // console.log(access_token);
    // $.ajax({
    //     url: '/oauth-login/naver',
    //     dataType: 'json',
    //     data: {
    //         token: access_token,
    //         _csrf: yii.getCsrfToken(),
    //     },
    //     type: 'post',
    //     success: function (res) {
    //         if (res.success) {
    //             // window.location.href = '/';
    //             window.close();
    //             window.onunload = refreshParent;
    //             function refreshParent() {
    //                 window.opener.location.href = '/';
    //             }
    //         }
    //     }, error: function (res) {
    //         alert('Error');
    //     }
    // });
// }
