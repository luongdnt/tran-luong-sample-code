function krpanoplugin() {
    var local = this;

    var krpano = null;
    var plugin = null;

    local.registerplugin = function (krpanointerface, pluginpath, pluginobject) {
        krpano = krpanointerface;
        plugin = pluginobject;

        plugin.dosomething = action_dosomething;

        plugin.registercontentsize(200, 200);
        if (annyang) {
            // Add our commands to annyang
            annyang.addCommands({
                'up': handlerUpCommand,
                'down': handlerDownCommand,
                'left': handlerLeftCommand,
                'right': handlerRightCommand,
            });

            // Tell KITT to use annyang
            SpeechKITT.annyang();

            var baseUrl = krpano.get('player_url');
            // Define a stylesheet for KITT to use
            SpeechKITT.setStylesheet(baseUrl + '/plugins/speechrecognition.css');

            // Render KITT's interface
            SpeechKITT.vroom();
        }
    };

    local.unloadplugin = function () {
        plugin = null;
        krpano = null;
    };

    local.onresize = function (width, height) {
        return false;
    };

    function handlerUpCommand() {
        krpano.call("set(vlookat_moveforce,+2)");
    }
    function handlerDownCommand() {
        krpano.call("set(vlookat_moveforce,-2)");
    }
    function handlerLeftCommand() {
        krpano.call("set(hlookat_moveforce,-2)");
    }
    function handlerRightCommand() {
        krpano.call("set(hlookat_moveforce,+2)");
    }

    function action_dosomething() {
        // trace the given action arguments
        krpano.trace(1, "dosomething() was called with " + arguments.length + " arguments:");
        for (var i = 0; i < arguments.length; i++)
            krpano.trace(1, "arguments[" + i + "]=" + arguments[i]);

        // trace some infos
        krpano.trace(1, "mode=" + plugin.mode);
        krpano.trace(1, "lookat=" + krpano.view.hlookat + " / " + krpano.view.vlookat);

        // call krpano actions
        plugin.accuracy = 1;    // disable grid fitting for smoother size changes
        krpano.call("tween(width|height, 500|100)", plugin);
        krpano.call("lookto(0,0,150); wait(1.0); lookto(90,0,90);");
        krpano.call("tween(width|height, 200|200)", plugin);
    }
}