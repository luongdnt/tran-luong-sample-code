var krpanoplugin = function()
{
    var local = this,
        krpano = null, plugin = null,

        onDblClick, supportLayer;


    ////////////////////////////////////////////////////////////
    // plugin management

    local.registerplugin = function(krpanointerface, pluginpath, pluginobject)
    {
        krpano = krpanointerface;
        plugin = pluginobject;

        if (krpano.version < "1.0.8.14" || krpano.build < "2011-03-30")
        {
            krpano.trace(3,"dblclick plugin - too old krpano version (min. 1.0.8.14)");
            return;
        }

        // register attributes
        plugin.registerattribute("ondblclick","", function(arg){ onDblClick = arg }, function(){ return onDblClick; });

        plugin.registerattribute("supportdbclick","", function(arg){ supportLayer = arg }, function(){ return supportLayer; });

        // register eventlistener

        console.log(plugin.getfullpath());

        krpano.control.layer.addEventListener("dblclick",  handleDblClick, true);
    };


    local.unloadplugin = function()
    {
        krpano.control.layer.removeEventListener("dblclick",  handleDblClick);

        plugin = null;
        krpano = null;
    };


    ////////////////////////////////////////////////////////////
    // public methods


    ////////////////////////////////////////////////////////////
    // private methods

    function handleDblClick(event)
    {
        if (supportLayer) {
            krpano.call( onDblClick );
        }
    }
};