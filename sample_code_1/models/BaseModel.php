<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii2tech\ar\softdelete\SoftDeleteQueryBehavior;

/**
 * This is the model class for table "promotion_used".
 *
 * @property int $id
 * @property string $user_id
 * @property int $promotion_id
 * @property string $created_at
 */
class BaseModel extends ActiveRecord
{
    protected $timestamp = true;
    protected $softDelete = false;

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [];
        if ($this->timestamp) {
            $behaviors['timestamp'] = [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ];
        }
        if ($this->softDelete) {
            $behaviors['softDeleteBehavior'] = [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_at' => new Expression('NOW()')
                ],
                'replaceRegularDelete' => true
            ];
        }
        return $behaviors;
    }

    /**
     * @return \yii\db\ActiveQuery|SoftDeleteQueryBehavior
     */
    public static function find()
    {
        $query = parent::find();
//        $query->attachBehavior('softDelete', [
//            'class' => SoftDeleteQueryBehavior::class,
//            'deletedCondition' =>
//                ['!=', 'deleted_at', new Expression('null')]
//            ,
//            'notDeletedCondition' => [
//                'deleted_at' => null,
//            ],
//        ]);
//        return $query->notDeleted();
        return $query;
    }

    public function formName()
    {
        return '';
    }
}
