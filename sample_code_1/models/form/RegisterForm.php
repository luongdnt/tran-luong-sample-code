<?php

namespace app\models\form;

use app\components\HashHelper;
use app\repositories\interfaces\UserRepository;
use yii\base\Model;

class RegisterForm extends Model
{

    public $firstName;
    public $lastName;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            [['firstName', 'lastName'], 'string', 'max' => 255],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     * @throws \yii\base\Exception
     */
    public function register()
    {
        if (!$this->validate()) {
            return null;
        }

        /* @var $userRepository UserRepository */
        $userRepository = \Yii::$container->get('UserRepository');

        $user = $userRepository->create([
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'email' => $this->email,
            'status' => UserRepository::STATUS_ENABLE,
            'password' => HashHelper::hashUserPassword($this->password),
            'auth_key' => \Yii::$app->security->generateRandomString()
        ]);

        return $user;
    }

}