<?php


namespace app\models\form;


class TextHotspotForm
{
    const ALPHA_DEFAULT = 100;
    const COLOR_TXT_DEFAULT = "000000";
    public static $fontFamiliesSupported = [
        0 => 'disabled',
        1 => 'Nanum Gothic',
        2 => 'Nanum Gothic Bold',
        3 => 'Nanum Barun Gothic',
        4 => 'Nanum Barun Gothic Bold',
    ];
}