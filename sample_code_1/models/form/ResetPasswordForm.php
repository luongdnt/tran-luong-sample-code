<?php

namespace app\models\form;


use app\models\User;
use app\repositories\interfaces\PasswordResetRepository;
use app\repositories\interfaces\UserRepository;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use Yii;

class ResetPasswordForm extends Model
{

    public $password;
    public $retype_password;
    public $token;

    /**
     * @var resetRecord
     */
    private $resetRecord;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var PasswordResetRepository
     */
    protected $passwordResetRepository;

    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidArgumentException('Password reset token cannot be blank.');
        }

        $this->userRepository = \Yii::$container->get('UserRepository');
        $this->passwordResetRepository = \Yii::$container->get('PasswordResetRepository');

        $this->resetRecord = $this->passwordResetRepository->findOne(['token' => $token]);
        if (empty($this->resetRecord)) {
            throw new InvalidArgumentException('Your reset request does not exist.');
        }

        if ($token !== $this->resetRecord->token) {
            throw new InvalidArgumentException('Password reset token is not valid.');
        }

        parent::__construct($config);

    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['retype_password', 'required'],
            ['token', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     * @throws \yii\base\Exception
     */
    public function resetPassword()
    {
        $user = $this->userRepository->findOne(['email' => $this->resetRecord->email, 'status' => UserRepository::STATUS_ENABLE]);
        if (!$user) {
            throw new InvalidArgumentException('User does not exist.');
        }

        $newPassword = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        
        $result = $user->updateAttributes(['password' => $newPassword]);

        $this->resetRecord->updateAttributes(['is_verify' => PasswordResetRepository::IS_VERIFIED, 'updated_at' => date('Y-m-d H:i:s')]);

        return $result;
    }
}