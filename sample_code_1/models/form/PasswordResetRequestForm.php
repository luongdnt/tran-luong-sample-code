<?php

namespace app\models\form;

use app\models\User;
use app\models\PasswordReset;
use app\repositories\interfaces\PasswordResetRepository;
use app\repositories\interfaces\UserRepository;
use app\models\SendMailForgotPasswordJob;
use Yii;
use yii\base\Model;

class PasswordResetRequestForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\app\models\User',
                'filter' => ['status' => UserRepository::STATUS_ENABLE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $userRepository UserRepository */
        $userRepository = Yii::$container->get('UserRepository');
        /* @var $passwordResetRepository PasswordResetRepository */
        $passwordResetRepository = Yii::$container->get('PasswordResetRepository');

        /* @var $user User */
        $user = $userRepository->findOne([
            'status' => UserRepository::STATUS_ENABLE,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }

        $token = Yii::$app->security->generateRandomString();
        $passwordResetRepository->create([
            'email' => $this->email,
            'token' => $token,
            'expired_time' => date('Y-m-d H:i:s', strtotime("+" . PasswordResetRepository::EXPIRED_TIME . " minutes")),
            'created_at' => date('Y-m-d H:i:s')
        ]);

        Yii::$app->queue->push(new SendMailForgotPasswordJob([
            'toMail' => $user->email,
            'token' => $token,
        ]));
    }

}