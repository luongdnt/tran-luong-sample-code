<?php


namespace app\models\form;


class PolygonHotspotForm
{
    const NONE_ACTION_TYPE = 0;
    const TELEPORT_ACTION_TYPE = 1;
    const PHOTO_ACTION_TYPE = 2;
    const LINK_ACTION_TYPE = 3;
    const TEXT_ACTION_TYPE = 4;

    public $supportActionType = [
        self::NONE_ACTION_TYPE => 'None',
        self::TELEPORT_ACTION_TYPE => 'Teleport',
        self::PHOTO_ACTION_TYPE => 'VR Photo',
        self::LINK_ACTION_TYPE => 'Web Link',
        self::TEXT_ACTION_TYPE => 'Text Writing',
    ];
}