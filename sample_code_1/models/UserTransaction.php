<?php

namespace app\models;

use Yii;

class UserTransaction extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    protected $softDelete = false;

    protected $timestamp = false;

    public static function tableName()
    {
        return 'user_transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'value'], 'integer'],
            [['transaction_id', 'type'], 'string'],
            [['updated_at', 'created_at', 'transaction_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User Id',
            'type' => 'Type',
            'value' => 'Value',
            'transaction_time' => 'Transaction Time',
            'details' => 'Details',
            'created_at' => 'Created At',
        ];
    }

}
