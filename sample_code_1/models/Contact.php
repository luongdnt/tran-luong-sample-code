<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $email
 * @property string $full_name
 * @property string $phone_number
 * @property string $title
 * @property string $content
 * @property int $status 0: unread; 1: read
 * @property string $updated_at
 * @property string $created_at
 * @property string $deleted_at
 */
class Contact extends BaseModel
{
    protected $softDelete = true;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'phone_number', 'content', 'company_name', 'full_name', 'title'], 'required'],
            [['email'], 'email'],
            [['content'], 'string'],
            [['status'], 'integer'],
            [['updated_at', 'created_at', 'deleted_at'], 'safe'],
            [['email', 'full_name'], 'string', 'max' => 150],
            [['phone_number'], 'string', 'max' => 15],
            [['title','company_name'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'company_name' => 'Company name',
            'full_name' => 'Full Name',
            'phone_number' => 'Phone Number',
            'title' => 'Title',
            'content' => 'Content',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
        ];
    }
}
