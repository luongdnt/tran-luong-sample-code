<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "oauth_client".
 *
 * @property string $id
 * @property string $name
 * @property string $secret_key
 * @property string $redirect_url
 * @property string $created_at
 * @property string $updated_at
 */
class OauthClient extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'oauth_client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['id'], 'string', 'max' => 20],
            [['name'], 'string', 'max' => 100],
            [['secret_key', 'redirect_url'], 'string', 'max' => 250],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'secret_key' => 'Secret Key',
            'redirect_url' => 'Redirect Url',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
