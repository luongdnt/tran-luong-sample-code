<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "password_reset".
 *
 * @property string $email
 * @property string $token
 * @property string $created_at
 */
class PasswordReset extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'password_reset';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'token'], 'required'],
            [['created_at', 'expired_time'], 'safe'],
            [['email', 'token'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'token' => 'Token',
            'created_at' => 'Created At',
            'expired_time' => 'Expired time'
        ];
    }
}
