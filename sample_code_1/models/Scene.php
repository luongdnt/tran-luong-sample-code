<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "scene".
 *
 * @property int $id
 * @property string $project_id
 * @property string $title
 * @property string $file_id
 * @property int $type 0: 360 image
 * @property double $hfov
 * @property double $vfov
 * @property double $voffset
 * @property int $multires
 * @property double $multiresthreshold
 * @property int $titlesize
 * @property int $baseindex
 * @property string $cubelabels
 * @property string $cubetrip_striporder
 * @property int $stereo
 * @property string $stereolabels
 * @property string $stereoformat
 * @property string $prealign
 * @property string $bg_music_file_id
 * @property string $metadata
 * @property string $updated_at
 * @property string $created_at
 * @property string $deleted_at
 * @property $image bool|string
 * @property $file File
 * @property string $thumb
 */
class Scene extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'scene';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'file_id', 'type', 'multires', 'titlesize', 'baseindex', 'stereo', 'bg_music_file_id'], 'integer'],
            [['hfov', 'vfov', 'voffset', 'multiresthreshold'], 'number'],
            [['metadata'], 'string'],
            [['updated_at', 'created_at', 'deleted_at'], 'safe'],
            [['title', 'cubelabels', 'cubetrip_striporder', 'stereolabels', 'stereoformat', 'prealign'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'project_id' => Yii::t('app', 'Project ID'),
            'title' => Yii::t('app', 'Title'),
            'file_id' => Yii::t('app', 'File ID'),
            'type' => Yii::t('app', '0: 360 image'),
            'hfov' => Yii::t('app', 'Hfov'),
            'vfov' => Yii::t('app', 'Vfov'),
            'voffset' => Yii::t('app', 'Voffset'),
            'multires' => Yii::t('app', 'Multires'),
            'multiresthreshold' => Yii::t('app', 'Multiresthreshold'),
            'titlesize' => Yii::t('app', 'Titlesize'),
            'baseindex' => Yii::t('app', 'Baseindex'),
            'cubelabels' => Yii::t('app', 'Cubelabels'),
            'cubetrip_striporder' => Yii::t('app', 'Cubetrip Striporder'),
            'stereo' => Yii::t('app', 'Stereo'),
            'stereolabels' => Yii::t('app', 'Stereolabels'),
            'stereoformat' => Yii::t('app', 'Stereoformat'),
            'prealign' => Yii::t('app', 'Prealign'),
            'bg_music_file_id' => Yii::t('app', 'Bg Music File ID'),
            'metadata' => Yii::t('app', 'Metadata'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::class, ['id' => 'file_id']);
    }

    /**
     * @return bool
     */
    public function getImage()
    {
        return !empty($this->file->public_path) ? $this->file->public_path : false;
    }

    public function getThumb()
    {
        return !empty($this->file->thumb_path) ? $this->file->thumb_path : false;
    }

    /**
     * @return bool
     */
    public function getCubePath()
    {
        if (!empty($this->file->cube_path)) {
            return $this->file->cube_path;
        } elseif (!empty($this->file->public_path)) {
            $path_info = pathinfo($this->file->public_path);
            return $path_info['dirname'] . '/' . $path_info['filename'];
        }
        return false;
    }
}
