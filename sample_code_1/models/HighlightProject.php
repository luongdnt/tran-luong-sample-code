<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "highlight_project".
 *
 * @property int $id
 * @property int $project_id
 * @property int $type 1: best vr tour, 2: new awesome
 * @property int $order_number
 * @property string $updated_at
 * @property string $created_at
 * @property Project $project
 */
class HighlightProject extends BaseModel
{
    protected $softDelete = false;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'highlight_project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'type', 'order_number'], 'integer'],
            [['updated_at', 'created_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'type' => 'Type',
            'order_number' => 'Order Number',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }
}
