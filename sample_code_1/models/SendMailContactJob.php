<?php

namespace app\models;

use app\components\SendMailHelper;
use yii\base\BaseObject;

class SendMailContactJob extends BaseObject implements \yii\queue\JobInterface
{
    public $data;
    public $toMail;

    public function execute($queue)
    {
        SendMailHelper::sendMail($this->data, $this->toMail);
    }
}