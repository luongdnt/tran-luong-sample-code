<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxSize' => 1024 * 1024 * 4]
        ];
    }

    public function upload($name, $folder)
    {
        if ($this->validate()) {
            $name = $name . '-' . Yii::$app->formatter->asTimestamp(date('Y-d-m h:i:s'));

            $url = 'uploads/' . $folder . '/' . $name . '.' . $this->imageFile->extension;

            $this->imageFile->saveAs($url);

            return [
                'success' => 1,
                'data' => '/' . $url
            ];
        } else {
            return [
                'success' => 0,
                'msg' => $this->getFirstError('imageFile')
            ];
        }
    }
}