<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property int $id
 * @property string $user_id
 * @property string $title
 * @property string $alias
 * @property string $description
 * @property int $type
 * @property double $lat
 * @property double $lng
 * @property string $address
 * @property string $google_analytic_code
 * @property int $view
 * @property int $like
 * @property int $share_number
 * @property int $sharing
 * @property string $logo_path
 * @property string $launcher_path
 * @property int $show_title
 * @property int $show_location
 * @property int $show_author
 * @property int $show_logo
 * @property int $show_launcher
 * @property string $metadata
 * @property int $auto_rotation
 * @property string $bg_music_path
 * @property int $bg_music
 * @property string $updated_at
 * @property string $created_at
 * @property string $deleted_at
 */
class Project extends \yii\db\ActiveRecord
{
    protected $softDelete = true;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'type', 'view', 'like', 'share_number', 'sharing', 'show_title', 'show_location', 'show_author', 'show_logo', 'show_launcher', 'auto_rotation', 'bg_music'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['metadata'], 'string'],
            [['updated_at', 'created_at', 'deleted_at'], 'safe'],
            [['title'], 'string', 'max' => 45],
            [['alias'], 'string', 'max' => 150],
            [['description', 'google_analytic_code'], 'string', 'max' => 500],
            [['address', 'logo_path', 'launcher_path'], 'string', 'max' => 250],
            [['bg_music_path'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'title' => Yii::t('app', 'Title'),
            'alias' => Yii::t('app', 'Alias'),
            'description' => Yii::t('app', 'Description'),
            'type' => Yii::t('app', 'Type'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
            'address' => Yii::t('app', 'Address'),
            'google_analytic_code' => Yii::t('app', 'Google Analytic Code'),
            'view' => Yii::t('app', 'View'),
            'like' => Yii::t('app', 'Like'),
            'share_number' => Yii::t('app', 'Share Number'),
            'sharing' => Yii::t('app', 'Sharing'),
            'logo_path' => Yii::t('app', 'Logo Path'),
            'launcher_path' => Yii::t('app', 'Launcher Path'),
            'show_title' => Yii::t('app', 'Show Title'),
            'show_location' => Yii::t('app', 'Show Location'),
            'show_author' => Yii::t('app', 'Show Author'),
            'show_logo' => Yii::t('app', 'Show Logo'),
            'show_launcher' => Yii::t('app', 'Show Launcher'),
            'metadata' => Yii::t('app', 'Metadata'),
            'auto_rotation' => Yii::t('app', 'Auto Rotation'),
            'bg_music_path' => Yii::t('app', 'Bg Music Path'),
            'bg_music' => Yii::t('app', 'Bg Music'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    public function getAuthors()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return bool
     */
    public function isPublic()
    {
        return $this->sharing == 1 || $this->sharing == 2 ? true : false;
    }
}
