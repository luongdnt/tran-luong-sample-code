<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plan".
 *
 * @property int $id
 * @property string $name
 * @property string $price_kr
 * @property string $price_us
 * @property string $price_detail json encode detail price config.
 * @property int $storage
 * @property int $num_of_tour
 * @property int $max_photo_size
 * @property int $max_video_size
 * @property int $custom_brand 0: cannot custom brand; 1: enable custom branch
 * @property int $advertisement 0: no adv; 1: has adv
 * @property int $vr_tour_download 0: cannot download; 1: can download
 * @property int $vr_project_3rd_party 0: disabled; 1: enabled
 * @property string $hotspots json_encode list available hotspot. Empty: available all.
 * @property string $updated_at
 * @property string $created_at
 * @property string $deleted_at
 */
class Plan extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price_kr', 'price_us'], 'number'],
            [['storage', 'num_of_tour', 'max_photo_size', 'max_video_size', 'custom_brand', 'advertisement', 'vr_tour_download', 'vr_project_3rd_party'], 'integer'],
            [['updated_at', 'created_at', 'deleted_at', 'hotspots'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['name', 'price_kr', 'price_us'], 'required'],
            [['price_detail'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price_kr' => 'Price Kr',
            'price_us' => 'Price Us',
            'price_detail' => 'Price Detail',
            'storage' => 'Storage',
            'num_of_tour' => 'Num Of Tour',
            'max_photo_size' => 'Max Photo Size',
            'max_video_size' => 'Max Video Size',
            'custom_brand' => 'Custom Brand',
            'advertisement' => 'Advertisement',
            'vr_tour_download' => 'Vr Tour Download',
            'vr_project_3rd_party' => 'Vr Project 3rd Party',
            'hotspots' => 'Hotspots',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
        ];
    }

    public function getHotspotDropdown()
    {
        return [
            'text' => 'Text',
            'teleport' => 'Teleport',
            'product' => 'Product',
            'video' => 'Video',
            'info_box' => 'Info box',
            'call_number' => 'Call number',
            'image' => 'Image',
            'audio' => 'Audio',
            'gallery' => 'Gallery',
            'web_link' => 'Web link',
            'floor_design' => 'Floor design',
            '3D_audio' => '3D audio',
            'embed_code' => 'Web embeding code',
            'polygon' => 'Polygon'
        ];
    }

    public function getSelectedHotspot()
    {
        $items = explode(',', $this->hotspots);
        $data = [];
        foreach ($items as $key) {
            $data[$key] = [
                'selected' => true
            ];
        }

        return $data;
    }

    public function storageVal()
    {
        return $this->storage  / (1024*1024) . 'Gb';
    }
}
