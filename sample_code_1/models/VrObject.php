<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vr_object".
 *
 * @property int $id
 * @property string $user_id
 * @property string $file_ids List file id, separator by ,
 * @property string $thumb_path
 * @property string $created_at
 * @property string $updated_at
 */
class VrObject extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vr_object';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['file_ids', 'thumb_path'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'file_ids' => 'File Ids',
            'thumb_path' => 'Thumb Path',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
