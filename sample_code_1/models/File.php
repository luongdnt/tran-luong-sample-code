<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "file".
 *
 * @property int $id
 * @property string $folder_id
 * @property string $user_id
 * @property string $name
 * @property string $storage_path
 * @property string $public_path
 * @property string $thumb_path
 * @property string $cube_path
 * @property int $size
 * @property string $original_name
 * @property int $type 1: image; 2: video; 3: audio
 * @property int $show_on_library 1: show on library
 * @property string $library_type panorama | icon
 * @property int $is_pano 1: panorama
 * @property int $cube_converted 1: converted to cube
 * @property int $multires 1: converted to cube
 * @property string $updated_at
 * @property string $created_at
 * @property string $deleted_at
 */
class File extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['folder_id', 'user_id', 'size', 'type', 'show_on_library', 'is_pano', 'cube_converted', 'multires'], 'integer'],
            [['updated_at', 'created_at', 'deleted_at'], 'safe'],
            [['name', 'original_name'], 'string', 'max' => 150],
            [['storage_path', 'public_path', 'thumb_path', 'cube_path'], 'string', 'max' => 250],
            [['library_type'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'folder_id' => Yii::t('app', 'Folder ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Name'),
            'storage_path' => Yii::t('app', 'Storage Path'),
            'public_path' => Yii::t('app', 'Public Path'),
            'thumb_path' => Yii::t('app', 'Thumb Path'),
            'size' => Yii::t('app', 'Size'),
            'original_name' => Yii::t('app', 'Original Name'),
            'type' => Yii::t('app', '1: image; 2: video; 3: audio'),
            'show_on_library' => Yii::t('app', '1: show on library'),
            'library_type' => Yii::t('app', 'panorama | icon'),
            'is_pano' => Yii::t('app', '1: panorama'),
            'cube_converted' => Yii::t('app', '1: converted to cube'),
            'multires' => Yii::t('app', '1: converted to cube'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }
}
