<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hotspot".
 *
 * @property int $id
 * @property string $scene_id
 * @property string $project_id
 * @property string $name
 * @property int $type
 * @property int $style_id
 * @property string $url
 * @property string $html
 * @property string $css
 * @property double $scale
 * @property int $width
 * @property int $height
 * @property double $alpha
 * @property double $ath
 * @property double $atv
 * @property string $edge
 * @property double $rx
 * @property double $ry
 * @property double $rz
 * @property double $ox
 * @property double $oy
 * @property int $depth
 * @property int $zorder
 * @property int $distored
 * @property int $capture
 * @property int $keep
 * @property int $hand_cursor
 * @property int $mask_children
 * @property int $mip_mapping
 * @property string $bg_color
 * @property string $color_border
 * @property string $color_shadow
 * @property string $color_text
 * @property double $round_border_all
 * @property double $round_border_right_top
 * @property double $round_border_right_bottom
 * @property double $round_border_left_top
 * @property double $round_border_left_bottom
 * @property int $bg_shadow_x_offset
 * @property int $bg_shadow_y_offset
 * @property int $bg_shadow_blur
 * @property int $txt_shadow_x_offset
 * @property int $txt_shadow_y_offset
 * @property int $txt_shadow_blur
 * @property int $padding_all
 * @property int $padding_top
 * @property int $padding_right
 * @property int $padding_bottom
 * @property int $padding_left
 * @property int $over_sampling
 * @property int $vcenter
 * @property int $word_wrap
 * @property int $change_width_background
 * @property int $merged_alpha
 * @property string $metadata
 * @property string $updated_at
 * @property string $created_at
 * @property string $deleted_at
 * @property double $rotate
 */
class Hotspot extends \yii\db\ActiveRecord
{

    const TYPE_TEXT = 1;
    const TYPE_TELEPORT = 2;
    const TYPE_PRODUCT = 3;
    const TYPE_VIDEO = 4;
    const TYPE_INFO_BOX = 5;
    const TYPE_CALL_NUMBER = 6;
    const TYPE_IMAGE = 7;
    const TYPE_AUDIO = 8;
    const TYPE_GALLERY = 9;
    const TYPE_WEB_LINK = 10;
    const TYPE_FLOOR_DESIGN = 11;
    const TYPE_POLYGON = 12;
    const TYPE_START_VIEW = 13;
    const TYPE_MENU = 14;
    const TYPE_PROJECT_UPDATE = 15;

    public static $default = [
        self::TYPE_TEXT => [
            'icon' => 'images/icon/text_hotspot.png',
            'scale' => 1,
        ],
        self::TYPE_TELEPORT => [
            'icon' => 'images/icon/teleport_hotspot.png',
            'scale' => 1,
        ],
        self::TYPE_PRODUCT => [
            'icon' => 'images/icon/product_hotspot.png',
            'scale' => 1,
        ],
        self::TYPE_VIDEO => [
            'icon' => 'images/icon/video_hotspot.png',
            'scale' => 1,
        ],
        self::TYPE_INFO_BOX => [
            'icon' => 'images/icon/info_hotspot.png',
            'scale' => 1,
        ],
        self::TYPE_CALL_NUMBER => [
            'icon' => 'images/icon/call_hotspot.png',
            'scale' => 1,
        ],
        self::TYPE_IMAGE => [
            'icon' => 'images/icon/img_hotspot.png',
            'scale' => 1,
        ],
        self::TYPE_AUDIO => [
            'icon' => 'images/icon/audio_hotspot.png',
            'scale' => 1,
        ],
        self::TYPE_GALLERY => [
            'icon' => 'images/icon/gallery_hotspot.png',
            'scale' => 1,
        ],
        self::TYPE_WEB_LINK => [
            'icon' => 'images/icon/link_hotspot.png',
            'scale' => 1,
        ],
        self::TYPE_FLOOR_DESIGN => [
            'icon' => 'images/icon/floormap_hotspot.png',
            'scale' => 1,
        ],
        self::TYPE_POLYGON => [
            'icon' => '',
            'scale' => 1,
        ],
        self::TYPE_START_VIEW => [
            'icon' => 'images/icon/setstartview_hotspot.png',
            'scale' => 1,
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hotspot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['scene_id', 'project_id', 'type', 'style_id', 'width', 'height', 'depth', 'zorder', 'distored', 'capture', 'keep', 'hand_cursor', 'mask_children', 'mip_mapping', 'bg_shadow_x_offset', 'bg_shadow_y_offset', 'bg_shadow_blur', 'txt_shadow_x_offset', 'txt_shadow_y_offset', 'txt_shadow_blur', 'padding_all', 'padding_top', 'padding_right', 'padding_bottom', 'padding_left', 'over_sampling', 'vcenter', 'word_wrap', 'change_width_background', 'merged_alpha'], 'integer'],
            [['scale', 'alpha', 'ath', 'atv', 'rx', 'ry', 'rz', 'ox', 'oy', 'round_border_all', 'round_border_right_top', 'round_border_right_bottom', 'round_border_left_top', 'round_border_left_bottom', 'rotate'], 'number'],
            [['metadata'], 'string'],
            [['updated_at', 'created_at', 'deleted_at'], 'safe'],
            [['name', 'url'], 'string', 'max' => 150],
            [['html', 'css'], 'string', 'max' => 250],
            [['edge'], 'string', 'max' => 50],
            [['bg_color', 'color_border'], 'string', 'max' => 6],
            [['color_shadow', 'color_text'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'scene_id' => Yii::t('app', 'Scene ID'),
            'project_id' => Yii::t('app', 'Project ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'style_id' => Yii::t('app', 'Style ID'),
            'url' => Yii::t('app', 'Url'),
            'html' => Yii::t('app', 'Html'),
            'css' => Yii::t('app', 'Css'),
            'scale' => Yii::t('app', 'Scale'),
            'width' => Yii::t('app', 'Width'),
            'height' => Yii::t('app', 'Height'),
            'alpha' => Yii::t('app', 'Alpha'),
            'ath' => Yii::t('app', 'Ath'),
            'atv' => Yii::t('app', 'Atv'),
            'edge' => Yii::t('app', 'Edge'),
            'rx' => Yii::t('app', 'Rx'),
            'ry' => Yii::t('app', 'Ry'),
            'rz' => Yii::t('app', 'Rz'),
            'ox' => Yii::t('app', 'Ox'),
            'oy' => Yii::t('app', 'Oy'),
            'depth' => Yii::t('app', 'Depth'),
            'zorder' => Yii::t('app', 'Zorder'),
            'distored' => Yii::t('app', 'Distored'),
            'capture' => Yii::t('app', 'Capture'),
            'keep' => Yii::t('app', 'Keep'),
            'hand_cursor' => Yii::t('app', 'Hand Cursor'),
            'mask_children' => Yii::t('app', 'Mask Children'),
            'mip_mapping' => Yii::t('app', 'Mip Mapping'),
            'bg_color' => Yii::t('app', 'Bg Color'),
            'color_border' => Yii::t('app', 'Color Border'),
            'color_shadow' => Yii::t('app', 'Color Shadow'),
            'color_text' => Yii::t('app', 'Color Text'),
            'round_border_all' => Yii::t('app', 'Round Border All'),
            'round_border_right_top' => Yii::t('app', 'Round Border Right Top'),
            'round_border_right_bottom' => Yii::t('app', 'Round Border Right Bottom'),
            'round_border_left_top' => Yii::t('app', 'Round Border Left Top'),
            'round_border_left_bottom' => Yii::t('app', 'Round Border Left Bottom'),
            'bg_shadow_x_offset' => Yii::t('app', 'Bg Shadow X Offset'),
            'bg_shadow_y_offset' => Yii::t('app', 'Bg Shadow Y Offset'),
            'bg_shadow_blur' => Yii::t('app', 'Bg Shadow Blur'),
            'txt_shadow_x_offset' => Yii::t('app', 'Txt Shadow X Offset'),
            'txt_shadow_y_offset' => Yii::t('app', 'Txt Shadow Y Offset'),
            'txt_shadow_blur' => Yii::t('app', 'Txt Shadow Blur'),
            'padding_all' => Yii::t('app', 'Padding All'),
            'padding_top' => Yii::t('app', 'Padding Top'),
            'padding_right' => Yii::t('app', 'Padding Right'),
            'padding_bottom' => Yii::t('app', 'Padding Bottom'),
            'padding_left' => Yii::t('app', 'Padding Left'),
            'over_sampling' => Yii::t('app', 'Over Sampling'),
            'vcenter' => Yii::t('app', 'Vcenter'),
            'word_wrap' => Yii::t('app', 'Word Wrap'),
            'change_width_background' => Yii::t('app', 'Change Width Background'),
            'merged_alpha' => Yii::t('app', 'Merged Alpha'),
            'metadata' => Yii::t('app', 'Metadata'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'rotate' => Yii::t('app', 'Rotate'),
        ];
    }

    public function getDefaultIcon()
    {
        if (array_key_exists($this->type, self::$default) && !empty(self::$default[$this->type]['icon'])) {
            return self::$default[$this->type]['icon'];
        } else {
            return false;
        }
    }

    public function getDefaultScale()
    {
        if (array_key_exists($this->type, self::$default) && !empty(self::$default[$this->type]['scale'])) {
            if ($this->type == self::TYPE_TEXT && empty($this->html)) {
                return 1;
            }
            return self::$default[$this->type]['scale'];
        } else {
            return 1;
        }
    }
}
