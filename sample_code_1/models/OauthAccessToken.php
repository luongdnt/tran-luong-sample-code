<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "oauth_access_token".
 *
 * @property int $id
 * @property int $client_id
 * @property string $user_id
 * @property string $token
 * @property string $expire_at
 * @property string $created_at
 * @property string $updated_at
 */
class OauthAccessToken extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'oauth_access_token';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'user_id'], 'integer'],
            [['expire_at', 'created_at', 'updated_at'], 'safe'],
            [['token'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'user_id' => 'User ID',
            'token' => 'Token',
            'expire_at' => 'Expire At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
