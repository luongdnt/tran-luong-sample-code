<?php

namespace app\models;

use app\components\SendMailHelper;
use yii\base\BaseObject;

class SendMailVerifyJob extends BaseObject implements \yii\queue\JobInterface
{
    public $verifyCode;
    public $toMail;

    public function execute($queue)
    {
        SendMailHelper::sendMailVerifyAccount($this->verifyCode, $this->toMail);
    }
}