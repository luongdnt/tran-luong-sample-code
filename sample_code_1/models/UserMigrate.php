<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "highlight_project".
 *
 * @property int $id
 * @property int $user_id
 * @property int $vrmaker_id
 * @property int $password_changed
 * @property string $updated_at
 * @property string $created_at
 */
class UserMigrate extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_migrate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'vrmaker_id', 'password_changed'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
        ];
    }
}
