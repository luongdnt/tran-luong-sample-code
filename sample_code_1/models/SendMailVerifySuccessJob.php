<?php

namespace app\models;

use app\components\SendMailHelper;
use Yii;
use yii\base\BaseObject;

class SendMailVerifySuccessJob extends BaseObject implements \yii\queue\JobInterface
{
    public $toMail;

    public function execute($queue)
    {
        SendMailHelper::sendMailVerifySuccess($this->toMail);
    }
}