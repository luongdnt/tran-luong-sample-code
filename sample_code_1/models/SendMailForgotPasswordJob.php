<?php

namespace app\models;

use app\components\SendMailHelper;
use Yii;
use yii\base\BaseObject;

class SendMailForgotPasswordJob extends BaseObject implements \yii\queue\JobInterface
{
    public $toMail;
    public $token;

    public function execute($queue)
    {
        SendMailHelper::sendMailForgotPassword($this->toMail, $this->token);
    }
}