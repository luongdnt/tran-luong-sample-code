<?php

namespace app\models;

use app\repositories\interfaces\UserRepository;
use Yii;
use yii\web\IdentityInterface;

/**
 * Class User
 * @package app\models
 * @inheritdoc
 *
 * @property string email
 * @property string first_name
 * @property string last_name
 * @property string id
 * @property int status
 */
class User extends BaseModel implements IdentityInterface
{
    protected $softDelete = true;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'auth_key'], 'required'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            [['is_active', 'status', 'plan_id', 'gender', 'like', 'view', 'used_storage', 'bonus_storage'], 'integer'],
            [['type_start_time', 'type_expire_time', 'birth_date', 'last_login_time', 'active_code_expire_at', 'updated_at', 'created_at', 'deleted_at'], 'safe'],
            [['username', 'first_name', 'last_name', 'active_code'], 'string', 'max' => 50],
            [['email', 'website'], 'string', 'max' => 150],
            [['password', 'avatar_url', 'cover_img_url', 'facebook_url'], 'string', 'max' => 250],
            [['phone_number'], 'string', 'max' => 25],
            [['introduction'], 'string', 'max' => 500],
            ['username', 'match', 'pattern' => '/^[a-z\d_]{6,32}$/', 'message' => 'Username cannot contain special characters, ranging from 6-32 characters.'],
            [['auth_key'], 'string', 'max' => 32],
            [['email', 'username', 'phone_number'], 'unique'],
            ['status', 'default', 'value' => UserRepository::STATUS_WAITING],
            ['status', 'in', 'range' => [UserRepository::STATUS_WAITING, UserRepository::STATUS_ENABLE, UserRepository::STATUS_DISABLE]],
            ['gender', 'in', 'range' => [UserRepository::GENDER_UNDEFINED, UserRepository::GENDER_MALE, UserRepository::GENDER_FEMALE]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'avatar_url' => Yii::t('app', 'Avatar Url'),
            'cover_img_url' => Yii::t('app', 'Cover Img Url'),
            'is_active' => Yii::t('app', 'true: user active'),
            'status' => Yii::t('app', '0: waiting; 1: enable; 2: disable'),
            'plan_id' => Yii::t('app', 'Plan id'),
            'type_start_time' => Yii::t('app', 'Type Start Time'),
            'type_expire_time' => Yii::t('app', 'Type Expire Time'),
            'gender' => Yii::t('app', '0: undefined; 1: male; 2: female; 3: third gender'),
            'birth_date' => Yii::t('app', 'Birth Date'),
            'like' => Yii::t('app', 'Like'),
            'view' => Yii::t('app', 'View'),
            'facebook_url' => Yii::t('app', 'Facebook Url'),
            'website' => Yii::t('app', 'Website'),
            'introduction' => Yii::t('app', 'Introduction'),
            'last_login_time' => Yii::t('app', 'Last Login Time'),
            'used_storage' => Yii::t('app', 'bytes'),
            'bonus_storage' => Yii::t('app', 'bytes'),
            'active_code' => Yii::t('app', 'Active Code'),
            'active_code_expire_at' => Yii::t('app', 'Active Code Expire At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'auth_key' => Yii::t('app', 'Auth Key'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => UserRepository::STATUS_ENABLE]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => UserRepository::STATUS_ENABLE]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
            'status' => [self::STATUS_ENABLE, self::STATUS_WAITING],
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @param $token
     * @param null $type
     * @return User|null
     * @throws \yii\base\InvalidConfigException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        /** @var Module $module */
        $module = Yii::$app->getModule('oauth2');
        $token = $module->getServer()->getResourceController()->getToken();
        return !empty($token['user_id'])
            ? static::findIdentity($token['user_id'])
            : null;
    }

    /**
     * @param $username
     * @param $password
     * @return bool
     */
    public function checkUserCredentials($username, $password)
    {
        $user = static::findByUsername($username);
        if (empty($user)) {
            return false;
        }
        return $user->validatePassword($password);
    }

    /**
     * @param string $username
     * @return array|false
     */
    public function getUserDetails($username)
    {
        $user = static::findByUsername($username);
        return ['user_id' => $user->getId()];
    }

    public function getTotalProjects()
    {
        return $this->hasMany(Project::class, ['user_id' => 'id'])->where(['sharing' => 1])->count();
    }

    public function getPlan()
    {
        return $this->hasOne(Plan::class, ['id' => 'plan_id']);
    }
    
    public function getFullName()
    {
        return $this->first_name . " " . $this->last_name;
    }
}
