<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "highlight_project".
 *
 * @property int $id
 * @property int $project_id
 * @property int $vrmaker_id
 * @property string $updated_at
 * @property string $created_at
 */
class ProjectMigrate extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project_migrate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'vrmaker_id', 'user_id'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
        ];
    }
}
