<?php

namespace app\components;


use yii\base\BootstrapInterface;

class Repository implements BootstrapInterface
{
    private $list = [
        'Contact',
        'File',
        'HighlightProject',
        'OauthClient',
        'PasswordReset',
        'Plan',
        'Project',
        'ProjectMigrate',
        'User',
        'UserTransaction',
        'UserAccount',
        'UserMigrate',
        'Hotspot',
        'Scene',
        'VrObject'
    ];

    public function bootstrap($app)
    {
        foreach ($this->list as $model_name) {
            \Yii::$container->set( $model_name . 'Repository',
                ['class' => '\app\repositories\activerecords\\Db' . $model_name . 'Repository']);
        }
    }
}