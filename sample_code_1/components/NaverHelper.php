<?php

namespace app\components;


use Yii;
use yii\httpclient\Client;

class NaverHelper
{
    public static function getAccessToken($authCode, $state)
    {
        $url = 'https://nid.naver.com/oauth2.0/token';
        $data = [
            'client_id' => Yii::$app->params['services']['naver']['app_id'],
            'client_secret' => Yii::$app->params['services']['naver']['secret_key'],
            'grant_type' => 'authorization_code',
            'state' => $state,
            'code' => $authCode
        ];

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl($url)
            ->setData($data)
            ->send();

        return $response->data;
    }

    public static function getUserInfo($tokenType, $accessToken)
    {
        $url = 'https://openapi.naver.com/v1/nid/me';

        $client = new Client(['baseUrl' => $url]);
        $request = $client->createRequest();
        $request->headers->set('Authorization', $tokenType . ' ' . $accessToken);

        $response = $request->send();

        return $response->data;

    }
}