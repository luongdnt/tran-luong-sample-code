<?php
namespace app\components;

use Yii;

class IamportHelper
{
    const CACHE_EXPIRED_NAME = 'iamport_expired';
    const CACHE_TOKEN_NAME = 'iamport_token';
//    const SECRET_KEY = 'keyl@gid@y';
    const API_BASE_URL = 'https://api.iamport.kr';

    public $token;
    public $expired = 0;

    public function __construct()
    {
        // Get token from cache
        list($newToken, $newExpired) = $this->getTokenFromCache();
        $this->token = $newToken;
        $this->expired = $newExpired;

        $this->refreshToken();
    }

    /**
     * Refresh token if not valid
     */
    public function refreshToken()
    {
        $now = time();
        if (empty($this->token) || $this->expired < $now) {

            // Get new token
            list($newToken, $newExpired) = $this->getNewToken();
            if (empty($newToken)) {
                throw new Exception('Cannot connect with payment system!', 401);
            }

            Yii::$app->cache->set(self::CACHE_TOKEN_NAME, $newToken);
            Yii::$app->cache->set(self::CACHE_EXPIRED_NAME, $newExpired);

            $this->token = $newToken;
            $this->expired = $newExpired;
        }
    }

    /**
     * Get token from cache
     * @return array
     */
    public function getTokenFromCache()
    {
        $expired = Yii::$app->cache->get(self::CACHE_EXPIRED_NAME);
        $token = Yii::$app->cache->get(self::CACHE_TOKEN_NAME);

        if (empty($token) || empty($expired)) {
            return array(null, null);
        }
        return array($token, $expired);
    }

    /**
     * Get new token from iamport
     * @return array($token, $expired)
     */
    public function getNewToken()
    {
//        $accountInfo = Configure::read('iamport_account_info');
        $apiKey = Yii::$app->params['payment']['iamport']['client_id'];
        $apiSecret = Yii::$app->params['payment']['iamport']['client_secret'];
        $url = self::API_BASE_URL . '/users/getToken';
        $sendData = array(
            'imp_key' => $apiKey,
            'imp_secret' => $apiSecret,
        );
        $result = CurlHelper::sendCurl($url, $sendData, CurlHelper::METHOD_POST, CurlHelper::TYPE_JSON);
        if ($result['http_code'] == 200 && isset($result['body']['code']) && $result['body']['code'] == 0 && isset($result['body']['response']['access_token'])) {
            return array($result['body']['response']['access_token'], $result['body']['response']['expired_at']);
        }
        return array(null, null);
    }

    /**
     * Get transaction info from iap
     * @param  string $impId
     * @return mixed (array|null)
     */
    public function getTransactionInfo($impId)
    {
        $url = self::API_BASE_URL . '/payments/' . $impId;
        $data = array(
            '_token' => $this->token,
        );
        $this->refreshToken();
        $result = CurlHelper::sendCurl($url, $data, CurlHelper::METHOD_GET, CurlHelper::TYPE_JSON);
        $body = $result['body'];
        if ($result['http_code'] == 200 && isset($body['code']) && $body['code'] == 0 && isset($body['response'])) {
            $transactionInfo = array(
                'impId' => $body['response']['imp_uid'],
                'transactionId' => $body['response']['merchant_uid'],
                'transactionName' => $body['response']['name'],
                'created' => $body['response']['paid_at'],
                'status' => $body['response']['status'], // paid|ready
                'email' => $body['response']['buyer_email'],
                'name' => $body['response']['buyer_name'],
                'amount' => $body['response']['amount'],
                'currency' => $body['response']['currency'],
                'pay_method' => $body['response']['pay_method']
            );
            Yii::info('[Info] Get transaction id : ' . $impId . ' success', 'payment');

            return $transactionInfo;
        }

        Yii::info('[Info] Get transaction id : ' . $impId . ' failed', 'payment');

        return null;
    }

    /**
     * Generate an unique transaction id contain productId and userId
     * @param  int $productId
     * @param  int $userId
     * @return string (P1_U21_timestamp_hash)
     */
    public static function generateTransactionId($productId, $userId, $time = null)
    {
        $secretKey = self::SECRET_KEY;
        $now = $time !== null ? $time : time();
        $hashConcat = "P{$productId}-U{$userId}-{$now}-{$secretKey}";
        $hashPart = substr(md5($hashConcat), 0, 10);
        return "P{$productId}_U{$userId}_{$now}_{$hashPart}";
    }

    /**
     * Get product id and user id from transaction id + validate
     * @param  string $transactionId
     * @return array($productId, $userId)
     */
    public function getProductIdAndUserIdFromTransactionId($transactionId)
    {
        $parts = explode('_', $transactionId);
        if (!is_array($parts) || count($parts) !== 4) {
            return array(null, null);
        }
        if (strlen($parts[0]) < 2 || strlen($parts[1]) < 2) {
            return array(null, null);
        }
        $productId = substr($parts[0], 1);
        $userId = substr($parts[1], 1);
        if ($transactionId != $this->generateTransactionId($productId, $userId, $parts[2])) {
            return array(null, null);
        }
        return array($productId, $userId);
    }

    /**
     * @param $customer_uid
     * @param $card_number
     * @param $card_expiry
     * @param $card_pwd_digit
     * @param $birth
     * @param array $customer_info
     * @return array|null
     */
    public function createCustomerToken($customer_uid, $card_number, $card_expiry, $card_pwd_digit, $birth, $customer_info = [])
    {
        $this->refreshToken();
        $url = self::API_BASE_URL . '/subscribe/customers/' . $customer_uid . '?_token=' . $this->token;

        $data = [
            'customer_uid'      => $customer_uid,
            'card_number'       => $card_number,
            'expiry'            => $card_expiry,
            'birth'             => $birth,
            'pwd_2digit'        => $card_pwd_digit,
            'customer_name'     => isset($customer_info['customer_name']) ? $customer_info['customer_name'] : '',
            'customer_tel'      => isset($customer_info['customer_tel']) ? $customer_info['customer_tel'] : '',
            'customer_email'    => isset($customer_info['customer_email']) ? $customer_info['customer_email'] : '',
            'customer_addr'     => isset($customer_info['customer_addr']) ? $customer_info['customer_addr'] : '',
            'customer_postcode' => isset($customer_info['customer_postcode']) ? $customer_info['customer_postcode'] : '',
        ];

        $result = CurlHelper::sendCurl($url, $data, CurlHelper::METHOD_POST, CurlHelper::TYPE_JSON);
        $body = $result['body'];
        Yii::info('[IamPortCustomerToken] Customer #' . $customer_uid . '. Data: ' . json_encode($body));
        if ($result['http_code'] == 200 && isset($body['code']) && $body['code'] == 0 && isset($body['response'])) {
            return ['success' => true, 'data' => $body];
        } else {
            return ['success' => false, 'data' => $body];
        }
    }

    /**
     * @param $customer_uid
     * @param $amount
     * @param $merchant_uid
     * @param $schedule_at
     * @param $name
     * @param array $buyer_info
     * @return array
     */
    public function createSubscribePaymentsSchedule($customer_uid, $amount, $merchant_uid, $schedule_at, $name, $buyer_info = [])
    {
        $this->refreshToken();
        $url = self::API_BASE_URL . '/subscribe/payments/schedule?_token=' . $this->token;

        $data = [
            'customer_uid'      => $customer_uid,
            'checking_amount'   => 0,
            'schedules'         => [
                [
                    'merchant_uid'   => $merchant_uid,
                    'schedule_at'    => $schedule_at,
                    'amount'         => $amount,
                    'name'           => $name,
                    'buyer_name'     => isset($buyer_info['buyer_name']) ? $buyer_info['buyer_name'] : '',
                    'buyer_email'    => isset($buyer_info['buyer_email']) ? $buyer_info['buyer_email'] : '',
                    'buyer_tel'      => isset($buyer_info['buyer_tel']) ? $buyer_info['buyer_tel'] : '',
                    'buyer_addr'     => isset($buyer_info['buyer_addr']) ? $buyer_info['buyer_addr'] : '',
                    'buyer_postcode' => isset($buyer_info['buyer_postcode']) ? $buyer_info['buyer_postcode'] : '',
                ]
            ]
        ];

        $result = CurlHelper::sendCurl($url, $data, CurlHelper::METHOD_POST, CurlHelper::TYPE_JSON);
        $body = $result['body'];
        Yii::info('[IamPortSubscribePaymentSchedule] Customer #' . $customer_uid . '. Data: ' . json_encode($body));
        if ($result['http_code'] == 200 && isset($body['code']) && $body['code'] == 0 && isset($body['response'])) {
            return ['success' => true, 'data' => $body];
        } else {
            return ['success' => false, 'data' => $body];
        }
    }

    /**
     * @param $customer_uid
     * @param $merchant_uid
     * @param $amount
     * @param $vat
     * @param $name
     * @param $card_number
     * @param $card_expiry
     * @param $card_pwd_digit
     * @param $birth
     * @param array $buyer_info
     * @return array
     */
    public function createSubscribePaymentOneTime($customer_uid, $merchant_uid, $amount, $vat, $name, $card_number, $card_expiry, $card_pwd_digit, $birth, $buyer_info = [])
    {
        $this->refreshToken();
        $url = self::API_BASE_URL . '/subscribe/payments/schedule?_token=' . $this->token;

        $data = [
            'merchant_uid'   => $merchant_uid,
            'customer_uid'   => $customer_uid,
            'amount'         => $amount,
            'vat'            => $vat,
            'card_number'    => $card_number,
            'expiry'         => $card_expiry,
            'birth'          => $birth,
            'pwd_2digit'     => $card_pwd_digit,
            'name'           => $name,
            'buyer_name'     => isset($buyer_info['buyer_name']) ? $buyer_info['buyer_name'] : '',
            'buyer_email'    => isset($buyer_info['buyer_email']) ? $buyer_info['buyer_email'] : '',
            'buyer_tel'      => isset($buyer_info['buyer_tel']) ? $buyer_info['buyer_tel'] : '',
            'buyer_addr'     => isset($buyer_info['buyer_addr']) ? $buyer_info['buyer_addr'] : '',
            'buyer_postcode' => isset($buyer_info['buyer_postcode']) ? $buyer_info['buyer_postcode'] : '',
        ];

        $result = CurlHelper::sendCurl($url, $data, CurlHelper::METHOD_POST, CurlHelper::TYPE_JSON);
        $body = $result['body'];
        Yii::info('[IamPortSubscribePaymentOneTime] Customer #' . $customer_uid . '. Data: ' . json_encode($body));
        if ($result['http_code'] == 200 && isset($body['code']) && $body['code'] == 0 && isset($body['response'])) {
            return ['success' => true, 'data' => $body];
        } else {
            return ['success' => false, 'data' => $body];
        }
    }
}
