<?php

namespace app\components;

use Yii;
use yii\helpers\Url;

class SendMailHelper
{
   public static function sendMail($data, $toMail = '')
   {
       $toEmails = [Yii::$app->params['mail']['to_email']];

       if (!empty($toMail)) {
           $toEmails[] = $toMail;
           $title_to_mail = '<b>To : </b>' . $toMail . '<br>';
       } else {
           $title_to_mail = '';
       }

       $html = 'You have new contact from VRMaker user. Please check and reply to connect with them. <br>' .
                '<b>Title : </b>' . $data['title'] . '<br>' . $title_to_mail .
                '<b>Name : </b>' . $data['full_name'] . '<br>' .
                '<b>Email : </b>' . $data['email'] . '<br>' .
                '<b>Tel : </b>' . $data['phone_number'] . '<br>' .
                '<b>Company name : </b>' . $data['company_name'] . '<br>' .
                '<b>Message : </b>' . $data['content'] . '<br>';

       $message = [];

       foreach ($toEmails as $email) {
            $message[] = Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['mail']['username'] => Yii::$app->params['mail']['name'] ])
                ->setTo($email)
                ->setSubject('You have new contact from VRMaker user')
                ->setTextBody('VR Maker')
                ->setHtmlBody($html);
       }

       Yii::$app->mailer->sendMultiple($message);

   }

   public static function sendMailVerifyAccount($verify_code, $toMail)
   {
       if (!empty($toMail)) {
           $toEmails[] = $toMail;
       }

       $url_verify = Yii::$app->params['appAuthDomain'] . '/site/login?verify_code=' . $verify_code;

       $message = [];

       Yii::$app->mailer->htmlLayout = "@app/mail/layouts/html";

       foreach ($toEmails as $email) {
           $message[] = Yii::$app->mailer->compose(['html' => '@app/mail/active-account'], ['url' => $url_verify])
               ->setFrom([Yii::$app->params['mail']['username'] => Yii::$app->params['mail']['name'] ])
               ->setTo($email)
               ->setSubject('VRMaker Activation Link')
               ->setTextBody('VR Maker');
       }

       Yii::$app->mailer->sendMultiple($message);

   }

   public static function sendMailVerifySuccess($toMail)
   {
       if (!empty($toMail)) {
           $toEmails[] = $toMail;
       }

       $message = [];

       Yii::$app->mailer->htmlLayout = "@app/mail/layouts/html";

       foreach ($toEmails as $email) {
           $message[] = Yii::$app->mailer->compose(['html' => '@app/mail/verify-success'], ['mail' => $toMail])
               ->setFrom([Yii::$app->params['mail']['username'] => Yii::$app->params['mail']['name'] ])
               ->setTo($email)
               ->setSubject('You have new contact from VRMaker user')
               ->setTextBody('VR Maker');
       }


       Yii::$app->mailer->sendMultiple($message);
   }

   public static function sendMailForgotPassword($toMail, $token)
   {
        if (!empty($toMail)) {
            Yii::$app->mailer->htmlLayout = "@app/mail/layouts/html";
            Yii::$app->mailer->compose(['html' => '@app/mail/reset-password'], ['email' => $toMail, 'token' => $token])
               ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot' ])
               ->setTo($toMail)
               ->setSubject('Password reset for ' . Yii::$app->name)
               ->setTextBody('VR Maker')
               ->send();
        }
   }
}