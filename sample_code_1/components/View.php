<?php


namespace app\components;

use Yii;

class View extends \yii\web\View
{

    public function url_image($url)
    {
        if (preg_match('/http(s?)\:\/\//i', $url)) {
            return $url;
        } else {
            return isset($url) ? Yii::$app->params['resource_domain'] . $url : '#';
        }
    }

    public function get_thumb($url, $width, $height){
        if (preg_match('/http(s?)\:\/\//i', $url)) {
            return $url;
        } else {
            return !empty($url) ? Yii::$app->params['resource_domain'] . '/thumb/' . $width . '-' . $height . $url : '#';
        }
    }
//    public function base_url($url)
//    {
//        return isset($url) ? Yii::$app->getRequest()->serverName . $url : '#';
//    }

    public function dd($data)
    {
        echo '<pre>';
        var_dump($data);
        echo '</pre>';
        die();
    }

    function generate_state() {
        $mt = microtime();
        $rand = mt_rand();
        return md5($mt . $rand);
    }

}
