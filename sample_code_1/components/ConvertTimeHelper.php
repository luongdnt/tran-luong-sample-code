<?php

namespace app\components;


class ConvertTimeHelper
{

    public static function timeNow()
    {
        return date('Y-m-d H:i:s');
    }
}