<?php

namespace app\components;


class HashHelper
{
    /**
     * Hash user password
     *
     * @param $password
     * @return string
     * @throws \yii\base\Exception
     */
    public static function hashUserPassword($password)
    {
        return \Yii::$app->security->generatePasswordHash($password);
    }

    public static function hashNumber($n) {
        return (((0x0000FFFF & $n) << 16) + ((0xFFFF0000 & $n) >> 16));
    }
}