<?php


namespace app\components;


use app\models\OauthClient;
use app\models\User;
use Firebase\JWT\JWT;

class AuthHelper
{
    /**
     * @param OauthClient $client
     * @param User $user
     * @return string
     */
    public static function createToken(OauthClient $client, User $user)
    {
        $token = [
            'first_name' => $user->first_name,
            'last_name' => $user->first_name,
            'email' => $user->first_name,
            'id' => $user->id,
        ];

        return JWT::encode($token, $client->secret_key);
    }
}