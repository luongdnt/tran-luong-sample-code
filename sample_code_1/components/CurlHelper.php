<?php

namespace app\components;

class CurlHelper
{

    const TYPE_JSON = 'json';
    const TYPE_PLAIN = 'plain';
    const METHOD_GET = 'get';
    const METHOD_POST = 'post';

    /**
     * Send curl
     * @param  string $url url to send
     * @param  array $data data to send
     * @param  string $method [get|post] (defaut 'get')
     * @param  string $type type of response body to return ('json', 'plain') (default TYPE_PLAIN)
     * @param  int $timeout timeout for request (default 15s)
     * @return array(
     *             'http_code' => int,
     *             'header' => string,
     *             'body' => mixed (string|array)
     *         )
     */
    public static function sendCurl($url, $data, $method = self::METHOD_GET, $decode = self::TYPE_PLAIN, $timeout = 15, $headers = [])
    {
        $post_info = http_build_query($data);

        $post_info = rtrim($post_info, '&');
//            send curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');
        if ($method === self::METHOD_POST) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Length: ' . strlen($post_info)));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_info);
        } else {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_URL, $url . "?" . $post_info);
        }

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $result = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

        $result_ary = array(
            'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
            'header' => substr($result, 0, $header_size),
            'body' => substr($result, $header_size),
        );
//    echo $result;
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200 && $decode === self::TYPE_JSON) {
            $result_ary['body'] = json_decode($result_ary['body'], true);
        }

        return $result_ary;

    }
}
