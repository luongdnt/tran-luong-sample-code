<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<header id="header" class="cf">

    <div id="<?= $page_name != 'index' && $page_name != 'pricing' ? 'fix-head' : '' ?>" class="header-container <?= $page_name == 'index' || $page_name == 'pricing' ? 'main-header' : '' ?> cf">
        <div class="inner-wrap cf">
            <h1>
                <a href="/"><img src="<?= in_array($page_name, ['index', 'pricing']) ? '/images/layout/vrmaker_logo_w.png' : '/images/layout/logo.png' ?>" alt="logo"></a>
            </h1>
            <nav id="nav">
                <ul id="gnb" class="cf">
                    <li class="<?= $page_name == 'explore' ? 'onpage' : '' ?>"><a href="<?= Url::toRoute(['/explore']) ?>"><?= Yii::t('app', 'EXPLORE') ?></a></li>
                    <li class="<?= $page_name == 'map' ? 'onpage' : '' ?>"><a href="<?= Url::toRoute(['/map']) ?>"><?= Yii::t('app', 'VR WORLD MAP') ?></a></li>
                    <li class="<?= $page_name == 'authors' ? 'onpage' : '' ?>"><a href="<?= Url::toRoute(['/authors']) ?>"><?= Yii::t('app', 'VR CREATORS') ?></a></li>
                    <li class="<?= $page_name == 'business' ? 'onpage' : '' ?>"><a  href="<?= Url::toRoute(['/vr-business']) ?>"><?= Yii::t('app', 'VRMAKER FOR BUSINESS') ?></a></li>
                    <li class="<?= $page_name == 'pricing' ? 'onpage' : '' ?>"><a href="<?= Url::toRoute(['/pricing']) ?>"><?= Yii::t('app', 'PRICING') ?></a></li>
                </ul>
            </nav>
            <p class="top-menu cf">
                <?php if(!Yii::$app->user->isGuest): ?>
                    <a href="<?= '/auth/authorize?client_id=' . Yii::$app->params['cms_client_id'] ?>" target="_blank"><?= Yii::t('app', 'VR Manage') ?></a>
                    <?= Html::a(
                        Yii::t('app', 'My Page'),
                        [Url::to(['/' . Yii::$app->user->getIdentity()->username])],
                        ['class' => '']
                    ) ?>
                    <?= Html::a(
                        Yii::t('app', 'Sign out'),
                        ['/site/logout'],
                        ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                    ) ?>
                <?php else: ?>
                    <a href="<?= Url::toRoute(['/signup-agreement']) ?>"> <?= Yii::t('app', 'Sign up'); ?> </a>
                    <a href="<?= Url::toRoute(['/login']) ?>"> <?= Yii::t('app', 'Log in'); ?> </a>
                <?php endif; ?>
                <span class="lan_contents cf">
	   				<a href="/site/change-language?language=ko" class="<?= $language == 'ko' ? 'this' : ''; ?>">KOR</a> / <a href="/site/change-language?language=en" class="<?= $language !== 'ko' ? 'this' : ''; ?>">ENG</a>
	   			</span>
            </p>
        </div> <!-- //inner-wrap -->
    </div> <!-- //.header-container -->
    <div id="fix-head" class="header-container sticky-head cf">
        <div class="inner-wrap cf">
            <h1>
                <a href="/"><img src="/images/layout/logo.png" alt="logo"></a>
            </h1>
            <nav id="nav">
                <ul id="gnb" class="cf">
                    <li><a href="<?= Url::toRoute(['/explore']) ?>"><?= Yii::t('app', 'EXPLORE') ?></a></li>
                    <li><a href="<?= Url::toRoute(['/map']) ?>"><?= Yii::t('app', 'VR WORLD MAP') ?></a></li>
                    <li><a href="<?= Url::toRoute(['/authors']) ?>"><?= Yii::t('app', 'VR CREATORS') ?></a></li>
                    <li><a href="<?= Url::toRoute(['/vr-business']) ?>"><?= Yii::t('app', 'VRMAKER FOR BUSINESS') ?></a></li>
                    <li><a href="<?= Url::toRoute(['/pricing']) ?>"><?= Yii::t('app', 'PRICING') ?></a></li>
                </ul>
            </nav>
            <p class="top-menu cf">
                <?php if(!Yii::$app->user->isGuest): ?>
                    <a href="<?= '/auth/authorize?client_id=' . Yii::$app->params['cms_client_id'] ?>" target="_blank"><?= Yii::t('app', 'VR Manage') ?></a>

                    <?= Html::a(
                        Yii::t('app', 'My Page'),
                        [Url::to(['/' . Yii::$app->user->getIdentity()->username])],
                        ['class' => '']
                    ) ?>
                    <?= Html::a(
                        Yii::t('app', 'Sign out'),
                        ['/site/logout'],
                        ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                    ) ?>
                <?php else: ?>
                    <a href="<?= Url::toRoute(['/signup-agreement']) ?>"><?= Yii::t('app', 'Sign up') ?></a>
                    <a href="<?= Url::toRoute(['/login']) ?>"><?= Yii::t('app', 'Log in') ?></a>
                <?php endif; ?>
                <span class="lan_contents cf">
                    <a href="/site/change-language?language=ko" class="<?= $language == 'ko' ? 'this' : ''; ?>">KOR</a> / <a href="/site/change-language?language=en" class="<?= $language !== 'ko' ? 'this' : ''; ?>">ENG</a>
	   			</span>
            </p>
        </div> <!-- //inner-wrap -->
    </div> <!-- //.header-container -->

    <div class="header-m cf">
        <h1>
            <a href="/"><img src="/images/layout/logo.png" alt="logo"></a>
        </h1>
        <div id="menu_btn">
            <p class="menu_btn_line"></p>
            <p class="menu_btn_line"></p>
            <p class="menu_btn_line"></p>
            <p class="menu_btn_line"></p>
        </div> <!-- menu_btn -->
        <nav id="nav_m">
            <div class="nav_inner">
	   			<span class="lan_contents cf">
                    <a href="/site/change-language?language=ko" class="<?= $language == 'ko' ? 'this' : ''; ?>">KOR</a> / <a href="/site/change-language?language=en" class="<?= $language !== 'ko' ? 'this' : ''; ?>">ENG</a>
	   			</span>
                <div class="nav_inner_m">
                    <ul id="gnb_m" class="cf">
                        <li><a href="<?= Url::toRoute(['/explore']) ?>"><?= Yii::t('app', 'EXPLORE') ?></a></li>
                        <li><a href="<?= Url::toRoute(['/map']) ?>"><?= Yii::t('app', 'VR WORLD MAP') ?></a></li>
                        <!--                        <li><a href="/stock">STOCK</a></li>-->
                        <li><a href="<?= Url::toRoute(['/authors']) ?>"><?= Yii::t('app', 'VR CREATORS') ?></a></li>
                        <li><a href="<?= Url::toRoute(['/vr-business']) ?>"><?= Yii::t('app', 'VRMAKER FOR BUSINESS') ?></a></li>

                        <li><a href="<?= Url::toRoute(['/pricing']) ?>"><?= Yii::t('app', 'PRICING') ?></a></li>

                    </ul>
                    <ul class="footer_menu_m">
                        <?php if (!Yii::$app->user->isGuest): ?>
                            <li>
                                <a href="<?= '/auth/authorize?client_id=' . Yii::$app->params['cms_client_id'] ?>" target="_blank"><?= Yii::t('app', 'VR Manage') ?></a>
                            </li>
                        <?php endif; ?>
                        <li><a href="<?= Url::toRoute(['/contact']) ?>"><?= Yii::t('app', 'Contact') ?></a></li>
                    </ul>
                    <p class="login cf">
                        <?php if(!Yii::$app->user->isGuest): ?>

                            <?= Html::a(
                                'Sign out',
                                ['/site/logout'],
                                ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                            ) ?>

                            <?= Html::a(
                                'My Page',
                                [Url::to(['/' . Yii::$app->user->getIdentity()->username])],
                                ['class' => '']
                            ) ?>
                        <?php else: ?>
                            <a href="<?= Url::toRoute(['/login']) ?>"> <?= Yii::t('app', 'Login') ?> </a>
                            <a href="<?= Url::toRoute(['/signup-agreement']) ?>"> <?= Yii::t('app', 'Join') ?> </a>

                        <?php endif; ?>
                    </p>
                </div>
            </div> <!-- //.nav_inner -->
        </nav> <!-- //#nav_m -->
    </div> <!-- //.header-m -->
</header> <!-- //header -->