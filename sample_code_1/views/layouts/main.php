<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="-webkit-">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no">
    <link rel="apple-touch-icon" href="/apple-touch-icon.png">
    <link rel="shortcut icon" href="<?= $this->url_image('/images/favicon.png') ?>" type="image/x-icon"/>
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php if(!empty(Yii::$app->params['ga_code'])): ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?= Yii::$app->params['ga_code'] ?>"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '<?= Yii::$app->params['ga_code'] ?>');
    </script>
    <?php endif; ?>
</head>
<body>
<?php $this->beginBody() ?>

<div id="sikpNav" class="hide">
    <p><a href="#contents">본문바로가기</a></p>
    <p><a href="#gnb">메뉴바로가기</a></p>
</div>
<?php $page_name = isset($this->params['pageName']) ? $this->params['pageName'] : ''; ?>
<?php $language = Yii::$app->session->get('language'); ?>

    <?= $this->render(
        'header.php',
            ['page_name' => $page_name, 'language' => $language]
    ) ?>

    <?= $content ?>

    <?= $this->render(
        'footer.php'
    ) ?>

<?php $this->endBody() ?>
</body>
<?php echo $this->blocks['script'] ?>
</html>
<?php $this->endPage() ?>
