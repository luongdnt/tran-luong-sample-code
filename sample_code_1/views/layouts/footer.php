<?php

use yii\helpers\Url;

?>
<footer id="footer">
    <div class="inner-wrap cf">
        <p class="logo"><img src="/images/layout/vrmaker_logo_w.png" alt="logo"></p>
        <div class="foot_m">
            <ul class="foot_menu cf">
                <li><a href="<?= Url::toRoute(['/explore']) ?>"><?= Yii::t('app', 'EXPLORE') ?></a></li>
                <li><a href="<?= Url::toRoute(['/map']) ?>"><?= Yii::t('app', 'VR WORLD MAP') ?></a></li>
                <!--                <li><a href="/stock">STOCK</a></li>-->
                <li><a href="<?= Url::toRoute(['/authors']) ?>"><?= Yii::t('app', 'VR CREATORS') ?></a></li>
                <li><a href="<?= Url::toRoute(['/about']) ?>"><?= Yii::t('app', 'ABOUT US') ?></a></li>
                <li><a href="<?= Url::toRoute(['/pricing']) ?>"><?= Yii::t('app', 'PRICING') ?></a></li>
            </ul>
            <ul class="f_link cf">
                <li><a href="<?= Url::toRoute(['/contact']) ?>"><?= Yii::t('app', 'Contact') ?></a></li>
                <li><a href="<?= Url::toRoute(['/privacy']) ?>"><?= Yii::t('app', 'Privacy Policy') ?></a></li>
                <li><a href="<?= Url::toRoute(['/terms']) ?>"><?= Yii::t('app', 'Terms of Use') ?></a></li>
                <li><a href="https://blog.naver.com/atojet" target="_blank"><?= Yii::t('app', 'blog') ?></a></li>
            </ul>
        </div>
        <div class="sns cf">
            <a href="https://www.youtube.com/channel/UCLVNeKoWTWLuueh_GWp_a1w" target="_blank"><img src="/images/main/youtube.png" alt="youtube" /></a>
            <a href="https://www.facebook.com/vrmaker" target="_blank" ><img src="/images/main/facebook.png" alt="facebook" /></a>
        </div>
        <p class="ft_info">
            <?= Yii::t('app', 'Atojet Corporation  | TEL:02-6426-2501 | E-MAIL: info@vrmaker.io | 330, Cheomdan-ro, Jeju-si, Jeju-do, Republic of Korea') ?> <br/>
            <em><?= Yii::t('app', 'Copyright © 2018 Atojet Corp. All rights reserved.') ?></em>
        </p>
    </div>
    <div id="top"><i class="fas fa-chevron-up"></i></div>
</footer>