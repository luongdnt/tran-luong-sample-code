<?php

use app\assets\PlayerAsset;
use yii\bootstrap\BootstrapAsset;
use yii\debug\Module;
use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\web\View;

/* @var $this View */
/* @var $content string */
JqueryAsset::register($this);
BootstrapAsset::register($this);
PlayerAsset::register($this);
if (class_exists('yii\debug\Module')) {
    $this->off(View::EVENT_END_BODY, [Module::getInstance(), 'renderToolbar']);
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= Yii::getAlias("@web") . '/favicon.png' ?>" type="image/x-icon">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body style="margin:0;">

<?php $this->beginBody() ?>
<div class="wrap">
    <?= $content ?>
</div>
<div class="modal"></div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
