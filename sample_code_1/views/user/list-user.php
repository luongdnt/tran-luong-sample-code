<?php
use yii\helpers\Html;
use yii\helpers\Url;

foreach ($authors as $author): ?>
    <div class="author_contents">
        <a href="<?= Url::to(['author', 'author' => $author->username]) ?>" style="<?= !empty($author->cover_img_url) ? 'background: url('. $this->url_image($author->cover_img_url) .') no-repeat content-box' : '' ?>" class="authors_bg">
            <div class="pano-img">
                <div class="mask"></div>
                <div class="author_profile">
                    <img src="<?= !empty($author->avatar_url) ? $this->url_image($author->avatar_url) : '/images/authors/author_profile02.jpg' ?>">
                </div>
                <div class="author_info cf">
                    <p><?= empty($author->username) ? $author->email : Html::encode($author->username) ?></p>
                    <div class="pano_cnt"><i class="fas fa-vr-cardboard"></i><span><?= $author->totalProjects ?></span></div>
                </div>
            </div>
        </a>
    </div>
<?php endforeach; ?>