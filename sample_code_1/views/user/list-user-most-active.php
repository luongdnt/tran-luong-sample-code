<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<?php foreach ($authors as $author): ?>
    <?php
    if (empty($author['authors'])) continue;
    ?>
    <div class="author_contents">
        <a href="<?= Url::to(['author', 'author' => $author['authors']['username']]) ?>" style="<?= !empty($author['authors']['cover_img_url']) ? 'background: url('. $this->url_image($author['authors']['cover_img_url']) .') no-repeat content-box' : '' ?>" class="authors_bg">
            <div class="pano-img">
                <div class="mask"></div>
                <div class="author_profile">
                    <img src="<?= !empty($author['authors']['avatar_url']) ? $this->url_image($author['authors']['avatar_url']) : '/images/authors/author_profile02.jpg' ?>">
                </div>
                <div class="author_info cf">
                    <p><?= empty($author['authors']['username']) ? $author['authors']['email'] : Html::encode($author['authors']['username']) ?></p>
                    <div class="pano_cnt"><i class="fas fa-vr-cardboard"></i><span><?= $author['total_project'] ?></span></div>
                </div>
            </div>
        </a>
    </div>
<?php endforeach; ?>