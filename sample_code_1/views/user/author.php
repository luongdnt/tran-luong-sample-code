<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\repositories\interfaces\FileRepository;

$this->title = 'Author';
$search_key = Yii::$app->request->get('search_key');
?>
    <section id="author_visual" class="visual">
        <div class="mask">
            <div class="visual-contents stock_info_contents">
                <h1>
                    <?= Yii::t('app', 'VR Creators') ?>
                    <p class="text_intro"><?= Yii::t('app', 'World Best VR Creators With VRMaker') ?></p>
                </h1>

                <form class="search-form">
                    <div class="search_container">
                        <div class="search_container_table">
                            <div class="search_table">
                                <input class="tour_search" type="text" placeholder="Search..." name="search_key" value="<?= !empty($search_key) ? $search_key : '' ?>">
                            </div>
                            <div class="search_table no-stretch">
                                <div class="search_btn"><button><i class="xi-search"></i></button></div>
                            </div>
                        </div><!-- //.search_container_table -->
                    </div><!-- //.search_container -->
                </form><!-- //.search-form -->
            </div> <!-- //.stock_info_contents -->
        </div><!-- //.mask -->
    </section><!-- //#visual -->

<div id="container">
    <section id="contents">
        <div class="author-container">
            <?php if (!empty($search_key)): ?>
                <h2 class="tiltle-search"><?= Yii::t('app', 'Search VR creator') ?></h2>
                <h4 class="key-search"><?= $search_key ?> (<?= $total ?> <?= Yii::t('app', 'results') ?>)</h4>
            <?php endif; ?>

            <div class="tour-container cf">

                <div class="author-list cf">
                    <?php if (!empty($search_key)): ?>

                        <?php foreach ($authors as $author): ?>
                            <div class="author_contents">
                                <a href="<?= Url::to(['author', 'author' => $author->username]) ?>" style="<?= !empty($author->cover_img_url) ? 'background: url('. $this->get_thumb($author->cover_img_url, 400, 0) .') no-repeat content-box' : '' ?>" class="authors_bg">
                                    <div class="pano-img">
                                        <div class="mask"></div>
                                        <div class="author_profile">
                                            <img src="<?= !empty($author->avatar_url) ? $this->url_image($author->avatar_url) : '/images/authors/author_profile02.jpg' ?>">
                                        </div>
                                        <div class="author_info cf">
                                            <p><?= empty($author->username) ? $author->email : Html::encode($author->username) ?></p>
                                            <div class="pano_cnt"><i class="fas fa-vr-cardboard"></i><span><?= $author->totalProjects ?></span></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <?php foreach ($authors as $key => $author): ?>
                            <div class="author_contents">
                                <a href="<?= Url::to(['author', 'author' => $author['authors']['username']]) ?>" style="<?= !empty($author['authors']['cover_img_url']) ? 'background: url('. $this->get_thumb($author['authors']['cover_img_url'], 400, 0) .') no-repeat content-box' : '' ?>" class="authors_bg">
                                    <div class="pano-img">
                                        <div class="mask"></div>
                                        <div class="author_profile">
                                            <img src="<?= !empty($author['authors']['avatar_url']) ? $this->url_image($author['authors']['avatar_url']) : '/images/authors/author_profile02.jpg' ?>">
                                        </div>
                                        <div class="author_info cf">
                                            <p><?= empty($author['authors']['username']) ? $author['authors']['email'] : Html::encode($author['authors']['username']) ?></p>
                                            <div class="pano_cnt"><i class="fas fa-vr-cardboard"></i><span><?= $author['total_project'] ?></span></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>

                </div> <!-- //.author-list -->

            </div>
        </div>
    </section><!-- //content -->
</div><!-- //#container -->

<?php $this->beginBlock('script') ?>
<script>
    $('.center').slick({
        centerMode: true,
        // autoplay: true,
        // autoplaySpeed: 2000,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.slick-dots li button').text("");
    $('.slider_container .slick-arrow').text('');

    var page = 0;
    var is_loading = false;

    $(document).ready(function(){
        var search_key = '<?= $search_key ?>';

        $(window).scroll(function(){
            // if (search_key === '') {
            //     return false;
            // }

            var position = $(window).scrollTop() + $(window).height();
            var $el = $('.tour-container');
            var bottom = $el.offset().top + $el.outerHeight(true);

            if( position >= bottom){
                if (is_loading === true) {
                    return false;
                }

                if (page == 'is_last') {
                    return false;
                }

                $.ajax({
                    url: '/user/load-more-author',
                    dataType: 'json',
                    async: true,
                    data: {
                        page : page + 1,
                        search_key: search_key
                    },
                    type: 'get',
                    beforeSend: function() {
                        is_loading = true;
                    },
                    complete: function(){
                        is_loading = false;
                    },
                    success: function (res) {
                        if (res.success) {
                            console.log(res);
                            if (res.is_last == true) {
                                page = 'is_last';
                            } else {
                                page = page + 1;
                            }
                            $('.tour-container').append(res.html);
                        }

                    }, error: function (res) {
                        console.log('Error');
                    }
                });

            }

        });

    });

</script>
<?php $this->endBlock() ?>