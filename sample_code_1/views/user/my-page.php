<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\web\View;

$this->title = 'My page';

?>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 500px;
            width: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
    </style>
<?php $page_name = isset($this->params['pageName']) ? $this->params['pageName'] : ''; ?>

    <section class="cover_visual">
        <div class="author-cover">
            <div class="mask">
                <div style="background: url('<?= !empty($user->cover_img_url) ? $this->url_image($user->cover_img_url) : '/images/authors/cover01.jpg' ?>') no-repeat;" class="profile_cover_img"></div>
                <?php if ($myPage): ?>
                    <div class="form-position">
                        <form id="frm-change-cover" class="author-cover-form" action="/site/change-cover" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                            <input type="hidden" name="type" value="cover">
                            <label class="profile_cover">
                                <span><img src="/images/icon/camera_icon.png" ></span> Change Cover
                                <input type="file" name="UploadForm[imageFile]" id="imageFile" class="profile_cover_1 cover-up" onchange="readURL(this);">
                            </label>
                        </form>
                    </div>
                <?php endif; ?>
            </div><!-- //.mask -->
        </div> <!-- //.author-cover -->
        <div class="cover-contents">
            <div class="profile">
                <img src="<?= empty($user->avatar_url) ? '/images/authors/author_profile02.jpg' : $this->url_image($user->avatar_url) ?>" >
                <?php if ($myPage): ?>
                    <form id="frm-change-avatar" class="edit_profile_img" enctype="multipart/form-data" method="post">
                        <input id="" type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                        <input type="hidden" name="type" value="avatar">
                        <label class="edit_img_btn">
                            <input id="cover-avatar" type="file" name="UploadForm[imageFile]" class="profile_img">
                            <div class="mask"><i class="fas fa-camera"></i></div>
                        </label>
                    </form>
                <?php endif; ?>
                <span class="label-cover <?= !empty($user->plan) && $user->plan->name != 'Starter' ? 'pro-cover' : '' ?>"><?= !empty($user->plan) && $user->plan->name != 'Starter' ? ($user->plan->name == 'Professional' ? 'Pro' : 'Business') : '' ?></span>
            </div>
            <h1 class="name-profile"><?= !empty($user->username) ? $user->username : $user->email ?></h1>
            <div class="authors-contact"><a href="javascript:" class="contact_btn">Contact</a></div>
        </div>
    </section><!-- //.cover_visual -->
    <div class="authors_nav">
        <div class="authors_nav_bar">
            <a href="javascript:">VR Tour <span><?= $total; ?></span></a>
        </div>
    </div> <!-- //.authors_nav -->
    <div id="container">
        <section id="contents">
            <div class="authors-container">
                <div class="map-container">
                    <div id="map" class="map-contents"></div>

                    <div class="fullScreenBtn" onclick="">Full Screen <span> > </span></div>
                </div>

                <div class="pano-contents">
                    <div class="tour-container cf">
                        <?php foreach ($projects as $project): ?>
                            <div class="tour-item">
                                <div class="tour_contents">
                                    <a href="/pano-player/<?= \app\components\HashHelper::hashNumber($project->id) ?>">
                                        <div class="pano-img">
                                            <div class="mask"></div>
                                            <img src="<?= !empty($project->thumb_path) ? $this->url_image($project->thumb_path) : '/images/explore/tour07.jpg' ?>" />
                                            <div class="tour-author cf">
                                                <div class="profile"><img src="<?= !empty($project->authors->avatar_url) ? $this->url_image($project->authors->avatar_url) : '/images/authors/author_profile02.jpg' ?>"></div>
                                                <div class="author-name"><?= empty($project->authors->username) ? $project->authors->email : $project->authors->username ?></div>
                                                <div class="view-cnt"><i class="fas fa-eye"></i><span><?= Html::encode($project->view) ?></span></div>
                                            </div>
                                        </div>
                                        <div class="pano-info">
                                            <strong><?= Html::encode($project->title) ?></strong>
                                            <span><?= $project->address ? Html::encode($project->address) : 'No location' ?></span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div> <!-- //.tour-container -->
                </div> <!-- //.pano-contents -->
                <div class="contact_popup">
                    <div class="contact_wrap">
                        <div class="close"><a href="javascript:"><i class="xi-close"></i></a></div>
                        <div class="row">
                            <form id="frm-contact" method="post" class="contact_form" enctype="multipart/form-datas">
                                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />

                                <input type="hidden" name="author_email" value="<?= $user->email ?>">
                                <div>
                                    <label class="hide">Title</label>
                                    <input type="text" name="title" class="form-box" placeholder="Title">
                                    <span id="error_title" class="label-danger"></span>

                                </div>
                                <div class="s_area cf">
                                    <label class="hide">Name</label>
                                    <input type="text" name="full_name" class="full-name form-box" placeholder="Name">
                                    <span id="error_full_name" class="label-danger"></span>

                                </div>
                                <div class="s_area cf">
                                    <p>
                                        <label class="hide">Email</label>
                                        <input type="text" name="email" class="email form-box" placeholder="Email">
                                        <span id="error_email" class="label-danger"></span>

                                    </p>
                                    <p>
                                        <label class="hide">Phone</label>
                                        <input type="text" name="phone_number" class="phone-number form-box" placeholder="Tel">
                                        <span id="error_phone_number" class="label-danger"></span>

                                    </p>
                                </div>
                                <div>
                                    <label class="hide">Company name</label>
                                    <input type="text" name="company_name" class="form-box" placeholder="Company Name">
                                    <span id="error_company_name" class="label-danger"></span>

                                </div>
                                <div class="message_area">
                                    <label class="hide">Message</label>
                                    <p class="right_txt">Character Limit <em class="bold">500</em></p>
                                    <span id="error_content" class="label-danger"></span>
                                    <textarea name="content" cols="40" rows="10" placeholder="Message..." maxlength="500"></textarea>
                                </div>
                                <div class="submit_btn_wrap">
                                    <button type="submit" value="<?= Yii::t('app', 'Submit') ?>" class="submit_btn" onkeyup="len_chk()">
                                        <i class="icon-loading fa fa-spinner fa-spin"></i> Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div> <!-- //.contact_wrap -->
                </div> <!-- //.contact_popup -->
        </section><!-- //content -->
    </div><!-- //#container -->

<?php $this->beginBlock('script') ?>
    <script>
        function getLocationMyProject() {
            $.ajax({
                url: '/site/get-location-my-project?id=<?= $user->id ?>',
                dataType: 'json',
                // data: $(form).serialize(),
                type: 'get',
                success: function (res) {
                    if (res.success) {
                        var locations = res.data;
                        initMap(locations);

                    } else {
                        initMap([]);
                    }

                }, error: function (res) {
                    initMap([]);
                }
            });
        }

        var min = .99999;
        var max = 1.00001;

        function initMap(locations) {

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 5,
                center: {lat: 37.5652894, lng: 126.8494645},
                minZoom: 3,
                maxZoom: 20
            });

            var infowindow = new google.maps.InfoWindow();

            var base_url = '<?= Yii::$app->params['resource_domain'] ?>';

            var markers = locations.map(function(location, i) {

                var lat = parseFloat(location.lat);
                var lng = parseFloat(location.lng);

                if (pointExists(locations, location.lat, location.lng)) {
                    lat = lat * (Math.random() * (max - min) + min);
                    lng = lng * (Math.random() * (max - min) + min);
                }

                var marker = new google.maps.Marker({
                    position: {
                        lat: lat,
                        lng: lng
                    },
                });

                var contentString =
                    '<div class="info-window">' +
                        '<div class="map-pop-contents">' +
                            '<div class="map-pop-contents-wrap">' +
                                '<a href="/pano-player/'+ location.id +'" target="_blank">'+
                                    '<div class="pop-thumb-box">'+
                                        '<div class="mask"></div>'+
                                        '<img src="'+ base_url + location.thumb_path +'">'+
                                    '</div>'+
                                '</a>'+
                                '<div class="pop-info-box">'+
                                    '<h2><a href="/pano-player/'+ location.id +'" target="_blank">' + location.title + '</a></h2>'+
                                    '<a href="/pano-player/'+ location.id +'" target="_blank" class="tour-location">' + (location.address ? location.address : 'No location') + '</a>'+
                                '</div>'+
                            '</div>'+
                        '</div>' +
                    '</div>';

                google.maps.event.addListener(marker, 'click', function(evt) {
                    infowindow.setContent(contentString);
                    infowindow.open(map, marker);
                });

                $(document).on('click', '.info-window', function (e) {
                    infowindow.close();
                });

                return marker;
            });

            // Add a marker clusterer to manage the markers.
            var markerCluster = new MarkerClusterer(map, markers,
                {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

            $('.fullScreenBtn').click(function() {
                $('#map button.gm-control-active.gm-fullscreen-control').trigger('click');
            });
        }

        function pointExists(arr, lat, lng, id) {
            return arr.some(function(el) {
                return el.lat == lat && el.lng == lng && el.id != id;
            });
        }

        $('#imageFile').change(function () {
            $('#frm-change-cover').submit();
        });

        $('#cover-avatar').change(function () {
            $('#frm-change-avatar').submit();
        });

        $('#frm-change-cover').submit(function(e){
            e.preventDefault();
            $.ajax( {
                url: '/user/change-cover',
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false,
                success: function (res) {
                    if (res.success == 1) {
                        window.location.reload();
                    } else {
                        alert(res.msg);
                    }
                }
            });
        });

        $('#frm-change-avatar').submit(function(e){
            e.preventDefault();
            $.ajax( {
                url: '/user/change-cover',
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false,
                success: function (res) {
                    if (res.success == 1) {
                        window.location.reload();
                    } else {
                        alert(res.msg);
                    }
                }
            });
        });

        $('#frm-contact').submit(function(e){
            e.preventDefault();
            $('.label-danger').text('');
            $('.icon-loading').css('display', 'inline-block');
            $.ajax( {
                url: '/user/author-contact',
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false,
                success: function (res) {
                    $('.icon-loading').css('display', 'none');

                    if (res.success == 1) {
                        alert('Sent successfully');
                        window.location.reload();
                    } else {
                        for (var key in res.message) {
                            if (!res.message.hasOwnProperty(key)) continue;
                            var obj = res.message[key];
                            for (var prop in obj) {
                                if(!obj.hasOwnProperty(prop)) continue;
                                $('#error_' + key).text(obj[prop]);
                            }
                        }
                    }
                }
            });
        });

        var page = 0;
        var is_loading = false;
        var user_id = '<?= $user->id ?>';
        $(document).ready(function(){
            $(window).scroll(function(){
                var position = $(window).scrollTop() + $(window).height();
                var $el = $('.tour-container');
                var bottom = $el.offset().top + $el.outerHeight(true);

                if( position >= bottom){
                    if (is_loading === true) {
                        return false;
                    }

                    if (page == 'is_last') {
                        return false;
                    }

                    $.ajax({
                        url: '/user/load-more-project',
                        dataType: 'json',
                        async: true,
                        data: {
                            page : page + 1,
                            user_id: user_id
                        },
                        type: 'get',
                        beforeSend: function() {
                            is_loading = true;
                        },
                        complete: function(){
                            is_loading = false;
                        },
                        success: function (res) {
                            if (res.success) {
                                console.log(res);
                                if (res.is_last == true) {
                                    page = 'is_last';
                                } else {
                                    page = page + 1;
                                }
                                $('.tour-container').append(res.html);
                            }

                        }, error: function (res) {
                            console.log('Error');
                        }
                    });

                }

            });

        });
    </script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=<?= Yii::$app->params['services']['google']['api_key'] ?>&callback=getLocationMyProject&sensor=false&region=KR">
    </script>

<?php $this->endBlock() ?>