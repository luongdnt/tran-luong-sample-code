<?php use yii\helpers\Html;

foreach ($projects as $project): ?>
    <div class="tour-item">
        <div class="tour_contents">
            <a href="/pano-player/<?= \app\components\HashHelper::hashNumber($project->id) ?>">
                <div class="pano-img">
                    <div class="mask"></div>
                    <img src="<?= !empty($project->thumb_path) ? $this->url_image($project->thumb_path) : '/images/explore/tour07.jpg' ?>" />
                    <div class="tour-author cf">
                        <div class="profile"><img src="<?= !empty($project->authors->avatar_url) ? $this->url_image($project->authors->avatar_url) : '/images/authors/author_profile02.jpg' ?>"></div>
                        <div class="author-name"><?= empty($project->authors->username) ? $project->authors->email : $project->authors->username ?></div>
                        <div class="view-cnt"><i class="fas fa-eye"></i><span><?= Html::encode($project->view) ?></span></div>
                    </div>
                </div>
                <div class="pano-info">
                    <strong><?= Html::encode($project->title) ?></strong>
                    <span><?= $project->address ? Html::encode($project->address) : 'No location' ?></span>
                </div>
            </a>
        </div>
    </div>
<?php endforeach; ?>