<html>
    <head>
        <meta charset="UTF-8">
        <title>VR Object View</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR:400,700&display=swap&subset=korean" rel="stylesheet">
        <link href="/css/vr-object.css" rel="stylesheet">
    </head>
    <body>
        <h1 class="text-center">사진을 좌우로 스와이프하여 돌려보실 수 있습니다.</h1>
        <div class="image-wrapper">
            <div id="view-object-wrapper">
                <?php if (!empty($images)) : ?>
                    <?php foreach ($images as $image) : ?>
                        <img data-src="<?= $image ?>"/>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="bottom-btn-wrapper text-center">
            <a href="https://play.google.com/store/apps/details?id=com.vr.vr_maker" target="_blank" class="link-btn">
                <i class="icon-google-play"></i> ObjectVr App 다운받기
            </a>
        </div>
        <script type="text/javascript" src="/js/jquery.js"></script>
        <script type="text/javascript" src="/js/circlr.min.js"></script>
        <script type="text/javascript">
            var crl = circlr('view-object-wrapper', {
                scroll : true,
                loader : 'loader'
            });
            var width = 0;
            var height = 0;
            var wrapper = null;
            window.onload = function() {
                width = window.innerWidth - 100;
                height = window.innerHeight - 300;
                if (width > height) {
                    width = Math.round(height * <?= $ratio ?>);
                } else {
                    height = Math.round(width / <?= $ratio ?>);
                }
                wrapper = document.getElementById("view-object-wrapper");
                wrapper.style.width = width + 'px';
                wrapper.style.height = height + 'px';
            }
        </script>
    </body>
</html>