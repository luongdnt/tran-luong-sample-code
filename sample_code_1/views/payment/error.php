<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\web\View;

$this->title = 'Error';

?>
<div id="container">
    <section id="contents">
        <div class="inner-wrap cf">
            <div class="privacy_wrap">
                <div class="payment-content">
                    <div class="modal-payment">
                        <div class="icon-box payment-error">
                            <i class="fa fa-exclamation" aria-hidden="true"></i>
                        </div>

                        <h4>Payment failed</h4>
                        <div class="modal-payment-body">
                            You have failed payment. Try paying again
                        </div>
                        <a href="/site/pricing">
                            <button class="btn-payment payment-error" data-dismiss="modal">OK</button>
                        </a>
                    </div>
                </div>
            </div>
        </div> <!-- //inner-wrap -->
    </section> <!-- //content -->
</div> <!-- //container -->

<?php $this->beginBlock('script') ?>

<?php $this->endBlock() ?>