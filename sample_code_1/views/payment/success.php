<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\web\View;

$this->title = 'Success';

?>
    <div id="container">
        <section id="contents">
            <div class="inner-wrap cf">
                <div class="privacy_wrap">
                    <div class="payment-content">
                        <div class="modal-payment">
                            <div class="icon-box">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </div>

                            <h4>Payment success</h4>
                            <div class="modal-payment-body">
                                You have successfully paid. Check your account for details.
                            </div>
                            <a href="/auth/authorize?client_id=<?= Yii::$app->params['cms_client_id'] ?>">
                                <button class="btn-payment" data-dismiss="modal">OK</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div> <!-- //inner-wrap -->
        </section> <!-- //content -->
    </div> <!-- //container -->

<?php $this->beginBlock('script') ?>

<?php $this->endBlock() ?>