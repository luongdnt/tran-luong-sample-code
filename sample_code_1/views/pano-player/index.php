<?php

use app\components\HashHelper;
use app\models\form\AudioHotspotForm;
use app\models\form\CallHotspotForm;
use app\models\form\FloorDesignHotspotForm;
use app\models\form\GaleryHotspotForm;
use app\models\form\InfoHotspotForm;
use app\models\form\PopupImageHotspotForm;
use app\models\form\ProductHotspotForm;
use app\models\form\TeleportHotspotForm;
use app\models\form\TextHotspotForm;
use app\models\form\VideoHotspotForm;
use app\models\form\WebLinkHotspotForm;
use app\models\Project;
use app\repositories\interfaces\ProjectRepository;
use yii\helpers\Url;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model Project */

$this->title = 'VR Player';
$assetManager = Yii::$app->assetManager;
$assetManager->publish('@webroot/player/krpano.js');
$assetManager->publish('@webroot/js/notify.min.js');

$this->registerJsFile(
    $assetManager->getPublishedUrl('@webroot/js/notify.min.js'),
    [View::POS_READY, 'depends' => ['yii\web\JqueryAsset']],
    'notify_jquery'
);

$this->registerJsFile(
    $assetManager->getPublishedUrl('@webroot/player/krpano.js'),
    [View::POS_READY, 'depends' => ['yii\web\JqueryAsset']],
    'playerVrmakerKrpano'
);

$assetManager->publish('@webroot/js/jquery.qrcode.min.js');

$this->registerJsFile(
    $assetManager->getPublishedUrl('@webroot/js/jquery.qrcode.min.js'),
    [View::POS_READY, 'depends' => ['yii\web\JqueryAsset']],
    'playerQRcode'
);

//$assetManager->publish('@webroot/js/clipboard.min.js');
//
//$this->registerJsFile(
//    $assetManager->getPublishedUrl('@webroot/js/clipboard.min.js'),
//    [View::POS_READY, 'depends' => ['yii\web\JqueryAsset']],
//    'clipboard'
//);

$this->registerJsFile(
    "https://cdnjs.cloudflare.com/ajax/libs/annyang/2.6.1/annyang.min.js",
    [View::POS_READY, 'depends' => ['playerVrmakerKrpano']],
    'playerAnnyAng'
);
$this->registerJsFile(
    "https://cdnjs.cloudflare.com/ajax/libs/SpeechKITT/1.0.0/speechkitt.min.js",
    [View::POS_READY, 'depends' => ['playerVrmakerKrpano']],
    'playerSpeechkitt'
);

$launcherUrl = "none";
if ($model->show_launcher && !empty($model->launcher_path)) {
    if ($model->launcher_type == ProjectRepository::LAUNCHER_TYPE_IMAGE) {
        $launcherUrl = "url('".Yii::$app->params['resource_domain'] . $model->launcher_path."')";
    } else {
        $launcherUrl = Yii::$app->params['resource_domain'] . $model->launcher_path;
    }
}

$qrCodeUrl = Url::current([], true);
?>
<iframe src="/player/silence.mp3" allow="autoplay" id="audio" style="position: absolute; top: -100px; left: -100px; width: 0; height: 0;"></iframe>
<!--<div class="modal-copy">-->
<!--    <label>Are u copy link</label>-->
<!--    <div>-->
<!--        <button class="btn-copy" data-clipboard-text="--><?//= $qrCodeUrl ?><!--">Yes</button>-->
<!--        <button>No</button>-->
<!--    </div>-->
<!--</div>-->

<div>
    <div class="loading_wrapper" style="background-image: <?= $launcherUrl ?>; background-color: #000000; background-repeat: no-repeat;background-attachment: fixed;background-position: center; background-size: cover;">
        <?php if($model->launcher_type == ProjectRepository::LAUNCHER_TYPE_VIDEO && $model->show_launcher): ?>
            <video width="100%" autoplay="" muted="" playsinline="" style="top: -100% !important">
                <source src="<?= $launcherUrl ?>">
                Your browser does not support HTML5 video.                
            </video>
        <?php else: ?>
        <div class="loading_wrap">
            <div class="loading-container">
                <div class="loading"></div>
                <div class="loading loading_inside"></div>
                <div class="loading loading_inside2"></div>
                <div class="loading loading_inside3"></div>
                <div class="loading-text">360º</div>
            </div>
            <div class="vvisit_logo">VRmaker</div>
        </div>
        <?php endif; ?>
        <a href="javascript:" class="vr-launch-screen-play-btn launch-project-elms">
            <span>Skip Intro</span>
        </a>
    </div>
</div>
<div class="project-view">
    <div id='pano-container' class="body-content" style="padding: 0;z-index: -1">
        <div id="pano" style="width:100%;height:100%;">
            <noscript>
                <table style="width:100%;height:100%;">
                    <tr style="vertical-align:middle;">
                        <td>
                            <div style="text-align:center;">Error:<br/><br/>Javascript is not open<br/><br/></div>
                        </td>
                    </tr>
                </table>
            </noscript>
        </div>
    </div>
</div>
<?php
$author = $model->authors;
$playerUrl = $assetManager->getPublishedUrl('@webroot/player');
$baseUrl = Url::base('');
$xmlUrl = Url::to(['pano-player/generate/', 'id' => HashHelper::hashNumber($model->id), 'v' => Yii::$app->formatter->asTimestamp($model->updated_at)]);
$qrCodeUrl = Url::current([], true);
$portraitUrl = !empty($author->avatar_url) ? $author->avatar_url : '/images/main/temp_profile.jpg';
$resourceUrl = Yii::$app->params['resource_domain'];

$shareParams = [
    'app_id' => Yii::$app->params['services']['facebook']['app_id'],
    'display' => 'popup',
    'href' => $qrCodeUrl,
    'redirect_uri' => $qrCodeUrl,
];
$shareFbUrl = "https://www.facebook.com/dialog/share?" . http_build_query($shareParams);
$autoRotation = $model->auto_rotation == 1 ? "true" : "false";

//background music
$bgMusic = $model->bg_music;
$bgMusicPath = pathinfo($resourceUrl.$model->bg_music_path, PATHINFO_DIRNAME);
$bgMusicName = pathinfo($resourceUrl.$model->bg_music_path, PATHINFO_FILENAME);
$bgMusicPathWithoutExt = $bgMusicPath . "/" . $bgMusicName;

$js = <<<JS
    var panoContainer = $('#pano-container');
    var height = $(window).height();
    var width = $(window).width();
    panoContainer.css('height', height);
    panoContainer.css('width', width);
    $(window).resize(function() {
        panoContainer.css('height', $(window).height());
        panoContainer.css('width', $(window).width());
    });
    $("body").css('overflow', 'hidden');
    
    var initvars = {
        project_id: "$model->id",
        player_url: "$playerUrl",
        base_url: "$baseUrl",
        qrcodeurl: "$qrCodeUrl",
        portrait_url: "$portraitUrl",
        resource_url: "$resourceUrl",
        share_fb_url: "$shareFbUrl",
        auto_rotation: "$autoRotation",
        bg_music: "$bgMusic",
        bg_music_path: "$bgMusicPathWithoutExt",
        bg_music_name: "$bgMusicName",
    };
    embedpano({
        xml : "$xmlUrl",
        target : "pano",
        initvars: initvars,
        passQueryParameters : false,
        mobilescale: 1.0
    });
JS;
$this->registerJs($js, View::POS_END);
?>

<style>
    @font-face {
        font-family: nanumgothic;
        src: url(<?= Yii::getAlias("@web") ?>/fonts/NanumGothic.woff);
    }
    @font-face {
        font-family: nanumgothicbold;
        src: url(<?= Yii::getAlias("@web") ?>/fonts/NanumGothicBold.woff);
    }
    @font-face {
        font-family: nanumbarungothic;
        src: url(<?= Yii::getAlias("@web") ?>/fonts/NanumBarunGothic.woff);
    }
    @font-face {
        font-family: nanumbarungothicbold;
        src: url(<?= Yii::getAlias("@web") ?>/fonts/NanumBarunGothicBold.woff);
    }

    .loading_wrapper {
        width: 100%;
        height: 100vh;
        background: #000;
    }
    .loading_wrapper .loading_wrap{
        width: 100%;
        height: 200px;
        position: absolute;
        top: 50%;
        margin-top: -100px;
    }
    .loading_wrap .vvisit_logo{
        text-align: center;
        color: #fff;
        font-family: 'SFProDisplay-Semibold';
        font-size: 55px;
        margin-top: 30px;
    }
    @keyframes rotate-loading {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }

    @-moz-keyframes rotate-loading {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }

    @-webkit-keyframes rotate-loading {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }

    @-o-keyframes rotate-loading {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }

    @keyframes rotate-loading {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }

    @-moz-keyframes rotate-loading {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }

    @-webkit-keyframes rotate-loading {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }

    @-o-keyframes rotate-loading {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }




    /*rotate-loading2*/
    @keyframes rotate-loading2 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }

    @-moz-keyframes rotate-loading2 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }

    @-webkit-keyframes rotate-loading2 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }

    @-o-keyframes rotate-loading2 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }

    @keyframes rotate-loading2 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }

    @-moz-keyframes rotate-loading2 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }

    @-webkit-keyframes rotate-loading2 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }

    @-o-keyframes rotate-loading2 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }
    /*//rotate-loading2*/


    /* rotate-loading3 */
    @keyframes rotate-loading3 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }

    @-moz-keyframes rotate-loading3 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }

    @-webkit-keyframes rotate-loading3 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }

    @-o-keyframes rotate-loading3 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }

    @keyframes rotate-loading3 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }

    @-moz-keyframes rotate-loading3 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }

    @-webkit-keyframes rotate-loading3 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }

    @-o-keyframes rotate-loading3 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
    }
    /* //rotate-loading3 */

    /*rotate-loading4*/
    @keyframes rotate-loading4 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }

    @-moz-keyframes rotate-loading4 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }

    @-webkit-keyframes rotate-loading4 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }

    @-o-keyframes rotate-loading4 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }

    @keyframes rotate-loading4 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }

    @-moz-keyframes rotate-loading4 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }

    @-webkit-keyframes rotate-loading4 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }

    @-o-keyframes rotate-loading4 {
        0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
        100% {transform: rotate(-360deg);-ms-transform: rotate(-360deg); -webkit-transform: rotate(-360deg); -o-transform: rotate(-360deg); -moz-transform: rotate(-360deg);}
    }
    /*//rotate-loading4*/





    @keyframes loading-text-opacity {
        0%  {opacity: 0}
        20% {opacity: 0}
        50% {opacity: 1}
        100%{opacity: 0}
    }

    @-moz-keyframes loading-text-opacity {
        0%  {opacity: 0}
        20% {opacity: 0}
        50% {opacity: 1}
        100%{opacity: 0}
    }

    @-webkit-keyframes loading-text-opacity {
        0%  {opacity: 0}
        20% {opacity: 0}
        50% {opacity: 1}
        100%{opacity: 0}
    }

    @-o-keyframes loading-text-opacity {
        0%  {opacity: 0}
        20% {opacity: 0}
        50% {opacity: 1}
        100%{opacity: 0}
    }

    .loading-container,
    .loading {
        height: 100px;
        position: relative;
        width: 100px;
        border-radius: 100%;
    }
    .loading-container {
        margin: auto;
    }

    .loading {
        border: 2px solid transparent;
        border-color: transparent #fff transparent #FFF;
        -moz-animation: rotate-loading 1.5s linear 0s infinite normal;
        -moz-transform-origin: 50% 50%;
        -o-animation: rotate-loading 1.5s linear 0s infinite normal;
        -o-transform-origin: 50% 50%;
        -webkit-animation: rotate-loading 1.5s linear 0s infinite normal;
        -webkit-transform-origin: 50% 50%;
        animation: rotate-loading 1.5s linear 0s infinite normal;
        transform-origin: 50% 50%;
    }

    .loading_inside{
        width: 100px;
        height:80px;
        border-radius:50%;
        border: 1px solid #fff;
        position:absolute;
        top:10px;
        left:0;
        -moz-animation: rotate-loading2 2s linear 0s infinite normal;
        -moz-transform-origin: 50% 50%;
        -o-animation: rotate-loading2 2s linear 0s infinite normal;
        -o-transform-origin: 50% 50%;
        -webkit-animation: rotate-loading2 2s linear 0s infinite normal;
        -webkit-transform-origin: 50% 50%;
        animation: rotate-loading2 2s linear 0s infinite normal;

    }
    .loading_inside2{
        width: 100px;
        height:60px;
        border-radius:50%;
        border: 1px solid #fff;
        position:absolute;
        top:20px;
        left:0;
        -moz-animation: rotate-loading3 3.5s linear 0s infinite normal;
        -moz-transform-origin: 50% 50%;
        -o-animation: rotate-loading3 3.5s linear 0s infinite normal;
        -o-transform-origin: 50% 50%;
        -webkit-animation: rotate-loading3 3.5s linear 0s infinite normal;
        -webkit-transform-origin: 50% 50%;
        animation: rotate-loading3 3.5s linear 0s infinite normal;

    }
    .loading_inside3{
        width: 100px;
        height:60px;
        border-radius:50%;
        border: 1px solid #fff;
        position:absolute;
        top:20px;
        left:0;
        -moz-animation: rotate-loading4 4s linear 0s infinite normal;
        -moz-transform-origin: 50% 50%;
        -o-animation: rotate-loading4 4s linear 0s infinite normal;
        -o-transform-origin: 50% 50%;
        -webkit-animation: rotate-loading4 4s linear 0s infinite normal;
        -webkit-transform-origin: 50% 50%;
        animation: rotate-loading4 4s linear 0s infinite normal;

    }

    .loading-container .loading {
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
    }

    .loading-text {
        -moz-animation: loading-text-opacity 2s linear 0s infinite normal;
        -o-animation: loading-text-opacity 2s linear 0s infinite normal;
        -webkit-animation: loading-text-opacity 2s linear 0s infinite normal;
        animation: loading-text-opacity 2s linear 0s infinite normal;
        color: #ffffff;
        font-family: "Helvetica Neue, "Helvetica", ""arial";
        font-size: 20px;
        font-weight: bold;
        opacity: 0;
        position: absolute;
        text-align: center;
        text-transform: uppercase;
        top: 0;
        width: 100px;
        line-height: 100px;
    }
    .vr-launch-screen-play-btn {
        position: absolute;
        right: 15px;
        bottom: 30px;
        width: 150px;
        height: 40px;
        display: none;
    }
    .vr-launch-screen-play-btn span {
        padding: 6px 30px;
        background: #333;
        display: inline-block;
        color: #fff;
    }

    .modal-copy {
        position: absolute;
        height: 100px;
        width: 150px;
        margin: -50px -75px;
        top:50%;
        left:50%;
        z-index:99999;
        /*display: none;*/
        background-color: rgb(0, 174, 239);
        border-radius: 15px;
        color: #fff;
        font-weight: bold;
        border: 1px;
        text-align: center;
    }
</style>

