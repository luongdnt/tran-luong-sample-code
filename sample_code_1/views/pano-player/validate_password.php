<?php

use app\models\Project;
use yii\helpers\Url;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model Project */

$this->title = 'VR Player';
$assetManager = Yii::$app->assetManager;
$assetManager->publish('@webroot/player/krpano.js');
$this->registerCssFile("/css/import.css");
$this->registerCssFile("/css/my-style.css?v=" . time());

$this->registerCssFile("https://use.fontawesome.com/releases/v5.5.0/css/all.css", [
    'integrity' => 'sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU',
    'crossorigin' => 'anonymous'
]);

$this->registerCssFile("//cdn.jsdelivr.net/npm/xeicon@2.3.3/xeicon.min.css");
$this->registerCssFile("https://fonts.googleapis.com/css?family=Noto+Sans+KR");

$this->registerJsFile("/js/jquery.js?v=" . time());
$this->registerJsFile("/js/jquery-ui.min.js?v=" . time());
$this->registerJsFile("/js/prefixfree.js?v=" . time());
$this->registerJsFile("/js/common.js?v=" . time());
?>
<div class="container sharing-pasword-form">
    <div class="password-form">
        <div class="form-container">
            <a href="/"><img src="../images/layout/vrmaker_logo_w.png" alt="logo" /></a>
            <h1><?= Yii::t('app', 'Enter Password') ?></h1>
            <form class="prj-pass-container" method="post" action="/pano-player/<?= $id ?>">
                <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken()?>" />
                <input type="password" name="sharing_password" placeholder="<?= Yii::t('app', 'Please enter sharing password') ?>" class="prj-pass-box">
                <?php if (Yii::$app->session->hasFlash('error')): ?>
                    <p class="incorrect-msg" style="display: block;"><?= Yii::$app->session->getFlash('error') ?></p>
                <?php endif; ?>
                <button type="submit" class="prj-button">Enter</button>
            </form>
        </div>
    </div>
</div>

