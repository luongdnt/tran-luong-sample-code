<?php

use app\models\form\PolygonHotspotForm;
use app\models\form\TextHotspotForm;
use app\models\Hotspot;
use app\models\Project;
use app\models\Scene;
use yii\helpers\Json;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\helpers\UtilHelper;
use app\repositories\interfaces\FileRepository;

/* @var $this View */
/* @var $model Project */
/* @var $scenes Scene[] */
/* @var $hotspots Hotspot[][] */
$setStartView = false;
$animationHotspotProductImg = 0;
$author = $model->authors;
$launcherUrl = $model->show_launcher && !empty($model->launcher_path) ? "$model->launcher_path" : "";

?>
<krpano>
    <include url="calc: player_url + '/view/route.xml'"/>
    <style name="text"
           url="%$player_url%/plugins/textfield.swf"
           background="false"
           css="color:#FFFFFF;"
           enabled="false"
    />
    <textstyle name="htmltext4"
               font="Arial" fontsize="18" bold="true"
               width="240" height="auto" roundedge="0"
               background="false" backgroundalpha="1"
               border="false"
               textcolor="0xFFFFFF"
               fadeintime="0.5"
               padding="20"
               textalign="center"
               edge="center" xoffset="0" yoffset="0"
    />
    <style name="project_menu" bgalpha="0" bgborder="0" bgroundedge="15" align="bottom"  padding="5 15" y="-30" css="font-size:18px; color:#ffffff; line-height=25px;" visible="true" />
    <author name="author_data" full_name="<?= $author->fullName ?>"
            plan="<?= $author->plan ? $author->plan->name : "" ?>" username="<?= !empty($author->username) ? $author->username : $author->email?>"
            avatarurl="<?= !empty($author->avatar_url) ? $author->avatar_url : Yii::getAlias("@web") . '/images/main/temp_profile.jpg' ?>" />
    <project_info name="project_info_data"
                  title="<?= htmlspecialchars($model->title) ?>" show_title="<?= htmlspecialchars($model->show_title) ?>"
                  lat="<?= $model->lat ?>" lng="<?= $model->lng ?>" show_location="<?= $model->show_location ?>"
                  auto_rotation="<?= $model->auto_rotation ?>"
                  description="<?= htmlspecialchars($model->description) ?>"
                  address="<?= htmlspecialchars($model->address) ?>"
                  create_time="<?= $model->created_at ?>"
                  launcherurl="<?= $launcherUrl ?>"
    />
    <?php if (!empty($menu)): ?>
        <?php $metadata = !empty($menu->metadata) ? Json::decode($menu->metadata, true) : []; ?>
        <?php if (!empty($metadata['show_menu']) && $metadata['show_menu'] == 1 &&!empty($metadata['menu_items'])): ?>
            <layer name="project_menu" keep="true" total="<?= count($metadata['menu_items']) ?>" type="container" align="top" bgalpha="0" oy="20px">
                <?php foreach ($metadata['menu_items'] as $index => $item): ?>
                    <layer name="project_menu_<?= $index ?>" type="text" align="center" style="project_menu"
                           html="<?= $item['name'] ?>" onautosized="menuSizeChanged()" y="20px"
                        <?php if ($item['type'] == 'url'): ?>
                            onclick="openurl('<?= $item['value'] ?>',_blank)"
                        <?php elseif ($item['type'] == 'scene'): ?>
                            onclick="loadscene(<?= 'scene_' . $item['value'] ?>)"
                        <?php endif; ?>
                    />
                <?php endforeach; ?>
            </layer>
        <?php endif; ?>
    <?php endif; ?>
    <?php foreach ($scenes as $key => $scene) { ?>
        <scene name="<?= "scene_" . $scene->id ?>" title="<?= $scene->title ?>" scene_id="<?= $scene->id ?>"
               thumburl="'<?= "%\$resource_url%" . $scene->thumb ?>" lat="<?= $model->lat ?>" lng="<?= $model->lng ?>"
               onstart="startSceneHook();">
            <?php if ($scene->file->type == FileRepository::TYPE_VIDEO) : ?>
                <image>
                    <sphere url="plugin:video" />
                </image>
                <plugin name="video"
                        url.html5="%$player_url%/plugins/videoplayer.js"
                        url.flash="%$player_url%plugins/videoplayer.swf"
                        videourl="<?= "%\$resource_url%" . (!empty($scene->file->cube_path) ? $scene->file->cube_path . '/' . pathinfo($scene->file->public_path, PATHINFO_BASENAME) : $scene->image) ?>"
                />
            <?php else: ?>
                <?php if ($scene->file && $scene->file->cube_converted): ?>
                    <?php if ($scene->file->multires): ?>
                        <image type="CUBE" multires="true" titlesize="512">
                            <level tiledimagewidth="1024" tiledimageheight="1024">
                                <cube url="<?= "%\$resource_url%" . $scene->cubePath ?>/l1/pano_%s_%v_%h.jpg" />
                            </level>
                            <level tiledimagewidth="1920" tiledimageheight="1920">
                                <cube url="<?= "%\$resource_url%" . $scene->cubePath ?>/l2/pano_%s_%v_%h.jpg" />
                            </level>
                        </image>
                    <?php else: ?>
                        <preview url="<?= "%\$resource_url%" . $scene->cubePath ?>/preview.jpg" />
                        <image>
                            <cube url="<?= "%\$resource_url%" . $scene->cubePath ?>/pano_%s.jpg" />
                        </image>
                    <?php endif; ?>
                <?php else: ?>
                    <image>
                        <sphere url="<?= "%\$resource_url%" . $scene->image ?>"/>
                    </image>
                <?php endif; ?>
            <?php endif; ?>
            <?php if (array_key_exists($scene->id, $hotspots)) : ?>
                <?php foreach ($hotspots[$scene->id] as $hotspot) : ?>
                    <?php if ($hotspot->type == Hotspot::TYPE_START_VIEW) { ?>
                        <?php if ($hotspot->scene_id == $scene->id): ?>
                            <view fovtype="MFOV" fov="140" maxpixelzoom="2.0" fovmin="30" fovmax="140" limitview="auto"
                                  hlookat="<?= $hotspot->ath ?: 0 ?>" vlookat="<?= $hotspot->atv ?: 0 ?>"
                            />
                            <?php $setStartView = true; ?>
                        <?php endif; ?>
                        <?php
                        continue;
                    } ?>
                    <?php if ($hotspot->css) {
                        try {
                            list($fontFamily, $fontSize) = explode(",", $hotspot->css);
                            $css = "font-family:" . TextHotspotForm::$fontFamiliesSupported[$fontFamily] . ";font-size:" . $fontSize . "px;";
                        } catch (Exception $e) {
                            $css = false;
                        }
                    }
                    ?>
                    <?php switch ($hotspot->type):
                        case Hotspot::TYPE_GALLERY: ?>
                            <?php
                            $data = Json::decode($hotspot->metadata, true);
                            $items = !empty($data['items']) ? $data['items'] : [];
                            ?>
                            <?php if (!empty($data['gallery_path'])) : ?>
                                <Set_<?= $hotspot->id ?> photo="%$resource_url%<?= rtrim($data['gallery_path'], '/') ?>/"
                                                         miniature_photo="%$resource_url%<?= rtrim($data['gallery_path'], '/') ?>/"
                                                         number_thumbnails="<?= count($items) ?>"
                                    <?php if (count($items) > 0): ?>
                                        <?php foreach ($items as $index => $file) : ?>
                                            titles_<?= $index + 1 ?>="<?= $file['type'] ?>"
                                            <?php if (!empty($file['youtube_url'])): ?>
                                                youtube_<?= $index + 1 ?>="<?= $file['youtube_url'] ?>"
                                            <?php endif; ?>
                                            <?php if (!empty($file['video_url'])): ?>
                                                video_<?= $index + 1 ?>="<?= $file['video_url'] ?>"
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                />
                                <hotspot name="<?= $hotspot->name ?>"
                                         url="%$resource_url%<?= $hotspot->url ?>"
                                         scale="<?= $hotspot->scale && $hotspot->defaultIcon != $hotspot->url ? $hotspot->scale : $hotspot->defaultScale ?>"
                                         ath="<?= $hotspot->ath ?: 0 ?>"
                                         atv="<?= $hotspot->atv ?: 0 ?>"
                                         alpha="<?= $hotspot->alpha ?>"
                                         onclick="Galereya(Set_<?= $hotspot['id'] ?>);"/>
                            <?php endif; ?>
                            <?php break; ?>
                        <?php case Hotspot::TYPE_FLOOR_DESIGN: ?>
                            <?php
                            $data = Json::decode($hotspot->metadata);
                            ?>
                            <floor_design_data name="<?= $hotspot->name ?>" design-name="<?= !empty($data['design_name']) ? $data['design_name'] : "" ?>"
                                               url="<?= !empty($data['design_url']) ? $data['design_url'] : "" ?>"
                                               design-width="<?= !empty($data['design_width']) ? $data['design_width'] : 0 ?>"/>
                            <?php foreach ($data['position_config'] as $key => $point): ?>
                                <floor_design_points name="<?= 'point_' . $key . $scene->id ?>"
                                                     key="<?= $key ?>"
                                                     scene_name="<?= 'scene_' . $key ?>"
                                                     x="<?= !empty($point['position_x_percent']) ? $point['position_x_percent'] : 50 ?>%"
                                                     y="<?= !empty($point['position_y_percent']) ? $point['position_y_percent'] : 50 ?>%"
                                                     degree="<?= $point['degree'] ?>"
                                />
                            <?php endforeach; ?>
                            <?php break; ?>
                        <?php case Hotspot::TYPE_AUDIO: ?>
                            <?php $data = Json::decode($hotspot->metadata); ?>
                            <audio_hotspot name="<?= $hotspot->name ?>"
                                           scene_id="<?= $scene->id ?>"
                                           path="<?= !empty($data['audio_url']) ? $data['audio_url'] : '' ?>"
                                           ath="<?= $hotspot->ath ?: 0 ?>"
                                           atv="<?= $hotspot->atv ?: 0 ?>"
                                           range="<?= !empty($data['range']) ? $data['range'] : '' ?>"
                                           rate="<?= !empty($data['rate']) ? $data['rate'] : '' ?>"
                                           volume="<?= !empty($data['volume']) ? $data['volume'] : '' ?>"/>
                            <?php break; ?>
                        <?php case Hotspot::TYPE_POLYGON: ?>
                            <?php
                            $data = Json::decode($hotspot->metadata);
                            $actionType = $data['action'];
                            switch ($actionType) {
                                case PolygonHotspotForm::PHOTO_ACTION_TYPE: ?>
                                    <Set_<?= $hotspot->id ?> photo="<?= !empty($data['actionData']['photoUrl']) ? "%\$resource_url%/" . $data['actionData']['photoUrl'] : "" ?>"
                                                             miniature_photo="<?= !empty($data['actionData']['photoUrl']) ? "%\$resource_url%/" . $data['actionData']['photoUrl'] : "" ?>"
                                                             number_thumbnails="1"
                                    />
                                    <?php break;
                                case PolygonHotspotForm::TEXT_ACTION_TYPE: 
                                    $txtAth = (min(ArrayHelper::getColumn($data['points'], 'ath')) + max(ArrayHelper::getColumn($data['points'], 'ath'))) / 2;
                                    $txtAtv = min(ArrayHelper::getColumn($data['points'], 'atv'));
                                    $txtCss = UtilHelper::generateFontTxt($data['actionData']['textWritingFont']). "font-size:" . $data['actionData']['textWritingSize'] . "px;color:#" . $data['actionData']['textWritingColor'] . ";";
                                ?>
                                    <hotspot name="text_writing_polygon_action_<?= $hotspot->name ?>" type="text"
                                             ath="<?= $txtAth ?>" atv="<?= $txtAtv ?>" html="<?= $data['actionData']['textWriting'] ?>"
                                             css="<?= $txtCss ?>" bg="false" oy="-25" distorted="true"
                                    />
                                    <?php break;
                            }
                            ?>
                            <hotspot name="<?= $hotspot->name ?>" url="" ath="" atv=""
                                     hotspotid="<?= $hotspot->id ?>"
                                     polygontitle="<?= !empty($data['title']) ? $data['title'] : '' ?>"
                                     enabled="true"
                                     capture="false"
                                     handcursor="true"
                                     zorder="0"
                                     fillcolor="<?= !empty($data['fillColor']) ? '0x' . substr($data['fillColor'], 1) : '0xFF0000' ?>"
                                     mem_fillcolor="<?= !empty($data['fillColor']) ? '0x' . substr($data['fillColor'], 1) : '0xFF0000' ?>"
                                     fillcolor2="<?= !empty($data['hoverColor']) ? '0x' . substr($data['hoverColor'], 1) : '0xFF0000' ?>"
                                     fillalpha="<?= !empty($data['fillAlpha']) ? $data['fillAlpha'] : 0.5 ?>"
                                     borderwidth="2"
                                     bordercolor="<?= !empty($data['outlineColor']) ? '0x' . substr($data['outlineColor'], 1) : '0x55FF00' ?>"
                                     borderalpha="1.00"
                                     onover="tween(hotspot[get(name)].fillcolor,get(hotspot[get(name)].fillcolor2),0.2);"
                                     onout="tween(hotspot[get(name)].fillcolor,get(hotspot[get(name)].mem_fillcolor),0.2);"
                                     actiontype="<?= $actionType ?>"
                                     teleportscene="<?= $data['actionData']['sceneId'] ?>"
                                     photosetid="Set_<?= $hotspot->id ?>"
                                     photoactionurl="<?= !empty($data['actionData']['photoUrl']) ? "%\$resource_url%/" . $data['actionData']['photoUrl'] : "" ?>"
                                     targetlink="<?= $data['actionData']['weblink'] ?>"
                                     text_writing="<?= $data['actionData']['textWriting'] ?>"
                                     text_writing_color="<?= $data['actionData']['textWritingColor'] ?>"
                                     text_writing_size="<?= $data['actionData']['textWritingSize'] ?>"
                                     text_writing_font="<?= $data['actionData']['textWritingFont'] ?>"
                                     onclick="polygon_click_handler();"
                            >
                                <?php foreach ($data['points'] as $key => $point): ?>
                                    <point ath="<?= $point['ath'] ?>"
                                           atv="<?= $point['atv'] ?>"
                                    />
                                <?php endforeach; ?>
                            </hotspot>
                            <?php break; ?>
                        <?php default: ?>
                            <?php $metadata = !empty($hotspot->metadata) ? Json::decode($hotspot->metadata, true) : []; ?>
                            <hotspot name="<?= $hotspot->name ?>"
                                     renderer="<?= preg_match("/^.*\.(GIF|gif)$/i", $hotspot->url) == 1 ? 'css3d' : 'webgl' ?>"
                                     url="%$resource_url%/<?= $hotspot->url ?>"
                                     scale="<?= $hotspot->scale && $hotspot->defaultIcon != $hotspot->url ? $hotspot->scale : $hotspot->defaultScale ?>"
                                     ath="<?= $hotspot->ath ?: 0 ?>"
                                     atv="<?= $hotspot->atv ?: 0 ?>"
                                     alpha="<?= $hotspot->alpha ?>"
                                <?php if (!empty($hotspot->rotate)): ?>
                                    rotate="<?= $hotspot->rotate ?>"
                                <?php endif; ?>
                                <?php if (!empty($hotspot->width)): ?>
                                    width="<?= $hotspot->width ?>"
                                <?php endif; ?>
                                <?php if (!empty($hotspot->height)): ?>
                                    height="<?= $hotspot->height ?>"
                                <?php endif; ?>
                                <?php if (!empty($hotspot->rx) || !empty($hotspot->ry) || !empty($hotspot->rz)): ?>
                                    distorted="true"
                                    rx="<?= $hotspot->rx ?: 0; ?>"
                                    ry="<?= $hotspot->ry ?: 0; ?>"
                                    rz="<?= $hotspot->rz ?: 0; ?>"
                                <?php endif; ?>
                                <?php switch ($hotspot->type) :
                                    case Hotspot::TYPE_TEXT: ?>
                                        <?php if (!empty($hotspot->html)): ?>
                                            type="text"
                                            html="<?= $hotspot->html ?>"
                                            capture="false"
                                            enable="false"
                                        <?php endif; ?>
                                        textbgcolor="<?= $hotspot->bg_color ?>"
                                        <?php if ($hotspot->bg_color == "none"): ?>
                                            bg="false"
                                        <?php else: ?>
                                            bgcolor="0x<?= $hotspot->bg_color ?>"
                                        <?php endif; ?>
                                        <?php if (!empty($css)): ?>
                                            <?php if (!empty($hotspot->color_text)) {
                                                $css .= "color:#$hotspot->color_text;";
                                            } ?>
                                            css="<?= $css ?>"
                                        <?php endif; ?>
                                        <?php break; ?>
                                    <?php case Hotspot::TYPE_INFO_BOX: ?>
                                        onclick="showToolTip();"
                                        onhover="showtext('<?= !empty($metadata['info_title']) ? $metadata['info_title'] : '' ?>', htmltext4)"
                                        <?php break; ?>
                                    <?php case Hotspot::TYPE_WEB_LINK: ?>
                                        onclick="openurl('<?= htmlspecialchars($hotspot->html); ?>', _blank);"
                                        onhover="showtext(Visit website: <?= htmlspecialchars($hotspot->html); ?>, htmltext4)"
                                        <?php break; ?>
                                    <?php case Hotspot::TYPE_TELEPORT: ?>
                                        nextscene="scene_<?= !empty($metadata['scene_id']) ? $metadata['scene_id'] : '' ?>"
                                        onclick="loadNewSceneInThumb()"
                                        <?php break; ?>
                                    <?php case Hotspot::TYPE_VIDEO: ?>
                                        vr="true"
                                        url.html5="%$player_url%/plugins/videoplayer.js"
                                        url.flash="%$player_url%plugins/videoplayer.swf"
                                        productphoto="<?= !empty($metadata['video_url']) ? $metadata['video_url'] : '' ?>"
                                        videourl="<?= !empty($metadata['video_url']) ? $metadata['video_url'] : '' ?>"
                                        enablecontrol="<?= !empty($metadata['enable_control']) ? $metadata['enable_control'] : '' ?>"
                                        <?php if (!empty($hotspot->ox)): ?>
                                            ox="<?= $hotspot->ox ?>"
                                        <?php endif; ?>
                                        <?php if (!empty($hotspot->oy)): ?>
                                            oy="<?= $hotspot->oy ?>"
                                        <?php endif; ?>
                                        <?php if (empty($hotspot->rx) && empty($hotspot->ry)): ?>
                                            distorted="true"
                                        <?php endif; ?>
                                        <?php if (!empty($metadata['enable_control']) && $metadata['enable_control'] == 1): ?>
                                            onloaded="addVideoButtons(<?= $hotspot->name ?>);"
                                        <?php endif; ?>
                                        <?php break; ?>
                                    <?php case Hotspot::TYPE_PRODUCT: ?>
                                        onclick='showProductHotspot();'
                                        <?php break; ?>
                                    <?php case Hotspot::TYPE_IMAGE: ?>
                                        onclick='showImageHospot();'
                                        <?php break; ?>
                                    <?php case Hotspot::TYPE_CALL_NUMBER: ?>
                                        onhover="showtext(Call now: <?= $hotspot->html ?>, htmltext4)"
                                        onclick="openurl('tel://<?= $hotspot->html ?>', _blank);"
                                        <?php break;?>
                                    <?php endswitch; ?>
                            />
                            <?php if ($hotspot->type == Hotspot::TYPE_INFO_BOX) : ?>
                                <hotspot name="background<?= $hotspot->name ?>" style="background_layer"
                                         ath="<?= $hotspot->ath ?>" distorted="true"
                                         parenthpname="<?= $hotspot->name ?>"
                                         atv="<?= $hotspot->atv ?>" hotspotvrtype="1" visible="false"
                                         fillcolor="0xFF0000" type="text" roundedge="6"
                                />
                                <hotspot name="titleinfo_<?= $hotspot->name ?>" ath="<?= $hotspot->ath ?>"
                                         distorted="true" parenthpname="<?= $hotspot->name ?>"
                                         atv="<?= $hotspot->atv ?>" type="text" hotspotvrtype="1" visible="false"
                                         oy="60" bgalpha="0" padding="15" handcursor="false"
                                         css="color:#000000; font-size:18px; letter-spacing: -0.9px; width: 100%;"
                                         align="left" width="350"
                                         html="<?= !empty($metadata['info_title']) ? $metadata['info_title'] : '' ?>"/>
                                <hotspot name="close_icon_<?= $hotspot->name ?>" ath="<?= $hotspot->ath ?>"
                                         atv="<?= $hotspot->atv ?>" distorted="true"
                                         parenthpname="<?= $hotspot->name ?>"
                                         url="%$player_url%/images/icon/close-b.jpg" scale="0.15" hotspotvrtype="1"
                                         visible="false"
                                         ox="158" oy="55" padding="15"
                                         onclick="closeToolTip()"/>
                                <hotspot name="divider_info_<?= $hotspot->name ?>" ath="<?= $hotspot->ath ?>"
                                         atv="<?= $hotspot->atv ?>" distorted="true"
                                         parenthpname="<?= $hotspot->name ?>"
                                         url="%$player_url%/images/icon/white.png" scale="0.5" width="600" height="1"
                                         hotspotvrtype="1" visible="false"
                                         oy="67"/>
                                <hotspot name="content_info_<?= $hotspot->name ?>" ath="<?= $hotspot->ath ?>"
                                         atv="<?= $hotspot->atv ?>" distorted="true" type="text"
                                         parenthpname="<?= $hotspot->name ?>"
                                         oy="125" bgalpha="0" handcursor="false" width="350" autoheight="true"
                                         hotspotvrtype="1" visible="false" padding="15"
                                         css="text-align:justify; color:#000000; font-family:Arial; font-size:16px white-space: pre-wrap"
                                         html="<?= !empty($metadata['info_content']) ? htmlentities($metadata['info_content']) : '' ?>"/>
                            <?php endif; ?>
                            <?php if ($hotspot->type == Hotspot::TYPE_PRODUCT): ?>
                                <hotspot name="background_prod_<?= $hotspot->name ?>" type="text" bgalpha="0.9"
                                         bgcolor="0xffffff" oy="160"
                                         width="278" height="260" ath="<?= $hotspot->ath ?>" distorted="true"
                                         parenthpname="<?= $hotspot->name ?>"
                                         atv="<?= $hotspot->atv ?>" hotspotvrtype="2" visible="false" html=" "
                                         bgroundedge="13"
                                />
                                <hotspot name="close_prod_<?= $hotspot->name ?>" ath="<?= $hotspot->ath ?>"
                                         distorted="true" parenthpname="<?= $hotspot->name ?>"
                                         atv="<?= $hotspot->atv ?>" hotspotvrtype="2" visible="false" width="20"
                                         height="20" oy="45" ox="125" zorder="3"
                                         url="%$player_url%/images/icon/close_red.png" bgcapture="true"
                                    <?php if ($metadata['product_animation'] == 1): ?>
                                        onclick="closeProductHotspotAnimation()"
                                    <?php else: ?>
                                        onclick="closeProductHotspot()"
                                    <?php endif; ?> />
                                <hotspot name="text_title_prod_<?= $hotspot->name ?>" hotspotvrtype="2"
                                         parenthpname="<?= $hotspot->name ?>" visible="false"
                                         width="268" bgalpha="0" type="text" oy="55" ox="10" ath="<?= $hotspot->ath ?>"
                                         distorted="true" atv="<?= $hotspot->atv ?>"
                                         css="text-align:justify; color:#2b2b2b; font-family:Arial; font-size:20px;"
                                         html="<?= str_replace("\n", '[br]', $metadata['product_name']); ?>"/>
                                <hotspot name="text_desc_prod_<?= $hotspot->name ?>" hotspotvrtype="2"
                                         parenthpname="<?= $hotspot->name ?>" visible="false"
                                         width="268" bgalpha="0" type="text" oy="85" ox="10" ath="<?= $hotspot->ath ?>"
                                         distorted="true" atv="<?= $hotspot->atv ?>"
                                         css="text-align:justify; color:#5b5b5b; font-family:Arial; font-size:12px;"
                                         html="<?= str_replace("\n", '[br]', $metadata['product_description']); ?>"/>
                                <hotspot name="text_price_prod_<?= $hotspot->name ?>" hotspotvrtype="2"
                                         parenthpname="<?= $hotspot->name ?>" visible="false"
                                         width="268" bgalpha="0" type="text" oy="205" ox="10" ath="<?= $hotspot->ath ?>"
                                         distorted="true" atv="<?= $hotspot->atv ?>"
                                         css="text-align:justify; color:#eb101d; font-family:Arial; font-size:26px;"
                                         html="<?= !empty($metadata['product_price']) ? is_numeric($metadata['product_price']) ? number_format((float)$metadata['product_price'], 0) : 0 . "[span style='font-size: 16px;']원[/span]" : '' ?>"/>
                                <hotspot name="button_ship_prod_<?= $hotspot->name ?>" hotspotvrtype="2"
                                         parenthpname="<?= $hotspot->name ?>" visible="false"
                                         width="80" bgalpha="0" type="text" oy="205" ox="70" ath="<?= $hotspot->ath ?>"
                                         distorted="true" atv="<?= $hotspot->atv ?>"
                                         css="text-align:justify; color:#5b5b5b; font-family:Arial; font-size:12px; padding: 3px 10px;"
                                         bgroundedge="10" bgborder="1 0xd0d8e1 1"
                                         html="<?= $metadata['product_shipping']; ?>"/>
                                <hotspot name="button_buy_prod_<?= $hotspot->name ?>" hotspotvrtype="2"
                                         parenthpname="<?= $hotspot->name ?>" visible="false"
                                         width="110" bgalpha="1" bgcolor="0xeb101d" type="text" oy="240" ox="-65"
                                         ath="<?= $hotspot->ath ?>" distorted="true" atv="<?= $hotspot->atv ?>"
                                         css="text-align:center; color:#5b5b5b; font-family:Arial; font-size:12px; color:#fff; padding: 5px 10px;"
                                         bgroundedge="5" bgborder="0"
                                         onclick="openurl('<?= $metadata['product_link'] ?>', '_blank');"
                                         html="Buy Now"/>
                                <?php
                                $extension = pathinfo($metadata['product_image'], PATHINFO_EXTENSION);
                                if ($extension == 'mp4'):
                                    ?>
                                    <hotspot name="animation_product_hotspot_img_<?= $hotspot->name ?>"
                                             ath="<?= $hotspot->ath ?>" atv="<?= $hotspot->atv ?>"
                                             hotspotvrtype="2" visible="false"
                                             pausedonstart="true" distorted="true"
                                             html5controls="true"
                                             onclick="togglepause();"
                                             bgcapture="true"
                                             loop="false"
                                             url.html5="%$player_url%/plugins/videoplayer.js"
                                             enabled="true"
                                             url.flash="%$player_url%/plugins/videoplayer.swf"
                                             align="lefttop" ox="308" oy="15"
                                             videourl="<?= !empty($metadata['product_image']) ? "%\$resource_url%/" . $metadata['product_image'] : "" ?>"
                                             height="260" width="prop"
                                             onloaded=""/>
                                <?php else: ?>
                                    <hotspot name="animation_product_hotspot_img_<?= $hotspot->name ?>"
                                             hotspotvrtype="2" visible="false"
                                             ath="<?= $hotspot->ath ?>" atv="<?= $hotspot->atv ?>" type="image"
                                             enabled="true" distorted="true"
                                             ox="428" oy="160" url="<?= !empty($metadata['product_image']) ? "%\$resource_url%/" . $metadata['product_image'] : "" ?>"
                                             height="260" width="prop" onloaded=""/>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php if ($hotspot->type == Hotspot::TYPE_IMAGE): ?>
                                <hotspot name="image_vr_hp_<?= $hotspot->name ?>" url="<?= !empty($metadata['image_url']) ? "%\$resource_url%/" . $metadata['image_url'] : "" ?>"
                                         ath="<?= $hotspot->ath ?>" distorted="true"
                                         parenthpname="<?= $hotspot->name ?>" height="prop"
                                         atv="<?= $hotspot->atv ?>" hotspotvrtype="3" visible="false"
                                         zorder="3"
                                />
                                <hotspot name="close_image_vr_<?= $hotspot->name ?>" ath="<?= $hotspot->ath ?>"
                                         distorted="true" parenthpname="<?= $hotspot->name ?>"
                                         atv="<?= $hotspot->atv ?>" hotspotvrtype="3" visible="false" width="24"
                                         height="24" zorder="3" bgcolor="0xffffff"
                                         url="%$player_url%/images/icon/pop-close-icon-2.png" bgcapture="true"
                                         onclick="closeImageHospot()"
                                />
                            <?php endif; ?>
                            <?php break; ?>
                        <?php endswitch; ?>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if (!$setStartView) { ?>
                <view fovtype="MFOV" fov="140" maxpixelzoom="2.0" fovmin="30" fovmax="140" limitview="auto"/>
            <?php } ?>
        </scene>
    <?php } ?>
</krpano>
