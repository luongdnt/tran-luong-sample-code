
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no">
    <link rel="stylesheet" href="/css/mail.css" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR:100,300,400,500,700,900&display=swap&subset=korean" rel="stylesheet">
    <!--[if lt IE 9]><script src="js/IE9.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="js/html5.js"></script><![endif]-->
</head>
<body>

<header class="header">
    <div class="logo"><a href="/" target="_blank"><img src="/images/mail/logo.png" alt="vrmaker logo" /></a></div>
</header>
<div class="main">
    <div class="contents">
        <h1><?= Yii::t('app', 'Welcome to VRMaker!') ?></h1>
        <br/>
        <p><?= Yii::t('app', 'Your account is ready!') ?> </p>
        <p><?= Yii::t('app', 'Create & Manage your VR contents with VRMaker') ?></p>
        <br>
        <p><?= Yii::t('app', 'Click VRMaker Tips below.') ?></p>
        <div class="hotspot-container">
            <a href="https://blog.naver.com/PostList.nhn?blogId=atojet&from=postList&categoryNo=1" target="_blank"><img src="/images/mail/teleport_hotspot.png" /></a>
            <a href="https://blog.naver.com/PostList.nhn?blogId=atojet&from=postList&categoryNo=1" target="_blank"><img src="/images/mail/text_hotspot.png" /></a>
            <a href="https://blog.naver.com/PostList.nhn?blogId=atojet&from=postList&categoryNo=1" target="_blank"><img src="/images/mail/info_hotspot.png" /></a>
            <a href="https://blog.naver.com/PostList.nhn?blogId=atojet&from=postList&categoryNo=1" target="_blank"><img src="/images/mail/img_hotspot.png" /></a>
            <a href="https://blog.naver.com/PostList.nhn?blogId=atojet&from=postList&categoryNo=1" target="_blank"><img src="/images/mail/call_hotspot.png" /></a>
            <a href="https://blog.naver.com/PostList.nhn?blogId=atojet&from=postList&categoryNo=1" target="_blank"><img src="/images/mail/link_hotspot.png" /></a>
            <a href="https://blog.naver.com/PostList.nhn?blogId=atojet&from=postList&categoryNo=1" target="_blank"><img src="/images/mail/gallery_hotspot.png" /></a>
        </div>
        <a href="https://blog.naver.com/PostList.nhn?blogId=atojet&from=postList&categoryNo=1" target="_blank"><?= Yii::t('app', 'Go to VRMaker Tutorial') ?></a>
        <br/>
        <br>
        <p>Username: <?= $email ?> </p>
        <br><br>
    </div>
</div>
<footer class="footer">
    <a href="https://vrmaker.io/">vrmaker.io</a>
</footer>


</body>
</html>






















 
