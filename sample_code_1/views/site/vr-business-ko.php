<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\web\View;

$this->title = 'VR Business';

?>

    <div id="container">
        <section id="contents" class="business-contents">
            <div class="business-visual">
                <div class="mask">
                    <h1>(주) 에이투젯은 360VR을 Business에 <br />접목 할 수 있는 기술을 연구하는 기업입니다.</h1>
                    <a href="/contact">문의하기</a>
                </div>
            </div>
            <div class="tab-container cf">
                <div class="biz-tab on" data-id="0">360VR 콘텐츠 제작 대행</div>
                <div class="biz-tab" data-id="1">기업 내 자체 VR플랫폼 개발</div>
            </div>
            <div class="biz-contents for-business show">
                <h1>VRMaker For Business</h1>
                <span>VR 촬영/제작 전문가가 고객의 니즈에 맞는 가상 체험 홍보물/콘텐츠를 제작해 드립니다.</span>
                <div class="liner"></div>
                <div class="for-biz-container">
                    <div class="biz-cnt">
                        <img src="../images/business-img/img1.png" width="" />
                        <h2>사전 기획</h2>
                        <p>고객의 스토리를 가장 잘 담을 수 있는<br />
                            콘텐츠 제작을 위해 전문 작가가<br />
                            기획부터 함께 합니다.
                        </p>
                    </div>
                    <div class="biz-cnt">
                        <img src="../images/business-img/img2.png" width="" />
                        <h2>VR 촬영/제작</h2>
                        <p>
                            경력 10년 이상의 베테랑 VR 촬영 감독이<br />
                            기획서에 따라 최고의 퀄리티로<br />
                            VR을 촬영합니다.
                        </p>
                    </div>
                    <div class="biz-cnt">
                        <img src="../images/business-img/img3.png" width="" />
                        <h2>편집/제작</h2>
                        <p>
                            360 콘텐츠를 스티칭 및 편집하고<br />
                            인터랙티브 핫스팟을 삽입하여<br />
                            현실감 높은 가상현실 콘텐츠를 제작합니다.
                        </p>
                    </div>
                </div>
                <!--                //.for-biz-container-->
                <h2>Reference</h2>
                <div class="reference-container cf">
                    <div class="item-ref">
                        <a href="https://beta.vrmaker.io/pano-player/98959360" target="_blank">
                            <span class="ref-txt">
                                <h4>애월 미하스</h4>
                                <span>55, Gwangnyeong 2-gil, Aewol-eup, Jeju-si, Jeju-do, Republic of Korea.</span>
                                <p>May 12, 2018</p>
                            </span>
                        </a>
                        <div class="ref-title">애월 미하스</div>
                    </div>
                    <div class="item-ref">
                        <a href="https://beta.vrmaker.io/pano-player/385941504" target="_blank">
                            <span class="ref-txt">
                                <h4>NS 홈쇼핑 - 글램핑</h4>
                                <span>NSHomeShopping Glamping</span>
                                <p>July 26, 2018</p>
                            </span>
                        </a>
                        <div class="ref-title">NS 홈쇼핑 - 글램핑</div>
                    </div>
                    <div class="item-ref">
                        <a href="https://beta.vrmaker.io/pano-player/98893824" target="_blank">
                            <span class="ref-txt">
                                <h4>당신의 과수원</h4>
                                <span>Yourfarm, Sinchonseo, Jocheon-eup, Jeju-si, Jeju-do, Republic of Korea.</span>
                                <p>December 06, 2018</p>
                            </span>
                        </a>
                        <div class="ref-title">당신의 과수원</div>
                    </div>
                    <div class="item-ref">
                        <a href="https://beta.vrmaker.io/pano-player/103284736" target="_blank">
                            <span class="ref-txt">
                                <h4>플레이케이팝</h4>
                                <span>15, Jungmungwangwang-ro 110beon-gil, Seogwipo-si, Jeju-do, Republic of Korea.</span>
                                <p>February 27, 2018</p>
                            </span>
                        </a>
                        <div class="ref-title">플레이케이팝</div>
                    </div>
                    <div class="item-ref">
                        <a href="https://beta.vrmaker.io/pano-player/98697216" target="_blank">
                            <span class="ref-txt">
                                <h4>휴애리</h4>
                                <span>256, Sillyedong-ro, Namwon-eup, Seogwipo-si, Jeju-do, Republic of Korea.</span>
                                <p>September 29, 2017</p>
                            </span>
                        </a>
                        <div class="ref-title">휴애리</div>
                    </div>
                    <div class="item-ref">
                        <a href="http://easyvr.co.kr/SETEC//" target="_blank">
                            <span class="ref-txt">
                                <h4>SETEC</h4>
                                <span>3104 Nambusunhwan-ro, Daechi-dong, Gangnam-gu, Seoul</span>
                                <p>August 23, 2019</p>
                            </span>
                        </a>
                        <div class="ref-title">SETEC</div>
                    </div>
                </div>
                <a href="/contact" class="contact-btn">견적 문의하기</a>
            </div><!-- //.for-business-->
            <div class="biz-contents for-solution">
                <h1>VRMaker white lavel Solution</h1>
                <span>기업에서 손쉽게 VR을 활용 할 수 있도록 VRMaker White label 솔루션을 공급합니다.</span>
                <div class="liner"></div>
                <div class="for-sol-container">
                    <div class="item-sol">
                        <img src="../images/business-img/img-1.png" />
                        <div class="mask">
                            <span>부동산/건설</span>
                        </div>
                    </div>
                    <div class="item-sol">
                        <img src="../images/business-img/img-2.png" />
                        <div class="mask">
                            <span>교육</span>
                        </div>
                    </div>
                    <div class="item-sol">
                        <img src="../images/business-img/img-3.png" />
                        <div class="mask">
                            <span>관광/엔터테인먼트</span>
                        </div>
                    </div>
                </div> <!-- //.for-sol-container -->
                <h2>Reference</h2>
                <div class="ref-container cf">
                    <div class="ref-contents cf">
                        <div class="ref-body">
                            <div class="ref-profile"><img src="../images/business-img/logo.png" /></div>
                            <span>국립 문화재 연구소</span>
                            <p>360VR로 감상하는 우리 문화재</p>
                        </div>
                    </div>
                    <div class="ref-contents cf">
                        <div class="ref-body">
                            <div class="ref-profile"><img src="../images/business-img/halla-logo.png" /></div>
                            <span>(주) 한라건설</span>
                            <p>건설현장 VR관리 플랫폼</p>
                        </div>
                    </div>
                </div>
                <a href="/contact" class="contact-btn">문의하기</a>
            </div> <!-- //.for-solution-->

        </section><!-- //content -->
    </div><!-- //container -->


<?php $this->beginBlock('script') ?>
    <script>
        function openService(evt, tourName) {
            var i, tour_service, service_tablinks;
            tour_service = document.getElementsByClassName("tour_service");
            for (i = 0; i < tour_service.length; i++) {
                tour_service[i].style.display = "none";
            }
            service_tablinks = document.getElementsByClassName("service_tablinks");
            for (i = 0; i < service_tablinks.length; i++) {
                service_tablinks[i].className = service_tablinks[i].className.replace(" active", "");
            }
            document.getElementById(tourName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
    <script type="text/javascript">
        var tabBtn = document.querySelectorAll('.biz-tab');
        var tabContents = document.querySelectorAll('.biz-contents');
        
        document.querySelector('.tab-container').addEventListener('click', function(e){            
            activeBtn(e.target.dataset.id);
            showBtn(e.target.dataset.id);
        });
        
        function activeBtn(i){
            tabBtn[0].classList.remove('on')
            tabBtn[1].classList.remove('on')
            tabBtn[i].classList.add('on')
        }
        
        function showBtn(i){
            tabContents[0].classList.remove('show')
            tabContents[1].classList.remove('show')
            tabContents[i].classList.add('show')
            
        }
    </script>
<?php $this->endBlock() ?>