<?php

/* @var $this yii\web\View */

$this->title = 'About';
?>

<section id="about-visual" class="visual">
    <div class="mask">
        <div class="visual-contents about-visual-contents">
            <h1><?= Yii::t('app', 'About Us') ?></h1>
            <p class="line"></p>
            <p><?= Yii::t('app', 'Create your VR contents in 10 minutes') ?></p>
        </div>
    </div>
</section>

<div id="container">
    <section id="contents">
        <div class="about-contents">
            <div class="about-info">
                <h2><?= Yii::t('app', 'What we do') ?></h2>
                <p class="line"></p>
                <ul class="cf">
                    <li>
                        <div class="img-position"><img src="../images/icon/do01.png" alt="do01"></div>
                        <strong><?= Yii::t('app', '360 VR platform') ?></strong>
                        <a href="/">vrmaker.io</a>
                    </li>
                    <li>
                        <div class="img-position"><img src="../images/icon/do02.png" alt="do02"></div>
                        <strong><?= Yii::t('app', 'Connect VR expert &amp; Client') ?></strong>
                        <a href="http://easyvr.co.kr/">easyvr.co.kr</a>
                    </li>
                    <li>
                        <div class="img-position"><img src="../images/icon/do03.png" alt="do03"></div>
                        <strong><?= Yii::t('app', 'White label VR Platform') ?><br><?= Yii::t('app', 'For Enterprise') ?></strong>
                    </li>
                </ul>
            </div>
            <div class="about-info-2">
                <h2><?= Yii::t('app', 'Company Overview') ?></h2>
                <div class="about-info-txt">
                    <?= Yii::t('app', 'Atojet is an immersive technology company powered by VRMaker,') ?> <br>
                    <?= Yii::t('app', 'the leading enterprise platform that enables brands and organizations to <br> engage and convert audiences through interactive 360 experiences.') ?>
                </div>
                <div class="about-info-txt">
                    <?= Yii::t('app', 'We combines the power of Vrmaker-winning production studio<br> to drive measurable results for over 3,000+ clients across mobile, desktop and VR.') ?><br>
                </div>

                <h2><?= Yii::t('app', 'VRMaker') ?></h2>
                <div class="about-info-txt">
                    <?= Yii::t('app', 'The proprietary VRmaker platform blends the interactive world with real-world 360° content.') ?><br>
                    <?= Yii::t('app', 'It enables users to engage in a story as deeply as they wish and convert directly from the experience.') ?>
                </div>
                <div class="about-info-txt">
                    <?= Yii::t('app', 'Averaging 10+ minutes, these longer engagements increase the likelihood of conversions <br> for clients including consumer brands, destinations, corporations, and academic institutions.') ?>
                </div>
            </div>
        </div> <!-- //.about-contents -->
    </section><!-- //content -->
</div>

<?php $this->beginBlock('script') ?>
<script>
    function len_chk(){
        var frm = document.insertFrm.test;

        if(frm.value.length > 500){
            alert("글자수는 500자로 제한됩니다.");
            frm.value = frm.value.substring(0,500);
            frm.focus();
        }

    }

    $('#frm-contact').submit(function(e){
        e.preventDefault();
        $('.label-danger').text('');
        $('.icon-loading').css('display', 'inline-block');
        $.ajax( {
            url: '/site/contact',
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            success: function (res) {
                $('.icon-loading').css('display', 'none');

                if (res.success == 1) {
                    alert('Sent successfully');
                    window.location.reload();
                } else {
                    for (var key in res.message) {
                        if (!res.message.hasOwnProperty(key)) continue;
                        var obj = res.message[key];
                        for (var prop in obj) {
                            if(!obj.hasOwnProperty(prop)) continue;
                            $('#error_' + key).text(obj[prop]);
                        }
                    }
                }
            }
        });
    });
</script>
<?php $this->endBlock() ?>


