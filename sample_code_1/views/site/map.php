<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\web\View;

$this->title = 'Map';

?>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>

    <div id="map"></div>


<?php $this->beginBlock('script') ?>
    <script>

        function getLocationProjects() {
            $.ajax({
                url: 'get-location-projects',
                dataType: 'json',
                // data: $(form).serialize(),
                type: 'get',
                success: function (res) {
                    if (res.success) {
                        var locations = res.data;
                        console.log(locations);
                        initMap(locations);

                    }
                }, error: function (res) {
                    // alert('Error');
                }
            });
        }

        var min = .99999;
        var max = 1.00001;

        function initMap(locations) {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 3,
                center: {lat: 37.5652894, lng: 126.8494645},
                minZoom: 3,
                maxZoom: 20
            });

            var infowindow = new google.maps.InfoWindow();

            var base_url = '<?= Yii::$app->params['resource_domain'] ?>';

            var markers = locations.map(function(location, i) {

                var lat = parseFloat(location.lat);
                var lng = parseFloat(location.lng);

                // if (pointExists(locations, location.lat, location.lng)) {
                //     lat = lat * (Math.random() * (max - min) + min);
                //     lng = lng * (Math.random() * (max - min) + min);
                // }

                var marker = new google.maps.Marker({
                    position: {
                        lat: lat,
                        lng: lng
                    },
                    // label: 'P',
                });

                var contentString =
                    '<div class="info-window">' +
                        '<div class="map-pop-contents">' +
                            '<div class="map-pop-contents-wrap">' +
                                '<a class="close-info-map" href="/pano-player/'+ location.id +'" target="_blank">'+
                                    '<div class="pop-thumb-box">'+
                                        '<div class="mask"></div>'+
                                        '<img src="'+ base_url + location.thumb_path +'">'+
                                    '</div>'+
                                '</a>'+
                                '<div class="pop-info-box">'+
                                    '<h2><a href="/pano-player/'+ location.id +'" target="_blank">' + location.title + '</a></h2>'+
                                    '<a href="/pano-player/'+ location.id +'" target="_blank" class="tour-location">' + (location.address ? location.address : 'No location') + '</a>'+
                                '</div>'+
                            '</div>'+
                        '</div>' +
                    '</div>';

                google.maps.event.addListener(marker, 'click', function(evt) {
                    infowindow.setContent(contentString);
                    infowindow.open(map, marker);
                });

                $(document).on('click', '.info-window', function (e) {
                    infowindow.close();
                });

                return marker;
            });

            // Add a marker clusterer to manage the markers.
            var markerCluster = new MarkerClusterer(map, markers,
                {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

        }

        function pointExists(arr, lat, lng, id) {
            return arr.some(function(el) {
                return el.lat == lat && el.lng == lng && el.id != id;
            });
        }

    </script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=<?= Yii::$app->params['services']['google']['api_key'] ?>&callback=getLocationProjects&sensor=false&region=KR">
    </script>

<?php $this->endBlock() ?>