<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\web\View;

$this->title = 'Stock';

?>
<div id="container">
    <section id="contents">
        <div class="author-stock-container">
            <div class="pano-contents author-stock-contents">
                <div class="authors_stock">
                    <div class="stock_img stock_img_lf"></div>
                    <div class="stock_img active_stock">
                        <div class="active_stock_thumb"></div>
                        <div class="pano_view">
                            <iframe src="https://vrmaker.io/view-project/10403" frameborder="0"></iframe>
                        </div>
                    </div>
                    <div class="stock_img stock_img_rt"></div>
                </div>
            </div> <!-- //.stock-contents -->
        </div> <!-- //.stock-container -->
        <div class="stock_detail_container">
            <div class="stock_pano_info">
                <p>Non-editorial</p>
                <p>No-commercial Use</p>
                <p>Image Size : 12,000 * 6,000 px</p>
            </div>
            <div class="row cf">
                <div class="detail_con con_rt">
                    <div class="stock_pay_container cf">
                        <h1>Marina Bay </h1>
                        <span>Non-editorial, Image Size : 12,000 * 6,000 px</span>
                        <div class="pay_contents pay_lf">
                            <h3>
                                <p>Use in VRMaker</p>
                                USD 30
                            </h3>
                            <a href="javascript:" class="pay_btn">Add to VRMaker Library</a>
                        </div>
                        <div class="pay_contents pay_rt">
                            <h3>
                                <p class="blue">Download</p>
                                USD 180
                            </h3>
                            <a href="javascript:" class="pay_btn btn_b">Buy Now</a>
                        </div>
                    </div>
                </div> <!-- //.detail_con -->
                <div class="detail_con con_lf">
                    <div class="author_con">
                        <h2>Author</h2>
                        <div class="stock_author_profile cf">
                            <div class="author_profile_img author_pf"><img src="../images/main/temp_profile.jpg"></div>
                            <div class="stock_author_name author_pf">Leo Kim</div>
                            <div class="view_btn author_pf">
                                <a href="#">View More</a>
                            </div>
                        </div>
                    </div> <!-- //.author_con -->
                    <div class="author_con">
                        <h2>Location</h2>
                        <div class="tour_loca"><span><img src="../images/icon/location_icon.png" /></span> #2433, Mariana Bay, Singapore</div>
                    </div>
                    <div class="author_con">
                        <h2>Description</h2>
                        <div class="stock_description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. </div>
                    </div>
                    <div class="author_con">
                        <div class="tag_contents">
                            <a href="#">Architecture</a>
                            <a href="#">City</a>
                            <a href="#">Singapore</a>
                            <a href="#">Marina Bay</a>
                        </div>
                    </div>
                </div> <!-- //.detail_con -->
            </div>
        </div>
    </section><!-- //content -->
</div><!-- //#container -->

<?php $this->beginBlock('script') ?>
    <script type='text/javascript' src="../js/lib/flexbox-gallery.js"></script>
    <script>

        $('.active_stock').on("click", function(){
            $('.active_stock .pano_view ').show();
        })
    </script>
<?php $this->endBlock() ?>