<?php

/* @var $this yii\web\View */

$this->title = 'VRMaker';
?>
<section id="main_visual" class="visual">
    <div class="clouds"></div>
    <div class="mask">
        <div class="visual-contents main_info_contents">
            <h1>
                <p class="text_main"><?= Yii::t('app', 'Make Virtual Tour Simple') ?></p>
                <p class="text_intro"><?= Yii::t('app', 'Your cloud-based VR authoring tool') ?></p>
            </h1>
            <a href="<?= Yii::$app->user->isGuest ? '/login' : '/auth/authorize?client_id=' . Yii::$app->params['cms_client_id'] ?>" class="create_tour_btn" target="_blank"><?= Yii::t('app', 'Create VR Tour Now') ?></a>
        </div> <!-- //.stock_info_contents -->
    </div><!-- //.mask -->
</section><!-- //#visual -->
<div id="container">
    <section id="contents">
        <div class="main-container">
            <div class="pano-contents main-contents">
                <h2><?= Yii::t('app', 'Introducing VRMaker') ?></h2>
                <p><?= Yii::t('app', 'VRMaker is the easiest way to create VR expeiences with') ?> </p>
                <p><?= Yii::t('app', 'Drag&Drop interface. No programming language skill is requried.') ?></p>
                <div class="ani_up">
                    <img src="../images/layout/center_visual.png" alt="">
                </div>
            </div>
            <div class="main_rolling_container">
                <div class="rolling_contents">
                    <div class="word_rotate">
                        <h1>
                            Easy to
                            <div class="word_rotate_item">
                                <div class=""><span>create</span></div>
                                <div class=""><span>Publish</span></div>
                                <div class=""><span>Share</span></div>
                            </div>
                            <br/>
                            your VR Experiences
                        </h1>
                        <div class="word_rotate_item2">
                            <div class="items"><p>Shoot 360 Photo/Video</p></div>
                            <div class="items"><p>Upload to vrmaker and Create 360 VR Tour</p></div>
                            <div class="items"><p>Share to Promote</p></div>
                        </div>
                        <img src="../images/icon/right_arrow_icon.png" alt="right-arrow">
                    </div>
                    <div class="rolling_item_wrap">
                        <div class="rolling_item">
                            <div class="items"><a><img src="../images/layout/create.jpg" alt="create" data-sizes="50vw"></a></div>
                            <div class="items"><a><img src="../images/layout/publish.jpg" alt="publish" data-sizes="50vw"></a></div>
                            <div class="items"><a><img src="../images/layout/share.jpg" alt="share" data-sizes="50vw"></a></div>
                        </div>
                    </div> <!-- //.rolling_item_wrap -->
                </div> <!-- //.rolling_contents -->
            </div> <!-- //.main_rolling_container -->
            <div class="customer_section">
                <div class="mask">
                    <!-- <div class="customer_bg"><div class="mask"></div></div> -->
                    <div class="counting_container">
                        <div class="cnt_item">
                            <!--                        <h1><span class="timer count-title count-number" data-to="18000" data-speed="1000"></span> <em>+</em></h1>-->
                            <h1><span data-count-from="17900" data-count-to="23800" class="stats">00</span> <em>+</em></h1>
                            <span>VR Tour created</span>
                        </div>
                        <div class="cnt_item">
                            <!--                        <h1><span class="timer count-title count-number" data-to="3000" data-speed="1000"></span> <em>+</em></h1>-->
                            <h1><span data-count-from="2900" data-count-to="4650" class="stats">00</span> <em>+</em></h1>
                            <span>VR authors</span>
                        </div>
                        <div class="cnt_item">
                            <!--                        <h1><span class="timer count-title count-number" data-to="10" data-speed="1000"></span> <em>M</em></h1>-->
                            <h1><span data-count-from="000" data-count-to="12" class="stats">00</span> <em>M</em></h1>
                            <span>Page View</span>
                        </div>
                    </div> <!-- //.counting_container -->
                    <div class="liner"></div>
                    <div class="customer_contents">
                        <h2><?= Yii::t('app', 'What Our Customer says') ?></h2>
                        <ul class="cf">
                            <li>
                                <img src="../images/layout/customer01.jpeg">
                                <div class="customer_info">
                                    <strong>
                                        <?= Yii::t('app', 'TaeHwa Woo') ?>
                                        <span><?= Yii::t('app', '360 Photographer') ?></span>
                                    </strong>
                                    <p><?= Yii::t('app', 'VRMaker is one of the best cloud-based service to manage my 360 VR portfolio. Easy to upload my work and share to clients, friends via weblink. Thanks to Map based portfolio feature, easy to search and demonstrate my works.') ?> </p>
                                </div>
                            </li>
                            <li>
                                <!--                            <img src="../images/layout/thebless1.jpg" alt="한라건설">-->
                                <img src="../images/layout/customer02.jpg">
                                <div class="customer_info">
                                    <strong>
                                        <?= Yii::t('app', 'Jihwan Choi') ?>
                                        <span><?= Yii::t('app', 'Halla Group') ?></span>
                                    </strong>
                                    <p><?= Yii::t('app', 'VR is the newest technology that perfectly fits with Construction Industry especially for the work process management with various partners and stake holders. VRMaker helps us a lot to reduce commutation cost and time. Also, very easy to use without any VR tech background.') ?> </p>
                                </div>
                            </li>
                            <li>
                                <img src="../images/layout/mihas.jpg">
                                <div class="customer_info">
                                    <strong>
                                        <?= Yii::t('app', 'SungJoon Nam') ?>
                                        <span><?= Yii::t('app', 'Dazayo Corp') ?></span>
                                    </strong>
                                    <p><?= Yii::t('app', 'Virtual showroom for accommodation provides much more information compare to traditional 2D photo and Video. It also provides virtual experience before visiting my place. Sales increased 30% - VR showroom page view incased 50%') ?> </p>
                                </div>
                            </li>
                        </ul>
                        <ul class="customer_contents_m cf">
                            <li>
                                <img src="../images/layout/customer01.jpeg">
                                <div class="customer_info">
                                    <strong>
                                        <?= Yii::t('app', 'TaeHwa Woo') ?>
                                        <span><?= Yii::t('app', '360 Photographer') ?></span>
                                    </strong>
                                    <p><?= Yii::t('app', 'VRMaker is one of the best cloud-based service to manage my 360 VR portfolio. Easy to upload my work and share to clients, friends via weblink. Thanks to Map based portfolio feature, easy to search and demonstrate my works.') ?></p>
                                </div>
                            </li>
                            <li>
                                <img src="../images/layout/customer02.jpg">
                                <div class="customer_info">
                                    <strong>
                                        <?= Yii::t('app', 'Jihwan Choi') ?>
                                        <span><?= Yii::t('app', 'Halla Group') ?></span>
                                    </strong>
                                    <p><?= Yii::t('app', 'VR is the newest technology that perfectly fits with Construction Industry especially for the work process management with various partners and stake holders. VRMaker helps us a lot to reduce commutation cost and time. Also, very easy to use without any VR tech background.') ?> </p>
                                </div>
                            </li>
                            <li>
                                <img src="../images/layout/mihas.jpg">
                                <div class="customer_info">
                                    <strong>
                                        <?= Yii::t('app', 'SungJoon Nam') ?>>
                                        <span><?= Yii::t('app', 'Dazayo Corp') ?></span>
                                    </strong>
                                    <p><?= Yii::t('app', 'Virtual showroom for accommodation provides much more information compare to traditional 2D photo and Video. It also provides virtual experience before visiting my place. Sales increased 30% - VR showroom page view incased 50%') ?></p>
                                </div>
                            </li>
                        </ul><!-- //mobile customer contents -->
                    </div> <!-- //.customer_contents -->


                </div>

            </div> <!-- //.customer_section -->

            <div class="foot_logo_rolling">
                <div class="foot_logo_wrap">
                    <div class="foot_logo_item"><p><img src="../images/main/foot_logo1.png"></p></div>
                    <div class="foot_logo_item"><p><img src="../images/main/foot_logo2.png"></p></div>
                    <div class="foot_logo_item"><p><img src="../images/main/foot_logo3.png"></p></div>
                    <div class="foot_logo_item"><p><img src="../images/main/foot_logo4.png"></p></div>
                    <div class="foot_logo_item"><p><img src="../images/main/foot_logo5.png"></p></div>
                    <div class="foot_logo_item"><p><img src="../images/main/foot_logo6.png"></p></div>
                    <div class="foot_logo_item"><p><img src="../images/main/foot_logo1.png"></p></div>
                    <div class="foot_logo_item"><p><img src="../images/main/foot_logo2.png"></p></div>
                    <div class="foot_logo_item"><p><img src="../images/main/foot_logo3.png"></p></div>
                    <div class="foot_logo_item"><p><img src="../images/main/foot_logo4.png"></p></div>
                    <div class="foot_logo_item"><p><img src="../images/main/foot_logo5.png"></p></div>
                    <div class="foot_logo_item"><p><img src="../images/main/foot_logo6.png"></p></div>
                </div>
            </div> <!-- //.foot_logo_rolling -->

        </div> <!-- //.main-container -->
    </section><!-- //content -->
</div><!-- //#container -->

<?php $this->beginBlock('script') ?>
<script type="text/javascript" src="../js/timber.master.min.js"></script>
<script type="text/javascript" src="../js/plugins/jquery.tm.horizon.js"></script>
<script type="text/javascript" src="../js/plugins/jquery.tm.counter.js"></script>
<script>
    function infiniteCount(){
        $( '.stats' ).counter( {
            autoStart: true
        });

        // Call horizon
        $( '.stats' ).horizon({
            recurring:	true,

            // Start counter once element is in view
            inView:	function(){
                $( '.stats' ).each( function(){
                    var counter = $( this ).data( 'counter' );
                    counter.startCounter();
                });
            },

            // Clear counter once element is out of view
            outOfView:	function(){
                $( '.stats' ).each( function(){
                    var counter = $( this ).data( 'counter' );
                    counter.clearCounter();
                });
            }
        });

    }
    infiniteCount();


    $('.rolling_item').slick({
        infinite: true,
        dots:true,
        centerMode: true,
        centerPadding: '60px',
        asNavFor: '.word_rotate_item, .word_rotate_item2',
        autoplay: true,
        autoplaySpeed: 2500,
        slidesToShow: 1,
        slidesToScroll: 1,

        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    centerPadding: '0',
                }
            },
            {
                breakpoint: 768,
                settings: {
                    centerPadding: '0',
                }
            },
            {
                breakpoint: 480,
                settings: {
                    centerPadding: '0',
                }
            }
        ]


    });
    $('.word_rotate_item').slick({
        infinite: true,
        asNavFor: '.rolling_item',
        vertical: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2500,
        slidesToShow: 1,
        slidesToScroll: 1
    });
    $('.word_rotate_item2').slick({
        infinite: true,
        asNavFor: '.rolling_item',
        vertical: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2500,
        slidesToShow: 1,
        slidesToScroll: 1
    });


    $('.foot_logo_wrap').slick({
        infinite: true,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 6,
        slidesToScroll: 1


    });

    $('.customer_contents_m').slick({
        infinite: true,
        arrows: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 2300,
        slidesToShow: 1,
        slidesToScroll: 1
    });
</script>
<?php $this->endBlock() ?>
