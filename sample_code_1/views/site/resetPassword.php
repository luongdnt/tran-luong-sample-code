<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="container">
    <section id="contents" class="gray_bg">
        <div class="inner-wrap cf">
            <div class="signIn">
                <h2>Reset password</h2>
                <div class="form-group">
                    <div class="sign_wrap">
                        <div class="sign_box-wrap">
                            <form class="sign_up_box" onsubmit="return handleSubmitRegister(this)" id='reset-password-form'>
                                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                                <input type="hidden" name="token" value="<?= Yii::$app->request->get('token'); ?>" />
                                <label>
                                    <input type="password" placeholder="PASSWORD" name="password" class="sign_in_input_box" required>
                                </label>
                                <label>
                                    <input type="password" placeholder="RETYPE PASSWORD" name="retype_password" class="sign_in_input_box" required>
                                </label>
                                <div class="error-login"></div>
                                <button id="find-password-btn" type="submit" name ='login-button' class="login-button"><i class="icon-loading fa fa-spinner fa-spin"></i><i class="icon-loading icon-loading-find-password fa fa-spinner fa-spin"></i> Reset</button>
                            </form>
                        </div>
                    </div>
                </div> <!-- //form-group -->
            </div><!--  //signIn -->
        </div><!-- //inner-wrap -->
    </section><!-- //content -->
</div><!-- //container -->

<script type="text/javascript">
function handleSubmitRegister(form) {
    if (checkRepeatPassword()) {
        $.ajax({
            url: '/site/reset-password',
            dataType: 'json',
            data: $(form).serialize(),
            type: 'post',
            beforeSend: function () {
                $('.icon-loading-find-password').css('display', 'inline-block');
                $('#find-password-btn').attr('disabled', 'disabled');
                $('#find-password-btn').css('cursor', 'wait');
            },
            complete: function () {
                $('.icon-loading-find-password').css('display', 'none');
                $('#find-password-btn').removeAttr('disabled');
                $('#find-password-btn').css('cursor', 'auto');
            },
            success: function (res) {
                if (res.success) {
                    alert(res.msg);
                    window.location.href = "/login";
                } else {
                    alert(res.msg);
                }
            },
            error: function (res) {
                alert('Connect error');
            },
        });
    }
    return false;
}

function checkRepeatPassword() {
    if ($('[name=password]').val() != $('[name=retype_password]').val()) {
        alert('Retype password is not match');
        return false;
    }
    
    return true;
}
</script>