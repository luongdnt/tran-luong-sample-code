<?php

/* @var $this yii\web\View */

use yii\web\View;

$this->title = Yii::t('app', 'Login');

?>

<div id="container">
    <section id="contents" class="gray_bg">
        <div class="inner-wrap cf">
            <div class="signUp">
                <h2><?= Yii::t('app', 'Sign up') ?></h2>
                <div class="form-group">
                    <div class="sns_login cf">
                        <span><?= Yii::t('app', 'Connect with') ?></span>
                        <a href="javascript:" id="login-fb" class="facebook facebook_login"><img src="../images/icon/facebook_icon_b.png" alt="facebook" /></a>
                        <a href="javascript:" id="login-gg" class="google google_login"><img src="../images/icon/google_icon.png" alt="google" /></a>
                        <a href="javascript:" class="naver naver_login" id="naver_id_login"  onclick="handleNaverLogin()"><img src="../images/icon/naver_icon.png" alt="naver" /></a>
                    </div>

                    <form class="sign_up_box" onsubmit="return handleSubmitRegister(this)">
                        <div class="sign_up_wrap">
                                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                                <fieldset>
                                    <legend><?= Yii::t('app', 'sign up') ?></legend>
                                    <label for="email"><b><?= Yii::t('app', 'Email') ?></b></label>
                                    <input type="text" name="email" class="sign_up_input_box" required>
                                    <p><?= Yii::t('app', 'You will received an E-mail for verfication. Please enter correct E-mail address.') ?></p>

                                    <label for="psw"><b><?= Yii::t('app', 'Password') ?></b></label>
                                    <input type="password" name="password"  class="sign_up_input_box" required>
                                    <input type="password" name="psw_repeat"  class="sign_up_input_box" required onblur="checkRepeatPassword()">
                                    <div class="text-danger check-password"></div>

                                    <label for="username"><?= Yii::t('app', 'Username') ?></label>
                                    <input type="text" name="username" id="username"  class="sign_up_input_box text" required>

                                    <label for="phone_number"><?= Yii::t('app', 'Phone number') ?></label>
                                    <input type="text" name="phone_number" id="phone_number"  class="sign_up_input_box text" required>
                                </fieldset>
                            <p><?= Yii::t('app', 'You will received a code if you forget your password. Please correct mobile number.') ?></p>
                            <button type="submit" class="signupbtn btn btn-primary btn-lg">
                                <span class="signupBtn"><?= Yii::t('app', 'Sign Up') ?></span>
                                <span class="loadBtn"><i class="fa fa-spinner fa-spin"></i></span>
                            </button>
                            <div class="signIn_btn cf">
                                <p><?= Yii::t('app', 'Already have an account?') ?></p>
                                <a href="/login" class="signInBtn"><?= Yii::t('app', 'Log in') ?></a>
                            </div>
                        </div> <!-- //sign_up_wrap -->
                    </form>

                </div> <!-- //signUp -->
                <div class="complete_wrap">
                    <div class="complete">
                        <h2><?= Yii::t('app', 'Welcome to VRMaker !') ?></h2>
                        <p><?= Yii::t('app', 'Your account has been created successfully!') ?><br>
                            <?= Yii::t('app', 'Please check your inbox to verify your account.') ?><br>
                            <?= Yii::t('app', 'Did not receive the email?') ?> <a class="resend-mail" href="#"><?= Yii::t('app', 'Resend email') ?></a></p>
                        <a href="/login" class="doneBtn"><?= Yii::t('app', 'Login now') ?></a>
                    </div>
                </div>
            </div>
        </div> <!-- //inner-wrap -->
    </section> <!-- //content -->
</div> <!-- //container -->

<?php $this->beginBlock('script') ?>
<script type="text/javascript" src="https://static.nid.naver.com/js/naverLogin_implicit-1.0.3.js" charset="utf-8"></script>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '<?= Yii::$app->params['services']['facebook']['app_id'] ?>',
            cookie     : true,  // enable cookies to allow the server to access
                                // the session
            xfbml      : true,  // parse social plugins on this page
            version    : 'v3.3' // The Graph API version to use for the call
        });

        // FB.AppEvents.logPageView();

    };

    var startApp = function() {
        gapi.load('auth2', function(){
            auth2 = gapi.auth2.init({
                client_id: '<?= Yii::$app->params['services']['google']['client_id'] ?>',
                cookiepolicy: '/',
            });
            attachSignin(document.getElementById('login-gg'));
        });
    };

    function handleNaverLogin() {
        var callBackUrl = '<?= Yii::$app->params['services']['naver']['call_back_url'] ?>';
        var client_id = '<?= Yii::$app->params['services']['naver']['app_id'] ?>';
        var state = '<?= $this->generate_state() ?>';
        var url = 'https://nid.naver.com/oauth2.0/authorize?client_id='+ client_id +'&response_type=code&redirect_uri='+ callBackUrl +'&state='+ state;
        // Open popup
        var newWindow = window.open(url,'Naver Login','height=450,width=800');
        if (window.focus) {
            newWindow.focus()
        }
    }

</script>
<script type="text/javascript" src="../js/login-social.js?v=1.0"></script>
<script type="text/javascript">
    function handleSubmitRegister(form) {
        if (checkRepeatPassword()) {
            $(".signupbtn .signupBtn").hide();
            $(".signupbtn .loadBtn").show();
            $.ajax({
                url: '/site/register',
                dataType: 'json',
                data: $(form).serialize(),
                type: 'post',
                success: function (res) {
                    if (res.success) {
                        $(".complete_wrap").show();
                    } else {
                        var msg = '';
                        jQuery.each(res.message, function(key, value){
                            msg = msg + ' ' + value;

                        });
                        alert(msg);
                    }
                    $(".signupbtn .signupBtn").show();
                    $(".signupbtn .loadBtn").hide();
                }, error: function (res) {
                    alert('Connect error');
                },
            });
        }
        return false;
    }

    function checkRepeatPassword() {
        $('[name=psw-repeat]').nextAll('.error').remove();
        if ($('[name=password]').val() != $('[name=psw_repeat]').val()) {
            var msg_error = '<?= Yii::t('app', 'Password not match') ?>';
            $('.check-password').removeClass('text-success').addClass('text-danger').text(msg_error);
            return false;
        }

        var msg_match = '<?= Yii::t('app', 'Password matching') ?>';

        $('.check-password').removeClass('text-danger').addClass('text-success').text(msg_match);
        return true;
    }

    $(document).ready(function () {
        $('.resend-mail').click(function (e) {
            e.preventDefault();
            $(this).hide();
            $.ajax( {
                url: '/site/resend-mail',
                type: 'GET',
                data: {
                    email: $("input[name=email]").val(),
                },
                success: function (res) {
                    if (res.success) {
                        var msg = '<?= Yii::t('app', 'Resend mail success. Check your email and activate account') ?>';
                        alert(msg);
                        window.location.href = '/login';
                    } else {
                        alert('An error occurred, please try again');
                    }
                }
            });
        })
    })
</script>
<?php $this->endBlock() ?>