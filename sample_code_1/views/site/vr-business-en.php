<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\web\View;

$this->title = 'VR Business';

?>

    <div id="container">
        <section id="contents" class="business-contents">
            <div class="business-contents-en">
                <div class="ve_solution">
                    <h1>Virtual experience <span class="blue">solutions</span></h1>
                    <ul class="solution_icon cf">
                        <li>
                            <a href="javascript:" class="service_tablinks" onclick="openService(event, 'vr_hotel')">
                                <img src="../images/icon/s_icon01.png" alt="s_icon01" />
                                <p class="Solution_name">Hotel Marketing</p>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:" class="service_tablinks" onclick="openService(event, 'vr_commerce')">
                                <img src="../images/icon/s_icon02.png" alt="s_icon02" />
                                <p class="Solution_name">VR Commerce</p>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:" class="service_tablinks" onclick="openService(event, 'vr_estate')">
                                <img src="../images/icon/s_icon03.png" alt="s_icon03" />
                                <p class="Solution_name">Real Estate Marketing</p>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:" class="service_tablinks" onclick="openService(event, 'vr_destination')">
                                <img src="../images/icon/s_icon04.png" alt="s_icon04" />
                                <p class="Solution_name">Destination Marketing</p>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:" class="service_tablinks" onclick="openService(event, 'vr_education')">
                                <img src="../images/icon/s_icon05.png" alt="s_icon05" />
                                <p class="Solution_name">Virtual Education</p>
                            </a>
                        </li>
                    </ul>
                    <div class="go-contact"><a href="/contact">Contact Us</a></div>
                </div>
                <div id="tour_main" class="tour_service">
                    <article class="solution01 solution cf">
                        <div class="sample_txt">
                            <h2>Hotel/Accomodation Marketing</h2>
                            <span>The best way to showcase your hotel</span>
                            <p class="sample_list">A new way to engage with client</p>
                            <p class="sample_list">Your new unfair advantage</p>
                            <p class="sample_list">Driver sales & direct booking</p>
                            <p class="sample_list">Tell your story in a new way</p>
                        </div>
                        <div class="sample_img">
                            <a href="https://vrmaker.io/view-project/12553" class="sampleBtn">Sample</a>
                        </div>
                    </article>
                    <article class="solution02 solution cf">
                        <div class="sample_txt">
                            <h2>VR shopping</h2>
                            <span>Increase sales & viral</span>
                            <p class="sample_list">Bring them to your mall</p>
                            <p class="sample_list">Virtual but real shopping experience</p>
                            <p class="sample_list">Diffirenciate your mall with others</p>
                            <p class="sample_list">Less mouse scroll to to buy products</p>
                        </div>
                        <div class="sample_img">
                            <a href="https://vrmaker.io/view-project/12636" class="sampleBtn">Sample</a>
                        </div>
                    </article>
                    <article class="solution03 solution cf">
                        <div class="sample_txt">
                            <h2>Real Estate Marketing</h2>
                            <span>Promote your property via Virtual Tour</span>
                            <p class="sample_list">Maximaze your exposure </p>
                            <p class="sample_list">Virtual but real shopping experienceSightseeing only for real prospects</p>
                            <p class="sample_list">Easy & Quick to create</p>
                            <p class="sample_list">Statistical evaluation</p>
                        </div>
                        <div class="sample_img">
                            <a href="https://vrmaker.io/view-project/15047" class="sampleBtn">Sample</a>
                        </div>
                    </article>
                    <article class="solution04 solution cf">
                        <div class="sample_txt">
                            <h2>Destination Marketing</h2>
                            <span>Increase tourism & convention sales</span>
                            <p class="sample_list">New way to showcase your destination</p>
                            <p class="sample_list">Direct sales & booking</p>
                            <p class="sample_list">More useful info for the visitor</p>
                            <p class="sample_list">Diffirenciate with others</p>
                        </div>
                        <div class="sample_img">
                            <a href="https://vrmaker.io/view-project/14779" class="sampleBtn">Sample</a>
                        </div>
                    </article>
                    <article class="solution05 solution cf">
                        <div class="sample_txt">
                            <h2>Virtual Education</h2>
                            <span>More attention, increase interest</span>
                            <p class="sample_list">Interactive learning</p>
                            <p class="sample_list">No more text only book</p>
                            <p class="sample_list">New way to teach & learn</p>
                            <p class="sample_list">Watch & Learn anywhere</p>
                        </div>
                        <div class="sample_img">
                            <a href="https://vrmaker.io/view-project/15286" class="sampleBtn">Sample</a>
                        </div>
                    </article>
                </div>
                <div id="vr_hotel" class="tour_service">
                    <article class="solution01 solution cf">
                        <div class="sample_txt">
                            <h2>Hotel/Accomodation Marketing</h2>
                            <span>The best way to showcase your hotel</span>
                            <p class="sample_list">A new way to engage with client</p>
                            <p class="sample_list">Your new unfair advantage</p>
                            <p class="sample_list">Driver sales & direct booking</p>
                            <p class="sample_list">Tell your story in a new way</p>
                        </div>
                        <div class="sample_img">
                            <a href="https://vrmaker.io/view-project/12553" class="sampleBtn">Sample</a>
                        </div>
                    </article>
                </div>
                <div id="vr_commerce" class="tour_service">
                    <article class="solution02 solution cf">
                        <div class="sample_txt">
                            <h2>VR shopping</h2>
                            <span>Increase sales & viral</span>
                            <p class="sample_list">Bring them to your mall</p>
                            <p class="sample_list">Virtual but real shopping experience</p>
                            <p class="sample_list">Diffirenciate your mall with others</p>
                            <p class="sample_list">Less mouse scroll to to buy products</p>
                        </div>
                        <div class="sample_img">
                            <a href="https://vrmaker.io/view-project/12636" class="sampleBtn">Sample</a>
                        </div>
                    </article>
                </div>
                <div id="vr_estate" class="tour_service">
                    <article class="solution03 solution cf">
                        <div class="sample_txt">
                            <h2>Real Estate Marketing</h2>
                            <span>Promote your property via Virtual Tour</span>
                            <p class="sample_list">Maximaze your exposure </p>
                            <p class="sample_list">Virtual but real shopping experienceSightseeing only for real prospects</p>
                            <p class="sample_list">Easy & Quick to create</p>
                            <p class="sample_list">Statistical evaluation</p>
                        </div>
                        <div class="sample_img">
                            <a href="https://vrmaker.io/view-project/15172" class="sampleBtn">Sample</a>
                        </div>
                    </article>
                </div>

                <div id="vr_destination" class="tour_service">
                    <article class="solution04 solution cf">
                        <div class="sample_txt">
                            <h2>Destination Marketing</h2>
                            <span>Increase tourism & convention sales</span>
                            <p class="sample_list">New way to showcase your destination</p>
                            <p class="sample_list">Direct sales & booking</p>
                            <p class="sample_list">More useful info for the visitor</p>
                            <p class="sample_list">Diffirenciate with others</p>
                        </div>
                        <div class="sample_img">
                            <a href="https://vrmaker.io/view-project/14779" class="sampleBtn">Sample</a>
                        </div>
                    </article>
                </div>
                <div id="vr_education" class="tour_service">
                    <article class="solution05 solution cf">
                        <div class="sample_txt">
                            <h2>Virtual Education</h2>
                            <span>More attention, increase interest</span>
                            <p class="sample_list">Interactive learning</p>
                            <p class="sample_list">No more text only book</p>
                            <p class="sample_list">New way to teach & learn</p>
                            <p class="sample_list">Watch & Learn anywhere</p>
                        </div>
                        <div class="sample_img">
                            <a href="https://vrmaker.io/view-project/15286" class="sampleBtn">Sample</a>
                        </div>
                    </article>
                </div>
                <div class="go-contact contact-b"><a href="/contact">Contact Us</a></div>
            </div><!-- //.business-contents-en-->
        </section><!-- //content -->
    </div><!-- //container -->


<?php $this->beginBlock('script') ?>
    <script>
        function openService(evt, tourName) {
            var i, tour_service, service_tablinks;
            tour_service = document.getElementsByClassName("tour_service");
            for (i = 0; i < tour_service.length; i++) {
                tour_service[i].style.display = "none";
            }
            service_tablinks = document.getElementsByClassName("service_tablinks");
            for (i = 0; i < service_tablinks.length; i++) {
                service_tablinks[i].className = service_tablinks[i].className.replace(" active", "");
            }
            document.getElementById(tourName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
    <script type="text/javascript">
        var tabBtn = document.querySelectorAll('.biz-tab');
        var tabContents = document.querySelectorAll('.biz-contents');
        
        document.querySelector('.tab-container').addEventListener('click', function(e){            
            activeBtn(e.target.dataset.id);
            showBtn(e.target.dataset.id);
        });
        
        function activeBtn(i){
            tabBtn[0].classList.remove('on')
            tabBtn[1].classList.remove('on')
            tabBtn[i].classList.add('on')
        }
        
        function showBtn(i){
            tabContents[0].classList.remove('show')
            tabContents[1].classList.remove('show')
            tabContents[i].classList.add('show')
            
        }
    </script>
<?php $this->endBlock() ?>