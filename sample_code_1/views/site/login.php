<?php

/* @var $this yii\web\View */

use yii\web\View;

$this->title = Yii::t('app', 'Login');

?>
    <div id="container">
        <section id="contents" class="gray_bg">
            <div class="inner-wrap cf">
                <div class="signIn">
                    <h2><?= Yii::t('app', 'Login') ?></h2>
                    <div class="form-group">
                        <div class="sign_wrap">
                            <div class="sign_box-wrap">
                                <form id="form-login" class="sign_box" enctype="multipart/form-data">
                                    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                                    <label>
                                        <input type="text" placeholder="<?= Yii::t('app', 'EMAIL') ?>" value="<?php $model->email ?>" name="LoginForm[email]" class="sign_in_input_box email">
                                    </label>
                                    <label>
                                        <input type="password" placeholder="<?= Yii::t('app', 'PASSWORD') ?>" value="<?php $model->password ?>" name="LoginForm[password]" class="sign_in_input_box">
                                    </label>
                                    <div class="login_check">
                                        <label class="check_container">
                                            <input value="1" type="checkbox" checked="checked" name="LoginForm[rememberMe]" id="remember">
                                            <span class="checkmark"></span>
                                            <label for="remember" style="margin-left: 26px;"></label><?= Yii::t('app', 'Remember me') ?>
                                        </label>
                                    </div>
                                    <div class="error-login"></div>
                                    <button type="submit" name ='login-button' class="login-button"><i class="icon-loading fa fa-spinner fa-spin"></i><?= Yii::t('app', 'Log in') ?></button>
                                    <a href="javascript:" class="forget_psw"><?= Yii::t('app', 'Forgot your E-mail or Password?')?></a>
                                </form>
                            </div> <!-- //sign_wrap -->
                            <div class="sns_login cf">
                                <span><?= Yii::t('app', 'or connect with') ?></span>
                                <a href="javascript:" id="login-fb" class="facebook facebook_login"><img src="../images/icon/facebook_icon_b.png" alt="facebook" /></a>
                                <a href="javascript:" id="login-gg" class="google google_login"><img src="../images/icon/google_icon.png" alt="google" /></a>
                                <a href="javascript:" class="naver naver_login" id="naver_id_login"  onclick="handleNaverLogin()"><img src="../images/icon/naver_icon.png" alt="naver" /></a>
                            </div> <!-- //sns_login -->

                            <div class="sign_txt cf">
                                <span><?= Yii::t('app', 'Don’t have an account?') ?> </span>
                                <a href="/signup-agreement" class="signBtn"><?= Yii::t('app', 'Sign up') ?></a>
                            </div>
                        </div> <!-- //sign_box-wrap -->
                    </div> <!-- //form-group -->
                </div><!--  //signIn -->
                <div class="popup_modal">
                    <div class="find_idpw cf">
                        <a href="javascript:" class="popup_close"><img src="../images/main/icon_x.png" width="15"></a>
                        <div class="find_id find_pop">
                            <div class="find_box">
                                <h3><?= Yii::t('app', 'Find Your Account') ?></h3>
                                <p><?= Yii::t('app', 'You will receive an email if entered email is registered.') ?></p>
                                <form class="login_area forget_id_form" method="post" id="form-find-account">
                                    <fieldset>
                                        <div>
                                            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                                            <input type="text" name="account_find" id="account_find" class="sign_in_input_box text" placeholder="E-mail">
                                            <span id="message-find-account" style="display: none;"></span>
                                            <button type="submit" class="submit_btn" id="find-account-btn"><i class="icon-loading icon-loading-find-account fa fa-spinner fa-spin"></i> <?= Yii::t('app', 'Okay')?></button>
                                            <p><?= Yii::t('app', 'Don’t have an account?') ?></p>
                                            <a href="/signup-agreement" class="singup_btn"><?= Yii::t('app', 'Sign up VRMaker') ?></a>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <div class="find_pw find_pop">
                            <div class="find_box">
                                <h3><?= Yii::t('app', 'Find Your Password') ?></h3>
                                <p><?= Yii::t('app', 'You will receive an email with temporary Password.') ?></p>
                                <form class="login_area forget_pw_form" method="post" id="form-find-password">
                                    <fieldset>
                                        <div>
                                            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                                            <input type="text" id="email-reset-pass" name="email" class="sign_in_input_box text" placeholder="E-mail">
                                            <span id="message-find-password" style="display: none;"></span>
                                            <button type="submit" class="submit_btn" id="find-password-btn"><i class="icon-loading icon-loading-find-password fa fa-spinner fa-spin"></i> <?= Yii::t('app', 'Get the link') ?></button>
                                            <p><?= Yii::t('app', 'You still can’t find your account?') ?></p>
                                            <a href="javascript:" class="submit_btn send_email_btn"><?= Yii::t('app', 'Email us') ?></a>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="popup_login">
                    <div class="find_idpw modal_login cf">
                        <input type="hidden" value="" name="re_email" id="re_email">
                        <a href="javascript:" class="popup_close close_error_login"><img src="../images/main/icon_x.png" width="15"></a>
                        <div class="modal_login_error">
                            <p><?= Yii::t('app', 'The account has not been activated') ?>,<br>
                                <?= Yii::t('app', 'please check your email to activate') ?>.<br>
                                <?= Yii::t('app', 'Did not receive the email?') ?>
                            </p>
                            <a href="#" class="resend-mail"><?= Yii::t('app', 'Resend email') ?></a>
                        </div>
                    </div>
                </div>
            </div><!-- //inner-wrap -->
        </section><!-- //content -->
    </div><!-- //container -->
<?php $this->beginBlock('script') ?>
    <script type="text/javascript" src="https://static.nid.naver.com/js/naverLogin_implicit-1.0.3.js" charset="utf-8"></script>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '<?= Yii::$app->params['services']['facebook']['app_id'] ?>',
                cookie     : true,  // enable cookies to allow the server to access
                                    // the session
                xfbml      : true,  // parse social plugins on this page
                version    : 'v3.3' // The Graph API version to use for the call
            });

            // FB.AppEvents.logPageView();

        };

        var startApp = function() {
            gapi.load('auth2', function(){
                auth2 = gapi.auth2.init({
                    client_id: '<?= Yii::$app->params['services']['google']['client_id'] ?>',
                    cookiepolicy: '/',
                });
                attachSignin(document.getElementById('login-gg'));
            });
        };

        function handleNaverLogin() {
            var callBackUrl = '<?= Yii::$app->params['services']['naver']['call_back_url'] ?>';
            var client_id = '<?= Yii::$app->params['services']['naver']['app_id'] ?>';
            var state = '<?= $this->generate_state() ?>';
            var url = 'https://nid.naver.com/oauth2.0/authorize?client_id='+ client_id +'&response_type=code&redirect_uri='+ callBackUrl +'&state='+ state;
            // Open popup
            var newWindow = window.open(url,'Naver Login','height=450,width=800');
            if (window.focus) {
                newWindow.focus()
            }
        }

    </script>
    <script type="text/javascript" src="/js/login-social.js?v=1.0"></script>
    <script>
        function findIdPass(){
            $(".forget_psw").on("click", function(){
                $('.find_idpw').show();
                $('.popup_modal').show();
            });

            $(".popup_close").on("click", function(){
                $('.find_idpw').hide();
                $('.popup_modal').hide();
            });
        }
        findIdPass();

        $('#form-login').submit(function (e) {
            e.preventDefault();
            $('.icon-loading').css('display', 'inline-block');
            $('.login-button').attr('disabled', 'disabled');
            $.ajax( {
                url: '/site/login',
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false,
                success: function (res) {
                    $('.icon-loading').css('display', 'none');
                    $('.login-button').removeAttr('disabled');

                    switch (res.success) {
                        case 0:
                            $('#re_email').val(res.email);
                            $('.popup_login').show();
                            $('.modal_login ').show();
                            break;
                        case 1:
                            window.location.href = '/';
                            break;
                        default:
                            var error = $('.error-login');
                            error.text(res.msg);
                            error.show();
                            break;
                    }

                }
            });
        });

        $('.close_error_login').click(function () {
            $('.popup_login').hide();
        });

        $('.resend-mail').click(function (e) {
            e.preventDefault();
            $(this).hide();
            $.ajax( {
                url: '/site/resend-mail',
                type: 'GET',
                data: {
                    email: $('#re_email').val(),
                },
                success: function (res) {
                    if (res.success) {
                        alert('Resend mail success. Check your email and activate account');
                        window.location.reload();
                    } else {
                        alert('An error occurred, please try again');
                    }
                }
            });
        })

        $('#form-find-account').submit(function (e) {
            e.preventDefault();
            var accountFind = $('#account_find').val();
            if (accountFind != "") {
                $.ajax( {
                    url: '/site/find-account',
                    type: 'POST',
                    data: new FormData( this ),
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        $('.icon-loading-find-account').css('display', 'inline-block');
                        $('#find-account-btn').attr('disabled', 'disabled');
                        $('#find-account-btn').css('cursor', 'wait');
                    },
                    complete: function () {
                        $('.icon-loading-find-account').css('display', 'none');
                        $('#find-account-btn').removeAttr('disabled');
                        $('#find-account-btn').css('cursor', 'auto');
                    },
                    success: function (res) {
                        if (res.success) {
                            $('#message-find-account').hide().html(res.msg).fadeIn('slow');
                        } else {
                            $('#message-find-account').hide().html(res.msg).fadeIn('slow');
                        }
                    }
                });
            }
        });

        $('#form-find-password').submit(function (e) {
            e.preventDefault();
            var email = $('#email-reset-pass').val();
            if (email != "") {
                $.ajax( {
                    url: '/site/forgot-password',
                    type: 'POST',
                    data: new FormData( this ),
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        $('.icon-loading-find-password').css('display', 'inline-block');
                        $('#find-password-btn').attr('disabled', 'disabled');
                        $('#find-password-btn').css('cursor', 'wait');
                    },
                    complete: function () {
                        $('.icon-loading-find-password').css('display', 'none');
                        $('#find-password-btn').removeAttr('disabled');
                        $('#find-password-btn').css('cursor', 'auto');
                    },
                    success: function (res) {
                        if (res.success) {
                            $('#message-find-password').hide().html(res.msg).fadeIn('slow');
                        } else {
                            $('#message-find-password').hide().html(res.msg).fadeIn('slow');
                        }
                    }
                });
            }
        });
    </script>


<?php $this->endBlock() ?>