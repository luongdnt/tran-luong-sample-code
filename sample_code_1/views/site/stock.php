<?php

/* @var $this yii\web\View */

use yii\web\View;

$this->title = 'Stock';

?>
<section id="stock_visual" class="visual">
    <div class="mask">
        <div class="visual-contents stock_info_contents">
            <h1>The World Best 360 Photo & Video</h1>
            <form class="search-form">
                <div class="search_container">
                    <div class="search_container_table">
                        <div class="search_table no-stretch">
                            <div class="select_box">
                                <select class="filter-view-type">
                                    <option selected="selected" value="all">All</option>
                                    <option value="images">Images</option>
                                    <option value="videos">Videos</option>
                                    <option value="author">Author</option>
                                    <option value="location">Location</option>
                                    <option value="title">Title</option>
                                    <option value="tag">Tag</option>
                                </select>
                                <i class="fas fa-chevron-down"></i>
                            </div>
                        </div>
                        <div class="search_table">
                            <input class="tour_search" type="text" placeholder="Find Panorama">
                        </div>
                        <div class="search_table no-stretch">
                            <div class="search_btn"><button><i class="xi-search"></i></button></div>
                        </div>
                    </div><!-- //.search_container_table -->
                </div><!-- //.search_container -->
            </form><!-- //.search-form -->
        </div> <!-- //.stock_info_contents -->
        <div class="visual_author">Tour by author name</div>
    </div><!-- //.mask -->
</section><!-- //#visual -->
<div id="container">
    <section id="contents">
        <div class="stock-container">
            <div class="pano-contents stock-contents">
                <h2>Most Viewed</h2>
                <div class="slider_container center">
                    <div>
                        <a href="">
                            <div class="author-img"><img src="../images/stock/most01.jpg" /></div>
                            <div class="author-info">
                                <p class="author-title">Title Marina Bay</p>
                                <span class="author-name">Author Leo Kim</span>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="">
                            <div class="author-img"><img src="../images/stock/most02.jpg" /></div>
                            <div class="author-info">
                                <p class="author-title">Title Marina Bay</p>
                                <span class="author-name">Author Leo Kim</span>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="">
                            <div class="author-img"><img src="../images/stock/most03.jpg" /></div>
                            <div class="author-info">
                                <p class="author-title">Title Marina Bay</p>
                                <span class="author-name">Author Leo Kim</span>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="">
                            <div class="author-img"><img src="../images/stock/most01.jpg" /></div>
                            <div class="author-info">
                                <p class="author-title">Title Marina Bay</p>
                                <span class="author-name">Author Leo Kim</span>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="">
                            <div class="author-img"><img src="../images/stock/most02.jpg" /></div>
                            <div class="author-info">
                                <p class="author-title">Title Marina Bay</p>
                                <span class="author-name">Author Leo Kim</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div> <!-- //.stock-contents -->
        </div> <!-- //.stock-container -->
        <div class="stock_submit_container">
            <h3>SUBMIT YOUR PANORAMA</h3>
            <p>Make your projects with VRMaker Stock</p>
            <div class="stock_submit_btn"><a href="javascript:">Submit</a></div>
        </div> <!-- //.stock_submit_container -->
        <div class="recent-container">
            <h2>Recently Added</h2>
            <div id="photos" class="random_tour_container">
                <div>
                    <a href="stock@leokim.html">
                        <div class="mask">
                            <div class="img_tour"></div>
                            <h2>Ta Prohm temple</h2>
                            <span>Jane</span>
                        </div>
                        <img src="../images/stock/most01.jpg" >
                    </a>
                </div>
                <div>
                    <a href="stock@leokim.html">
                        <div class="mask">
                            <div class="video_tour"></div>
                            <h2>Ta Prohm temple</h2>
                            <span>Jane</span>
                        </div>
                        <img src="../images/stock/most02.jpg" >
                    </a>
                </div>
                <div>
                    <a href="stock@leokim.html">
                        <div class="mask">
                            <div class="img_tour"></div>
                            <h2>Ta Prohm temple</h2>
                            <span>Jane</span>
                        </div>
                        <img src="../images/stock/most03.jpg" >
                    </a>
                </div>
                <div>
                    <a href="stock@leokim.html">
                        <div class="mask">
                            <div class="img_tour"></div>
                            <h2>Ta Prohm temple</h2>
                            <span>Jane</span>
                        </div>
                        <img src="../images/stock/most01.jpg" >
                    </a>
                </div>
                <div>
                    <a href="stock@leokim.html">
                        <div class="mask">
                            <div class="img_tour"></div>
                            <h2>Ta Prohm temple</h2>
                            <span>Jane</span>
                        </div>
                        <img src="../images/stock/most02.jpg" >
                    </a>
                </div>
                <div>
                    <a href="stock@leokim.html">
                        <div class="mask">
                            <div class="video_tour"></div>
                            <h2>Ta Prohm temple</h2>
                            <span>Jane</span>
                        </div>
                        <img src="../images/stock/most03.jpg" >
                    </a>
                </div>
                <div>
                    <a href="stock@leokim.html">
                        <div class="mask">
                            <div class="video_tour"></div>
                            <h2>Ta Prohm temple</h2>
                            <span>Jane</span>
                        </div>
                        <img src="../images/stock/most02.jpg" >
                    </a>
                </div>
            </div> <!-- //.random_tour_container -->
        </div>
    </section><!-- //content -->
</div><!-- //#container -->


<?php $this->beginBlock('script') ?>
    <script type='text/javascript' src="../js/lib/flexbox-gallery.js"></script>

    <script>
        $('.center').slick({
            centerMode: true,
            // autoplay: true,
            // autoplaySpeed: 2000,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        slidesToShow: 1
                    }
                }
            ]
        });

        $('.slick-dots li button').text("");
        $('.slider_container .slick-arrow').text('');

    </script>
<?php $this->endBlock() ?>