<?php

/* @var $this yii\web\View */

use yii\web\View;

$this->title = 'Signin';

?>
<div id="container">
    <section id="contents" class="gray_bg">
        <div class="inner-wrap cf">
            <div class="signIn">
                <h2>Log in</h2>
                <div class="form-group">
                    <div class="sign_wrap">
                        <div class="sign_box-wrap">
                            <form class="sign_box">
                                <label>
                                    <input type="text" placeholder="E-MAIL" name="email" class="sign_in_input_box email">
                                </label>
                                <label>
                                    <input type="password" placeholder="PASSWORD" name="psw" class="sign_in_input_box">
                                </label>
                                <div class="login_check">
                                    <label class="check_container">
                                        <input type="checkbox" checked="checked" name="remember" id="remember">
                                        <span class="checkmark"></span>
                                        <label for="remember" style="margin-left: 26px;"></label>Remember me
                                    </label>
                                </div>
                                <button type="submit">Log in</button>
                                <a href="javascript:" class="forget_psw">Forgot your E-mail or Password?</a>
                            </form>
                        </div> <!-- //sign_wrap -->
                        <div class="sns_login cf">
                            <span>or connect with</span>
                            <a href="javascript:" class="facebook facebook_login"><img src="../images/icon/facebook_icon_b.png" alt="facebook" /></a>
                            <a href="javascript:" class="google google_login"><img src="../images/icon/google_icon.png" alt="google" /></a>
                            <a href="javascript:" class="naver naver_login"><img src="../images/icon/naver_icon.png" alt="naver" /></a>
                        </div> <!-- //sns_login -->

                        <div class="sign_txt cf">
                            <span>Don’t have an accout? </span>
                            <a href="agreement.html" class="signBtn">Sign up</a>
                        </div>
                    </div> <!-- //sign_box-wrap -->
                </div> <!-- //form-group -->
            </div><!--  //signIn -->
            <div class="popup_modal">
                <div class="find_idpw cf">
                    <a href="javascript:" class="popup_close"><img src="../images/main/icon_x.png" width="15"></a>
                    <div class="find_id find_pop">
                        <div class="find_box">
                            <h3>Find Your Account</h3>
                            <p>You will receive an email if entered email is registered.</p>
                            <form class="login_area forget_id_form" method="post" >
                                <fieldset>
                                    <div>
                                        <input type="text" id="u-email" class="sign_in_input_box text" placeholder="E-mail">
                                        <button type="submit" class="submit_btn">Okay</button>
                                        <p>Don’t have an account?</p>
                                        <a href="agreement.html" class="singup_btn">Sign up VRMaker</a>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="find_pw find_pop">
                        <div class="find_box">
                            <h3>Find Your Password</h3>
                            <p>You will receive an email with temporary Password.</p>
                            <form class="login_area forget_pw_form" method="post" >
                                <fieldset>
                                    <div>
                                        <input type="text" id="email-reset-pass" class="sign_in_input_box text" placeholder="E-mail">
                                        <button type="submit" class="submit_btn">Get the link</button>
                                        <p>You still can’t find your account?</p>
                                        <a href="javascript:" class="submit_btn send_email_btn">Email us</a>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- //inner-wrap -->
    </section><!-- //content -->
</div><!-- //container -->

<?php $this->beginBlock('script') ?>
    <script>
        function findIdPass(){
            $(".forget_psw").on("click", function(){
                $('.find_idpw').show();
                $('.popup_modal').show();
            });

            $(".popup_close").on("click", function(){
                $('.find_idpw').hide();
                $('.popup_modal').hide();
            });
        }
        findIdPass();
    </script>
<?php $this->endBlock() ?>