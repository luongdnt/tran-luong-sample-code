<?php
    use yii\helpers\Html;
    $lang = Yii::$app->session->get('language');
    $price = $lang == 'ko' ? $plan->price_kr : $plan->price_us;
    $price_details = json_decode($plan->price_detail);
?>
<div class="pricing_popup_box">
    <div class="order_top plan1 cf">
        <h2><?= Yii::t('app', 'Read to enjoy Professional account?'); ?></h2>
        <a href="javascript:" class="pop_up_close"><i class="xi-close"></i></a>
        <input type="hidden" value="<?= $price ?>" id="price">
        <div class="order_enter">
            <h3>
                <?= $plan->name ?>
            </h3>
            <p> <?= $lang == 'ko' ? $price . ' 원 <span>/월</span>' : number_format($price) . ' $ <span>/month</span>' ?></p>
        </div>
        <div class="order_info">
            <?= Yii::t('app', 'You will have'); ?><br/>
            1. <em><?= $plan->storage / (1024*1024) ?>G</em> <?= Yii::t('app', 'Storage'); ?><br/>
            2. <?= Yii::t('app', 'Unlimited VR Tour production') ?><br/>
            3. <?= Yii::t('app', 'Unlimited file size(photo: unlimited, Video: 2G)') ?><br/>
<!--            4. 'Provides advanced features such as 3D audio and mini-map') -->
        </div>
    </div>

    <div class="order_bottom cf">
        <form id="form-pricing" class="info_area cf" action="/payment/checkout" method="post" enctype="multipart/form-data">
            <input type="hidden" id="plan_id" name="plan_id" value="<?= $plan->id ?>">
            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
            <div class="option">
                <fieldset>
                    <legend class="blind"><?= Yii::t('app', 'Payment Information'); ?></legend>
                    <div class="up_info cf">
                        <h2 class="up_title"><?= Yii::t('app', 'Subscription period') ?></h2>
                        <label class="select" for="Period">
                            <select name="payment_period" id="payment_period">
                                <?php foreach ($price_details as $price_detail): ?>
                                    <option value="<?= $price_detail->month ?>" data-sale="<?= $price_detail->discount ?>"><?= $price_detail->month ?> <?= Yii::t('app', 'month') ?> (<?= $price_detail->discount ?>% <?= Yii::t('app', 'Sale') ?>)</option>
                                <?php endforeach; ?>
                            </select>
                        </label>
<!--                        <div class="period_check_wrap cf">-->
<!--                            <label class="period_check">-->
<!--                                <input type="radio" checked="checked" name="period_check" value="12">-->
<!--                                Recurring payment-->
<!--                          </label>-->
<!--                            <label class="period_check">-->
<!--                               <input type="radio" name="period_check" value="1">-->
<!--                               One time payment (1 month)-->
<!--                            </label>-->
<!--                        </div>-->

                    </div>
                    <div class="up_info cf">
                        <label for="Promotion" class="up_title"><?= Yii::t('app', 'Promotion Code') ?></label>
                        <input type="text" name="promotion_code">
                        <a href="javascript:" class="btn"><?= Yii::t('app', 'Apply') ?></a>
                    </div>
                    <div class="up_info cf">
                        <h2 class="up_title"><?= Yii::t('app', 'Payment method') ?></h2>
                        <label for="period" class="select">
                            <select name="payment_method" id="">
                                <?php if ($lang == 'ko'): ?>
                                    <option value="card_paid">신용카드</option>
                                <?php else: ?>
                                    <option value="paypal">Paypal</option>
                                    <option value="paypal_subscription">Paypal subscription</option>
                                <?php endif; ?>
                            </select>
                        </label>
                    </div>
                </fieldset>
            </div>
            <div class="price_list">
                <div class="up_info cf">
                    <h2 class="up_title"><?= Yii::t('app', 'Order Summary') ?></h2>
                    <table class="pricing_table">
                        <tbody>
                        <tr>
                            <th><?= $plan->name ?></th>
                            <td><?= Yii::$app->session->get('language') == 'ko' ? '₩' : '$' ?> <span id="total"><?= $price ?></span></td>
                        </tr>
                        <tr class="total">
                            <th><?= Yii::t('app', 'Sum') ?></th>
                            <td class="total_price">
                                <?= Yii::$app->session->get('language') == 'ko' ? '₩' : '$' ?> <span id="total_price"><?= $price ?></span>
                            </td>
                        </tr>
                        <tr class="sale_price">
                            <th></th>
                            <td><span>(<?= Yii::t('app', 'Discount') ?> : <em class="discount">0</em><?= Yii::$app->session->get('language') == 'ko' ? '원' : '$' ?>)</span></td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="javascript:" class="paid_btn"><i class="fas fa-spinner fa-spin btn-loading"></i><?= Yii::t('app', 'Pay now') ?></a>
                </div>
            </div>
        </form>

</div> <!-- //.pricing_popup_box -->

<?php if ($lang == 'ko'): ?>
<div class="popup-modal auto_card_popup popup-subscription-iamport-card">
    <div class="popup-modal_box update-account-box">
        <a href="javascript:" class="popup-modal-close"><i class="xi-close"></i></a>
        <h3>결제 요청이 접수되었습니다.</h3>
        <p class="payment-desc">아래 계좌로 "<em class="amount">Amount</em>"원을 "<em class="amount-duedate">Due date</em>"까지 입금해 주세요.</p>
        <p><span class="vbank-name"></span><span class="vbank-num"></span></p>
    </div>
    <div class="popup-modal_box login_box auto_card_box">
        <div class="normal_login" style="padding:0">
            <a href="javascript:" class="popup-modal-close"><i class="xi-close"></i></a>
            <div class="auto_card_info_box">
                <h3>신용카드 결제</h3>
                <table class="auto_card_info">
                    <tr>
                        <th>이용기간</th>
                        <td><span class="blue"><span class="sub-start-date">2018.03.13</span> ~ <span class="sub-end-date">2018.05.13</span></span> (자동결제)</td>
                    </tr>
                    <tr>
                        <th class="liner">상품금액</th>
                        <td class="liner"><span class="blue"><span class="payment-amount">9,900</span>\</span> (부가세 포함)</td>
                    </tr>
                </table>
            </div>
            <form class="form-get-billing-token login_area">
                <fieldset>
                    <legend class="blind">신용카드 결제</legend>
                    <table class="cf">
                        <tr class="card_num">
                            <th>카드번호</th>
                            <td>
                                <input type="text" name="card_num1" id="card_num1" required="true" class="text" maxlength="4"/>
                                <input type="text" name="card_num2" id="card_num2" required="true" class="text" maxlength="4"/>
                                <input type="text" name="card_num3" id="card_num3" required="true" class="text" maxlength="4"/>
                                <input type="text" name="card_num4" id="card_num4" required="true" class="text" maxlength="4"/>
                            </td>
                        </tr>
                        <tr class="date">
                            <th>유효기간</th>
                            <td>
                                <input type="text" name="card_m" id="card_m" required="true" class = "text" maxlength="2" />월
                                <input type="text" name="card_y" id="card_y" required="true" class = "text" maxlength="2" />년
                            </td>
                        </tr>
                        <tr class="date card_pass">
                            <th>카드비밀번호</th>
                            <td>
                                <input type="password" name="card_pw" id="card_pw" required="true" class = "text"  maxlength="2" />** <span>(비밀번호 앞 2자리 입력)</span>
                            </td>
                        </tr>
                        <tr class="card_num birth_num">
                            <th>생년월일<span>(6자리)</span></th>
                            <td>
                                <input type="text" name="card_birthday" id="card_birthday" required="true" class = "text" maxlength="6" />
                                <!--//    You change this code if You checked compay_num -->
                                <!--//
                                    <input type="text" name="compay_num1" id="compay_num1" required="true" class = "text" maxlength="3" />
                                    <input type="text" name="compay_num2" id="compay_num2" required="true" class = "text" maxlength="2" />
                                    <input type="text" name="compay_num3" id="compay_num3" required="true" class = "text" maxlength="5" />-->
                                <label for="compay_num" class="card_check">
                                    <input type="checkbox" name="company_num" id="compay_num" value="Y" />
                                    <span>법인카드<span class="pc_v">는 체크 후 사업자번호 입력</span></span>
                                </label>
                            </td>
                        </tr>
                    </table>
                    <div class="cf auto_card_agree">
                        <ul class="cf">
                            <li>
                                <input type="checkbox" name="auto_card_agree1" id="auto_card_agree1" value="Y" />
                                <label for="auto_card_agree1">[필수] 매월 정기 결제에 동의</label>
                                <a href="#" class="blue"> (전문보기)</a>
                            </li>
                            <li>
                                <input type="checkbox" name="auto_card_agree2" id="auto_card_agree2" value="Y" />
                                <label for="auto_card_agree2">[필수] 개인(신용)정보의 수집 및 이용 동의</label>
                                <a href="#" class="blue"> (전문보기)</a>
                            </li>
                            <li>
                                <input type="checkbox" name="auto_card_agree3" id="auto_card_agree3" value="Y" />
                                <label for="auto_card_agree3">[필수] 이용약관 및 결제 안내사항에 동의</label>
                                <a href="#" class="blue"> (전문보기)</a>
                            </li>
                            <li>
                                <input type="checkbox" name="auto_card_agree4" id="auto_card_agree4" value="Y" />
                                <label for="auto_card_agree4">[필수] 개인(신용)정보의 제공에 대한 동의</label>
                                <a href="#" class="blue"> (전문보기)</a>
                            </li>
                        </ul>
                        <div class="cf">
                            <input type="hidden" name="customer_uid" />
                            <input type="hidden" name="promotion_code" value=""/>
                            <input type="hidden" name="plan_type" value=""/>
                            <input type="hidden" name="interval" value=""/>
                            <input type="hidden" name="interval_type" value="m"/>
                            <a href="javascript:" class="s_left btn_submit half_btn white_btn">취소</a>
                            <a href="javascript:" class="s_right btn_submit half_btn blue_btn">확인</a>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div> <!-- //.auto_card_popup -->
<?php endif; ?>