
<?php

/* @var $this yii\web\View */

use yii\web\View;

$this->title = 'Terms';
$lang = Yii::$app->session->get('language');

?>

<div id="container">
    <section id="contents">
        <div class="inner-wrap cf">
            <div class="term_wrap">
                <h2>Terms of Use</h2>
                <?php if ($lang == 'ko'): ?>
                    <div class="term_txt">
                    제1장 “약관”의 목적 및 정의 <br/><br/>
                    제1조 (목적) <br/><br/>

                    본 이용약관(이하 "약관"이라고 함)은 ㈜에이투젯(이하 "회사"이라고 함)가 운영 및 관리하는 vrmaker(http://www.vrmaker.io)”(이하 "서비스"이라고 함)에 대하여 "회사" 와 "서비스"를 이용하는 이용자간의 의무와 기타 필요한 사항을 구체적으로 규정하는데 목적이 있습니다. <br/>

                    제2조 (용어의 정의) <br/><br/>

                    가. “콘텐츠” : “회사” 또는 “회원”이 “서비스”에 등록, 게재한 모든 자료(데이터) 및 문서 등을 말합니다. <br/>
                    나. “회원” : “서비스”의 이용약관, 개인정보취급방침 및 기타 이용안내를 숙지하고 동의한 후 “회사”에 개인정보를 제공하고 “서비스”에 가입한 자를 말합니다. <br/>

                    제3조 (약관의 효력 및 변경) <br/><br/>

                    가. 본 “약관”은 “서비스”를 통해 온라인으로 공지함으로써 그 효력이 발생하며, 합리적인 사유가 발생한 경우 관련법에 위배되지 않는 범위 안에서 개정할 수 있습니다. <br/>
                    나. “회사”가 본 “약관”을 개정할 경우에는 적용 일부터 14일간 적용일자 및 개정사유를 명시하여 “서비스”의 초기화면 또는 팝업화면을 통해 공지합니다. <br/>
                    다. “회원”이 변경된 “약관”에 동의할 수 없을 경우 변경 이전의 “약관”을 적용할 수 있으나, 변경된 “약관”의 효력 발생일로부터 14일이 경과된 이후에도 거부의사를 표시하지 않고 서비스를 계속 이용할 경우, 변경된 “약관”에 동의한 것으로 간주합니다. <br/><br/><br/>


                    제 2장 서비스 이용 계약의 체결 <br/><br/>

                    제4조 (“회원”가입 및 이용 계약의 성립) <br/><br/>

                    가. “회사”의 “서비스”를 이용하려는 이용자는 반드시 “회사”에서 요청하는 이름, 연락처 등 제반 정보를 성실히 제공하고 본 “약관”과 “개인정보취급방침”에 동의하고 기타 이용안내를 충분히 숙지하고 준수하여야만 합니다. <br/>
                    나. 이용자는 반드시 본인의 실명 또는 회사명으로 본 서비스를 이용할 수 있으며, 실명으로 등록하지 않은 경우에는 일체의 권리를 주장할 수 없습니다. <br/>
                    다. 타인의 명의 또는 정보를 도용하여 “회원”가입한 이용자는 관련법에 따라 처벌을 받을 수 있으며, 해당 이용자의 정보는 모두 삭제 처리됩니다. <br/>
                    라. 본 “약관”은 “서비스” 이용자가 정상적인 “회원”가입 절차를 완료한 직후 성립되며, 그 즉시 효력이 발생합니다. <br/><br/>

                    제5조 (이용 계약의 승낙과 제한) <br/><br/>

                    가. “회사”는 본 “약관” 제4조의 규정에 의한 “서비스” “회원”가입 및 이용 계약의 성립을 원칙적으로 접수 순서에 따라 승낙하고 있습니다. 단, “회사”의 업무 수행상 또는 기술상 지장이 있는 경우는 예외로 합니다. <br/>
                    나. “회사”는 아래 각 호에 해당하는 경우 “서비스” “회원”가입 및 이용 계약의 체결을 거부할 수 있습니다. <br/>
                    ① 타인의 명의를 이용하거나, 도용하여 신청한 경우 <br/>
                    ② 서비스이용계약 신청서에 허위로 내용을 기재한 경우 <br/>
                    ③ 성인물 제작 및 사회 안녕과 질서, 미풍양속을 저해할 목적으로 신청한 경우 <br/>
                    ④ 부정한 용도로 본 서비스를 이용하고자 하는 경우 <br/>
                    ⑤ “유료 서비스” 사용료를 납부하지 않았거나 미납한 경우 <br/>
                    ⑥ 기타 규정한 제반 사항을 위반하며 신청하는 경우 <br/>
                    다. “회사”는 다음 각 호에 해당하는 경우, “서비스” “회원”가입 및 이용 계약의 체결에 대한 승낙을유보 또는 보류할 수 있습니다. <br/>
                    ① “회사” 설비에 여유가 없는 경우 <br/>
                    ② “회사”의 기술상 지장이 있는 경우 <br/>
                    ③ 기타 “회사”의 귀책 사유로 승낙이 곤란한 경우 <br/><br/>

                    제6조 (회원 아이디 부여 및 변경 등) <br/><br/>

                    가. “회사”는 “회원”에게 한 개의 아이디를 제공하며, 원칙상 멀티로그인(동시접속)을 허용하지 않습니다. <br/>
                    나. “회원” 아이디는 원칙적으로 변경이 불가하며 부득이한 사유로 인하여 변경하고자 하는 경우, 해당 아이디에 대한 “회원” 탈퇴 후 재가입을 해야 합니다. <br/><br/>

                    제7조 (서비스 이용 시간) <br/><br/>

                    가. “서비스”는 “회사”의 업무상 또는 기술상 특별한 지장이 없는 한 연중무휴, 1일 24시간 운영을 원칙으로 합니다. 단, “회사”는 시스템 정기점검, 증설 및 교체를 위해 “회사”가 정한 날이나 시간에 “서비스”를 일시 중단할 수 있으며, 예정되어 있는 작업의 경우 “서비스”를 통해 사전에 공지합니다. <br/>
                    나. “회사”는 긴급한 시스템 점검, 증설 및 교체 등 부득이한 사유로 인하여 예고 없이 “서비스”를 일시 중단할 수 있으며, 새로운 “서비스”로의 교체 등 “회사”가 적절하다고 판단하는 사유에 의하여 현재 제공되는 “서비스”를 완전히 중단할 수 있습니다. <br/>
                    다. “회사”는 국가비상사태, 정전, 시스템 디스크 장애, 시스템 다운, 기간통신사업자 등의 고의, 과실로 인한 시스템중단 등과 같은 통제할 수 없는 사유가 발생한 경우 “서비스”를 전부 또는 일부를 제한하거나 중지할 수 있습니다. <br/>
                    라. “회사”는 “서비스”를 특정범위로 분할하여 각 범위 별로 이용 가능한 시간을 별도로 지정할 수 있습니다. 다만 이 경우 그 내용을 “서비스” 통해 공지합니다. <br/><br/>

                    제8조 (회원에 대한 통지) <br/><br/>

                    가. “회사”는 “회원”이 제공한 전자우편 주소 또는 연락처 정보로 통지문을 발송할 수 있습니다. 단, 불특정다수의 “회원”에게 통지를 할 경우 일주일 이상 "서비스"에 통지문을 게시함으로써 개별통지를 대신하며, 이는 개별통지와 동일한 효력이 있는 것으로 간주합니다. <br/>
                    나. “회사”는 본 조 가.항의 방법으로 “회원”에게 필요한 통지문을 발송함으로써 통지 내용의 경중과 상관없이 “회원”에 대한 모든 통지 의무를 다한 것으로 간주합니다. <br/>

                    제9조 (정보 제공 및 광고 게재) <br/><br/>

                    가. “회사”는 “회원”에게 전자우편이나 서신 등의 방법으로 “회원”에게 필요하다 판단되는 “서비스” 정보와 기타 각종 정보를 제공할 수 있습니다. <br/>
                    나. “회사”는 “서비스” 개선 및 기타 각종 목적으로 “회원”에게 설문조사를 진행하거나 추가적인 정보를 요구할 수 있습니다. <br/>
                    다. “회사”는 “회사”의 독자적 판단에 따라 “서비스”의 관계 유무에 상관 없이 “회원” 및 제3자를 대상으로 한 각종 광고를 “서비스” 내 게재, 노출할 수 있습니다. 해당 광고를 통해 발생 가능한 수익은 모두 “회사”에 귀속되며, “회원”은 어떠한 경우에도 이와 관계된 권리를 주장하거나 청구할 수 없습니다. <br/><br/>

                    제10조 (“회원” 탈퇴) <br/><br/>

                    가. “회원”이 “서비스” 이용을 중단하고 “회원” 탈퇴를 하고자 할 경우 아래의 경로를 통해 “회원” 탈퇴를 신청하실 수 있습니다. <br/>
                    “회원” 탈퇴 신청 : (회원탈퇴 URL) <br/>
                    나. “회원” 탈퇴 이후에는 “회원”에게 부여된 모든 “서비스” 사용 권리와 혜택이 소멸되며, “회원”은어떠한 이유로도 이를 번복하거나 회복할 수 없으며, “회사”에 어떠한 사항에 대해서도 주장할 수 없습니다. <br/><br/><br/>


                    제3장 계약당사자의 의무 <br/><br/>

                    제11조 (“회사”의 의무) <br/><br/>

                    가. “회사”는 “회원”이 희망한 “서비스” 제공 개시일에 특별한 사정이 없는 한 “서비스”를 이용할 수 있도록 하여야 합니다. <br/>
                    나. “회사”는 계속적이고 안정적인 “서비스” 제공을 위하여 설비장애 또는 멸실 방지를 위하여 노력하여야 하며, 부득이한 경우 이를 지체 없이 수리, 복구하여야 합니다. <br/>
                    다. “회사”는 “회원”이 안전하게 “서비스”를 이용할 수 있도록 “회원”의 개인정보 보호를 위한 보안시스템을 갖추고, 개인정보보호취급방침을 공시 및 이를 준수하여야 합니다. <br/><br/>

                    제12조 (“회원”의 의무) <br/>

                    가. “회원”은 “회원”가입 신청 또는 “회원”정보 변경 시 실명으로 모든 사항을 사실에 근거하여 작성하여야 하며, 허위 또는 타인의 정보를 등록할 경우 일체의 권리를 주장할 수 없습니다. <br/>
                    나. “회원”은 본 “약관”과 개인정보취급방침 및 기타 이용안내와 관련법을 반드시 준수하여야 하며, 기타 “회사”의 업무 방해 행위, “회사”의 명예를 손상시키는 행위를 하여서는 안됩니다. <br/>
                    다. “회원”은 주소, 연락처, 전자우편주소 등 이용 계약 사항이 변경된 경우 이를 회사에 즉시 알려야 하며, 이러한 문제로 발생한 문제에 대해서는 일체의 권리를 주장할 수 없습니다. <br/>
                    라. “회사”가 “회원”에게 부여한 아이디와 비밀번호 등 각종 권리와 혜택을 “회원”이 소홀이 관리하거나 타인과 공유하여 발생한 모든 문제의 책임은 “회원”에게 있습니다. <br/>
                    마. “회원”은 “회사”의 내용증명 등 명시적 동의가 없는 한 “서비스”의 이용권한, 기타 이용계약상의 지위 및 범위를 제3자에게 담보제공, 양도 및 증여할 수 없습니다. <br/>
                    바. “회원”은 “회사” 및 제3자의 저작권 및 기타 지적재산권을 침해하여서는 안됩니다. <br/>
                    사. “회원”은 다음 각 호에 해당하는 행위를 하여서는 안 되며, 해당 행위를 하는 경우 “회사”는 “회원”의 “서비스” 이용 제한 및 해당 게시물 삭제 등 법적 조치를 포함한 각종 제재를 가할 수 있습니다. <br/>
                    ① “회원”가입 신청 또는 “회원”정보 변경 시 허위내용을 등록하는 행위 <br/>
                    ② 다른 “회원”의 아이디, 비밀번호, 주민등록번호를 도용하는 행위 <br/>
                    ③ “회원” 아이디를 타인과 거래하는 행위 <br/>
                    ④ 타 “회원” 또는 제3자에게 심한 모욕을 주거나 명예를 손상시키는 행위 <br/>
                    ⑤ “회사”의 운영진, 직원 또는 관계자를 사칭하는 행위 <br/>
                    ⑥ “회사”로부터 특별한 권리를 부여 받지 않고 “회사”의 각종 프로그램을 변경하거나, “회사”의 서버를 해킹, “회사” “서비스” 또는 게시된 정보의 일부분 또는 전체를 임의로 변경하는 행위 <br/>
                    ⑦ “회사” “서비스”에 위해를 가하거나 고의로 방해하는 행위 <br/>
                    ⑧ “서비스”를 통해 얻은 정보를 “회사”의 사전 허가 없이 “서비스” 이용 외의 목적으로 복제하거나, 제3자에게 제공하는 행위 <br/>
                    ⑨ 성인물 및 공공질서, 미풍양속에 위반되는 저속, 음란한 내용의 정보, 문장, 도형, 음향, 동영상을 전송, 게시하거나 전자우편 또는 기타의 방법으로 타인에게 유포하는 행위 <br/>
                    ⑩ 범죄와 결부된다고 객관적으로 판단되는 행위 <br/>
                    ⑪ “본 약관” 및 개인정보취급방침을 포함하여 기타 “회사”가 정한 기타 이용안내를 위반하는 행위 <br/>
                    ⑫ 기타 관계 법령에 위배되는 행위 <br/><br/><br/>


                    제4장 “콘텐츠”의 관리 <br/><br/>

                    제13조 (“콘텐츠”의 관리) <br/><br/>

                    가. “회사”가 “서비스”를 통해 “회원”에게 제공하는 모든 “콘텐츠”는 “회사”의 독자적 판단에 의해 임의로 삭제, 변경할 수 있습니다. <br/>
                    나. “회사”에서 “서비스”를 통해 “회원”에게 제공하는 “콘텐츠”에 대하여 제 3자가 사용 중지 및 저작권 침해 소송을 제기하는 경우, “회사”는 즉시 사실관계를 확인한 후 해당 “콘텐츠”의 삭제 조치 및 “서비스”를 통한 고지를 실시합니다. <br/>
                    다. “회원”이 “서비스”를 통해 “회사” 및 다른 “회원”에게 제공하는 모든 “콘텐츠”는 “회원”의 독자적 판단에 의해 임의로 삭제, 변경할 수 있습니다. <br/>
                    라. . “회원”이 “서비스”에 등록한 모든 “콘텐츠”에 대하여 “회사”는 “회원”을 위한 별도의 백업 서비스를 제공하지 않습니다. 즉 “회원”이 “서비스”에 등록한 모든 “콘텐츠”의 백업 등 관리에 필요한 사항들은 “회원” 스스로 해결하여야 하며, “회사”는 “콘텐츠”의 유실, 삭제 등에 대한 어떠한 책임도 지지 않습니다. <br/><br/>

                    제14조 (“콘텐츠” 저작권 및 보증) <br/><br/>

                    가. “회사”가 “서비스”를 통해 “회원”에게 제공하는 모든 “콘텐츠”의 저작권 및 기타 지적재산권은 “회사”에 있습니다. <br/>
                    나. “회사”가 “서비스”를 통해 “회원”에게 제공하는 모든 “콘텐츠”는 반드시 본 “약관”과 개인정보취급방침 및 기타 이용안내를 준수하여 열람 또는 사용하여야 하며, 무단으로 “콘텐츠”를 사용, 복제, 배포하는 등 “회사”와 사전 협의 없이 이루어지는 모든 행위에 대해서는 저작권법 및 관련법률에 의한 권리 침해로 간주합니다. <br/>
                    다. “회사”가 “서비스”를 통해 “회원”에게 제공하는 일부 “콘텐츠”는 “회사”가 초상권, 저작권, 상표권, 특허권을 보유하지 않은 것일 수 있습니다(예: 인물의 초상권, 로고/심벌/캐릭터/트레이드마크/서비스마크 등의 상표권, 건물의 재산권 등). 따라서 일부 “콘텐츠”는 단순 열람 이외의 용도로 사용하실 경우 반드시 “회사”로 문의하여 주시기 바라며, 사용 전에 권리자로부터 직접 해당 권리를 취득하여야 주시기 바랍니다. 만일 “회사”와 사전 협의 없이 사용하여 제3자와 이들 권리에 대한 분쟁이 발생할 경우 “회사”는 “회원”에게 발생 가능한 그 어떠한 직·간접적인 피해에 대해서도 책임 지지 않습니다. <br/>
                    라. “회원”이 “서비스”를 통해 “회사” 또는 다른 “회원”에게 제공하는 모든 “콘텐츠”의 저작권 및 기타 지적재산권은 “회원”에게 있습니다. <br/>
                    마. “회원”이 “서비스”를 통해 등록한 “콘텐츠”가 제3자의 저작권 및 기타 지적샌산권을 침해한 사실이 밝혀진 경우 모든 문제에 대한 책임은 “회원”에게 있으며, “회원”은 “회사”는 물론 다른 “회원”들에게 피해가 발생하지 않도록 이를 책임지고 해결하여야 합니다. <br/><br/>

                    제15조 (항공촬영된 "콘텐츠"의 관리) <br/><br/>

                    가. "회원"이 "서비스"를 통해 등록한 "콘텐츠" 중 항공촬영된 "콘텐츠"는 항공법 및 관련법률을 준수하여 생산된 "콘텐츠" 이어야 합니다. <br/>
                    나. “회원”이 “서비스”를 통해 등록한 항공촬영된 “콘텐츠”가 항공법 및 관련법률을 위반한 사실이 밝혀진 경우 모든 문제에 대한 책임은 “회원”에게 있으며, “회원”은 “회사”는 물론 다른 “회원”들에게 피해가 발생하지 않도록 이를 책임지고 해결하여야 합니다. <br/><br/><br/>


                    제5장 서비스 이용 제한 <br/><br/>

                    제16조 (“서비스” 이용제한) <br/><br/>

                    가. “회사”는 “회원”이 본 “약관”과 개인정보취급방침 및 기타 이용 안내를 준수하지 않거나 위반한 사실이 확인된 경우 또는 다음 각 호에 해당하는 경우 “서비스” 이용을 제한할 수 있습니다. <br/>
                    ① 미풍양속을 저해하는 비속한 아이디 및 별명을 이용한 경우 <br/>
                    ② 타 “회원”에게 심한 모욕을 주거나, “서비스” 이용을 방해한 경우 <br/>
                    ③ 기타 정상적인 “서비스” 운영에 방해가 될 경우 <br/>
                    ④ 정보통신윤리위원회 등 관련 공공기관의 시정 요구가 있는 경우 <br/>
                    나. “회사”는 본 조 가.항 규정에 따라 별도의 공지 없이 “회원”이 사용 중인 “서비스”를 일시정지, 초기화, 이용 계약 해지 등의 조치를 취할 수 있습니다. <br/><br/><br/>


                    제6장 손해배상 및 면책 <br/><br/>

                    제17조 (손해배상) <br/><br/>

                    가. “회원”이 본 “약관”과 개인정보취급방침 및 기타 이용 안내를 준수하지 않거나 위반한 사실이 확인된 경우 “회사”는 해당 “회원”에게 “회사”에 발생 가능하거나 이미 발생한 모든 직·간접적인 피해에 대하여 손해 배상을 청구할 수 있습니다. <br/>
                    나. “회원”이 “서비스”를 통해 제공 받은 “콘텐츠”를 무단으로 사용하거나 본 “약관”과 개인정보취급방침 및 기타 이용 안내를 벗어난 사용 사실이 확인된 경우, “회원”은 “회사”의 법률 대리인에게 지급되는 비용을 포함한 모든 손해와 피해에 상응하는 손해배상금을 “회사”에 지불하여야 합니다. <br/><br/>

                    제18조 (면책조항) <br/><br/>

                    가. “회사”는 아래와 같은 사유에 해당하는 경우 “회원”에게 발생 가능한 그 어떠한 직·간접적인 피해에 대해서도 책임 지지 않습니다. <br/>
                    ① 홍수, 지진등 천재지변, 전쟁 및 기타 이에 준하는 불가항력으로 인하여 “서비스”를 제공할 수 없는 경우 <br/>
                    ② 기간통신사업자가 전기통신 서비스를 중지하거나 정상적으로 제공하지 아니하여 “서비스”를 제공하지 못한 경우 <br/>
                    ③ “회사”의 “서비스”와 관련된 설비의 구축, 보수, 교체, 정기점검, 공사 등 부득이한 사유로 “서비스”를 제공하지 못한 경우 <br/>
                    ④ “회원”의 귀책사유로 인한 “서비스” 장애 또는 손해가 발생한 경우 <br/>
                    ⑤ “회원”의 컴퓨터 오류로 인해 “서비스”를 제공하지 못한 경우 <br/>
                    ⑥ “회원”이 신상정보 및 전자우편 주소를 부실하게 기재하여 “회사”가 “서비스”를 제공하지 못한 경우 <br/>
                    나. “회사”는 “회원”이 본 “약관”과 개인정보취급방침 및 기타 이용 안내를 준수하지 않거나 위반하여 발생한 “회원”의 그 어떠한 직·간접적인 피해에 대해서도 책임 지지 않습니다. <br/>
                    다. “회사”는 “회원”이 “서비스”를 이용함으로써 기대하는 어떠한 효과 또는 이익을 보장하지 않으며, 발생 가능한 그 어떠한 직·간접적인 피해에 대해서도 책임 지지 않습니다. <br/>
                    라. “회사”는 “회원”이 “서비스”에 게재한 각종 정보, 자료로 인해 발생 가능한 그 어떠한 직·간접적인 피해에 대해서도 책임 지지 않습니다. <br/>
                    마. “회사”는 “서비스”를 매개로 “회원”과 제3자 간의 분쟁이 발생한 경우 개입할 의무가 없으며, 발생 가능한 그 어떠한 직·간접적인 피해에 대해서도 책임 지지 않습니다. <br/><br/><br/>


                    제7장 분쟁해결 및 준거법 <br/><br/>

                    제19조 (분쟁해결) <br/><br/>

                    가. “회사”는 “회원”이 제기하는 정당한 의견이나 불만을 반영하고 그 피해를 보상처리하기 위하여 고객지원센터를 설치, 운영합니다. <br/>
                    나. “회사”는 “회원”으로부터 제기되는 불만사항 및 의견을 처리하기 위해 노력하며, 신속한 처리가 곤란한 경우 “회원”에게 그 사유와 처리일정을 통보해 드립니다. <br/>
                    다. “회사”와 “회원”간에 발생한 분쟁은 전자거래기본법 제28조 및 동 시행령 제15조에 의하여 설치된 전자거래분쟁조정위원회의 조정에 따를 수 있습니다. <br/><br/>

                    제20조 (관할법원) <br/><br/>

                    본 “서비스” 이용으로 발생한 분쟁에 대해 소송이 제기되는 경우 한국법을 적용하며, 서울지방법원을 관할 법원으로 합니다. <br/><br/><br/>



                    <부칙> <br/>
                        1. 이 약관은 2017년 01월 01일부터 시행합니다. <br/>



                </div>
                <?php else: ?>
                    <div class="term_txt">
                    Please read these terms of use carefully before you start to use vrmaker.io(“Site”), whether as a guest or a registered user. By using our site, you indicate that you accept these terms of use and that you agree to abide by them. Vrmaker.io reserves the right to amend these terms from time to time without prior notice, and you agree to be bound by such changes.<br>
                    <br>
                    <br>
                    1. OWNERSHIP<br>
                    <br>
                    This Site is owned and operated by ATOJET CORPORATION. All images on this web site are owned by ATOJET or its partners / contributors and are protected by International copyright laws and international treaty provisions. ATOJET retains all rights not expressly granted by this agreement. The license contained in this agreement will terminate automatically without notice from ATOJET, if you fail to comply with any provision of this agreement. In addition, ATOJET reserves the right to terminate this site without notice.<br>
                    <br>
                    <br>
                    2. INTELLECTUAL PROPERTY RIGHTS<br>
                    <br>
                    All content on this Site, including but not limited to videos, images, or any other content (collectively "Content"), are protected by copyright, trademark, patent, trade secret and other intellectual property laws and treaties. Any unauthorized use of any Content violates such laws and this Terms of Use. Except as expressly provided herein or in a separate license agreement between you and ATOJET, ATOJET does not grant any express or implied permission to use the Site, the Site Services (as hereinafter defined), or any Content. You agree not to copy, republish, frame, link to, download, transmit, modify, adapt, create derivative works based on, rent, lease, loan, sell, assign, distribute, display, perform, license, sublicense or reverse engineer the Site or any ATOJET Content. In addition, you agree not to use any data mining, robots or similar data and/or image gathering and extraction methods in connection with the Site or ATOJET Content. Unless you enter into a license agreement with ATOJET you may not download, distribute, display and/or copy any Content. You may not remove any watermarks or copyright notices contained in the Content.<br>
                    <br>
                    <br>
                    3. USE OF THE SITE<br>
                    <br>
                    You will access and use the Site and participate in the Site for lawful purposes only and only in accordance with these Terms of Use and the guidelines, policies, restrictions and agreements on the Site.<br>
                    YOU REPRESENT, WARRANT AND COVENANT THAT YOU WILL NOT:<br>
                    a. Use the Content in any way that is not permitted by this Agreement, or any other relevant licensing agreements<br>
                    b. Violate any rights including but not limited to copyrights, intellectual property rights, trademark, privacy, or any other applicable law or regulation of any country, state, other governmental entities or any third party<br>
                    c. Conduct any activity which consists of or contains software viruses, political campaigning, commercial solicitation, chain letters, mass mailings or any form of "spam"<br>
                    d. Use any data mining, robots or similar data gathering and extraction tools on or at the Website or use any other automated means to access the Website<br>
                    e. Provide information to ireadytofly.com which is inaccurate and untrue, including without limitation all transaction and credit card information<br>
                    f. Share or disclose any password or log in information of your vrmaker.io account to any other user or third party other than as specifically provided for herein. If you breach any provision of this section, we shall be entitled to<br>
                    i. terminate this Agreement immediately with notice to you<br>
                    ii. retain all unpaid payments pursuant to this Agreement and<br>
                    iii. seek any legal or equitable remedies.<br>
                    <br>
                    <br>
                    4. YOUR CONTENT<br>
                    <br>
                    For any video, image, or any other content that you upload or post to the Site (“Your Content”), including but not limited to the Site Services, you represent and warrant that: (i) you have all necessary rights to submit Your Content to the Site Services and grant the licenses set forth herein; (ii) ireadytofly.com will not need to obtain licenses from any third party or pay royalties to any third party with respect to Your Content; (iii) Your Content does not infringe any third party's rights, including intellectual property rights and privacy rights; and (iv) Your Content complies with these Terms of Use and all applicable laws.
                    You may not upload, post, or transmit any video, image, or other content that:<br>
                    • Infringes any third party's copyrights or other intellectual property rights or any right of publicity or privacy<br>
                    • Contains any pornographic, defamatory, or otherwise unlawful or immoral content.<br>
                    • Depicts unlawful or violent acts<br>
                    • Promotes fraudulent schemes or gives rise to a claim of deceptive advertising or unfair competition; or<br>
                    • Violates any law, statute, or regulation.<br>
                    <br>
                    <br>
                    5. RESTRICTION AND TERMINATION OF USE<br>
                    <br>
                    We may block, restrict, disable, suspend or terminate your access to all or part of the Site, the Site Services, and/or Content at any time in our discretion without prior notice or liability to you, if you fail to comply with any provision of these Terms of Use or any other agreement with us. Any conduct by you that, in vrmaker.io’s sole discretion, restricts or inhibits any other person or entity from using or enjoying the Site or the Site Services is strictly prohibited and may result in the termination of your access to the Site or the Site Services without further notice.<br>
                    <br>
                    <br>
                    6. WARRANTIES AND DISCLAIMERS<br>
                    <br>
                    Your use of the Site and the Site Services are at your own risk. The Site and the Site Services, are provided by ireadytofly.com under these Terms of Use "as is" without warranty of any kind, either express, implied, statutory or otherwise. ireadytofly.com expressly disclaims any and all warranties of any kind, whether express or implied, to each and any Site Services made available at any time, including, but not limited to the implied warranties of merchantability, fitness for a particular purpose, non-infringement, and any other warranty that might arise under any law. Without limiting the foregoing, ireadytofly.com makes no warranty that: (i) the Site will meet your requirements; (ii) access to the Site will be uninterrupted; (iii) the quality of the Site will meet your expectations; and (iv) any errors or defects in the site, services or materials will be corrected.<br>
                    Certain states do not allow the exclusion of implied warranties, so the above exclusion may not apply to you. You have specific rights under this warranty, but you may have others, which vary from state to state.<br>
                    <br>
                    <br>
                    7. INDEMNIFICATION<br>
                    <br>
                    You agree to defend, indemnify and hold harmless ATOJET, its subsidiaries, affiliates, licensors, employees, agents, third party information providers, Submitters and independent<br>contractors against any claims, damages, costs, liabilities and expenses (including, but not limited to, reasonable attorneys' fees) arising out of or related to your conduct, your use or inability to use Site, the Site Services, your breach or alleged breach of the Website Terms of Use or of any representation or warranty contained herein, your unauthorized use of ATOJET’s Content, or your violation of any rights of another.<br>
                    <br>
                    <br>
                    <br>
                    8. GOVERNING LAW AND JURISDICTION<br>
                    The interpretation and enforcement of this Agreement shall be governed according to international treaty provisions and other applicable laws.<br>
                    <br>
                    <br>
                    9. PRIVACY POLICY<br>
                    We do not sell, share or rent your personal information to any third party and we only use your information as described in the Privacy Policy which is incorporated into these Terms and Conditions by reference. You may review our Privacy Policy here: http://www.vrmaker.io/privacy.php You further agree that by your continued use of the site, you agree to the terms of our Terms of Use and Privacy Policy. If you do not agree to your information being used in the manner stated in our Privacy Policy, please cease from using the service(s) provided by ireadytofly.com.<br>
                    <br>
                    <br>
                    <br>
                </div>
                <?php endif; ?>
            </div>
        </div> <!-- //inner-wrap -->
    </section> <!-- //content -->
</div> <!-- //container -->

<?php $this->beginBlock('script') ?>

<?php $this->endBlock() ?>