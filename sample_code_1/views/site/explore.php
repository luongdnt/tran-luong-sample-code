<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Explore';
$search_key = Yii::$app->request->get('search_key');
?>

    <section id="explore_visual" class="visual">
        <div class="mask">
            <div class="visual-contents explore_info_contents">
                <h1><?= Yii::t('app', 'Explore The World Via VR') ?></h1>
                <form class="search-form" action="" method="get">
                    <div class="search_container">
                        <div class="search_container_table">
                            <div class="search_table">
                                <input name="search_key" class="tour_search" type="text" value="<?= !empty($search_key) ? $search_key : '' ?>" placeholder="<?= Yii::t('app', 'Find Tour') ?>">
                            </div>
                            <div class="search_table no-stretch">
                                <div class="search_btn"><button type="submit"><i class="xi-search"></i></button></div>
                            </div>
                        </div><!-- //.search_container_table -->
                    </div><!-- //.search_container -->
                </form><!-- //.search-form -->
            </div> <!-- //.stock_info_contents -->
        </div><!-- //.mask -->
    </section><!-- //#visual -->
    <?php if (!empty($search_key)): ?>
        <div id="container">
            <section id="contents">
                <div class="explore-container">
                    <div class="pano-contents">
                        <h2 class="tiltle-search"><?= Yii::t('app', 'Search VR tours')?></h2>
                        <h4 class="key-search"><?= $search_key ?> (<?= $data['total'] ?> <?= Yii::t('app', 'results') ?>)</h4>

                        <div class="tour-container cf">
                            <?php foreach ($data['data'] as $project) : ?>
                                <div class="tour-item">
                                    <div class="tour_contents">
                                        <a href="/pano-player/<?= \app\components\HashHelper::hashNumber($project->id) ?>">
                                            <div class="pano-img">
                                                <div class="mask"></div>
                                                <!--                                    <img src="../images/explore/tour07.jpg" />-->
                                                <img src="<?= !empty($project->thumb_path) ? $this->url_image($project->thumb_path) : '/images/explore/tour07.jpg' ?>" alt="<?= Html::encode($project->title) ?>"/>
                                                <div class="tour-author cf">
                                                    <div class="profile"><img src="<?= !empty($project->authors->avatar_url) ? $this->url_image($project->authors->avatar_url) : '/images/authors/author_profile02.jpg' ?>"></div>
                                                    <div class="author-name"><?= !empty($project->authors->username) ? $project->authors->username : "unknown" ?></div>
                                                    <div class="view-cnt"><i class="fas fa-eye"></i><span><?= $project->view ?></span></div>
                                                </div>
                                            </div>
                                            <div class="pano-info">
                                                <strong><?= Html::encode($project->title) ?></strong>
                                                <span><?= !empty($project->address) ? Html::encode($project->address) : 'No location' ?></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div> <!-- //.explore-container -->
            </section><!-- //content -->
        </div><!-- //#container -->
    <?php else: ?>
        <div id="container">
            <section id="contents">
                <div class="explore-container">
                    <div class="pano-contents explore-contents">
                        <h2><?= Yii::t('app', 'Best VR Tour') ?></h2>
                        <div class="slider_container center">
                            <?php foreach ($best_projects as $best_project) : ?>
                                <?php if (empty($best_project->project) || empty($best_project->project->authors)) continue; ?>
                                <div>
                                    <a href="/pano-player/<?= \app\components\HashHelper::hashNumber($best_project->project->id) ?>">
                                        <div class="tour-img">
                                            <div class="mask"></div>
                                            <img src="<?= !empty($best_project->project) ? $this->url_image($best_project->project->thumb_path) : '/images/explore/tour07.jpg' ?>" />
                                            <div class="tour-author cf">
                                                <div class="profile"><img src="<?= !empty($best_project->project) ? $this->url_image($best_project->project->authors->avatar_url) : '/images/authors/author_profile02.jpg' ?>"></div>
                                                <div class="author-name"><?= !empty($best_project->project->authors->username) ? $best_project->project->authors->username : $best_project->project->authors->email ?></div>
                                                <div class="view-cnt"><i class="fas fa-eye"></i><span><?= !empty($best_project->project) ? Html::encode($best_project->project->view) : '' ?></span></div>
                                            </div>
                                        </div>
                                        <div class="tour-info">
                                            <p class="tour-title"><?= !empty($best_project->project) ? Html::encode($best_project->project->title) : '' ?></p>
                                            <span class="tour-name">Author <?= !empty($best_project->project->authors->username) ? Html::encode($best_project->project->authors->username) : $best_project->project->authors->email ?></span>
                                        </div>
                                    </a>
                                </div>
                            <?php endforeach; ?>

                        </div>
                    </div> <!-- //.explore-contents -->
                    <div class="pano-contents">
                        <h2><?= Yii::t('app', 'New Awesome VR Tour') ?></h2>
                        <div class="tour-container cf">
                            <?php foreach ($awesome_projects as $awesome_project) : ?>
                                <div class="tour-item">
                                    <div class="tour_contents">
                                        <a href="/pano-player/<?= \app\components\HashHelper::hashNumber($awesome_project['id']) ?>">
                                            <div class="pano-img">
                                                <div class="mask"></div>
                                                <!--                                    <img src="../images/explore/tour07.jpg" />-->
                                                <img src="<?= !empty($awesome_project['thumb_path']) ? $this->url_image($awesome_project['thumb_path']) : '/images/explore/tour07.jpg' ?>" alt="<?= Html::encode($awesome_project['title']) ?>"/>
                                                <div class="tour-author cf">
                                                    <div class="profile"><img src="<?= !empty($awesome_project['avatar_url']) ? $this->url_image($awesome_project['avatar_url']) : '/images/authors/author_profile02.jpg' ?>"></div>
                                                    <div class="author-name"><?= empty($awesome_project['username']) ? $awesome_project['email'] : $awesome_project['username'] ?></div>
                                                    <div class="view-cnt"><i class="fas fa-eye"></i><span><?= $awesome_project['view'] ?></span></div>
                                                </div>
                                            </div>
                                            <div class="pano-info">
                                                <strong><?= Html::encode($awesome_project['title']) ?></strong>
                                                <span><?= !empty($awesome_project['address']) ? Html::encode($awesome_project['address']) : 'No location' ?></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div> <!-- //.explore-container -->
            </section><!-- //content -->
        </div><!-- //#container -->
    <?php endif; ?>

<?php $this->beginBlock('script') ?>
    <script>
        $('.center').slick({
            centerMode: true,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 2000,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
            responsive: [
                {
                    breakpoint: 1441,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        slidesToShow: 3,
                        centerPadding:0,
                        variableWidth: true,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        slidesToShow: 1,
                        swipeToSlide: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: 0,
                        slidesToShow: 1,
                        swipeToSlide: true
                    }
                }
            ]
        });

        var page = 0;
        var is_loading = false;

        $(document).ready(function(){
            var search_key = '<?= $search_key ?>';
            $(window).scroll(function(){
                var position = $(window).scrollTop() + $(window).height();
                var $el = $('.tour-container');
                var bottom = $el.offset().top + $el.outerHeight(true);

                if( position >= bottom){
                    if (is_loading === true) {
                        return false;
                    }

                    if (page == 'is_last') {
                        return false;
                    }

                    $.ajax({
                        url: '/site/load-more-project',
                        dataType: 'json',
                        async: true,
                        data: {
                            page : page + 1,
                            search_key: search_key
                        },
                        type: 'get',
                        beforeSend: function() {
                            is_loading = true;
                        },
                        complete: function(){
                            is_loading = false;
                        },
                        success: function (res) {
                            if (res.success) {
                                console.log(res);
                                if (res.is_last == true) {
                                    page = 'is_last';
                                } else {
                                    page = page + 1;
                                }
                                $('.tour-container').append(res.html);
                            }

                        }, error: function (res) {
                            console.log('Error');
                        }
                    });

                }

            });

        });

    </script>
<?php $this->endBlock() ?>