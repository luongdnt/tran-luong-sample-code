<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\web\View;

$this->title = 'Pricing';
$lang = Yii::$app->session->get('language');
$features = [];
$user = Yii::$app->getUser()->identity;
//$hotspots = $model->HotspotDropdown;
?>

<div id="container">
    <section id="contents">

        <div class="pricing-container">
            <h2>Pricing</h2>
            <div class="pricing-header">
                <?php foreach ($plans as $key => $plan): ?>
                    <div class="pricing-card <?= $key >= 1 ? 'card2' : 'card1' ?>" data-select="feature-<?= $key == 2 ? $key - 1 : $key?>">
                        <div class="pricing-card-title"><?= Html::encode($plan->name) ?></div>
                        <div class="pricing-description" style="color: rgb(255, 255, 255);">
                            <?php
                                if ($plan->name == 'Starter') {
                                    echo 'If you are new to VRMaker';
                                }

                                if ($plan->name == 'Professional') {
                                    echo 'Ready to Enjoy more?';
                                }
                            ?>
                        </div>
                        <div class="pricing-option <?= $key == 2 ? 'pricing-option-plus' : '' ?> ">
                            <?php if ($key == 2): ?>
                                <?= Yii::t('app', 'All professtional features and Plus') ?> <br><br>
                                <?= Yii::t('app', 'Commercial use for business') ?><br>
                                <?= Yii::t('app', 'VR Tour Download for Web hosting') ?>
                            <?php else: ?>
                                <?= $plan->num_of_tour == 0 ? 'Unlimited VR Tour' : $plan->num_of_tour . ' VR Tour' ?><br>
                                <?= $plan->storage / (1024*1024) ?>G Storage<br>
                                <?= $plan->max_photo_size == 0 ? 'Unlimited size for Photo' : $plan->max_photo_size / 1024 . 'M(Photo)' ?> / <br>
                                <?= $plan->max_video_size == 0 ? 'Unlimited size for Video Upload' : $plan->max_video_size / 1024 . 'M(Video) Upload' ?> <br>
                                <?php if ($plan->name == 'Professional'): ?>
                                    <?= Yii::t('app', 'Privacy setting to hide your Tours from VRMaker listing') ?>
                                <?php endif; ?>
                            <?php endif; ?>

                        </div>
                        <div class="card-btn">

                            <a href="<?= Yii::$app->user->isGuest ? '/login' : (($plan->name != 'Starter' && $plan->name != 'Professional') ? '/contact' : 'javascript:') ?>" class="<?= $plan->name == 'Professional' ? 'pro-plan-btn' : '' ?>">
                                <?php
                                    if ($lang != 'ko') {
                                        switch ($plan->name) {
                                            case 'Starter':
                                                echo 'Free Forever';
                                                break;
                                            case 'Professional':
                                                echo '$' . $plan->price_us . ' / mo';
                                                break;
                                            default:
                                                echo 'Contact us';
                                                break;
                                        }
                                    } else {
                                        switch ($plan->name) {
                                            case 'Starter':
                                                echo 'Free Forever';
                                                break;
                                            case 'Professional':
                                                echo number_format($plan->price_kr) . ' / 월';
                                                break;
                                            default:
                                                echo 'Contact us';
                                                break;
                                        }
                                    }
                                ?>
                            </a>
                        </div>
                    </div>

                    <div class="feature-box-m">
                        <h3>Features</h3>
                        <div class="">
                            <div class="features-list">
                                <?php
                                    $hotspots = json_decode($plan->hotspots);

                                    $features[$key] = !empty($hotspots) ? $hotspots : array_values(array_flip($model->HotspotDropdown));

                                    $count = 0;

                                ?>

                                <?php
                                    foreach ($model->HotspotDropdown as $hotspot_key => $hotspot){

                                        if (empty($hotspots)) {

                                            $count++;

                                            echo ($count != 1 ? ' / ' : '') . $hotspot;

                                        } else {

                                            if (in_array($hotspot_key, $hotspots)) {

                                                $count++;

                                                echo ($count != 1 ? ' / ' : '') . $hotspot;

                                            }
                                        }
                                    }

                                ?>

                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>

            <div class="feature-contents">
                <h2>Features</h2>
                <div class="features-option" style="background-color: transparent; color: rgb(255, 255, 255);">
                        <?php foreach ($features as $feature_key => $feature): ?>
                            <?php if ($feature_key > 1) continue; ?>
                            <div class="features-list featuress feature-<?= $feature_key > 0 ? $feature_key.' hide-div' : $feature_key ?>">
                                <?php
                                    foreach ($feature as $key => $row) {
                                        echo ($key == 0 ? '' : ' / ') . $model->HotspotDropdown[$row];
                                    }
                                ?>

                            </div>
                        <?php endforeach; ?>
                </div>
                <div class="features-option-txt">Plus</div>
                <div class="features-option-plus" style="background-color: transparent; color: rgb(255, 255, 255);">
                    <div class="features-list">
                        <?php
                            $hotspot_plus = array_diff($features[1], $features[0]);
                            $count = 0;
                            foreach ($hotspot_plus as $feature_key => $hotspot) {
                                $count++;
                                echo ($count == 1 ? '' : ' / ') . $model->HotspotDropdown[$hotspot];
                            }
                        ?>
                    </div>
                </div>
            </div> <!-- //.feature-contents -->

        </div> <!-- //.pricing-container -->

        <?php if (!Yii::$app->user->isGuest): ?>
            <div class="pricing_popup">

            </div> <!-- //.pricing_popup 결제팝업-->
        <?php endif; ?>
    </section> <!-- //#contents -->
</div>

<?php $this->beginBlock('script') ?>
<?php if (!Yii::$app->user->isGuest): ?>
<script type="text/javascript" src="https://cdn.iamport.kr/js/iamport.payment-1.1.5.js"></script>
<script>
    var IMP = window.IMP; // 생략가능
    var merchant_id = '<?= Yii::$app->params['payment']['iamport']['merchant_id'] ?>';
    var user_id = '<?= $user->id ?>';
    var username = '<?= $user->username ?>';

    IMP.init(merchant_id); // 'iamport' 대신 부여받은 "가맹점 식별코드"를 사용

    function iamport() {
        var amount = parseInt($('#total_price').text());
        var payment_period = $('#payment_period').val();
        var plan_id = $('#plan_id').val();

        var merchant_uid = user_id + '_' + plan_id + '_' + payment_period + '_' + new Date().getTime();
        console.log(merchant_uid);

        IMP.request_pay({
            pg : 'html5_inicis', // version 1.1.0부터 지원.
            pay_method : 'card',
            merchant_uid : merchant_uid,
            name : 'VRMaker Update Account',
            amount : amount,
            buyer_email : '<?= $user->email ?>',
            buyer_name : '<?= !empty($user->name) ? $user->name : $user->email ?>',
            buyer_tel : '<?= !empty($user->phone_number) ? $user->phone_number : '010-1234-5678' ?>',
            // buyer_addr : '서울특별시 강남구 삼성동',
            // buyer_postcode : '123-456',
            // m_redirect_url : 'http://localhost:8000/payment/call-back-iamport'
        }, function(rsp) {
            console.log('Response : ',rsp);
            if ( rsp.success ) {
                // var msg = '결제가 완료되었습니다.';
                var imp_uid = rsp.imp_uid;
                var merchant_uid = rsp.merchant_uid;
                // msg += '결제 금액 : ' + rsp.paid_amount;
                // msg += '카드 승인번호 : ' + rsp.apply_num;

                var data = {
                    imp_uid : imp_uid,
                    merchant_uid : merchant_uid,
                    _csrf : '<?= Yii::$app->request->getCsrfToken() ?>'
                };

                $.ajax({
                    url: '<?= Yii::$app->params['payment']['iamport']['url_call_back'] ?>',
                    dataType: 'json',
                    type: 'post',
                    data: data,
                    success: function (res) {
                        if (res.status == 1) {
                            alert(res.msg);
                            window.location.href = '/' + username;
                        } else {
                            alert(res.msg);
                        }

                    },
                    error: function (res) {
                        console.log(res);
                    }
                });
            } else {
                var msg = 'Error :' + rsp.error_msg;
                alert(msg);
            }
        });
    }

</script>
<?php endif; ?>
<script>
    var lang = '<?= $lang ?>';
    $(document).on('click', '.paid_btn', function (e) {
        e.preventDefault();
        $('.btn-loading').css('display', 'inline-block');
        if (lang != 'ko') {
            $('#form-pricing').submit();
        } else {
            // alert('a');
            iamport();
        }
    });

    function pricingOption(){
        $('.card1').mouseover(function(){
            $('.features-option').css({backgroundColor:"rgba(255,255,255,1)", color:"#3e84b6"});
            $(this).children('.pricing-description').css("color","#3e84b6");
        });
        $('.card1').mouseleave(function(){
            $('.features-option').css({backgroundColor:"transparent", color:"#fff"});
            $(this).children('.pricing-description').css("color","#fff");
        });

        $('.card2').mouseover(function(){
            $('.features-option, .features-option-plus').css({backgroundColor:"#fff", color:"#3e84b6"});
        });
        $('.card2').mouseleave(function(){
            $('.features-option, .features-option-plus').css({backgroundColor:"transparent", color:"#fff"});
        });



    }
    pricingOption();

    $('.pricing-card').hover(function () {
        $('.featuress').addClass('hide-div');

        var elm = $(this).data('select');

        $('.' + elm).removeClass('hide-div');
    }, function () {
    });


     $('.pro-plan-btn').click(function (e) {
         // var type = $(this).data('type');
         var type = 1;

         $.ajax({
             url: '/site/pricing-popup',
             dataType: 'json',
             data: {
                 type: type
             },
             type: 'get',
             success: function (res) {
                 if (res.success == 1) {
                     $('.pricing_popup').empty().append(res.data);
                 }
             }, error: function (res) {
                 alert('Error');
             }
         });
     });

    function payPop(){
        $(document).on('click', '.pro-plan-btn', function (e) {
            $(".pricing_popup, .plan1, .pricing_popup_box").show();
            // $(".plan2").hide();
        });

        $(document).on('click', '.enterprise-plan-btn', function (e) {
            $(".pricing_popup, .plan1, .pricing_popup_box").show();
            // $(".plan1").hide();
        });

        $(document).on('click', '.pop_up_close', function (e) {
            $(".pricing_popup").css("display","none");
        });

        <?php if($lang == 'ko'): ?>
            // $(document).on('click', '.paid_btn', function (e) {
                // $(".pricing_popup_box").hide();
                // $('.auto_card_popup').show();
            // });
        <?php endif; ?>

        $(document).on('click', '.popup-modal-close', function (e) {
            $('.auto_card_popup').hide();
            $(".pricing_popup").hide();
        });

    }

    payPop();

     $(document).on('change', '#payment_period', function () {
         var month = parseInt($(this).val());
         var price = parseFloat($('#price').val());

         var element = $(this).find('option:selected');
         var sale = parseInt(element.data("sale"));

         var total = (price * month) - (price * month * (sale / 100));

         $('#total').text(month * price);
         $('#total_price').text(total);
         $('.discount').text(round((month * price) - total), 2);
     });

    function round(n) {
        return Math.round(n * 100)/100
    }
</script>
<?php $this->endBlock() ?>