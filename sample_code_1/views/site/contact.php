<?php

/* @var $this yii\web\View */

use yii\web\View;

$this->title = 'Contact';

?>
<div id="container">
    <section id="contents">
        <div class="contac_bg_container">
            <div class="mask2">
                <div class="inner-wrap cf">
                    <div class="contact_info_container">
                        <div class="contact_title cf">
                            <h2><?= Yii::t('app', 'We are here for you.') ?></h2>
                            <a class="tel" href="tel:+82-2-6426-2501"><p>+82-2-6426-2501</p></a>
                            <p><i class="far fa-envelope"></i> info@vrmaker.io</p>
                        </div>
                    </div> <!-- //.contact_info_container -->
                    <div class="contact_wrap">
                        <div class="row">
                            <form id="frm-contact" class="contact_form sign_up_box" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />

                                <div>
                                    <label class="hide">Title</label>
                                    <input type="text" name="title" class="form-box" placeholder="<?= Yii::t('app', 'Title') ?>">
                                    <span id="error_title" class="label-danger"></span>
                                </div>
                                <div class="s_area cf">
                                    <label class="hide">Name</label>
                                    <input type="text" name="full_name" class="full-name form-box" placeholder="<?= Yii::t('app', 'Name') ?>" required>
                                    <span id="error_full_name" class="label-danger"></span>

                                </div>
                                <div class="s_area cf">
                                    <p>
                                        <label class="hide">Email</label>
                                        <input type="text" name="email" class="email form-box" placeholder="<?= Yii::t('app', 'Email') ?>" required>
                                        <span id="error_email" class="label-danger"></span>

                                    </p>
                                    <p>
                                        <label class="hide">Phone</label>
                                        <input type="text" name="phone_number" class="phone-number form-box" placeholder="<?= Yii::t('app', 'Tel') ?>" required>
                                        <span id="error_phone_number" class="label-danger"></span>

                                    </p>
                                </div>
                                <div>
                                    <label class="hide">Company name</label>
                                    <input type="text" name="company_name" class="form-box" placeholder="<?= Yii::t('app', 'Company Name') ?>">
                                    <span id="error_company_name" class="label-danger"></span>

                                </div>
                                <div class="message_area">
                                    <label class="hide">Message</label>
                                    <p class="right_txt">Character Limit <em class="bold">500</em></p>
                                    <span id="error_content" class="label-danger"></span>
                                    <textarea name="content" cols="40" rows="10" placeholder="<?= Yii::t('app', 'Message') ?>..." maxlength="500" required></textarea>
                                </div>
                                <div class="submit_btn_wrap">
                                    <button type="submit" value="<?= Yii::t('app', 'Submit') ?>" class="submit_btn" onkeyup="len_chk()">
                                        <i class="icon-loading fa fa-spinner fa-spin"></i> Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div> <!-- //.contact_wrap -->
                </div> <!-- //.inner-wrap -->
            </div> <!-- //.mask -->
        </div> <!-- //.contac_bg_container -->
    </section> <!-- //content -->
</div> <!-- //container -->

<?php $this->beginBlock('script') ?>
<script>
    function len_chk(){
        var frm = document.insertFrm.test;

        if(frm.value.length > 500){
            alert("글자수는 500자로 제한됩니다.");
            frm.value = frm.value.substring(0,500);
            frm.focus();
        }

    }

    $('#frm-contact').submit(function(e){
        e.preventDefault();
        $('.label-danger').text('');
        $('.icon-loading').css('display', 'inline-block');
        $.ajax( {
            url: '/site/contact',
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            success: function (res) {
                $('.icon-loading').css('display', 'none');

                if (res.success == 1) {
                    alert('Sent successfully');
                    window.location.reload();
                } else {
                    for (var key in res.message) {
                        if (!res.message.hasOwnProperty(key)) continue;
                        var obj = res.message[key];
                        for (var prop in obj) {
                            if(!obj.hasOwnProperty(prop)) continue;
                            $('#error_' + key).text(obj[prop]);
                        }
                    }
                }
            }
        });
    });
</script>
<?php $this->endBlock() ?>