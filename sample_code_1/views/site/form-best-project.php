<?php use yii\helpers\Html;

foreach ($awesome_projects as $awesome_project): ?>
    <div class="tour-item">
        <div class="tour_contents">
            <a href="/pano-player/<?= \app\components\HashHelper::hashNumber($awesome_project['id']) ?>">
                <div class="pano-img">
                    <div class="mask"></div>
                    <!--                                    <img src="../images/explore/tour07.jpg" />-->
                    <img src="<?= !empty($awesome_project['thumb_path']) ? $this->url_image($awesome_project['thumb_path']) : '/images/explore/tour07.jpg' ?>" alt="<?= Html::encode($awesome_project['title']) ?>"/>
                    <div class="tour-author cf">
                        <div class="profile"><img src="<?= !empty($awesome_project['avatar_url']) ? $this->url_image($awesome_project['avatar_url']) : '/images/authors/author_profile02.jpg' ?>"></div>
                        <div class="author-name"><?= empty($awesome_project['username']) ? $awesome_project['email'] : $awesome_project['username'] ?></div>
                        <div class="view-cnt"><i class="fas fa-eye"></i><span><?= $awesome_project['view'] ?></span></div>
                    </div>
                </div>
                <div class="pano-info">
                    <strong><?= Html::encode($awesome_project['title']) ?></strong>
                    <span><?= !empty($awesome_project['address']) ? Html::encode($awesome_project['address']) : 'No location' ?></span>
                </div>
            </a>
        </div>
    </div>
<?php endforeach; ?>
